# GALILEO Search

The access point to all that GALILEO provides!

## Links
 - Production - [www.galileo.usg.edu](https://www.galileo.usg.edu)
 - For others see [active environments](https://gitlab.galileo.usg.edu/galileo/galileo_search/-/environments)

## System Requirements
 - Ruby 3.2.2
 - Bundler 2
 - Vagrant
 - NodeJS > 12 with `node` in your `PATH`
 - Python 2 with `python2` in your `PATH`
 - `google-chrome-stable` installed

## Development setup
 1. Clone the repo
 2. Build the VM with `vagrant up`
 3. Install Chrome Stable for headless testing support with `sudo apt-get install google-chrome-stable`
 4. Install the gems `bundle install`
 5. Get `development` and `test` credential keys from another dev. Place these in `config/credentials/`.
 6. Install JavaScript packages with `yarn install --check-files`
 7. Setup the database with `bundle exec rake db:setup`
 8. Index sample data from `provision/data.json` with `./load_solr_data.sh`
 9. Start development server with `bundle exec rails s`
 10. Run test suite with `bundle exec rspec`

## Architecture Notes
This project relies upon the [galileo_admin](https://gitlab.galileo.usg.edu/galileo/galileo_admin) site to manage data and configuration, and also to build and maintain the Solr index of GALILEO resources.

As well, GALILEO Admin provides a web service for supporting authentication requests for the 400+ GALILEO member institutions.

Non-production instances of this application point to `ga-staging` for the purposes of authentication support.

## Testing With OpenAthens

`localhost:3000` was already taken in OpenAthens Keystone. Add a `rails.local` host definition to `/etc/hosts`:

```
  127.0.0.1     localhost   rails.local
```

Now you will be able to test login flow with OpenAthens.

## Updating Solr config

The Solr config for this project is shared with [GALILEO Admin](https://gitlab.galileo.usg.edu/galileo/galileo_admin). Modifications to this config can be made in [this project](https://gitlab.galileo.usg.edu/infra/solr-configs/galileo). After config changes have ben made, you can load new `solrconfig.xml` and `schema.xml` files into you Vagrant Solr using `./reload_solr_config.sh`.

### License
© 2024 Board of Regents of the University System of Georgia