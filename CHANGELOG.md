# Changelog 
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

## [20250221] (Sprint D25)
### Changed
- Update nokogiri (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1214)

### Staging
- Demo of multi-tab bento layout (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1214, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1215, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1216)


## [20250124] (Sprint B25)
### Changed
- Rails 7 Upgrade (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/1005)
- Rails 7 post-update fixes (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1205)

## Staging
- K12 UI: change background image (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/1010)
- Fix bug that made Cosmo become untethered in the background of the new Kids UI... (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1204)

## [20241115] (Sprint W24)
### Changed
- Add Accessibility link to footer (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/1006)
- Update Bootstrap (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1200)

## [20241101] (Sprint V24)
### Changed
- Update Rails (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1195)

## [20241018] (Sprint U24)

### Changed
- Update Rails to 6.1.7.9 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/1003)

### Staging
- Starting point for new kids UI (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1190, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1191)


## [20241004] (Sprint T24)
### Changed
- Set up new OpenAthens proxy workaround API key (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/1001)
- Make Prod push step in gitlab a step process instead of 2 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/999)


## [20240906] (Sprint R24)
### Changed
- Update selenium for rexml vulnerability (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/996)
- Add view constraint for banners (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/992)


## [20240823] (Sprint Q24)
### Changed
- Staging connector for EZproxy workaround (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/946)
- Multiple search box div ids on a page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/990)


## [20240809] (Sprint P24)
### Changed
- Fix bugs in search box (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/982)
- Remove in the spotlight text (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/986)
- Move Tagline (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1169)
- Update spotlights' border width (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1171)
- Add Linkedin logo to footer (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/987)
- Change footer: REDESIGN RELEASE NOTES to RELEASE NOTES (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/988)
- Add version of logo with tagline (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1173)
- Small fixes for header at mobile screen sizes (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1174)


## [20240726] (Sprint O24)
### Changed
- Remove powernotes (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/980)
- Always bundle Audit (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1162)
- Turn off deprecation warnings (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1163)
- Make individual bento boxes currently available as shared links available as embeddable bento boxes (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/965)
- Turn on view switching in prod (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/984)


## [20240712] (Sprint N24)
### Changed
- Remove unnecessary code from searchbox javascript (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/979)

### Staging
- Update view switching (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1155)
- Make individual bento boxes currently available as shared links available as embeddable bento boxes (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/965)

### Fixed
- Shareable Links are missing for k12 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/978)

## [20240628] (Sprint M24)
### Changed
- Develop GALILEO Embedded Search Box Generator Tool (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/760)
- Add form_url to contact emails (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/977)
### Staging
- Add a switch at inst level to turn Max View on/off (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/976)

## [20240614] (Sprint L24)
### Changed
- Turn LibChat widget on in production (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/975)
- Add online/offline notification for chat widget (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/961)
- Update Nokogiri (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1142)
- Update rexml to latest 3.2.x (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1143)
- Hide LibChat slideout (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1144)
- Use changed LibChat credentials format (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1145)

### Staging
- Limit K-12 view switching (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/972)

## [20240419] (Sprint H24)
### Changed
- Solr Institution Features/Spotlights (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/962)

## [20240405] (Sprint G24)
### Changed
- Change widget Hash (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1125)
- Honor "Native EDS Profile" saved with service credentials in GALILEO Admin (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/966)
- Update Rails to 6.1.7.7 and update json-jwt to latest patch (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1132)

## [20240223] (Sprint D24)
### Changed
- Banner for Inst Groups (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/958)

## [20240216] (Mid-Sprint D24)
### Changed
- Change `from:` address in comment emails (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/957)

## [20240209] (Sprint C24)
### Changed
- Improve EDS API error handling (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1115)
### Staging
- Popout LibChat (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1114)


## [20240126] (Sprint B24)
### Changed
- Update view_component (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1110)
- Update puma (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1111)

### Staging
- Add Chatwidget (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/949)

## [20231215] (Sprint Y23)
### Added
- User-level bento customization in production (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1106/diffs)

### Changed
- Bento customization: Add permalink and "Restore Defaults" functionality (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1096)
- Bento customization: Drag and drop into last position (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1106)
- Fix accessibility issues in bento view (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1098)
- Fix drag-and-drop issues in MobileSafari (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1099)
- Rearrange and restyle bento customization menu (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1101)
- Restore "Done" back to being it's own top-level button (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1102)
- Replace the "x" in instructions with a better imitation of the remove button (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1103)
- Help text for bento customization permalink modal (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1105)

### Staging
- Button differentiation demo page for user testing (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1097)


## [20231117] (Sprint W23)
### Changed
- Change wayfinder tokenizer from whitespace to nonword (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/939)
- Update Postgres version in development and dev environments (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/940, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/942)
- Make "All Databases" resilient to unavailable Postgres (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/941)


## [20231103] (Sprint V23)
### Changed
- Update site code validation for DOE school ids (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/937)


## [20231020] (Sprint U23)
### Changed
- Notify users when attempting to access OpenAthens proxy workaround links from a non-workaround institution (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/930)

### Staging
- Add persistence to bento customization demo (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1086, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1087)

## [20231006] (Sprint T23)
### Changed
- Update sidekiq to 7.1.4 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1081)
- Change Twitter icon to X (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/933, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/934)

### Fixed
- Bento description tooltip info icons are hidden and shouldn't be (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/926)

### Staging
- Partially functional prototype of user bento customization (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1078, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1079)

## [20230825] (Sprint Q23)
### Changed
- Display banners saved in GALILEO Admin (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1069)
- Use new URL format for Publication Finder title links (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/927)
- Update Rails and Puma (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1075)

## [20230602] (Sprint K23)
### Changed
- Add alt text to refinable results filters toggle on mobile (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1061)

## [20230407] (Sprint G23)
### Changed
- Old CI setup stuff lingering in web entrypoint (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/908)
- Create tool to convert EZProxied EBSCO Permalinks to Permalinks that work with the OpenAthens Proxy Workaround (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/907)
- Remove feature flags that are always true (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/909)
- Add additional format to OA Proxy Link Converter (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1051)
- Encode EDS search query in URL when encoding link for OA Proxy Workaround (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/910)

## [20230324] (Sprint F23)
### Changed
- Openathens: accept array or string for scoped affiliation (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/904)
- Fix pipeline by adding commits from staging (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1041)
- Hide "Peer Reviewed Journals" limiter when limiting to library catalog (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/900)
- Update rack to 2.2.6.4 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1046)
- Update rails to 6.1.7.3 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1046)

## [20230224] (Sprint D23)
### Added
- Options to suppress boolean EDS limiters per-bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/899)

## [20230131] (Sprint B23)
### Added
- Institution-agnostic links to individual bento pages (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/895)

### Changed
- Update Rails to latest 6.1.x (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/896)

## [20230113] (Sprint A23)
### Added
- Add FAQ button to header (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/891)
- Add FAQ links to tour (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/892)

### Changed
- Updates to help install on M1 Macs (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1023)
- Minor update to HTTParty (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1024)
- Fixes for EDS bentos (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1025)
- Page-specific tooltips for tour button (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/889)
- Styled topbar section with new FAQ link (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1030)
- Updated rocket icon in hero. increased background opacity for accessibility. (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1031)

### Fixed
- Refinable results: permalink should not include page number (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/893)
- Library Catalog Only Limiter Displaying for institutions who do not have EDS Custom Catalog Setup (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/890)

## [20221216] (Sprint Y22)
### Added
- Refinable results view in production (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/886)
- Tour of bento results and refinable results pages (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/873)
- Refinable results: Option to expand/collapse long abstracts (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/879)
- Refinable results: Add search terms to `<title>` (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/878)
- New homepage tour button (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1013)

### Changed
- Change development to point to the dev s3 bucket (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/996)
- Add bento description popover to refinable results (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/854)
- Refinable DLG results date limiter (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/844)
- Update nokogiri to 1.13.10 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1001)
- CRDL bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/880)
- Expand date limiters by default (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/881)
- Page through EDS results more performantly (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/877)
- More facet results for DLG bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/874)
- Update rails-html-sanitizer and loofah (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1009)
- Show Publication Finder HTTP errors as "possibly intermittent" (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/883)
- Bento headers: keep `i` icon from being orphaned on wrap (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/884)
- Pre-sort journals facet items by count, descending (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/1016)
- DLG/CRDL date limiter changes (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/885)

### Fixed
- Express Link modal `[x]` link not working correctly (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/863)
- EDS "Did you mean..." and "Are you looking for the journal..." placards broken by recent refactor (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/887)

## [20221202] (Sprint X22)
### Added
- Implement filters for EDS limiters (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/814)
- Refinable results page: failure indicator (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/856)
- Refinable results: "See Results in (Classic Search|DLG|Publication Finder|etc)" button (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/852)
- Refinable results permalink modal (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/853)
- Numbered results on refinable page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/868)
- Add bento links to shareable links list (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/866)
- Refinable EDS results date limiter (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/843)
- Refinable results: Option to sort by date (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/861)

### Changed
- Add borders to thumbnails (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/859)
- Add branding back to Databases bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/864)
- Refinable results page: hide full text checkbox for non-EDS bentos (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/840)
- Powernotes - different link for academic insts (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/862)
- If ansible is slow to start the deploy job for staging or prod, deploy script can fall through the loop (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/870)
- Labeling changes on refinable results page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/869)

### Fixed
- EDS bentos without additional limiter clauses fail with blank search (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/841)
- DLG bento is ORing search terms instead of ANDing (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/867)

## [20221116] (Mid-Sprint W22)
### Changed
- Move Galileo record page to bento title link (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/842)
- Refinable results page: bottom pagination (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/838)
- Reorder facets for EDS bentos and put facet names into strings file (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/836)
- Revert change that made encyclopedia title links go directly to external site (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/837)
- GALILEO record metadata. (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/829)
- Refinable results page: don't show any results for a blank search (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/839)
- Sync subject facets with prod (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/850)
- DLG bento refinements (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/831)
- Add password to survey banner (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/857)

### Fixed
- Encyclopedia blurb logic has RegEx with unescaped user input (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/847)
- Error when display title is blank (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/855)

## [20221104] (Sprint V22 - 2)
### Changed
- Point production environment to refactored bento models (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/832)
- EDS Record Express links: use GKR profile when appropriate and include user view (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/830)
- EDS Record Express links: turn on in production (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/830)
- Refinable results: facet improvements (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/811)
- GALILEO record metadata page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/829)

## [20221102] (Sprint V22)
### Changed
- Refinable results: facet improvements (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/811)
- Remove "preload" option from refinable results page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/815)
- Provide access to BentoResultRecord without a search (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/817)
- Bento EDS record page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/816)
- Add Buttons To galileo Record (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/819)
- Define display fields for galileo record (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/818)
- Refinements to galileo record display fields (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/820)
- Add annual survey banner for 2022-11 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/823)
- EDS Journals record page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/822)

### Fixed
- GALILEO Comment form does not generate ticket if no last name is entered (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/812)
- Partial fix for `undefined method `institution_bento_home_path'` issue (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/821)

## [20221021] (Sprint U22)
### Added
- Facet support for Primo Journals bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/807)
- Add Sentry (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/907)
- Facet support for Databases bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/808)


### Changed
- Set sentry environments (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/908)
- Update Sentry environment keys (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/910)
- Update nokogiri to 1.13.9 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/809)
- Make EDS detailed record links into express links (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/810)


## [20221007] (Sprint T22)
### Added
- Individual Bento results page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/768)
- Digital Library of Georgia bento using DLG API (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/630)
- Refinable results page: back to bento button (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/797)
- Refinable results page: full text limiter (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/799)
- Add facets to DLG bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/800)
- Add facets to Journals (Publication Finder) bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/801)

### Changed
- Make thumbnail links in bentos using the new BentoResponse model open in new tabs (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/897)
- Tweak SCSS rule that works in development but not staging (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/898)
- Carry full-text limiter into EDS refine search links (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/802)

### Fixed
- Fix styles on refinable results page in non-debug contexts (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/795)
- Refinable results: Reset pagination when applying facet (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/796)


## [20220923] (Sprint S22)
### Changed
- Cleaned up en.yml and fixed mistakes (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/446)

## [20220909] (Sprint R22)
### Changed
- Additional spam filtering (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/789)

### Fixed
- Characters "=0D=0A=0D=0A" are being added to email subject link to ServiceNow when comment contains a carriage return (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/791)

## [20220812] (Sprint P22)
### Changed
- update sassc (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/881)
- Refactor bento to consolidate partial views (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/774)
- Contact form revisited (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/788)
- Use Open Athens API Name for redirector links (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/787)


## [20220727] (Sprint O22)
### Changed
- Classic Search Cookie (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/783)
- DLG/GHNP/CRDL Support/Contact Us forms (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/782)
- Add per page parm to A-Z links (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/785)
- Hardcode api name for AUC (emergency) (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/786)
- Remove feature flag for saved institutions in wayfinder (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/779) 

## [20220721] (Sprint N22)
### Changed
- DLG support forms (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/771)
- Update Rails to 6.0.5.1 and make it work with Blacklight (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/869)
- Disable sms feature (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/691)
- Update Blacklight to 7.28 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/777)

### Fixed
- Mercer University Express Link trying to authenticate as Mercer Law or Mercer Medical (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/778)
- Awkward styles on wayfinder recent institutions (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/780)

## [20220701] (Sprint M22)
### Added
- Allow individual deletion of recent institutions under wayfinder in Staging (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/641)

### Changed
- Update JMESpath (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/770)
- added aria-hidden to icon and aria-label to submit with label "remove" in Staging (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/863)
- Update rails-html-sanitizer (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/772)


## [20220519] (Sprint J22)
### Changed
- Add Google Analytics tracking when switching between Classic/Bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/763)
- Update rails to 6.0.4.8 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/764)
- Remove feature flag code  Update rails to 6.0.4.8 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/715)

## [20220422] (Sprint H22)
### Changed
- Change open_athens_subscope_ss to _sms (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/757)
- Update nokogiri to 1.13.4 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/758)

## [20220408] (Sprint G22)
### Changed
- Update favicon with new logo (includes Greenland) (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/738)
- Update puma to ~> 4.3.12 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/756)

### Fixed
- "Continue Search" button sometimes shows up when it shouldn't (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/754)

## [20220325] (Sprint F22)

### Change
- Reverse order of `<div>`/`<script>` tags in embedded search code (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/731)
- "You need to login to access this resource" message when searching bento using embedded GALILEO search widget (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/729)
- K-12 View  GALILEO bento search box still asks user to select a view (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/742)
- Update exception notifier (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/751)
- Fix bug that made bento searches fail when search terms contain "%" (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/752)
- Honor new flag to make bentos behave like "Encyclopedia" (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/750)


## [20220318] (Mid-Sprint F22)
- For EDS bentos, force users to move to EBSCO's site after viewing 12 pages of "more results",
  as further pages get increasingly slow (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/748)

## [20220311] (E22)

### Change
- Update database subjects to sort alphabetically(https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/725)
- Update Primo Journals bento for compatibility with Primo VE (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/740)
- Update rails and nokogumbo (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/741)

## [20220225] (Sprint D22)

### Change
- Banner for March 5-6 Maintenance (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/730)
- Upgrade Rails, Nokogiri, Puma, etc. (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/733)

## [20220211] (Sprint C22)

### Change
- Change logging level in staging and prod to warn (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/726)
- Upgrade to ruby 2.7.5 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/720)
- Fix CVE-2022-23837 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/727)
- Improve alignment of buttons in bento displays (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/717)

## [20220114] (Sprint A22)

### Add
- Support custom "No results..." message (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/721)

### Change
- Update/Delete UGA EDS CustomLink Images (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/718)

## [20220107] (Mid-Sprint A22)

### Change
- Rework "Research Starters" bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/713)
- Make Custom Catalog "Online Access" links show up in bento results (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/687)
- Update specs to use solr 8.11.1 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/714)
- On Embedding a GALILEO Search Box, show the Bento option first (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/716)

## [20211220] (Sprint Z21)

### Add
- Make Bento the default view (from staging, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/642)
- Display bento description (from staging, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/682)
- Add express links to databases bento and replace express link button with icon site-wide (from staging, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/637)

## [20211216] (Sprint Y21)

### Add
- Support Journals Bento (Alma API) (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/671)
- Support bento box for print materials through EDS (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/627)

### Change
- Turn on :bento_config_json_credentials feature flag (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/774)
- Fix Bento results horizontal scrollbar issue (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/692)
- Incorrect Number of Arguments error when trying to display EDS Custom Resource for resource with colon in name (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/694)
- Fix: New Georgia Encyclopedia / Custom Catalog "Online Access" links not showing in Bento Box, but do show in EDS search (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/687)
- Fix: New Bento with no set sort order displays first even though it shows last in GALILEO Admin (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/693)
- Make "Databases" bento better match other bentos (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/698)
- Update rails to 6.0.4.3 (from 6.0.4.1) (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/699)

### Staging
- Make Bento search the default (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/642)
- Preserve search terms when switching between bento and classic tabs (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/696)
- Remove Databases by Subject/Type from homepage (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/695)

## [20211203] (Sprint X21)
- Allow admins to change Publication Finder API profile (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/672)
- Change Galileo to GALILEO in user facing text (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/686)
- A K-12 teacher who is searching for databases in bento can access the express links in the databases bento box for each database in the results (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/637)
- Make behavior of bento description popovers more logical (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/688)
- Add Try Bento checkbox to embedded search box (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/643)
- Make Bento search the default (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/642)
- Catch empty re-rank queries before they go to Solr (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/689)

## [20211119] (Sprint W21)

### Add
- Custom Bento Soft Launch (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/669)
- Connect New Georgia Encyclopedia to bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/628)
- Hosting new Libraries button icons for UGA - USG-INC0540158 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/674)

### Change
- Fix sticky footer issue when survey alert is present (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/675)
- The embedded search box javascript should treat  `"bento_search":0` like `"bento_search":false` (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/679)
- Clean up embed_test page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/680)
- Alphabetize badges on More Info page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/681)
- Add profile to bento embed code (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/684)


### Staging
- A K-12 teacher who is searching for databases in bento can access the express links in the databases bento box for each database in the results (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/637)
- Display Bento Description (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/682)



## [20211104] (Sprint V21)

### Add
- Add Survey link to GALILEO (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/665)
- Add a spam detection pattern (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/664)
- Changes to Welcome page (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/661)

### Change
- Update index.html.erb. Moved closed container div to after password accordion section (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/746)

### Staging
- Configured_Bentos from solr (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/662)


## [20211021] (Sprint U21)

### Change
 - Add a banner for 10/23 maintenance (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/656)
 - Implement user view limiters (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/621)
 - Adjust iframe height of embedded search box when logo/url is removed (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/649)

### Fix
 - Unhandled bento exception should result in special error message with “contact support” link (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/594)
 - Update Puma (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/726)

### Staging
 - Change 'elem' to 'elementary' (etc.) in User::Site object https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/385

## [20211011] (Mid Sprint U21)

### Fix
  - Fix proxy access to non-openathens resource (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/651)


## [20211008] (Sprint T21)

### Change
  - “Availability” link displaying for Britannica results in the Encyclopedia bento (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/639)

## [20210924] (Sprint S21)

### Change
  - Arrange bentos left->right; top->bottom (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/623)
  - Missing links stats count on eds search (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/640)
  - Explore comment spam filtering (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/606)

## [20210910] (Sprint R21)

### Add
- Make relevance "facet-aware" by default (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/624)


### Staging
- Implement user view limiters (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/621)


## [20210827] (Sprint Q21)

### Change
- Make login with PINES ID direct to OpenAthens for OA-enabled institutions (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/619)
- Update Rails to 6.0.4.1 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/697)

### Staging
- Implement user view limiters (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/621)
- Changes to "Relevance (facet-aware)" sorting: (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/692, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/693)

## [20210813] (Sprint P21)

### Change
- Remove jquery-rails gem in favor of native jquery (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/600)
- Intercept bad json for /cgi/search_galileo route (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/616)
- Added specs for eds_result_format_helper (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/615)

## [20210730] (Sprint O21)

### Change
- Bring back "Refine Search" button in bento modal (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/611)

### Fix
- Make LearningExpress and Encyclopedia bentos better match EDS search results (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/607)
- Bug: Redirects to EDS land at blank search screen for users logged in via OpenAthens but eligible to be logged in via IP authentication (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/593)
- Add automatic retry for a certain common, intermittent EDS API error (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/595)

### Staging
- New variations on "Relevance" sort order (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/609, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/610)

## [20210720] (Sprint N21 - Hotfix)

### Fix

- Fix issue where PowerNotes link displayed for all institutions regardless of setting (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/599)
- Log bento searches to GALILEO stats service only once per search (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/601)
- Update jquery-rails to 4.4.0 (jQuery 3.5.1). (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/602)

## [20210716] (Sprint N21)

### Change

- Move Bento into production
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/522
- Move Bento toggle into production
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/524
- Move new header design into production
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/523
- When turning off bento, keep current keywords in the search box after pageload
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/570)
- Change bento toggle colors
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/580)
- Remove Bento Search toggle from bento search page, shorten text on home page.
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/586)
- Save Bento state in cookie
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/577)
- Can the long bento toggle text appear on hover, too?
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/587)
- Make Bento errors less urgent looking
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/571)
- Only show "contact support" for error 500s
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/589)
- Encyclopedia blurb and trade publications
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/579)
- Tour revisions
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/572)
- Tweak bento toggle design & fix VIEW size for widescreen
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/584)
- Style bug: more results modal pagination misaligned when there's no "Refine Search" link
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/582)
- Add feature flag to easily stop logging bento errors to slack
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/581)
- Test Every Institution's Bento Round 2
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/576)

### Fix

- Properly detect when eduPersonScopedAffiliation is missing in openathen login
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/583)
- Ampersand symbol converting to &amp; for resource short description.
 (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/575)

## [20210701] (Sprint M21)

### Change
- What's new/Tour has improved step ordering and styles (and new copy in staging) (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/463, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/566, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/567)
- Bugfix: exception for blank attachments (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/568)

### Staging
- More logical heading hierarchy in bento search results (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/531)
- Move bento toggle under search box (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/569)
- Accessibility fixes in bento search (https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/525)


## [20210617] (Sprint L21)

### Change

- Move GLRI Tools link feature into production
 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/410

### Staging

- Add view to pill links on more info https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/559
- Make bento pills link to more results modal.  https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/550
- Add Learning Express bento https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/561
- Magazine Articles: investigate if there is a way to exclude magazine titles. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/541
- Create Bento For LearningExpress Library https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/548
- Style issues with modal https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/562

## [20210604] (Sprint K21)

### Change

- Add Zotero to Tools https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/544
- In Tools section, allow institutions to exclude links https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/445
- Testing Plans https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/536
- Make the keywords and subjects on the "More" page link to a search https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/417
- Support new keywords array https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/545

### Staging

- Add the user's name, when available, indicating that user is logged in https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/407
- Allow user to see more of the abstract in the "Show More Results" modal https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/500
- Bento query is not being carried over via the "refine search" link in the modal window for Encyclopedia bento https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/538
- Add placeholder content in bento boxes https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/543

## [20210520] (Sprint J21)

### Change
- Add Release Notes link to search footer https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/513

### Staging
- Embedded Search Box Bento Option https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/517
- Add titles to Magazine and News Bentos https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/535
- Bento search alt tags for thumbnails/buttons https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/532
- Hide bentos when no view is selected https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/529
- Journals box https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/465
- Handle stale sessions and errors in More Results modal https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/508
- Implement placards https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/516

## [20210507] (Sprint I21)

### Change
- Move wayfinder_data method to an API controller https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/503
- Minor Rails Update https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/521

### Staging
- add :bento_search feature flag to toggle https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/518
- Encyclopedia bento improvements
 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/506
- Update Hero design for Bento home page https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/502
- change colors used for toggle/beta items
 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/515
- Improve more results modal styles in mobile
 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/496
- Make the user view selector replace the view in the current url https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/498
- Apply longer list of Pubtype to Bento -> Native Ebsco links https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/489
- Encyclopedia bento for elementary https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/494
- Explore showing more results in modal https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/481
- Hide cataloglinks https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/484
- Remove Express links from Databases Bento https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/490
- Bento boxes displaying content for incorrect institution https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/495
- Create Encyclopedia Bento https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/491
- Add space after comma separator for keywords field on database More Info page https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/488
- Update view => bento mapping to match new spec https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/507
- Make database bento use show more results modal https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/505
- Remove horizontal scroll seen on Search home page after logged-in https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/483
- Investigate Google Fonts issue https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/492
- hide bento toggle on databases page https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/510
- Remove Database By boxes on bento homepage https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/509
- New design of header section https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/413
- Fix issue with view selector being faded https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/511


## [20210423] (Sprint H21)

### Change
- Update puma to 4.3.7 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/478
- Remove remote url from openathens redirect logic https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/477

### Fix
- Remote logins for old_sessions not working https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/456

### Staging
- Fix bento layout issues at different screen sizes https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/464
- Full text search option https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/466
- Full-text checkbox: wire up to browser history https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/480
- Fix session and eds profile issue https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/460
- Try to create "View More Results" links for EDS bentos https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/475
- Link "View More Results" to *filtered* Ebsco search https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/479
- Remove redirect for Databases Bento https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/473
- Organize bento js code into packs? https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/468

## [20210408] (Sprint G21)

### Change
- Update rails - removes mimemagic dependency https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/451
- Add Release Notes https://gitlab.galileo.usg.edu/galileo/about_galileo/-/issues/2
- Add PowerNotes FAQ link to tools https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/474

### Fix
- Spotlight express link bug https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/450

### Staging
- EDS links: Decode URLs and display icon if no label is available https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/470
- More minor changes to bento view https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/472
- Style tweaks to bento EDS results https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/471
- Database Bento Box https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/458
- Create buttons for all available full text links https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/469
- Ajax proof of concept (Bento) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/437
- Full text pdf button https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/462
- EDS error handling/messages https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/454
- Bento box loading indicator https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/453
- Restructure bento api output and include result count https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/455
- Speed up bento api (a little) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/457



## [20210326] (Sprint F21)

### Change
- Change Patron view to "Full View" (GALILEO Search) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/421
- Move Change View button into production https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/411
- Move Public Library Kids views feature into production https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/397
- Add "Try Bento" button (show in staging only) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/438
- Ability to add keywords to institutions in the Wayfinder https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/327
- Move hard coded strings to translation file https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/415
- Fill in white space for embed search https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/441
- Add social card meta tags to GALILEO Search and About https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/440
- Added GALILEO Youtube link and icon to Footer https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/443
- Anchor footer to bottom of display https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/444

### Fix
- EDS Bento API Auth token issue - needs redis cache https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/442
- OA proxy workaround not redirecting to resource when using Wayfinder https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/439



## [20210312] (Sprint E21)

### Change
- Add HR image hero and change view image https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/435
- additional design edits to embed search https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/433


## [20210226] (Sprint D21)

### Fixed
- Redirect URLs not IP authenticating https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/423
- Change View button: make outline more visible (Staging) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/424
- Update GALILEO Search Embed code/design (Staging) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/373

## [20210212] (Sprint C21)

### Changed

- Link Syntax https://www.galileo.usg.edu/instcode Not IP Authenticating https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/403
- Change how we handle expired sessions https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/402
- EZproxy workaround and express links (GALILEO Search) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/416


## [20210129] (Sprint B21)

### Added

### Changed
- Change "K12" to "Public Libraries" in Shareable Links page https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/405

### Fixed
  - "What's New" problems https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/382

  - GLRI = False parameter still shows GLRI in database list. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/387

  - Fix 'Change View' modal not reappearing after clicking 'X'. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/395


## [20210115] (Sprint A21)

### Added
  - Add GLRI link under Tools on homepage https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/389
  - OpenAthens API for interim EZproxy replacement solution https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/309
  - Public Library Kids views https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/381
  - Implement 'disallow_open_athens' on resource (allocation) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/390

### Changed
  - Remove `uga1` references from code (after UGA OpenAthens golive) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/356
  - (GALILEO Search) Upgrade to Ruby 2.7 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/377

### Fixed
  - Remove sidekiq artifacts from gitlab-ci https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/142


## [20201218] (Sprint Y)

### Changed
  - Press enter in Wayfinder to autoselect institution https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/358
  
### Fixed
  - Inactive institutions still have instruction pages https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/365
  - 500 error for `/view` route https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/376

## [20201204] (Sprint X)

### Added
  - Feature flag for UGA OpenAthens golive https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/349

### Changed
  - New favicon https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/341
  - Clean up old feature flags 
  
### Fixed
  - Minor typo in GoVIEW instructions page https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/340
  - Code cleanup WRT OpenAthens scope/subscope lookups https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/338

### Security
  - Validate institution and resource codes in paths (routes) https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/368 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/364 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/277
  - Suppress display of user-provided parameter value in error message https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/363

## [20201120] (Sprint W)
### Added
- Add route for database more info page
 https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/351
 - GALILEO Search needs to know if a resource code is valid and the resource active https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/262

### Changed
- Search "Start Over" button does not retain the 'view' parameter https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/347
- Database show page 'Back to Search' button not lined up https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/346



## [20201105] (Sprint V)
### Added
- GALILEO Survey Banner https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/337

### Changed
- Fix: A-Z bar facets not retaining view parameter https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/339
- Change from `search` route to `databases` route https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/336
- Change All Databases button to use databases route https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/334
- Allow UGA Law students into UGA Main GALILEO https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/335
- Add "sharable links" page with shortcut URLs in the "TOOLS" card on the home page https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/286
- Have target parameter for GALILEO search box apply to GALILEO URL https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/330
- Fix: Can't logout if institution does not have ezproxy defined, e.g., Guest https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/302


## [20201023] (Sprint U)
### Added
- A-Z Horizontal Facet https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/314, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/331, https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/328
- Outage Banner Added https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/332

### Changed
- Updated GPLS password instructions to reflect PINES e-card. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/322
- Added robots meta tag to remove *staging environment* from Google Crawler https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/290
- Redis fix, added try/catch logic to lti and ip login strategies https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/345

## [20201008] (Sprint T)
### Added
- "Database List" link on the Tools section on the homepage. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/321
- "1000" option to "per page" pulldown on search displays. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/301
- support for "view" shortcut URLs. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/284

### Changed
- Hero image on wayfinder page now changes when a "view" shortcut URL is clicked. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/287
- Minor rails update addressing vulnerability https://gitlab.galileo.usg.edu/galileo/galileo_search/-/merge_requests/337

### Removed
- "home" from interface URLS. https://gitlab.galileo.usg.edu/galileo/galileo_search/-/issues/288


