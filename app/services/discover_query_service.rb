# frozen_string_literal: true

# :nodoc:
class DiscoverQueryService
  # @param [String] discovery_url
  # @param [User] user
  def initialize(discovery_url, user)
    @user = user
    @discovery_url = discovery_url
    @recoded_params = {}
    @option_index = 0
  end

  # @param [Hash] params
  def compose(params, oa_proxy)
    extra_query_params =
      '&' + if params['advanced']
              'type=1'
            else
              params.each do |param, value|
                param = param.to_sym
                recode_param(param, value) if param.to_sym.in? handled_params
              end
              @recoded_params.to_query
            end
    if (@user.auth_type == :oa || oa_proxy) && @user.remote?
      extra_query_params = URI.encode_www_form_component(extra_query_params)
                              .gsub('%2B', '+')
    end
    @discovery_url + extra_query_params
  end

  private

  def recode_param(param, value)
    if param.in? boolean_params
      return if value == 'off'
    end

    case param
    when :search_galileo_query
      encode_search_terms value
    when :discipline
      encode_disciplines value
      @option_index += 1
    when :library_collection
      encode_library_collection
      @option_index += 1
    when :full_text
      encode_full_text
      @option_index += 1
    when :peer_reviewed
      encode_peer_reviewed
      @option_index += 1
    when :catalog_only
      encode_catalog_only
      @option_index += 1
    when :an
      @recoded_params['AN'] = value
    when :db
      @recoded_params['db'] = value
    end
  end

  def encode_search_terms(value)
    @recoded_params['bquery'] = value
  end

  def encode_disciplines(values)
    disciplines = values.map { |value| "LO system.dis-#{value}" }
    @recoded_params['bquery'] = "(#{disciplines.join(" AND ")}) AND #{@recoded_params['bquery']}"
  end

  def encode_library_collection
    @recoded_params["cli#{@option_index}"] = 'FT1'
    @recoded_params["clv#{@option_index}"] = 'Y'
  end

  def encode_full_text
    @recoded_params["cli#{@option_index}"] = 'FT'
    @recoded_params["clv#{@option_index}"] = 'Y'
  end

  def encode_peer_reviewed
    @recoded_params["cli#{@option_index}"] = 'RV'
    @recoded_params["clv#{@option_index}"] = 'Y'
  end

  def encode_catalog_only
    @recoded_params["cli#{@option_index}"] = 'FC'
    @recoded_params["clv#{@option_index}"] = 'Y'
  end

  def handled_params
    %i[search_galileo_query discipline an db] + boolean_params
  end

  def boolean_params
    %i[library_collection full_text peer_reviewed
       catalog_only profile]
  end

  def self.eds_profile_for(inst, view)
    case view&.to_sym
    when :elementary then inst.elem_eds_profile
    when :middle then inst.midd_eds_profile
    when :highschool then inst.high_eds_profile
    else inst.eds_profile
    end
  end
end
