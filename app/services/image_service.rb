# frozen_string_literal: true

# :nodoc:
class ImageService
  def initialize
    s3 = Aws::S3::Resource.new region: 'us-east-2'
    @bucket = s3.bucket Rails.application.config.s3_bucket
  end

  def url_for(identifier_key)
    return if identifier_key.blank?

    @bucket.object(identifier_key).presigned_url(:get)
  end
end
