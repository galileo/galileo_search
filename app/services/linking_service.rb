# frozen_string_literal: true

class LinkingService

  PROXIFIED_DOMAIN_REGEX = /^([-\w]+)\.(us1\.proxy\.openathens\.net|proxygsu-\w{4}\.galileo\.usg\.edu)$/
  OA_PROXIFIED_WITH_SUBDOMAINS_VARIATION = /^([-.\w]+)\.(us1\.proxy\.openathens\.net)$/
  PROXY_LOGIN_DOMAIN_REGEX = /^(proxy\.openathens\.net|(login\.)?proxygsu-\w{4}\.galileo\.usg\.edu)$/

  def self.base_url_for(url)

    # Be tolerant of extraneous whitespaces and people pasting in URLs without the scheme
    url = url.strip
    unless url.starts_with? /https?:\/\//
      url = 'https://' + url
    end

    parsed = URI.parse url
    result = parsed.clone
    if PROXY_LOGIN_DOMAIN_REGEX.match?(parsed.host) && parsed.path == '/login'

      # OA Proxy login URLs and newer EZ Proxy URLs with qurl= parameter
      params = CGI::parse(parsed.query)
      if params['qurl'] && params['qurl'].size == 1
        login_url = URI.parse params['qurl'].first
        if %w[http https].include? login_url.scheme
          result = login_url
        end
      end

      # OLD EZ Proxy URLs with the url= parameter, which holds an unescaped URL
      if params['url'] && params['url'].size == 1
        unencoded_url = parsed.query =~ /url=(.+)$/ && $1
        login_url = URI.parse unencoded_url
        if %w[http https].include? login_url.scheme
          result = login_url
        end
      end

    elsif parsed.host =~ PROXIFIED_DOMAIN_REGEX
      # URLs people copy-pasted from their address bar in a proxy session
      # i.e., proxy urls without the login prefix
      result.host = $1.gsub('-', '.').gsub('..', '-')

    elsif parsed.host =~ OA_PROXIFIED_WITH_SUBDOMAINS_VARIATION
      # Like the case above but with search.ebscohost.com.us1.proxy.openathens.net/path
      #   instead of search-ebscohost-com.us1.proxy.openathens.net/path
      result.host = $1
    end

    # Upgrade scheme to https if http
    result.scheme = 'https'
    result.to_s
  rescue URI::InvalidURIError
    nil
  end

  def self.oa_proxy_url_for(url)
    base_url = base_url_for(url)
    return if base_url.nil?
    encoded_dest = CGI.escape base_url
    "https://proxy.openathens.net/login?entityID=https://idp.wa.galileo.usg.edu/entity&qurl=#{encoded_dest}"
  end

  def self.ez_proxy_url_for(url, inst_code)
    base_url = base_url_for(url)
    return if base_url.nil?
    encoded_dest = CGI.escape base_url
    "https://login.proxygsu-#{inst_code}.galileo.usg.edu/login?qurl=#{encoded_dest}"
  end
end
