# frozen_string_literal: true

# :nodoc:
class PrimoApiService
  URESOLVE_URL_QS_PARAMETERS = %w[ctx_tim ctx_ver url_ver rfr_id rft_dat].freeze
  URESOLVE_URL_PARAMETERS = (URESOLVE_URL_QS_PARAMETERS + %w[inst_id1 inst_id2]).freeze
  PRIMO_VE_EDELIVERY_PARAMETERS = %w[record subdomain vid]

  include HTTParty
  PRIMO_BASE_URL = 'https://api-na.hosted.exlibrisgroup.com/primo/v1'

  def initialize(service_credentials)
    @service_credentials = service_credentials
  end

  def primo_search_base_url
    "#{PRIMO_BASE_URL}/search?vid=#{vid}&scope=MyInstitution&tab=LibraryCatalog&apikey=#{api_key}"
  end

  def build_url(query_term, offset, count, filters, sort)
    url = URI.parse primo_search_base_url
    parameters = URI.decode_www_form(url.query).to_h
    parameters[:q] = "any,contains,#{ query_term.gsub(/[;,]/, ' ') }"
    parameters[:qInclude] = { rtype: 'Journals' }.merge(filters)
                                   .map { |key, value| "facet_#{key},exact,#{value}" }
                                   .join '|,|'
    parameters[:offset] = offset
    parameters[:limit] = count
    parameters[:sort] = sort if sort.present?
    url.query = URI.encode_www_form parameters
    url.to_s
  end

  def search(query_term, offset = 0, count = 5, filters = {}, sort)
    request = HTTParty.get build_url(query_term, offset, count, filters, sort)
    throw Error unless request.response.code == '200'
    parsed = request.parsed_response
    records = parsed['docs']
    count = parsed.dig 'info', 'total'
    {
      records: records.map { |r| transform_record r },
      count: count,
      facets: parsed['facets'] || []
    }
  end

  def transform_record(record)
    PrimoRecord.new record, @service_credentials
  end

  def api_key
    @service_credentials.credential 'API Key'
  end

  def vid
    @service_credentials.credential 'VID'
  end

  def primo_scope
    @service_credentials.credential 'Scope'
  end

  def self.fetch_links_from_uresolve_info(info_str)
    fetch_links_from_uresolve delivery_info_to_uresolve_url(info_str)
  end

  def self.fetch_links_from_uresolve(url)
    request = HTTParty.get url
    throw Error unless request.response.code == '200'
    parsed = request.parsed_response
    services = potential_array parsed.dig('uresolver_content', 'context_services', 'context_service')
    links = services.map do |s|
      next if s['service_type'] != 'getFullTxt'
      info = OpenStruct.new
      info[:resolution_url] = s['resolution_url']
      keys = potential_array s.dig('keys', 'key')
      keys.each do |key|
        info[key['id']] = key['__content__']
      end
      info[:computed_url] = info.resolution_url || info.url
      info[:computed_label] = info.package_display_name || info.public_name || info.name || 'Online Access'
      info
    end
    links.filter { |l| l&.computed_url.present? && l.Filtered != 'true' }
  end

  # Alternative to fetch_links_from_uresolve_info. This uses an undocumented(?) Primo VE API instead of Alma's uresolve
  def self.fetch_edelivery_links(info_str)
    request = HTTParty.get delivery_info_to_url(info_str)
    throw Error unless request.response.code == '200'
    parsed = request.parsed_response
    services = parsed.dig('electronicServices') || []
    services.map do |s|
      info = OpenStruct.new
      info[:computed_url] = s['serviceUrl']
      info[:computed_label] = s['packageName']
      info
    end
  end

  def self.potential_array(potential_array)
    if potential_array.nil?
      []
    elsif potential_array.is_a? Array
      potential_array
    else
      [potential_array]
    end
  end

  # Pre-Primo VE
  def self.distill_uresolve_url(url)
    components = {}
    uri = URI.parse url
    components['inst_id1'] = Regexp.last_match(1) if uri.host =~ /^([\w-]+)\.userservices\.exlibrisgroup\.com$/i
    components['inst_id2'] = Regexp.last_match(1) if uri.path =~ %r{^/view/uresolver/([\w+$-]+)/openurl$}i
    query = CGI.parse uri.query
    URESOLVE_URL_QS_PARAMETERS.each do |key|
      components[key] = query.dig(key, 0) if query.dig(key, 0).present?
    end
    components.size == URESOLVE_URL_PARAMETERS.size ? components : nil
    URI.encode_www_form components
  end

  # Pre-Primo VE
  def self.reconstitute_uresolve_url(encoded_components)
    components = (CGI.parse encoded_components).transform_values { |v| v && v[0] }
    raise 'Invalid uresolve components' unless (URESOLVE_URL_PARAMETERS - components.keys).empty?

    subdomain = components['inst_id1']
    path_comp = components['inst_id2']
    raise 'Invalid uresolve components' unless subdomain.match?(/^[\w-]+$/) && path_comp.match(/^[\w+$-]+$/)

    uri = URI.parse "https://#{subdomain}.userservices.exlibrisgroup.com/view/uresolver/#{path_comp}/openurl"
    query = {
      'ctx_enc' => 'info:ofi/enc:UTF-8',
      'url_ctx_fmt' => 'info:ofi/fmt:kev:mtx:ctx',
      'svc_dat' => 'cto',
      'u.ignore_date_coverage' => 'true'
    }.merge(components.slice(*URESOLVE_URL_QS_PARAMETERS))
    uri.query = URI.encode_www_form query
    uri.to_s
  end

  # Primo VE counterpart of distill_uresolve_url
  def self.distill_delivery_info(recordId, subdomain, vid)
    URI.encode_www_form({
      'record' => recordId,
      'subdomain' => subdomain,
      'vid' => vid
    })
  end

  # Primo VE counterpart of reconstitute_uresolve_url
  def self.delivery_info_to_uresolve_url(encoded_components)
    components = (CGI.parse encoded_components).transform_values { |v| v && v[0] }
    raise 'Invalid components' unless (PRIMO_VE_EDELIVERY_PARAMETERS - components.keys).empty?
    mms_id = components['record'].gsub /^alma/, ''
    subdomain = components['subdomain']
    inst = components['vid'].split(':')[0]
    uri = URI.parse "https://#{subdomain}.userservices.exlibrisgroup.com/view/uresolver/#{inst}/openurl"
    query = {
      'svc_dat' => 'cto',
      #'response_type' => 'xml',
      'u.ignore_date_coverage' => 'true',
      'rfr_id' => 'info:sid/primo.exlibrisgroup.com',
      'rft.mms_id' => mms_id
    }
    uri.query = URI.encode_www_form query
    uri.to_s
  end

  # Alternative to delivery_info_to_uresolve_url for use with undocumented(?) Primo VE API
  def self.delivery_info_to_url(encoded_components)
    components = (CGI.parse encoded_components).transform_values { |v| v && v[0] }
    raise 'Invalid components' unless (PRIMO_VE_EDELIVERY_PARAMETERS - components.keys).empty?
    record = components['record']
    subdomain = components['subdomain']
    vid = components['vid']
    "https://#{subdomain}.primo.exlibrisgroup.com/primaws/rest/pub/edelivery/#{record}?vid=#{vid}"
  end
end
