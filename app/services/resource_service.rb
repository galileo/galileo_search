# frozen_string_literal: true

# :nodoc:
class ResourceService
  # @param [String] institution_code
  def initialize(institution_code)
    @conn = Blacklight.default_index.connection
    @code = institution_code
    raise StandardError, 'Blacklight not initialized' unless @conn
  end

  # Find a particular resource by code
  # @param [String] resource_code
  # @return [Resource]
  def find(resource_code)
    return unless ValidationService.new.is_valid_resource_code?(resource_code)

    response = @conn.get 'select', params: {
      fq: "+class_ss:\"Allocation\"
           +for_institution_ss:\"#{@code}\"
           +for_resource_ss:\"#{resource_code}\"".squish
    }
    return nil if response['response']['docs'].empty?

    Resource.new response['response']['docs'][0]
  end

  # Return an array of names of Resources for the Institution
  # @return [Array<Array>]
  def all_names_with_codes
    response = @conn.get 'select', params: {
      fl: %w[resource_name_ss for_resource_ss],
      fq: "+class_ss:\"Allocation\"
           +for_institution_ss:\"#{@code}\"".squish,
      rows: 1000
    }
    response['response']['docs'].map do |doc|
      "#{doc['resource_name_ss']} (#{doc['for_resource_ss']})"
    end
  end
end
