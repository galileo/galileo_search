# frozen_string_literal: true

require 'redis'

# Interface for making queries to redis
class RedisService
  NONCE_TTL_SECS = 3600

  # @param [Integer] database - Defaults to the nonce database '0'
  def initialize(database: Rails.application.credentials.redis[:db][:nonce])
    @redis = if Rails.env.test?
               MockRedis.new
             else
               Redis.new(url: Rails.application.credentials.redis[:url],
                         db: database,
                         driver: :hiredis)
             end
  end

  # Add something to redis
  # @param [String] key
  # @param [Object] value
  # @param [Integer] expire - expire time in seconds
  def add(key, value, expire = nil)
    if expire
      @redis.set key, value, ex: expire
    else
      @redis.set key, value
    end
  end

  # Check if something exists at key
  # @param [String] key
  # @return [TrueClass, FalseClass]
  def exists?(key)
    get(key).present?
  end

  # Return value from redis based on key
  # @param [String] key
  # @return [Object]
  def get(key)
    @redis.get(key)
  end

  # Store a nonce value for later validation
  # @param [String] nonce
  def nonce(nonce)
    add(nonce, Time.zone.now.to_i, NONCE_TTL_SECS)
  end

  # Check if nonce has been used
  # @param [String] nonce
  # @return [TrueClass, FalseClass]
  # @todo should this be "recently?"
  def unique_nonce?(nonce)
    !exists? nonce
  end
end