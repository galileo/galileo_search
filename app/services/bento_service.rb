# frozen_string_literal: true

# :nodoc:
class BentoService
  class BentoServiceError < StandardError; end

  # @param [String] institution_code
  def initialize(institution_code)
    @conn = Blacklight.default_index.connection
    @inst_code = institution_code
    raise BentoServiceError, 'Blacklight not initialized' unless @conn
  end

  # @param [String] view_type
  # @return [Array<BentoConfig>]
  def find(view_type, service = 'eds_api')
    response = @conn.get 'select', params: {
      fq: "+class_ss:\"BentoConfig\"
           +service_ss: \"#{service}\"
           +view_type_ss:\"#{view_type}\"
           +inst_code_ss:\"#{@inst_code}\"".squish
    }
    results = parse_results response.dig('response', 'docs')
    if results.nil?
      raise BentoServiceError, t('bento.error.not_configured')
    end

    results
  end

  def find_by_id(id_string)
    response = @conn.get 'select', params: {
      fq: "+class_ss:\"BentoConfig\"
           +id:\"#{id_string}\"
           +inst_code_ss:\"#{@inst_code}\"".squish
    }
    results = parse_results response.dig('response', 'docs')
    results.first
  end

  # @param [String] view_type
  # @return [BentoConfig]
  def config_for_view(view_type)
    (if view_type
      find view_type, 'eds_api'
    else
      find 'default', 'eds_api'
    end).first
  end

  # @param [Array<Hash>] solr_results
  # @return [Array<BentoConfig>]
  def parse_results(solr_results)
    solr_results.map do |bento|
      BentoConfig.new bento
    end
  end

  # @param [String] view_type
  # @return [Array<Hash>]
  def bentos_for_render(view_type)
    BentoProvider.for_view(view_type).map do |bento|
      bento.for_render @inst_code
    end
  end

  def self.query_bento(bento_type, bento_query)
    bento = BentoProvider.lookup(bento_type)
    if bento.is_a? EdsBento
      bento_serv = BentoService.new bento_query.inst.code
      eds_api = EdsApi.new bento_serv.config_for_view(bento_query.view)
      return bento.do_query bento_query, eds_api
    end
    bento.do_query bento_query
  end

  def self.query_eds_placards(bento_query, inst, view)
    bento_serv = BentoService.new inst.code
    if bento_query.terms&.strip.blank?
      return { eds_results: nil, result_count: nil, suggested_terms: nil }
    end
    eds_api = EdsApi.new bento_serv.config_for_view(view)
    eds_results = eds_api.search({
                                   query: bento_query.terms,
                                   results_per_page: 1,
                                   highlight: false,
                                   auto_suggest: true,
                                   related_content: ['emp']
                                 })
    {
      eds_results: eds_results,
      result_count: eds_results.results.dig('SearchResult', 'Statistics', 'TotalHits'),
      suggested_terms: eds_results.results.dig('SearchResult', 'AutoSuggestedTerms')
    }
  end


end
