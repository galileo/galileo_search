# frozen_string_literal: true

# Wrapper for GALILEO Admin auth support API
class ValidationService

  def initialize
    @conn = Blacklight.default_index.connection
    raise StandardError, 'Blacklight not initialized' unless @conn
  end

  # Restrict institution code to four lowercase alphanumerics
  # TODO: move to SolrInstitution or InstitutionService?
  # @param [String] inst_code
  # @return [Boolean]
  def is_valid_institution_code?(inst_code)
    return false unless inst_code
    inst_code.match?(/^[a-z0-9]{4}$/)
  end

  # Check if institution (or site) is currently active
  # TODO: move to SolrInstitution or InstitutionService?
  # @param [String] inst_code
  # @return [Boolean]
  def is_active_institution?(inst_code)
    return false unless is_valid_institution_code?(inst_code)

    response = @conn.get 'select', params: {
      fq: "+class_ss:(\"Institution\" OR \"Site\")
           +code_ss:\"#{inst_code}\"
           +active_bs:true".squish,
      rows: 1
    }

    response['response']['docs'].any?
  end

  # Restrict resource code to these formats:
  # - four lowercase alphanumerics, e.g., 'dlg1'
  # - additionally hyphenated with instituion code (glri), e.g., 'salt-sga1'
  # - or additionally colon-concatenated deeplink qualifier, e.g., 'zupn:washington_post'
  # TODO: move to Resource or ResourceService?
  # @param [String] resource_code
  # @return [Boolean]
  def is_valid_resource_code?(resource_code)
    return false unless resource_code
    resource_code.match?(/^#{ValidationService.resource_code_regx}$/)
  end

  def self.resource_code_regx
    /[a-z0-9]{4}(?::[a-z0-9_\-]+|-[a-z0-9]{4})?/
  end

  # Check if resource (i.e., allocation) is currently active
  # TODO: move to Resource or ResourceService?
  # @param [String] resource_code
  # @return [Boolean]
  def is_active_resource?(resource_code)
    return false unless is_valid_resource_code?(resource_code)

    # search for resource code as either resource or blacklight_id
    response = @conn.get 'select', params: {
      fq: "+class_ss:\"Allocation\" AND
           (for_resource_ss:\"#{resource_code}\" OR
           blacklight_id_ss:\"#{resource_code}\")".squish,
      rows: 1
    }

    response['response']['docs'].any?
  end

  # Restrict institution (or site) name to alphanumerics with selected punctuation
  # TODO: move to SolrInstitution or InstitutionService or ...?
  # TODO: does this match galileo_admin validation?
  # @param [String] inst_name
  # @return [Boolean]
  def is_valid_institution_name?(inst_name)
    return false unless inst_name
    inst_name.match?(/^[a-zA-Z0-9 '\/@&():,.-]+$/)
  end

  # Restrict site code to these formats
  # - four lowercase alphanumerics (i.e., institution code), e.g., 'asc1'
  # - additionally with colon-concatenated school/branch designator, e.g., 'ath1:oconeecoli', 'sful:660-0101'
  # TODO: move to SolrInstitution or InstitutionService or ...?
  # TODO: does this match galileo_admin validation?
  # @param [String] site_code
  # @return [Boolean]
  def is_valid_site_code?(site_code)
    return false unless site_code
    site_code.match?(/^[a-z0-9]{4}(?::[a-z0-9-]+)?$/)
  end

  # Restrict passphrase (passwords) to alphanumerics and '@'
  # TODO: move to SolrInstitution or InstitutionService or ...?
  # TODO: does this match galileo_admin validation?
  # @param [String] passphrase
  # @return [Boolean]
  def is_valid_passphrase?(passphrase)
    return false unless passphrase
    passphrase.match?(/^[a-zA-Z0-9@]+$/)
  end

end
