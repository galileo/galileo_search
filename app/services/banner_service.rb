# frozen_string_literal: true

# :nodoc:
class BannerService
  class BannerServiceError < StandardError; end

  DEFAULT_FQ = "+class_ss:Banner".squish.freeze

  # @return [Array<BannerSolrDocument>]
  def self.find(institution_code: nil, institution_group_code: nil)

    @conn = Blacklight.default_index.connection
    raise StandardError, 'Blacklight not initialized' unless @conn

    fq = if institution_code || institution_group_code
           "#{DEFAULT_FQ} +(audience_ss:everyone #{"OR institution_code_ss:#{institution_code}" if institution_code } #{"OR institution_group_code_ss:#{institution_group_code}" if institution_group_code })"
         else
           "#{DEFAULT_FQ} +audience_ss:everyone"
         end
    fq += " +start_time_ds:[* TO NOW] +end_time_ds:[NOW TO *]"


    response = @conn.get 'select', params: {
      fq: fq,
      rows: 10
    }
    return [] if response['response']['docs'].empty?

    response['response']['docs'].map do |doc|
      BannerSolrDocument.new doc
    end
  end
end