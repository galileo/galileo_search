# frozen_string_literal: true

class FeatureFlags
  def self.enabled?(feature_name)
    case feature_name.to_sym
    when :ezproxy_error_notification
      !Rails.env.production?
    when :open_athens_proxy_error_notification
      !Rails.env.production?
    when :exception_debug_info_on_record_page
      !Rails.env.production?
    when :galileo_record_page
      !(Rails.env.production? || Rails.env.staging?)
    when :pubfinder_title_id_links
      true
    when :bento_tabs
      !Rails.env.production?
    when :libchat
      true
    when :limit_k12_view_switching
      true
    when :hide_tagline
      true
    when :new_kids_view
      !Rails.env.production?
    else
      false
    end
  end
end

