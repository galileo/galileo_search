# frozen_string_literal: true

# Wrapper for GALILEO Admin auth support API
class AuthApiService
  BASE_URI = Rails.application.credentials.auth_support_api[:url]

  # Query API for institution data based on IP address
  # @param [String] ip address
  def self.ip(ip)
    institution_by ip: ip
  end

  # Query API for institution data based on password
  # TODO: do we need to consider IP here for remote/local context?
  # @param [String] passphrase
  # @param [Object] ip address
  def self.passphrase(passphrase)
    institution_by passphrase: passphrase
  end

  # Query API for institution data based on OA ID
  # @param [String] oa_id
  # @param [Object] ip address
  def self.oa_id(oa_id, ip)
    institution_by oa_id: oa_id, ip: ip
  end

  # Query API for institution data based on Zip Code
  # @param [String] zip code
  def self.zip(zip)
    institution_by zip: zip
  end

  # Query API for institution data based on PINES ID
  # @param [String] pines_id
  def self.pines(pines_id)
    institution_by pines: pines_id
  end

  def self.institution_by(options)
    data = HTTParty.get(BASE_URI + 'inst', params(options)).parsed_response
    if data['inst']
      Response::Institution.new(data)
    elsif data['message']
      Response::Error.new data['message']
    else
      Response::Error.new 'Request failed'
    end
  rescue StandardError => e
    Response::Error.new(e)
  end

  def self.params(query = {})
    options = default_options
    options[:query] = query
    options
  end

  def self.default_options
    {
      headers: {
        'X-User-Token': Rails.application.credentials.auth_support_api[:token]
      },
      verify: false # TODO: fix this!
    }
  end

  # wrap up responses nicely
  module Response
    # Institution response object
    class Institution
      attr_accessor :code, :name, :pass, :public_code, :auth_context
      # @param [Hash] response
      def initialize(response)
        inst_data = response['inst'].symbolize_keys
        @code = inst_data[:code]
        @name = inst_data[:name]
        @pass = inst_data[:passphrase]
        @public_code = inst_data[:public_code]
        @auth_context = AuthContext.new response['context']
      end

      def success?
        true
      end

      # authentication context
      class AuthContext
        attr_accessor :remote
        def initialize(context)
          @remote = context['remote']
          @type = context['auth_type']
        end

        def remote?
          @remote == true
        end

        def type
          @type.to_sym
        end
      end
    end

    # Error response object
    class Error
      attr_accessor :message
      def initialize(message)
        @message = message
      end

      def success?
        false
      end
    end
  end
end