# frozen_string_literal: true

MAX_ATTEMPTS_EDS_ERROR_106 = 3

# Authenticate and search the EDS API
class EdsApi
  include HTTParty

  class EdsApiError < StandardError; end

  class EDSSessionWithCaching < EBSCO::EDS::Session
    def initialize(cache, options)
      @info_cache = cache
      # use_cache: false because we don't want to use the EDS gem's cache, which is not institution-specific
      super(options.merge(use_cache: false, guest: false, timeout: 60, open_timeout: 60))
    end

    def do_request(method, path:, payload: nil, attempt: 0)
      if path == '/edsapi/rest/Info'
        cache_key = "info-#{@user}-#{@profile}"
        cache_hit = @info_cache.get cache_key
        begin
          return JSON.parse(cache_hit) if cache_hit.present?
        rescue JSON::ParserError;
          # remove bad value from cache
          @info_cache.add cache_key, '', 1
        end
        result = super
        @info_cache.add cache_key, result.to_json, 600
        result
      else
        super
      end
    end

    def search(options = {}, add_actions = false, increment_page = true)
      return super unless options[:cached_session_token] && (options['page'] || 0) > 1
      self.session_token = options[:cached_session_token]
      self.search_options = EBSCO::EDS::Options.new(options.merge('page' => 1), info)
      self.search_options.SearchCriteria = options[:cached_search_criteria] if options[:cached_search_criteria]
      get_page options['page']
    end
  end

  base_uri 'https://eds-api.ebscohost.com'

  attr_reader :auth_token_cache

  # @param [BentoConfig] bento
  def initialize(bento)
    raise EdsApiError, 'Bento configuration is not set.' if bento.nil?

    @bento = bento
    @auth_token_cache = RedisService.new database: Rails.application.credentials.redis[:db][:eds_auth_tokens]
  end

  def new_session
    EDSSessionWithCaching.new(auth_token_cache, { user: user_id, profile: api_profile, auth_token: auth_token })
  end

  # @param [Hash] search_params
  def search_and_get_token(search_params)
    attempts = 0
    max_attempts = MAX_ATTEMPTS_EDS_ERROR_106
    s = new_session
    loop do
      return s.search(search_params), s.session_token
    rescue EBSCO::EDS::BadRequest => e
      error_number = e.fault.dig :error_body, 'ErrorNumber'
      raise unless error_number == '106'

      attempts += 1
      raise unless attempts < max_attempts
    end
    [nil, nil]
  end

  # @param [Hash] search_params
  # @return [EBSCO::EDS::Results, String]
  def search(search_params)
    results, _token = search_and_get_token search_params
    results
  end

  def retrieve(dbid, an)
    new_session.retrieve dbid: dbid, an: an
  end

  def self.source_type(bento_type)
    case bento_type
    when 'ebooks'
      'eBooks'
    when 'videos'
      'Videos'
    when 'audio'
      'Audio'
    when 'academic_journals'
      'Academic Journals'
    when 'news'
      'News'
    when 'primary_sources'
      'Primary Source Documents'
    end
  end

  # Checks cache or retrieves a new eds auth_token
  def auth_token
    auth_token = auth_token_cache.get @bento.inst_code

    return auth_token unless auth_token.blank?

    response = new_auth_token
    # we've seen two slightly-different formats for this response
    response = response['AuthResponseMessage'] if response['AuthResponseMessage']
    auth_token = response['AuthToken']
    @auth_token_cache.add @bento.inst_code, auth_token, (response['AuthTimeout'].to_i - 60)

    auth_token
  end

  def new_auth_token
    resp = HTTParty.post 'https://eds-api.ebscohost.com/authservice/rest/uidauth',
                         body: {
                           "UserId": user_id,
                           "Password": password
                         }.to_json,
                         headers: {
                           "Content-Type": 'application/json'
                         }
    resp.parsed_response if resp.response.code == '200'
  end

  def user_id
    @bento.credential 'User ID'
  end

  def password
    @bento.credential 'Password'
  end

  def api_profile
    @bento.credential 'API Profile'
  end

end
