# frozen_string_literal: true

# :nodoc:
class PublicationFinderService
  include HTTParty

  def initialize(endpoint_prefix)
    @endpoint_prefix = endpoint_prefix
  end

  def build_url(query_term, offset, count)
    "#{@endpoint_prefix}?search=#{CGI.escape(query_term)}&offset=#{offset}&count=#{count}&orderby=relevance&searchfield=titlename&includefacets=false&searchtype=contains&resourcetype=Journal"
  end

  def search(query_term, offset = 1, count = 5)
    request = HTTParty.get build_url(query_term, offset, count)
    throw Error unless request.response.code == '200'
    parsed = request.parsed_response
    records = parsed.dig 'SearchResult', 'Data', 'Records'
    count = parsed.dig 'SearchResult', 'Statistics', 'TotalHits'
    {
      records: records,
      count: count
    }
  end
end
