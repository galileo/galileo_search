# frozen_string_literal: true

# This gets configured bentos from solr
class ConfiguredBentoService
  class ConfiguredBentoServiceError < StandardError; end

  # @param [String] institution_code
  def initialize(institution_code, user_view)
    @conn = Blacklight.default_index.connection
    @inst_code = institution_code
    @user_view = user_view || 'default'
    raise ConfiguredBentoServiceError, 'Blacklight not initialized' unless @conn
  end

  def list
    query
  end

  def default_list
    query(nil, true)
  end

  def hidden_list
    query(nil, false)
  end

  def get(slug)
    query(slug).first
  end

  def self.get(inst_code, user_view, slug)
    new(inst_code, user_view).get slug
  end

  private

  def query(slug = nil, shown_by_default=nil)
    fq = ['+class_ss:"ConfiguredBento"',
          "+user_view_ss:\"#{@user_view}\"",
          "+inst_code_ss:\"#{@inst_code}\"",
          "+active_bs:true"]
    fq << "+slug_ss:\"#{slug}\"" unless slug.nil?
    fq << "+shown_by_default_bs:#{shown_by_default}" unless shown_by_default.nil?

    response = @conn.get 'select', params: { fq: fq, rows: 1000, sort: 'order_es asc, slug_ss asc' }
    docs = (response.dig 'response', 'docs') || []
    results = docs.map { |doc| ConfiguredBento.new doc }
    results.sort_by { |cb| [cb.order, cb.slug] }
  end

end
