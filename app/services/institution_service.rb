# frozen_string_literal: true

# Wrapper for GALILEO Admin auth support API
class InstitutionService
  MAX_ROWS = 5000

  def initialize
    @conn = Blacklight.default_index.connection
    raise StandardError, 'Blacklight not initialized' unless @conn
    @vserv = ValidationService.new
  end

  def open_athens_ready
    @conn.get 'select', params: { fq: %w[+class_ss:"Institution" +open_athens_bs:true] }
  end

  def all
    @conn.get 'select', params: { fq: %w[+class_ss:"Institution" +active_bs:true] }
  end

  # Get an Array of all active Institution names
  # @return [Array<Array>]
  def all_names_with_codes
    response = @conn.get 'select', params: {
      fl: %w[name_ss code_ss],
      fq: %w[+class_ss:("Institution") +active_bs:true],
      sort: 'name_ss asc', rows: MAX_ROWS
    }
    response['response']['docs'].map do |doc|
      "#{doc['name_ss']} (#{doc['code_ss']})"
    end
  end

  # Get all Institutions and Site intended for wayfinder inclusion
  # @return [Hash]
  def all_with_sites_codes_and_keywords
    response = @conn.get 'select', params: {
      fl: ['name_ss, code_ss, wayfinder_keywords_ss, type_ss, parent_inst_code_ss'],
      fq: ['+class_ss:("Institution" OR "Site")', ' +show_in_wayfinder_b:true'],
      sort: 'name_ss asc', rows: MAX_ROWS
    }

    # TODO: remove elem: and midd: after galileo_admin:site_type feature flag is done
    wayf_data = {
      elem: [], elementary: [], midd: [], middle: [], highschool: [], otherk12: [], highered: [],
      publiclib: [], k12: [], default: []
    }

    response['response']['docs'].map do |doc|
      type = doc['type_ss']
      next if !type || type == 'none'

      type = 'otherk12' if type == 'k12' && doc.key?('parent_inst_code_ss')

      datum = { name: doc['name_ss'], code: doc['code_ss'] }
      datum[:keywords] = doc['wayfinder_keywords_ss'] if doc['wayfinder_keywords_ss'];
      wayf_data[type.to_sym] << datum
    end

    wayf_data
  end

  def find(inst_code)
    return unless @vserv.is_valid_site_code?(inst_code)

    response = @conn.get 'select', params: {
      fq: "+class_ss:(\"Institution\" OR \"Site\")
           +code_ss:\"#{inst_code}\"".squish,
      rows: 1
    }
    return nil if response['response']['docs'].empty?

    SolrInstitution.new response['response']['docs'][0]
  end

  # @param [String] site_code
  # @return [SolrInstitution]
  def find_site(site_code)
    return unless @vserv.is_valid_site_code?(site_code)

    response = @conn.get 'select', params: {
      fq: "+class_ss:(\"Site\")
           +code_ss:\"#{site_code}\"".squish,
      rows: 1
    }
    return nil if response['response']['docs'].empty?

    SolrInstitution.new response['response']['docs'][0]
  end

  def find_by_name(name)
    return unless @vserv.is_valid_institution_name?(name)

    response = @conn.get 'select', params: {
      fq: "+class_ss:(\"Institution\" OR \"Site\")
           +name_lower_s:\"#{name.downcase}\"".squish,
      rows: 1
    }
    return nil if response['response']['docs'].empty?

    SolrInstitution.new response['response']['docs'][0]
  end

  def find_by_pass(passphrase)
    return unless @vserv.is_valid_passphrase?(passphrase)

    response = @conn.get 'select', params: {
      fq: "+class_ss:\"Institution\" +active_bs:true
           +current_passphrase_ss:\"#{passphrase}\"".squish,
      rows: 1
    }
    return nil if response['response']['docs'].empty?

    SolrInstitution.new response['response']['docs'][0]
  end

  # @return [SolrInstitution]
  # @param [String] consumer_key
  # @param [String] inst_id
  def find_for_lti(consumer_key, inst_id = nil)
    field_query = " +active_bs:true +class_ss:\"Institution\" +lti_consumer_key_ss:\"#{consumer_key}\""
    field_query += " +lti_custom_inst_id_ss:\"#{inst_id}\"" if inst_id
    response = @conn.get 'select', params: {
      fq: field_query,
      rows: 1
    }
    return nil if response['response']['docs'].empty?

    SolrInstitution.new response['response']['docs'][0]
  end

  # @return [SolrInstitution]
  # @param [String] scope
  def find_for_oa_scope(scope)
    response = @conn.get 'select', params: {
      fq: "+class_ss:\"Institution\" +active_bs:true
           +open_athens_scope_ss:\"#{scope}\"".squish
    }
    return nil if response['response']['docs'].empty?

    # if more than one found by scope, filter out subscopes
    if response['response']['numFound'] > 1
      response['response']['docs'].each do |doc|
        return SolrInstitution.new doc if not doc['open_athens_subscope_sms']
      end
    end

    SolrInstitution.new response['response']['docs'][0]
  end

  # @return [SolrInstitution]
  # @param [String] subscope
  def find_for_oa_subscope(scope)
    response = @conn.get 'select', params: {
      fq: "+class_ss:\"Institution\" +active_bs:true
           +open_athens_subscope_sms:\"#{scope}\"".squish,
      rows: 1
    }
    return nil if response['response']['docs'].empty?

    SolrInstitution.new response['response']['docs'][0]
  end

  # @return [SolrInstitution]
  # @param [String] code
  def find_for_pines(code)
    response = @conn.get 'select', params: {
      fq: "+class_ss:\"Institution\" +active_bs:true
           +pines_codes_sms:\"#{code}\"".squish,
      rows: 1
    }
    return nil if response['response']['docs'].empty?

    SolrInstitution.new response['response']['docs'][0]
  end
end
