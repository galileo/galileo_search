# frozen_string_literal: true

# :nodoc:
class FeatureService < SolrService
  class FeatureServiceError < StandardError; end

  DEFAULT_FQ = "+class_ss:Feature".squish.freeze

  # @param [SolrInstitution] institution
  # @return [Array<Feature>]
  def self.find_all(institution, view)
    view = view || "default"
    find_by_institution(institution.code, view) + find_by_institution_group(institution.institution_group_code, view)
  end

  # @param [String] institution_code
  # @return [Array<Feature>]
  def self.find_by_institution(institution_code, view)
    view = view || "default"
    fq = "#{DEFAULT_FQ} +featuring_type_ss:Institution
          +featuring_code_ss:\"#{institution_code}\" +view_type_ss:\"#{view}\""
    find(fq)
  end

  # @param [String] institution_group_code
  # @return [Array<Feature>]
  def self.find_by_institution_group(institution_group_code, view)
    view = view || "default"
    fq = "#{DEFAULT_FQ} +featuring_type_ss:InstitutionGroup
          +featuring_code_ss:#{institution_group_code} +view_type_ss:#{view}"
    find(fq)
  end
end