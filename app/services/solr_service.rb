# frozen_string_literal: true

class SolrService
  # @return [Array<Feature>]
  def self.find(fq, rows: 10)
    @conn = Blacklight.default_index.connection
    raise StandardError, 'Blacklight not initialized' unless @conn

    response = @conn.get 'select', params: {
      fq: fq,
      rows: rows
    }
    return [] if response['response']['docs'].empty?

    response['response']['docs'].map do |doc|
      Feature.new doc
    end
  end
end
