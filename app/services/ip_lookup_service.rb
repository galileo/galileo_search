# frozen_string_literal: true

require 'redis'

# Interface for looking up IP addresses from Redis
class IpLookupService
  # @param [RedisService] redis_service
  def initialize(redis_service = nil)
    @service = redis_service || RedisService.new(database: Rails.application.credentials.redis[:db][:ip])
  end

  # @param [String] ip
  # @return [TrueClass, FalseClass]
  def exists?(ip)
    key = ip_key(ip)
    return false unless key

    @service.exists? key
  end

  # @param [String] ip
  # @param [String] inst_code
  # @return [TrueClass, FalseClass]
  def ip_is_for?(ip, inst_code)
    inst_code_for(ip) == inst_code
  end

  # @param [String] ip
  def inst_code_for(ip)
    key = ip_key(ip)
    return false unless key

    @service.get ip_key(ip)
  end

  # @param [String] ip
  # @return [String (frozen)]
  def ip_key(ip)
    ip_int = IPAddr.new(ip).to_i
    "ip_#{ip_int}"
  rescue IPAddr::InvalidAddressError => _e
    nil
  end
end