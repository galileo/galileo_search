# frozen_string_literal: true

# :nodoc:
class StatisticsService
  STATS_FILE_DATE_SUFFIX_FORMAT = 'D%Y%m%d'

  def self.login(request, session, inst_code, login_type, remote, site_code)
    # Queue background job to write stat
    # WriteLoginStatJob.perform_later(
    #   session_id: session.id.to_s,
    #   begin_time: DateTime.now.strftime('%Y%m%d %H:%m:%S'),
    #   ip: request.remote_ip,
    #   server: host_prefix(Socket.gethostname),
    #   inst_code: inst_code.upcase,
    #   login_type: login_type
    # )

    # Force ActionDispatch to load session data
    session[:galileo_init] = true

    write_login session.id, now, request.remote_ip,
                host_prefix(Socket.gethostname), inst_code.upcase, login_type,
                remote, site_code
  end

  def self.link(session, inst_code, resource_code)
    # Queue background job to write stat
    # WriteLinkStatJob.perform_later(
    #   session_id: session.id.to_s,
    #   begin_time: DateTime.now.strftime('%Y%m%d %H:%m:%S'),
    #   resource_code: resource_code.upcase,
    #   server: host_prefix(Socket.gethostname),
    #   inst_code: inst_code.upcase
    # )
    session_val = session.respond_to?(:id) ? session.id : session
    write_link session_val, now, resource_code.upcase,
               host_prefix(Socket.gethostname), inst_code.upcase
  end

  # Write a Search Stat
  # @param [String] query
  # @param [Symbol] type
  # @param [String] inst_code
  # @param [String] site_code
  def self.search(query, type, inst_code, site_code = nil)
    write_search now, inst_code, query, type, site_code
  end

  def self.write_link(session_id, begin_time, resource_code, server, inst_code)
    logger = Logger.new "./stats/links.stats.#{file_suffix}", 'daily',
                        shift_period_suffix: STATS_FILE_DATE_SUFFIX_FORMAT
    logger <<
      "#{session_id},#{begin_time},_LINK,#{resource_code},#{server},#{inst_code}\n"
    logger.close
  end

  def self.write_login(session_id, begin_time, ip, server, inst_code, login_type, remote, site_code)
    logger = Logger.new "./stats/login.stats.#{file_suffix}",'daily',
                        shift_period_suffix: STATS_FILE_DATE_SUFFIX_FORMAT
    login_type = auth_code_for_stats(login_type, remote)
    logger <<
      "#{session_id},#{begin_time},_LOGIN,#{ip},#{server},#{inst_code},#{login_type},#{site_code}\n"
    logger.close
  end

  def self.write_search(time, inst_code, query, type, site_code)
    logger = Logger.new "./stats/search.stats.#{file_suffix}",
                        'daily',
                        shift_period_suffix: STATS_FILE_DATE_SUFFIX_FORMAT
    logger <<
      "#{time},#{inst_code},#{site_code},#{type},#{query}\n"
    logger.close
  end

  # @param [String] hostname
  def self.host_prefix(hostname)
    return hostname unless hostname.include? '.'

    hostname[0..(hostname.index('.') - 1)]
  end

  # @return [String]
  def self.now
    DateTime.now.strftime('%Y%m%d %H:%M:%S')
  end

  # @return [String]
  def self.file_suffix
    DateTime.now.strftime STATS_FILE_DATE_SUFFIX_FORMAT
  end

  # @param [Object] auth_type
  def self.auth_code_for_stats(auth_type, remote)
    suffix = remote ? '_remote' : '_ip'
    case auth_type
    when 'lti' then 'd2l' + suffix
    when 'password' then 'password'
    when 'ip' then 'ip'
    else
      auth_type + suffix
    end
  end
end