# frozen_string_literal: true

# :nodoc:
class ResourceResultPresenter < Blacklight::IndexPresenter
  self.thumbnail_presenter = GalileoThumbnailPresenter

end