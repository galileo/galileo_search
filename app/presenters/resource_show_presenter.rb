# frozen_string_literal: true

# :nodoc:
class ResourceShowPresenter < Blacklight::ShowPresenter
  self.thumbnail_presenter = GalileoThumbnailPresenter
end