# frozen_string_literal: true

# GALILEO Overrides of Blacklight ThumbnailPresenter
class GalileoThumbnailPresenter < Blacklight::ThumbnailPresenter
  ##
  # Render the thumbnail, if available, for a document and
  # link it to the document record.
  #
  # @param [Hash] image_options to pass to the image tag
  # @param [Hash] url_options to pass to #link_to_document
  # @return [String]
  def thumbnail_tag(image_options = {}, url_options = {})
    value = thumbnail_value(image_options.merge(class: 'img-fluid'))
    return value if value.nil? || url_options[:suppress_link]

    view_context.link_to_resource document, value
  end
end