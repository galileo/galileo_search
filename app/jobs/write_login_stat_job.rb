# frozen_string_literal: true

# Write login Statistic
class WriteLoginStatJob < ApplicationJob
  queue_as :default

  def perform(args)
    StatisticsService.write_login(
      args[:session_id], args[:begin_time], args[:ip], args[:server],
      args[:inst_code], args[:login_type]
    )
  end
end
