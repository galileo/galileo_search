# frozen_string_literal: true

# Write a Link stat, later
class WriteLinkStatJob < ApplicationJob
  queue_as :default

  def perform(args)
    StatisticsService.write_link(
      args[:session_id], args[:begin_time], args[:resource_code], args[:server],
      args[:inst_code]
    )
  end
end
