# frozen_string_literal: true

require 'rss'

# :nodoc:
class HomepageController < ApplicationController
  include Blacklight::Searchable
  include GalileoRss
  include ApplicationHelper
  helper FacetHelpers

  content_security_policy false, only: :embed
  before_action :validate_inst_param
  before_action :redirect_to_login, unless: :user
  before_action :validate_inst_matches_user
  before_action :set_solr_institution, unless: @institution
  before_action :validate_view
  before_action :set_features

  after_action :allow_iframe, only: :embed

  layout 'custom_base'

  def show
    set_top_facets
    set_about_content
    @spotlight_link = true
  end

  def bento_home
    set_top_facets
    set_about_content
    @spotlight_link = true
  end

  def embed;
  end

  def sharable_links;
  end

  def glri_tools_link;
  end

  private

  def validate_view
    return unless @view
    unless institution_display_views?(user.inst)
      @view = nil
      cookies.delete(:galileo_search_preferred_view)
      redirect_to institution_homepage_path(inst_code: user.inst.code)
    end
  end

  def validate_inst_matches_user
    return if params[:inst_code] == user.inst.code

    redirect_to institution_homepage_path(inst_code: user.inst.code, view: @view)
  end

  def validate_inst_param
    return unless params.include?(:inst_code)

    vserv = ValidationService.new
    inst_code = params[:inst_code]

    return if vserv.is_active_institution?(inst_code)

    if vserv.is_valid_institution_code?(inst_code)
      flash.alert = 'Institution is not active.'
    else
      # NOTE: this is handled by contraint on '/:inst_code' route, so shouldn't get here
      flash.alert = 'Not understood.'
    end

    redirect_to welcome_path
  end

  def redirect_to_login
    set_solr_institution
    login_path = welcome_path(inst: params[:inst_code], view: params[:view])
    return if warden.authenticate?(:ip)
    if @institution&.open_athens
      redirect_to oa_wayfless_link(@institution, login_path), allow_other_host: true
    else
      redirect_to instructions_path(inst: @institution&.code)
    end
  end

  def set_solr_institution
    @institution = InstitutionService.new.find(params[:inst_code])
  end

  def set_top_facets
    subjects_response = search_service.facet_field_response('subject_sms', 'facet.sort': 'index')
    formats_response = search_service.facet_field_response('format_sms', 'facet.sort': 'index')
    @top_facets = {}
    @top_facets[:subject] = group_counts 'subject_sms', subjects_response['facet_counts']['facet_fields']
    @top_facets[:type] = group_counts 'format_sms', formats_response['facet_counts']['facet_fields']
  end

  # Pull RSS content, but don't wait too long
  def set_about_content
    @about_content = {
      news: grab_news,
      alerts: grab_alerts
    }
  end

  # Blacklight returns facet counts in an unorganized array
  # This groups them into something nicer to work with
  # also, we stop after 4 facets
  # @param [String] field
  # @param [Array] response_fields
  # @return [Array<Hash>] nice array of hashes
  def group_counts(field, response_fields)
    facets = []
    return facets unless response_fields.key? field

    facet_count_array = response_fields[field]
    facet_count_array.each_with_index do |fc, i|
      break if i > 7

      next if i.odd?

      facets << { label: fc, count: facet_count_array[i + 1],
                  href: search_action_path(search_state.add_facet_params(field, fc))}
    end
    facets
  end

  def allow_iframe
    response.headers['X-Frame-Options'] = 'ALLOWALL'
  end

  # region bento_cookie

  def redirect_to_bento_home
    redirect_to institution_bento_home_path(inst_code: user.inst.code)
  end

  def bento_cookie_present?
    cookies[:bento].present?
  end

  def add_bento_cookie
    cookies[:bento] = 'true' if params[:bento_on].present?
  end

  def remove_bento_cookie
    cookies.delete :bento if params[:bento_off].present?
  end

  def set_features
    @features = FeatureService.find_all @institution, @view
  end

  # endregion
end
