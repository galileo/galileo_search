# frozen_string_literal: true

class WayfinderDataController < ApiBaseController

  def wayfinder_data
    wayfinder_items = InstitutionService.new.all_with_sites_codes_and_keywords
    if wayfinder_data_params[:wayfinder_items]
      render json: wayfinder_items[wayfinder_data_params[:wayfinder_items].to_sym]
    else
      render json: wayfinder_items
    end
  end

  def wayfinder_data_params
    params.permit(:wayfinder_items, :format)
  end

end

