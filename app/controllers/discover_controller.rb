# frozen_string_literal: true

# :nodoc:
class DiscoverController < ApplicationController
  include DiscoverHelper
  include LinkHelper
  include OpenAthensProxyHelper

  skip_before_action :verify_authenticity_token
  before_action :redirect_to_login, unless: :user
  before_action :sign_out_user, if: :user_session_old?
  before_action :validate_query, unless: :advanced_search?
  after_action :write_search_stat, if: :regular_search?

  def index
    discovery_code = discover_params[:profile] ? "zbds:#{discover_params[:profile]}" : 'zbds'
    discovery_resource = ResourceService.new(user.inst.code).find(discovery_code || 'zbds')
    discovery_url = auth_context_url_for user, discovery_resource

    discovery_query_url = DiscoverQueryService.new(discovery_url, user)
                                              .compose( discover_params, encode_for_open_athens_proxy?(discovery_resource) )
    StatisticsService.link session, user.inst.code, discovery_code
    redirect_to discovery_query_url, allow_other_host: true
  end

  def encode_for_open_athens_proxy?(resource)
    resource.oa_proxy_enabled? && open_athens_proxy
  end

  private

  def discover_params
    params.permit(:instcode, :search_galileo_query, :password,
                  :library_collection, :full_text, :peer_reviewed,
                  :catalog_only, :profile, :advanced, :authenticity_token,
                  :an, :db, discipline: [])
  end

  def advanced_search?
    discover_params[:advanced]
  end

  def detailed_record_link?
    discover_params[:db].present? && discover_params[:an].present?
  end

  def regular_search?
    !advanced_search? && !detailed_record_link?
  end

  def write_search_stat
    StatisticsService.search(discover_params[:search_galileo_query],
                             :discover,
                             user&.inst&.code,
                             user&.site&.code)
  end

  def validate_query
    return if discover_params[:search_galileo_query] || detailed_record_link?

    flash[:error] = 'Please enter a search term'
    redirect_to institution_homepage_path inst_code: user.inst.code
  end

  # if user is not logged in, redirect to login with destination set as
  # URL param
  def redirect_to_login
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(inst: discover_params[:instcode],
                           dest: discover_path(discover_params.to_h),
                           passphrase: discover_params[:password])
  end

  def user_session_old?
    Time.zone.now - user.login_time.to_datetime > Rails.configuration.user_session_life_span
  end

  # Logout user and redirects them to login again
  def sign_out_user
    redirect_to login_path(inst: discover_params[:instcode],
                           dest: discover_path(discover_params.to_h),
                           passphrase: discover_params[:password])
  end
end
