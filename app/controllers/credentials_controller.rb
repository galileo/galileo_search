# frozen_string_literal: true

# :nodoc:
class CredentialsController < ApplicationController
  include LinkHelper

  before_action :redirect_to_login, unless: :user

  def show
    # user must be authenticated here as the inst
    code = params[:code].to_s

    @resource = ResourceService.new(user.inst.code).find(code)

    if user.inst.code == @resource&.institution_code
      render :show
    else
      flash.alert = 'Sorry, you can\'t access this resource'
      redirect_to institution_homepage_path(user.inst.code)
    end
  end

  private

  # if user is not logged in, redirect to login with destination set as
  # URL param
  def redirect_to_login
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(dest: link_path(params[:code]))
  end
end