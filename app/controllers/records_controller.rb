class RecordsController < ApplicationController
  before_action :redirect_to_login, unless: :user
  before_action :set_solr_institution, unless: @institution


  def eds
    bento_serv = BentoService.new user.inst.code
    eds_api = EdsApi.new bento_serv.config_for_view(@view)
    result = eds_api.retrieve eds_params[:db], eds_params[:an]
    @record = GenericBento.new(BentoRequestEdsApi, @institution, @view).wrap_result(result)
  rescue EBSCO::EDS::Error => e
    display_error e
  end

  def journals_ebsco
    bento = GenericBento.new(BentoRequestEdsPublications, @institution, @view)
    request = BentoRequestEdsPublications.new bento, BentoQuery.new(terms: eds_params[:isxn])
    request.search_field = 'isxn'
    @record = request.respond.records.first;
  end

  private

  # Only allow a list of trusted parameters through.
  def eds_params
    params.permit(:inst_code, :db, :an, :isxn)
  end

  # if user is not logged in, redirect to login with destination set as URL param
  def redirect_to_login
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(inst: params[:inst_code])
  end

  def set_solr_institution
    @institution = InstitutionService.new.find(params[:inst_code])
  end

  def display_error(error)
    error_info = if error.is_a? EBSCO::EDS::Error
                   { class: error.class.to_s,
                     description: error.fault&.dig(:error_body, 'ErrorDescription'),
                     detailed_description: error&.fault.dig(:error_body, 'DetailedErrorDescription'),
                     eds_error_number: error.fault&.dig(:error_body, 'ErrorNumber') }
                 else
                   { class: error.class.to_s,
                     description: error.message }
                 end
    render 'error', locals: { error: error_info }
  end
end
