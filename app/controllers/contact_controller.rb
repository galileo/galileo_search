# frozen_string_literal: true

# Contact/Support actions
class ContactController < ApplicationController
  include GalileoRss

  protect_from_forgery :except => [:gkr, :nge_revisions, :nge_new_article, :nge_comments, :nge_errors, :dlg ]

  # show contact us page options, etc.
  def index; end

  # render main "Contact Us" form
  def support; end

  # render "Other" contact form
  def other; end

  # render "DLG" contact form
  def dlg; end

  # render "GKR" contact form
  def gkr; end

  # render "NGE" contact form
  def nge_revisions; end

  # render "NGE" contact form
  def nge_new_article; end

  # render "NGE" contact form
  def nge_comments; end

  # render "NGE" contact form
  def nge_errors; end

  # handle form submissions - form actions should point here
  def submit
    if authenticated? || verify_recaptcha
      form_params = contact_params.merge(user_params)
      mailer = select_mailer(contact_params[:formtype], form_params)
      mailer.deliver_now
      render :thank_you, locals: { formtype: contact_params['formtype'], from_url: contact_params['from_url'] }
    else
      render :recaptcha_fail
    end
  end

  private

  # include form field names here
  def contact_params
    params.permit(:formtype, :institution, :name, :phone, :email, :address,
                  :subject, :resource, :description, :error, :nge_error, :from_url,
                  :attachment1, :attachment2, :attachment3, :recaptcha,
                  :firstname, :lastname, :casetype, :comment, :item,
                  :article_title, :source, :suggested_topic, :page, :nge_page,
                  :correction, :suggestion)
  end

  def select_mailer(form_type, form_params)
    case form_type
    when 'support'
      ContactMailer.support(form_params)
    when 'other'
      ContactMailer.other(form_params)
    when 'dlg'
      ContactMailer.dlg(form_params)
    when 'crdl'
      ContactMailer.dlg(form_params)
    when 'ghnp'
      ContactMailer.dlg(form_params)
    when 'gkr'
      ContactMailer.gkr(form_params)
    when 'rev'
      ContactMailer.nge_revisions(form_params)
    when 'new' # nge_new-article-topics
      ContactMailer.nge_new_article(form_params)
    when 'com' # nge_suggestions-and-comments
      ContactMailer.nge_comments(form_params)
    when 'err' # nge_technical-errors-and-problems
      ContactMailer.nge_errors(form_params)
    else
      nil
    end
  end

  def user_params
    params = {
      ticket_type: parse_ticket_type,
      configuration_item: parse_configuration_item,
      browser: request.user_agent,
      ip: request.remote_ip,
      ezproxy: cookies['ezproxy'] ? 'logged in' : 'not logged in',
      session_id: request.session_options[:id],
      site: parse_site,
      site_code: parse_site_code
    }

    if authenticated?
      params[:remote] = user.remote ? 'remote' : 'on campus'
      params[:auth_type] = user.auth_type
      params[:login_time] =  user.login_time.to_s
      params[:central_ezproxy] = user.inst.proxy
    else
      params[:remote] = 'not logged in'
      params[:auth_type] = 'not logged in'
      params[:login_time] =  'not logged in'
      params[:central_ezproxy] = 'not logged in'
    end

    params
  end

  def parse_ticket_type
    case contact_params[:formtype]
    when 'dlg', 'crdl'
      'DLG'
    when 'ghnp'
      'GHNP'
    when 'com', 'err', 'new', 'rev'
      'NGE'
    when 'gkr'
      'GKR'
    else
      'GALILEO'
    end
  end

  def parse_configuration_item
    formtype = contact_params[:formtype]
    subject  = contact_params[:subject]
    if( %w(dlg crdl).include? formtype  )
      'GALILEO Digital Library of Georgia'
    elsif( formtype&.include?('ghnp') )
      'GALILEO DLG Georgia Historic Newspapers'
    elsif( %w(com err new rev).include? formtype )
      'GALILEO New Georgia Encyclopedia'
    elsif( formtype&.include?('gkr') )
      'GALILEO Georgia Knowledge Repository'
    elsif( subject&.include?('EDS') )
      'GALILEO EBSCO Discovery Service'
    elsif( subject&.include?('OpenAthens') )
      'GALILEO OpenAthens'
    elsif( subject&.include?('GLRI') )
      'GALILEO GLRI'
    elsif( subject&.include?('Alma') )
      'GALILEO Alma'
    else
      'GALILEO'
    end
  end

  def parse_site
    case contact_params[:formtype]
    when 'dlg'
      'Digital Library of Georgia'
    when 'crdl'
      'Civil Rights Digital Library'
    when 'ghnp'
      'Georgia Historic Newspapers'
    when 'com', 'err', 'new', 'rev'
      'New Georgia Encyclopedia'
    when 'gkr'
      'Georgia Knowledge Repository'
    else
      'GALILEO'
    end
  end

  def parse_site_code
    case contact_params[:formtype]
    when 'dlg'
      'DLG'
    when 'crdl'
      'CRDL'
    when 'ghnp'
      'GHNP'
    when 'com', 'err', 'new', 'rev'
      'NGE'
    when 'gkr'
      'GKR'
    else
      'GALILEO'
    end
  end

end
