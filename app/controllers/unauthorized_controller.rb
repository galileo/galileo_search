# frozen_string_literal: true

# :nodoc:
class UnauthorizedController < ActionController::Metal
  include ActionController::UrlFor
  include ActionController::Redirecting
  include Rails.application.routes.url_helpers
  include Rails.application.routes.mounted_helpers

  delegate :flash, to: :request

  def self.call(env)
    @respond ||= action(:respond)
    @respond.call(env)
  end

  def respond
    flash.alert = failure_message_for auth_type_attempted

    redirect_to welcome_path
  end

  def failure_message_for(auth_type = nil)
    case auth_type
    when :pass
      I18n.t('welcome.authentication.invalid_password_link_html', faq_link: ActionController::Base.helpers.link_to(I18n.t("welcome.authentication.invalid_password_link_text"), 'https://about.galileo.usg.edu/support/which_galileo_institutions_no_longer_provide_galileo_password_access'))
    when :pines
      I18n.t('welcome.authentication.failed_pines_authentication')
    else
      I18n.t('welcome.authentication.failed_authentication')
    end
  end

  # do your best to provide a failure message sensitive to the perceived auth_type
  def auth_type_attempted
    if params.keys.include? 'passphrase'
      :pass
    elsif params.keys.include? 'pinesid'
      :pines
    else
      :ambiguous
    end
  end
end
