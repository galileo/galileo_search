# frozen_string_literal: true

# :nodoc:
class ApplicationController < ActionController::Base
  include RailsWarden::Mixins
  include Blacklight::Controller
  layout :determine_layout if respond_to? :layout
  before_action :image_service
  before_action :set_institution
  before_action :set_view
  before_action :set_max_view
  before_action :check_max_view if FeatureFlags.enabled?(:limit_k12_view_switching)
  before_action :set_view_cookie
  before_action :set_available_views
  before_action :set_notification
  before_action :set_sentry_tags
  before_action :set_banners


  # @param [User::Institution] inst
  # @return [Bool]
  def institution_display_views?(inst)
    return false unless inst
    inst.type.to_sym == :k12 || inst.type.to_sym == :publiclib
  end

  private

  def image_service
    @image_service = ImageService.new
  end

  def set_view
    @view = if params[:view]
              # view explicit in params
              view_symbol params[:view]
            elsif user&.site && institution_display_views?(user&.inst)
              # view implicit from wayfinder selection
              view_symbol(user.site.type)
            end

    # view previously stored in cookie
    @view ||= cookies[:galileo_search_preferred_view]&.to_sym

    # Make sure selected view is valid for Institution when logged in
    return unless @institution


    return if @institution.all_views.include? @view

    @view = nil
    cookies.delete(:galileo_search_preferred_view)
  end

  def set_max_view
    return unless @institution&.display_views? && @view && session[:max_view].blank?

    session[:max_view] = @view
  end

  # @param [SolrInstitution] institution
  # @param [String] view
  # @param [String] max_view
  def check_max_view(institution = @institution, view = @view, max_view = session[:max_view])
    return unless institution&.display_views? && view && session[:max_view].present?

    unless institution.accessible_view? view.to_sym, max_view.to_sym
      @view = max_view
      redirect_to url_for(params.permit!.merge(view: @view, only_path: true))
    end
  end


  def set_view_cookie
    return unless @view
    cookies[:galileo_search_preferred_view] = @view
  end

  # @param [SolrInstitution] institution
  # @param [String] max_view
  def set_available_views(institution = @institution, max_view = session[:max_view])
    return unless institution&.display_views?

    @available_views = if FeatureFlags.enabled?(:limit_k12_view_switching) & max_view
                         institution.available_views max_view.to_sym
                       else
                         institution.all_views
                       end
  end

  def set_notification
    request.env['exception_notifier.exception_data'] = {
      notification_time: Time.now,
      server: request.env['SERVER_NAME'],
      remote_ip: request.remote_ip,
      view: @view
    }
    return unless user

    exception_data = request.env['exception_notifier.exception_data']
    exception_data[:inst] = user.inst&.code
    exception_data[:site] = user&.site&.code
    exception_data[:remote] = user.remote
    exception_data[:auth_type] = user.auth_type
    exception_data[:login_time] = user.login_time
    exception_data[:last_activity] = user.last_activity
  end

  # Normalize view values to consistent symbol
  # @param [String] value
  # @return [Symbol]
  def view_symbol(value)
    case value
    when 'elementary', 'middle', 'highschool', 'educator', 'full' then value.to_sym
    when 'elem' then :elementary
    when 'midd' then :middle
    when 'high' then :highschool
    else
      nil
    end
  end

  def set_institution
    return unless user

    @institution = InstitutionService.new.find(user.inst.code)
    end

  def set_banners
    @banners = if user
                 BannerService.find(institution_code: user.inst.code, institution_group_code: user.inst.inst_group_code)
               else
                 BannerService.find
               end
  rescue StandardError => exception
    Sentry.capture_exception(exception)
    @banners = []
  end


  def set_sentry_tags
    return unless user
    Sentry.set_tags('gs.auth_type': user.auth_type)
    Sentry.set_tags('gs.remote': user.remote)

    Sentry.set_tags('gs.inst_code': user.inst&.code)
    Sentry.set_tags('gs.inst_name': user.inst&.name)
    Sentry.set_tags('gs.inst_type': user.inst&.type)

    Sentry.set_tags('gs.site_code': user.site&.code)
    Sentry.set_tags('gs.site_name': user.site&.name)
    Sentry.set_tags('gs.site_type': user.site&.type)

    Sentry.set_tags('gs.view': @view)
  end
end
