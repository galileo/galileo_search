# frozen_string_literal: true

# :nodoc:
class LinkController < ApplicationController
  include LinkHelper

  before_action :redirect_to_login, unless: :user
  before_action :sign_out_user, if: :user_session_old?

  # Redirect user to a Resource URL, or credentials page
  def show
    code = params[:code].to_s
    resource = ResourceService.new(user.inst.code).find(code) ||
               ResourceService.new('publ').find(code)
    StatisticsService.link session, user.inst.code, resource.code if resource
    if resource && show_credentials?(resource)
      redirect_to credentials_path(code: resource.code)
    elsif resource
      redirect_to auth_context_url_for(user, resource), allow_other_host: true
    else
      flash.alert =
        "#{user.inst.name} (#{user.inst.code}) is not configured for access to #{code}."
      redirect_to institution_homepage_path(user.inst.code)
    end
  end

  private

  # Route user to credentials page if always_show_credentials is true
  # Otherwise, show credentials if user is remote and remote_show_credentials is
  # true
  # @param [Resource] resource
  # @return [TrueClass, FalseClass]
  def show_credentials?(resource)
    return true if resource.always_show_credentials?

    user.remote? && resource.remote_show_credentials?
  end

  # if user is not logged in, redirect to login with destination set as
  # URL param
  def redirect_to_login
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(dest: link_path(params[:code]))
  end

  def user_session_old?
    Time.zone.now - user.login_time.to_datetime > Rails.configuration.user_session_life_span
  end

  # Logout user and redirects them to login again
  def sign_out_user
    redirect_to login_path(dest: link_path(params[:code]), inst: user.inst.code)

  end
end
