# frozen_string_literal: true

# :nodoc:
class RedirectController < ApplicationController
  include ApplicationHelper

  before_action :validate_route
  before_action :lookup_and_validate_inst
  before_action :validate_url_parameter

  ##
  # redirect expects two parameters - inst and url
  # inst - must be an active, openathens institution
  # url - must be last param, since it is unencoded and contains invalid param
  # chars
  # with this info, compose an oa redirector link and redirect
  def show
    redirect_to oa_redirector_url(@institution, @url), allow_other_host: true
  end

  private

  def match_route_pattern
    original_path = request.original_fullpath
    original_path.match /\?inst=(?<instcode>[a-z0-9]{4})[&;]url=(?<url>.*)/
  end

  # TODO How do we make this part of rails routes?
  def validate_route
    matches = match_route_pattern

    unless matches
      flash[:error] = 'Sorry, that is not a valid URL'
      redirect_to welcome_url
    end

  end

  # TODO move this into the controller action
  def lookup_and_validate_inst
    matches = match_route_pattern

    instcode = matches['instcode']

    @institution = InstitutionService.new.find instcode

    unless @institution
      flash[:error] = 'Sorry, you cant use /redirect with this institution code'
      redirect_to welcome_url
    end
  end

  # TODO move this into the controller jaction
  def validate_url_parameter
    matches = match_route_pattern

    url = matches['url']

    unless url =~ URI.regexp
      flash[:error] = 'Sorry, that is not a valid URL'
      redirect_to welcome_url
    end

    @url = url

  end
end
