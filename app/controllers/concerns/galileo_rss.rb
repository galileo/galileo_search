# frozen_string_literal: true

require 'active_support/concern'

# Shared behavior for pulling GALILEO RSS feed data
module GalileoRss
  extend ActiveSupport::Concern

  RSS_NEWS_URL = 'https://about.galileo.usg.edu/news/feed/'
  RSS_ALERTS_URL = 'https://about.galileo.usg.edu/system_status/feed/'

  def grab_news
    items = []
    URI.open(RSS_NEWS_URL, read_timeout: 1, open_timeout: 1) do |rss|
      feed = RSS::Parser.parse(rss)
      return unless feed&.items

      items = feed.items&.take(5)
    end
    items
  rescue Exception => _e
    [OpenStruct.new(title: 'Unable to grab news feed. Please check back later.')]
  end

  def grab_alerts
    items = []
    URI.open(RSS_ALERTS_URL, read_timeout: 1, open_timeout: 1) do |rss|
      feed = RSS::Parser.parse(rss)
      return unless feed&.items

      items = feed.items.take(5)
    end
    items
  rescue Exception => _e
    [OpenStruct.new(title: 'Unable to retrieve alerts at this time. Please check back later.')]
  end
end
