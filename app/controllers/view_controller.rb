# frozen_string_literal: true

# :nodoc:
class ViewController < ApplicationController

  before_action :redirect_to_login, unless: :user

  def index
    cookies.delete(:galileo_search_preferred_view)
    render layout: false
  end

  private

  def redirect_to_login
    redirect_to login_path()
  end
end
