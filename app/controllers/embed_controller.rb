# frozen_string_literal: true

require 'oj'

# :nodoc:
class EmbedController < ApplicationController
  include EmbedHelper
  layout 'embed'

  before_action :redirect_to_login, unless: :user, only: :show
  content_security_policy false, only: [:index, :search_galileo]
  protect_from_forgery except: :search_galileo
  after_action :allow_iframe, only: :index

  # return response for actual embedded search box
  def index
    unless params[:json].present?
      head :bad_request
      return
    end
    begin
      json = Oj.load(CGI.unescape(params[:json]))
    rescue Oj::ParseError
      head :bad_request
      return
    end
    @config = {
      instcode: json['instcode'],
      input_width: json['input_width'] || '450px',
      profile: json['profile'],
      bento_search: js_bool(json['bento_search']),
      bento_type: json['bento_type'],
      arrangement: json['arrangement'],
      view: json['view'],
      target: json['target'],
      show_advanced: js_bool(json['advanced']),
      show_galileo_link: js_bool(json['home']),
      heading_text: json['heading'],
      full_text: json['full_text'],
      catalog_only: json['catalog_only'],
      peer_reviewed: json['peer_reviewed'],
      library_collection: json['library_collection'],
      disciplines: disciplines(json['disciplines']),
      disciplines_input_type: disciplines_input_type(json['disciplines_type']),
      disciplines_input_size: disciplines_input_size(json['disciplines_type']),
      disciplines_label: json['disciplines_label']
    }
  end

  # demo page with embed code
  def show
    render 'show', layout: false
  end

  def search_galileo; end

  private

  def redirect_to_login
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(dest: embed_test_path)
  end

  def js_bool(json_value)
    return false if json_value == ''
    return false if json_value == 0
    !!json_value
  end

  # @param [String] disciplines_string
  # @return [Array<Hash>]
  def disciplines(disciplines_string)
    return unless disciplines_string

    disciplines_string.split(',').map do |d|
      code = d.strip
      selected = code[-1] == '*'
      { code: code, selected: selected }
    end
  end

  # defaults to checkbox
  def disciplines_input_type(val = nil)
    return :checkbox unless val

    if val =~ /select/
      return :multiple if val =~ /multiple/
      return :select if val =~ /select/
    end

    :checkbox
  end

  # Return number of input rows to show, or nil
  # Wont work for values over 9, but that is very unlikely
  # @return [Integer]
  def disciplines_input_size(val = nil)
    return unless val&.start_with? 'select multiple '

    num = val[-1]

    num =~ /\d/ ? num.to_i : 3
  end

  def allow_iframe
    response.headers['X-Frame-Options'] = 'ALLOWALL'
  end

end
