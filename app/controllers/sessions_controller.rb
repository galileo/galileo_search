# frozen_string_literal: true

# :nodoc:
class SessionsController < ApplicationController
  include ApplicationHelper
  include EzproxyHelper
  include OpenAthensProxyHelper

  def new
    inst = sessions_params[:inst]
    # these cases override the current session, if any:
    # - password
    if sessions_params[:passphrase]
      session_login(:passphrase)
    # - pines
    elsif sessions_params[:pinesid]
      begin
        session_login(:pines)
      rescue UseOpenAthensForInstitution => e
        inst = e.inst_code
      end
    # - auth=ip
    elsif sessions_params[:auth] == 'ip'
      session_login(:ip)
    # - no current session
    elsif !user || user_session_old?
      session_login(:ip)
    end

    # login successful or already logged in
    if user
      flash.clear
      # - openathens proxy (ezproxy workaround)
      if open_athens_proxy and sessions_params[:returnData]
        destination = open_athens_proxy_login(return_data: sessions_params[:returnData])
      # - cross-institution oa proxy link?
      elsif sessions_params[:returnData]
        flash.alert = 'The link you clicked may require you to first log in to GALILEO with your institution\'s password.'
        destination = institution_homepage_path(inst_code: user.inst.code, view: @view)
      # - log in to the oa proxy connector
      elsif sessions_params[:passphrase] && open_athens_proxy
        url = if sessions_params[:dest]
                path_to_url(sessions_params[:dest])
              elsif sessions_params[:proxy]
                oa_proxy_link(sessions_params[:proxy])
              else
                institution_homepage_url(inst_code: user.inst.code)
              end
        destination = open_athens_proxy_login(return_url: url)
      # - destination from express controller
      elsif sessions_params[:dest]
        destination = sessions_params[:dest]
      elsif open_athens_proxy and sessions_params[:proxy]
        destination = oa_proxy_link(sessions_params[:proxy])
      else
        destination = institution_homepage_path(inst_code: user.inst.code, view: @view)
      end

    # remote user ... send to openathens?
    elsif requesting_oa_inst? inst
       destination = oa_wayfless_link lookup_inst(inst), sessions_params[:dest]

    # remote user ... go to wayfinder/instructions
    else
      # - instruction page (or wayfinder if no inst param)
      flash.keep
      destination = welcome_path(inst: inst,
                                 dest: sessions_params[:dest],
                                 proxy: sessions_params[:proxy],
                                 returnData: sessions_params[:returnData])
    end
    redirect_to destination, allow_other_host: true

  end

  def destroy
    redirect_target = welcome_path

    if user
      redirect_target = 'https://login.openathens.net/signout' if user&.auth_type == :oa
      # Needs to be called before warden logout
      remove_preferred_view_cookie if FeatureFlags.enabled?(:limit_k12_view_switching)
      remove_ezproxy_cookie if user.inst.proxy
      open_athens_proxy_logout if open_athens_proxy
      warden.logout
    end

    redirect_to redirect_target, allow_other_host: true
  end

  def show
    @cookies = {
      preferred_view: cookies[:galileo_search_preferred_view],
      ezproxy: cookies[:ezproxy],
      bento: cookies[:bento],
      classic: cookies[:classic]
    }
    @session = {
      current_view: @view,
      user: user,
      inst: user&.inst,
      site: user&.site,
      host: Socket.gethostname,
      ip: request.remote_ip,
      user_agent: request.env['HTTP_USER_AGENT']
    }
  end

  private

  def session_logout
    if user
      remove_ezproxy_cookie if user.inst.proxy
      open_athens_proxy_logout if open_athens_proxy
      warden.logout
    end
  end

  def session_login(auth_method)
    session_logout
    # don't want to show a failure message for IP logins
    if auth_method == :ip
      warden.authenticate(auth_method)
      return unless user
    else
      authenticate!(auth_method)
    end
    ezp_login_and_set_cookie if user.inst.proxy
  end

  def lookup_inst(inst_code)
    InstitutionService.new.find inst_code
  end

  def sessions_params
    params.permit(:auth, :dest, :inst, :passphrase, :pinesid, :site,
                  :authenticity_token, :returnData, :proxy)
  end

  def user_session_old?
    Time.zone.now - user.login_time.to_datetime > Rails.configuration.user_session_life_span
  end

  def user_on_campus_for_requested_inst?(inst_code)
    IpLookupService.new.ip_is_for?(request.remote_ip, inst_code)
  end

  def requesting_oa_inst?(inst_code)
    institution = lookup_inst(inst_code)
    institution&.open_athens
  end

  def remove_preferred_view_cookie
    cookies.delete(:galileo_search_preferred_view)
  end
end
