# frozen_string_literal: true

require 'oj'

# :nodoc:
class WelcomeController < ApplicationController
  helper WelcomeHelper
  include ApplicationHelper
  include DiscoverHelper
  include LinkHelper

  before_action :attempt_ip_login, only: :index, if: :from_root_path
  before_action :load_remembered_selections, only: %i[index select clear_remembered_selection]
  before_action :redirect_to_homepage, if: :user
  before_action :set_desired_institution, if: :inst_param?

  after_action :add_to_remembered_cookie, only: :select

  layout 'custom_base'

  def index
    if @institution
      redirect_to instructions_path(inst: @institution.code, dest: params[:dest])
    end
  end

  def select
    institution_service = InstitutionService.new
    @selected = institution_service.find_by_name welcome_params[:wayfinder_selection]
    verify_selected || return # redirect unless @selected is found
    @institution = if @selected.parent
                     institution_service.find @selected.parent
                   else
                     @selected
                   end
    if @institution&.open_athens
      redirect_to oa_wayfless_link(@institution, open_athens_destination), allow_other_host: true
    else
      flash.clear
      redirect_to instructions_path(
        params: { inst: @institution&.code, site: @selected&.code, proxy: params[:proxy],
                  dest: params[:dest], returnData: welcome_params[:returnData] }
      )
    end
  end

  def instructions
    @selected = if welcome_params[:site]
                  InstitutionService.new.find welcome_params[:site]
                else
                  @institution
                end
    if @selected && @institution && ValidationService.new.is_active_institution?(@institution&.code)
      if welcome_params[:dest]
        flash.alert = 'Please enter your password to continue'
      end
      render :instructions
    else
      redirect_to :welcome, alert: "Problem showing instructions"
    end
  end

  def clear_remembered
    cookies.delete :gs_wayf_remembered
    redirect_to :welcome, info: 'Remembered selections cleared.'
  end

  # Remove entry from remember_me cookie
  def clear_remembered_selection
    if @remembered.include?(params[:wayfinder_selection])
      @remembered.delete params[:wayfinder_selection]
      cookies.permanent[:gs_wayf_remembered] = Oj.dump @remembered[0..4]
    end

    redirect_to :welcome
  end

  private

  def open_athens_destination
    if cookies[:galileo_discovery]
      discover_url
    elsif params[:dest]
      params[:dest]
    else
      institution_homepage_url(inst_code: @institution.code)
    end
  end

  # this is a bit oblique, but when used with "verify_selected || return"
  # returns an early redirect from a controller method (redirect_to does not
  # stop method execution)
  def verify_selected
    unless @selected
      redirect_to(:welcome,
                  alert: 'Please select a displayed option in the menu below') &&
        return
    end
    true
  end

  def load_remembered_selections
    @remembered = Oj.load cookies[:gs_wayf_remembered] || '[]'
  end

  # Add selected entry to the remember_me cookie
  def add_to_remembered_cookie
    return if !@selected || @remembered.include?(@selected.name)

    @remembered.prepend @selected.name
    cookies.permanent[:gs_wayf_remembered] = Oj.dump @remembered[0..4]
  end

  # check if request is initiated by visiting root_path
  # @return [TrueClass, FalseClass]
  def from_root_path
    request.fullpath == root_path
  end

  # try IP authentication and fail silently
  # @return [TrueClass, FalseClass]
  def attempt_ip_login
    warden.authenticate?(:ip)
  end

  def inst_param?
    welcome_params.include? :inst
  end

  def redirect_to_homepage
    redirect_to institution_homepage_path(inst_code: user.inst.code, view: welcome_params[:view])
  end

  def set_desired_institution
    @institution = InstitutionService.new.find(welcome_params[:inst])
  end

  def welcome_params
    params.permit(:inst, :wayfinder_selection, :site, :proxy, :dest, :view, :returnData, :authenticity_token, :commit)
  end
end
