# frozen_string_literal: true

# blacklight configuration for GALILEO search!
class CatalogController < ApplicationController
  include Blacklight::Catalog

  layout false
  layout 'blacklight', except: :bento

  before_action :set_institution_and_index_redirect, only: :index, unless: :inst_in_params
  before_action :set_institution_and_show_redirect, only: :show, unless: :inst_in_params
  before_action :validate_inst_param
  before_action :validate_id_param, only: :show
  after_action :write_search_stat, only: :index, if: :query_present?
  before_action :set_subject_facet_order, only: :index

  configure_blacklight do |config|
    config.document_model = Resource
    config.search_builder_class = SearchBuilder

    # Configure document handler to use special unique id field
    config.document_solr_path = 'document'
    config.document_unique_id_param = :blacklight_id_ss

    # items to show per page, each number in the array represent another option to choose from.
    config.per_page = [10, 50, 100, 1000]
    config.max_per_page = 1000
    # solr field configuration for search results/index views
    config.index.title_field = 'resource_name_ss'
    config.index.display_type_field = 'class_ss'
    config.index.document_presenter_class = ResourceResultPresenter
    # remove thumbnail partial, as we customized the search results view a lot
    config.index.partials.delete :thumbnail
    config.index.thumbnail_method = :resource_thumbnail

    config.add_results_collection_tool(:sort_widget)
    config.add_results_collection_tool(:per_page_widget)

    config.show.document_presenter_class = ResourceShowPresenter
    config.show.thumbnail_method = :resource_thumbnail
    config.add_show_tools_partial(:email, callback: :email_action, validator: :validate_email_params)

    config.add_facet_field 'format_sms', label: 'Types', limit: true
    config.add_facet_field 'for_institution_ss', label: 'Institution', limit: true, show: false
    config.add_facet_field 'glri_b', label: 'GLRI', limit: true, show: false
    config.add_facet_field 'subject_sms', label: 'Subjects', limit: true, sort: 'index'
    config.add_facet_field 'user_view_sms', label: 'User Views', limit: true, show: false
    config.add_facet_field 'keywords_sms', label: 'Keywords', limit: true
    config.add_facet_field 'resource_name_first_char_ss', label: 'A-Z', limit: 50, sort: 'index'

    # Have BL send all facet field names to Solr, which has been the default
    # previously. Simply remove these lines if you'd rather use Solr request
    # handler defaults, or have no facets.
    config.add_facet_fields_to_solr_request!

    # solr fields to be displayed in the index (search results) view
    config.add_index_field 'resource_short_description_texts', label: 'Description', helper_method: :strip_html
    config.add_index_field 'access_note_us', label: 'Access Note', helper_method: :sanitize_html
    config.add_index_field 'thumbnail_us', label: 'Thumbnail', if: :json_api_request?, helper_method: :thumbnail_link
    # config.add_index_field 'score', label: 'Relevance Score'

    # solr fields to be displayed in the show (single result) view
    config.add_show_field 'express_link', accessor: :express_link, helper_method: :linkify
    config.add_show_field 'coverage_dates_us', label: 'Coverage Dates', helper_method: :sanitize_html
    config.add_show_field 'update_frequency_us', label: 'Update Frequency', helper_method: :sanitize_html
    config.add_show_field 'resource_short_description_texts', label: 'Short Description', helper_method: :strip_html
    config.add_show_field 'resource_long_description_texts', label: 'Long Description', helper_method: :sanitize_html
    config.add_show_field 'format_sms', label: 'Formats', helper_method: :format_array_field_as_badges
    config.add_show_field 'subject_sms', label: 'Subjects', helper_method: :format_array_field_as_badges
    config.add_show_field 'keywords_sms', label: 'Keywords', helper_method: :format_array_field_as_badges

    config.add_show_field 'vendor_name_texts', label: 'Vendor', helper_method: :format_array_field_as_badges

    config.add_show_field 'branding_text_us', label: 'Branding', helper_method: :sanitize_html
    config.add_show_field 'access_note_us', label: 'Access Note', helper_method: :sanitize_html

    # "fielded" search configuration. Used by pulldown among other places.
    # For supported keys in hash, see rdoc for Blacklight::SearchFields
    config.add_search_field 'all_fields' do |field|
      field.label = 'All Fields'
      field.solr_parameters = {
        qf: 'resource_name_texts^1000
             for_resource_ss^500
             resource_short_description_texts^250
             resource_long_description_texts^250
             resource_keywords_texts^100',
        pf: 'resource_name_texts^1000
             for_resource_ss^500
             resource_short_description_texts^250
             resource_long_description_texts^250
             resource_keywords_texts^100'
      }
    end

    config.add_sort_field 'score desc, resource_name_sort_s asc', label: 'Relevance'
    config.add_sort_field 'resource_name_sort_s asc', label: 'Name'

    # If there are more than this many search results, no spelling ("did you
    # mean") suggestion is offered.
    config.spell_max = 5

    # Configuration for autocomplete suggester
    config.autocomplete_enabled = false
    config.autocomplete_path = 'suggest'
    config.autocomplete_suggester = 'nameSuggester'
  end

  def sms_mappings
    {
      'Virgin' => 'vmobl.com',
      'AT&T' => 'txt.att.net',
      'Verizon' => 'vtext.com',
      'Nextel' => 'messaging.nextel.com',
      'Sprint' => 'messaging.sprintpcs.com',
      'T Mobile' => 'tmomail.net',
      'Alltel' => 'message.alltel.com',
      'Cricket' => 'mms.mycricket.com',
      'Google Fi' => 'msg.fi.google.com',
      'Boost Mobile' => 'myboostmobile.com',
      'Comcast' => 'comcastpcs.textmsg.com',
      'Metro PCS' => 'mymetropcs.com',
      'Tracfone' => 'txt.att.net',
      'US Cellular' => 'email.uscc.net',
      'Consumer Cellular' => 'mailmymobile.net'
    }
  end

  def bento
    unless authenticated?
      redirect_to welcome_path
      return
    end

    params[:per_page] = (params[:page] ? 20 : 5)
    params[:view] = @view.to_s
    (@response, _deprecated_document_list) = search_service.search_results

    records = @response.dig('response', 'docs').map do |doc|
      Resource.new doc
    end

    results = {
      count: @response.dig('response', 'numFound'),
      records: records
    }

    title = params[:display_name] || 'Databases'
    no_results_message = params[:no_results_message]
    no_results_message = "#{I18n.translate 'bento.bento_box.no_results_in'} #{title}" if no_results_message.blank?


    respond_to do |format|
      format.html do
        render partial: 'bento/results_database',
               locals: {
                 title: title,
                 no_results_message: no_results_message,
                 is_more_results: !params[:page].nil?,
                 query: params[:q],
                 results: results,
                 error: nil
               }
      end
    end
  end

  private

  def set_institution_and_index_redirect
    if user&.inst&.code
      redirect_to institution_search_path(inst_code: user.inst.code, view: @view)
    else
      redirect_to welcome_path
    end
  end

  def set_institution_and_show_redirect
    if user&.inst&.code
      params[:id] = "#{params[:id]}-#{user.inst.code}" if params[:id].length == 4
      redirect_to institution_resource_path(inst_code: user.inst.code, id: params[:id], view: @view)
    else
      redirect_to welcome_path
    end
  end

  def inst_in_params
    params[:inst_code]
  end

  def validate_inst_param
    return unless inst_in_params

    vserv = ValidationService.new
    inst_code = params[:inst_code]

    return if vserv.is_active_institution?(inst_code)

    flash.alert = if vserv.is_valid_institution_code?(inst_code)
                    'Institution is not active.'
                  else
                    'Not understood.'
                  end

    redirect_to welcome_path
  end

  def validate_id_param
    return unless params.include?(:id)

    vserv = ValidationService.new
    resource_code = params[:id]

    return if vserv.is_active_resource?(resource_code)

    flash.alert = if vserv.is_valid_resource_code?(resource_code)
                    'Resource is not active.'
                  else
                    'Not understood.'
                  end

    redirect_to welcome_path
  end

  def write_search_stat
    StatisticsService.search(params[:q],
                             :resource,
                             user&.inst&.code,
                             user&.site&.code)
  end

  def query_present?
    params[:q].present?
  end

  def json_api_request?
    request.format.json?
  end

  def set_subject_facet_order
    if query_present?
      @blacklight_config.facet_fields['subject_sms'].sort = 'count'
    end
  end

  def current_search_session
    super
  rescue ActiveRecord::ActiveRecordError
    nil
  end

end
