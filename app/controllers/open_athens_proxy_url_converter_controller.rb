# frozen_string_literal: true

class OpenAthensProxyUrlConverterController < ApplicationController

  def open_athens_proxy_url_converter
    urls_to_convert = open_athens_proxy_url_converter_params[:ezproxy_urls]&.split("\n")&.filter(&:presence)
    return unless urls_to_convert

    @ezproxy_urls = urls_to_convert
    @open_athens_proxy_urls = []
    urls_to_convert.each do |url|
      @open_athens_proxy_urls << LinkingService.oa_proxy_url_for(url)
    end
  end

  end

  private

  def open_athens_proxy_url_converter_params
    params.permit( :ezproxy_urls )
  end

