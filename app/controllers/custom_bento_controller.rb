# frozen_string_literal: true

# :nodoc:
class CustomBentoController < ApplicationController
  include DiscoverHelper
  include LinkHelper
  include ApplicationHelper
  skip_before_action :verify_authenticity_token
  before_action :redirect_to_login_then_bento, unless: :user, only: :bento_search
  before_action :redirect_to_login_then_refine_results, unless: :user, only: :full_results
  before_action :return_unauthorized, unless: :user, except: [:bento_search, :full_results]

  before_action :validate_query, except: [:bento_search, :eds_fulltext, :full_results, :full_results_api, :facet_api]
  before_action :set_solr_institution, unless: @institution

  layout false
  layout 'blacklight', except: :bento_api

  def bento_search
    @bentos = ConfiguredBentoService.new(@institution.code, @view).default_list.map do |cb|
      {
        title: cb.display_name,
        bento_type: cb.slug,
        endpoint: cb.endpoint,
        results_page: cb.results_page,
        tour_text: I18n.translate("walkthru.bentos.#{cb.code}", default: '').presence,
        description: cb.description
      }
    end
    @alternate_bentos = ConfiguredBentoService.new(@institution.code, @view)
                                              .hidden_list
                                              .sort_by { |cb| cb.display_name.downcase }
                                              .map do |cb|
      {
        title: cb.display_name,
        bento_type: cb.slug,
        endpoint: cb.endpoint,
        results_page: cb.results_page,
        description: cb.description
      }
    end
    @full_text = bento_params.key?(:full_text)
    render 'bento/bento_search'
  end

  def bento_api
    search_terms = bento_params[:bento_search_query]
    full_text = bento_params.key? :full_text
    slug = bento_params[:bento_type]
    page = bento_params[:page]
    filters = bento_params[:f]
    sort = bento_params[:sort]
    bento = ConfiguredBentoService.get(@institution.code, @view, slug)
    begin
      results_display = bento.query BentoQuery.new(terms: search_terms, full_text: full_text, page: page, filters: filters, sort: sort)
      if results_display.is_a? BentoResponse
        render partial: 'bento/results', locals: { response: results_display }
      else
        render partial: results_display[:partial],
               locals: results_display
      end
    rescue BentoRequest::BentoRequestError => e
      return_json_error e
    end
  end

  def full_results_api
    @bento = ConfiguredBentoService.get(@institution.code, @view, full_results_params[:bento_type])
    return head 404 unless @bento
    request = @bento.request_class.new @bento, query_from_params(full_results_params)
    if full_results_params[:search_state].present? && request.respond_to?(:set_search_state)
      request.set_search_state search_state
    end
    @result = request.respond

    render json: {
      result_count: @result.result_count,
      result_count_message: @result.result_count_message,
      max_allowed_page: @result.max_allowed_page,
      take_external_label: @result.take_external_label,
      external_link: @result.has_external_link? ? @result.external_link : nil,
      applied_filters: @result.applied_filters&.map(&:to_h),
      filters: @result.filters&.map(&:for_json),
      records_html: render_to_string(partial: 'bento/results_inner', locals: {response: @result}),
      search_state: encode_search_state_if_present(@result.search_state)
    }
  rescue BentoRequest::BentoRequestError => e
    return_json_error e
  end

  def full_results
    @search_terms = full_results_params[:q] || full_results_params[:bento_search_query] || ''
    @full_text = full_results_params.has_key?(:full_text)
    @facets = full_results_params[:f]
    @page = full_results_params[:page] || 1
    @bento = ConfiguredBentoService.get(@institution.code, @view, full_results_params[:bento_type])
    unless @bento
      view_name = @institution.name
      view_name += " (#{@view} view)" if institution_display_views?(@institution)
      flash.alert = "#{view_name} does not have access to a \"#{full_results_params[:bento_type]}\" bento box."
      redirect_to institution_bento_search_path(inst_code: @institution.code,
                                                bento_search_query: @search_terms)
      return
    end
    @sort_order = full_results_params[:sort]
    unless @bento.request_class.valid_sort_order? @sort_order
      redirect_to bento_full_results_path(raw_full_results_params.to_h.without(:sort, :inst_code, :bento_type))
    end
  end

  def facet_api
    bento = ConfiguredBentoService.get(@institution.code, @view, facet_api_params[:bento_type])
    facet = facet_api_params[:facet]
    page = facet_api_params[:facet_page].present? ? facet_api_params[:facet_page].to_i : 1
    sort = facet_api_params[:facet_sort].presence || 'count'
    return head 404 unless bento
    request = bento.request_class.new bento, query_from_params(facet_api_params)
    raise BentoRequest::BadRequestError unless facet && request.respond_to?(:query_facet)
    result = request.query_facet facet, page, sort
    render json: result.for_json
  end

  private

  def bento_params
    params.permit(:instcode, :bento_search_query, :inst_code, :bento_type, :arrangement, :full_text, :page, :password, :sort, f: {})
  end

  def f_params
    params.keys.filter {|k| /^f\.(\w+)$/.match? k}
  end

  SCALAR_FULL_RESULTS_PARAMS = [:inst_code, :q, :bento_search_query, :bento_type, :full_text, :page, :sort, :search_state].freeze

  def raw_full_results_params
    params.permit(*SCALAR_FULL_RESULTS_PARAMS, *f_params, f: {})
  end

  def facet_api_params
    params.permit(*SCALAR_FULL_RESULTS_PARAMS, :facet, :facet_page, :facet_sort, f: {}).except(:page, :sort)
  end

  def full_results_params
    p = raw_full_results_params
    processed_params = {}
    [*SCALAR_FULL_RESULTS_PARAMS, :f].each do |k|
      processed_params[k] = p[k] if p.has_key? k
    end
    processed_params[:f] ||= {}
    f_params.each do |k|
      k =~ /^f\.(\w+)$/
      field = $1
      processed_params[:f][field] = [p[k]]
    end
    processed_params
  end

  def query_from_params(query_params)
    BentoQuery.new terms: query_params[:q],
                   full_text: query_params.has_key?(:full_text),
                   page: query_params[:page] || 1,
                   filters: query_params[:f],
                   sort: query_params[:sort]
  end

  def return_json_error(error)
    status = error.is_a?(BentoRequest::BadRequestError) ? 400 : 500
    data = {
      error: error.user_message
    }
    if error.is_a? BentoRequest::PossibleIntermittentError
      data[:possibly_temporary] = true
    end
    render status: status, json: data
  end

  def validate_query
    return if bento_params[:bento_search_query]

    flash[:error] = 'Please enter a search term'
    redirect_to institution_homepage_path inst_code: user.inst.code
  end

  # if user is not logged in, redirect to login with destination set as
  # URL param
  def redirect_to_login_then_bento
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(inst: bento_params[:instcode] || bento_params[:inst_code],
                           dest: bento_search_query_path(bento_params.to_h),
                           passphrase: bento_params[:password])
  end

  def redirect_to_login_then_refine_results
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(inst: full_results_params[:inst_code],
                           dest: bento_full_results_path(full_results_params.to_h),
                           passphrase: full_results_params[:password])
  end

  def user_session_old?
    Time.zone.now - user.login_time.to_datetime > Rails.configuration.user_session_life_span
  end

  def set_solr_institution
    if params[:inst_code].blank? && user.inst&.code
      redirect_to params.permit!.merge(inst_code: user.inst&.code)
      return
    end
    @institution = InstitutionService.new.find(params[:inst_code])
  end

  def eds_fulltext_params
    params.permit(:dbid, :an, :type, :inst_code)
  end

  def get_discover_link(query, view)
    profile = @institution.profile_for view
    express_path(inst: @institution.code, profile: profile, search_galileo_query: query)
  end

  def return_unauthorized
    render json: {
      error: I18n.t('bento.error.no_user_session')
    }, status: 400 # A 401 would be more correct here, but that redirects to the login page -- pointless for an API
  end

  def search_state
    encrypted = full_results_params[:search_state]
    return nil unless encrypted.present?
    decrypted = self.class.search_state_crypt.decrypt_and_verify encrypted
    JSON.parse(decrypted)
  rescue ActiveSupport::MessageEncryptor::InvalidMessage, JSON::ParserError
    raise BentoRequest::BadRequestError
  end

  def encode_search_state_if_present(state)
    return nil if state.blank?
    self.class.search_state_crypt.encrypt_and_sign state.to_json
  end

  def self.search_state_crypt
    @search_state_crypt ||= create_search_state_crypt
  end

  def self.create_search_state_crypt
    key_gen = ActiveSupport::KeyGenerator.new(Rails.application.credentials.secret_key_base)
    # Encrypt with a key shared between all our servers/workers
    key = key_gen.generate_key('pagination token key')
    ActiveSupport::MessageEncryptor.new(key[0..31])
  end
end
