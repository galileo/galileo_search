# frozen_string_literal: true

require 'open_athens_worker'

# :nodoc:
class OpenAthensController < ApplicationController
  include EzproxyHelper

  before_action do
    # set maximum debug in development instances
    logger = if Rails.env.development?
               Logger.new(STDOUT)
             else
               Rails.logger
             end
    OpenIDConnect.debug! if Rails.env.development?
    OpenIDConnect.logger = logger
    Rack::OAuth2.logger = logger
    WebFinger.logger = logger
    SWD.logger = logger
  end

  # explicitly log user in using OA
  # User is sent here via redirector? From welcome page?
  def login
    warden.logout if user # logout if session exists

    oa_worker = OpenAthensWorker.new oa_redirect_url
    nonce = SecureRandom.hex 16

    # persist nonce in session - must reference and validate in authenticate!
    session[:oa_nonce] = nonce

    # Get URI for user's IDP login, using nonce
    auth_uri = oa_worker.auth_uri nonce
    session[:target] = params[:target_link_uri]

    redirect_to auth_uri, allow_other_host: true
  end

  def redirect
    unless redis.unique_nonce?(session[:oa_nonce])
      redirect_to_welcome
      return
    end
    authenticate! :open_athens
    # add used nonce
    redis.nonce session.delete(:oa_nonce)
    if user
      ezp_login_and_set_cookie
      flash.clear
      redirect_to session[:target] || root_path
    else
      # can this ever obtain? authenticate! moves to failure app if auth fails
      redirect_to_welcome
    end
  end

  private

  def redis
    @redis ||= RedisService.new
  end

  def redirect_to_welcome
    redirect_to welcome_path, alert: 'Sorry, OpenAthens login failed'
  end
end
