# frozen_string_literal: true

# :nodoc:
class ExpressController < ApplicationController
  include ApplicationHelper
  include EzproxyHelper
  include OpenAthensProxyHelper

  skip_before_action :verify_authenticity_token

  before_action :validate_inst_param
  before_action :validate_link_param
  before_action :bento_query, if: :bento_params?
  before_action :discover_query, if: :discover_params?
  before_action :handle_pinesid, if: :pinesid_params?
  before_action :handle_auth, if: :auth_params?
  before_action :handle_password, if: :password_param_only?
  before_action :lookup_inst

  def show
    case express_params.keys.sort
    when ['inst']
      redirect_to login_path(inst: params[:inst])
    when ['link'], %w[inst link]
      handle_allocation_link
    when %w[link password], %w[inst link password]
      redirect_to login_path(
        passphrase: params[:password],
        dest: destination
      )
    when %w[inst password]
      redirect_to login_path(passphrase: params[:password])
    when %w[proxy]
      handle_proxy_link
    when []
      redirect_to login_path()
    else
      flash.alert = 'Express link could not be understood!'
      redirect_to welcome_path
    end
  end

  private

  def bento_query
    redirect_to bento_search_query_path bento_params.to_h
  end

  def bento_params?
    params.include?(:bento_search_query)
  end

  def bento_params
    params.permit(:instcode, :bento_search_query, :bento_type, :arrangement, :full_text, :password)
  end

  def discover_query
    redirect_to discover_path discover_params.to_h
  end

  def discover_params?
    params.include?(:search_galileo_query) || params.include?(:advanced) || params.include?(:an)
  end

  def discover_params
    params.permit(:instcode, :search_galileo_query, :password,
                  :library_collection, :full_text, :peer_reviewed,
                  :catalog_only, :profile, :advanced, :authenticity_token,
                  :an, :db, discipline: [])
  end

  def handle_allocation_link
    if user
      redirect_to destination
    elsif @institution
      resource = ResourceService.new(@institution.code)
                                .find(express_params['link'])
      if resource&.bypass_authentication?
        # write stat here since we bypass the /link redirect where stats are
        # written
        write_link_stat_for resource
        redirect_to resource&.ip_link, allow_other_host: true
      elsif ip_lookup_matches_inst
        redirect_to destination
      elsif @institution&.open_athens
        redirect_to oa_wayfless_link(@institution, destination), allow_other_host: true
      else
        redirect_to instructions_path(inst: @institution.code, dest: destination)
      end
    elsif (resource = public_resource)
      write_link_stat_for resource, 'publ'
      redirect_to resource.ip_link, allow_other_host: true
    else
      # redirect to login
      redirect_to login_path(dest: destination),
                  alert: 'You need to login to access this resource.'
    end
  end

  def handle_proxy_link
    if user
      if open_athens_proxy
        redirect_to oa_proxy_link(params[:proxy]), allow_other_host: true
      else
        redirect_to institution_homepage_path(user.inst.code)
      end
    else
      redirect_to login_path(proxy: params[:proxy]),
                  alert: 'You need to login to access this resource.'
    end
  end

  def public_resource
    ResourceService.new('publ').find(express_params['link'])
  end

  def destination
    params[:link].blank? ? login_path : link_path(express_params['link'])
  end

  def express_params
    params.permit(:inst, :link, :password, :proxy)
  end

  def pinesid_params?
    params.include? :pinesid
  end

  def auth_params?
    params.include? :auth
  end

  def password_param_only?
    express_params.keys == ['password']
  end

  def handle_pinesid
    redirect_to login_path(pinesid: params[:pinesid])
  end

  def handle_auth
    redirect_to login_path(auth: params[:auth])
  end

  def handle_password
    redirect_to login_path(passphrase: params[:password])
  end

  def inst_param?
    params.include? :inst
  end

  def lookup_inst(inst_code = nil)
    @institution = if user
                     user.inst
                   elsif inst_code
                     InstitutionService.new.find inst_code
                   elsif params[:inst]
                     InstitutionService.new.find params[:inst]
                   elsif express_params['link']&.include? '-'
                     resource_code = express_params['link']
                     inst_code = resource_code[(resource_code.index('-') + 1)..-1]
                     InstitutionService.new.find inst_code
                   else
                     nil
                   end
  end

  def ip_lookup_matches_inst
    return false unless @institution

    if IpLookupService.new.ip_is_for? request.remote_ip, @institution.code
      authenticate!(:ip)
      true
    else
      false
    end
  end

  # @param [Resource] resource
  def write_link_stat_for(resource, inst_code = nil)
    inst_code ||= @institution&.code
    StatisticsService.link 'bypass', inst_code, resource.code
  end

  def validate_inst_param
    return unless express_params.include?(:inst)

    vserv = ValidationService.new
    inst_code = express_params[:inst]

    return if vserv.is_active_institution?(inst_code)

    if vserv.is_valid_institution_code?(inst_code)
      flash.alert = 'Institution is not active.'
    else
      flash.alert = 'Express link could not be understood!'
    end

    redirect_to welcome_path
  end

  def validate_link_param
    return unless express_params.include?(:link)

    vserv = ValidationService.new
    resource_code = express_params[:link]

    return if vserv.is_active_resource?(resource_code)

    if vserv.is_valid_resource_code?(resource_code)
      flash.alert = 'Resource is not active.'
    else
      flash.alert = 'Express link could not be understood!'
    end

    redirect_to welcome_path
  end
end
