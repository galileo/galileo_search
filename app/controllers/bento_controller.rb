# frozen_string_literal: true

# :nodoc:
class BentoController < ApplicationController
  include DiscoverHelper
  include LinkHelper
  include ApplicationHelper
  skip_before_action :verify_authenticity_token
  before_action :redirect_to_login, unless: :user
  before_action :validate_query, except: [:eds_fulltext, :alma_links_api]
  before_action :set_solr_institution, unless: @institution
  after_action :write_search_stat, only: :placard_api

  layout false
  layout 'blacklight'

  def index
    if bento_params.include? :bento_type
      redirect_to bento_full_results_path user.inst.code, bento_params[:bento_type], bento_params.to_h.except(:password)
    else
      redirect_to institution_bento_search_path user.inst.code, bento_params.to_h.except(:password)
    end
  end

  def placard_api
    query = BentoQuery.new terms: bento_params[:bento_search_query],
                           full_text: bento_params.key?(:full_text)
    begin
      result = BentoService.query_eds_placards query, @institution, @view
      render partial: 'bento/placards', locals: result
    rescue EBSCO::EDS::Error, BentoService::BentoServiceError, EdsApi::EdsApiError => e
      return_json_error BentoRequestEdsApi.wrap_eds_error(e)
    end
  end

  def alma_links_api
    uresolve_info = alma_links_params[:uresolve_info]
    links = PrimoApiService.fetch_links_from_uresolve_info uresolve_info
    @primary_links = links[0, 3]
    @more_links = links[3..]
    render partial: 'alma_links_api'
  end

  def eds_fulltext
    bento_serv = BentoService.new user.inst.code
    eds_api = EdsApi.new bento_serv.config_for_view(@view)
    result = eds_api.retrieve eds_fulltext_params[:dbid], eds_fulltext_params[:an]
    url = result.fulltext_link(eds_fulltext_params[:type])[:url]
    redirect_to url,  allow_other_host: true
  end

  private

  def bento_query
    redirect_to institution_bento_search_path user.inst.code, bento_params.to_h
  end

  def bento_params?
    params.include?(:bento_search_query)
  end

  def bento_params
    params.permit(:instcode, :bento_search_query, :inst_code, :bento_type, :arrangement, :full_text, :page, :password)
  end

  def return_json_error(error)
    status = error.is_a?(BentoRequest::BadRequestError) ? 400 : 500
    data = {
      error: error.user_message
    }
    if error.is_a? BentoRequest::PossibleIntermittentError
      data[:possibly_temporary] = true
    end
    render status: status, json: data
  end

  def write_search_stat
    StatisticsService.search(bento_params[:bento_search_query],
                             :bento,
                             user&.inst&.code,
                             user&.site&.code)
  end

  def validate_query
    return if bento_params[:bento_search_query]

    flash[:error] = 'Please enter a search term'
    redirect_to institution_homepage_path inst_code: user.inst.code
  end

  # if user is not logged in, redirect to login with destination set as
  # URL param
  def redirect_to_login
    flash.alert = 'You need to login to access this resource.'
    redirect_to login_path(inst: bento_params[:instcode],
                           dest: bento_search_query_path(bento_params.to_h),
                           passphrase: bento_params[:password])
  end

  def user_session_old?
    Time.zone.now - user.login_time.to_datetime > Rails.configuration.user_session_life_span
  end

  def set_solr_institution
    @institution = InstitutionService.new.find(params[:inst_code])
  end

  def eds_fulltext_params
    params.permit(:dbid, :an, :type, :inst_code)
  end

  def alma_links_params
    params.permit(:uresolve_info)
  end

  def get_discover_link(query, view)
    profile = @institution.profile_for view
    express_path(inst: @institution.code, profile: profile, search_galileo_query: query)
  end
end
