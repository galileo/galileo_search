# frozen_string_literal: true

# :nodoc:
class LtiController < ApplicationController
  include EzproxyHelper

  skip_before_action :verify_authenticity_token

  def create
    if user # logout if session exists
      remove_ezproxy_cookie
      warden.logout
    end

    authenticate!(:lti)
    ezp_login_and_set_cookie if user
    head :created # HTTP 201 "Created"
  end
end