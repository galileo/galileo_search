# frozen_string_literal: true

class ApplicationMailer < ActionMailer::Base
  default from: 'galileo-search-no-reply@galileo.usg.edu'
  layout 'mailer'
end
