# frozen_string_literal: true

# :nodoc:
class ContactMailer < ApplicationMailer

  helper ContactHelper

  # usgtst@service-now.com
  DEFAULT_RECIPIENTS = Rails.configuration.contact_mailer_default_recipients
  SPAM_RECIPIENTS = Rails.configuration.contact_mailer_spam_recipients
  DEFAULT_SENDER = 'gss@galileo.usg.edu'

  def support(form_params)
    @form = form_params

    add_attachments(form_params)

    subject = prepare_subject("#{@form['ticket_type']}: #{@form['description']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['name']}\" <#{DEFAULT_SENDER}>")
  end

  def other(form_params)
    @form = form_params

    add_attachments(form_params)

    subject = prepare_subject("#{@form['ticket_type']}: #{@form['description']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['name']}\" <#{DEFAULT_SENDER}>")
  end

  def dlg(form_params)
    @form = form_params

    add_attachments(form_params)

    subject = prepare_subject("#{@form['ticket_type']}: #{@form['description']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['name']}\" <#{DEFAULT_SENDER}>")
  end

  def gkr(form_params)
    @form = form_params

    add_attachments(form_params)

    subject = prepare_subject("#{@form['ticket_type']}: #{@form['description']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['firstname']} #{@form['lastname']}\" <#{DEFAULT_SENDER}>")
  end

  def nge_revisions(form_params)
    @form = form_params

    nge_error = @form['error']||@form['nge_error']

    add_attachments(form_params)

    subject = prepare_subject("NGE: Type: Revision or Correction Article: #{@form['article_title']} Error: #{nge_error} Correction: #{@form['correction']} Source: #{@form['source']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['firstname']} #{@form['lastname']}\" <#{DEFAULT_SENDER}>")
  end

  def nge_new_article(form_params)
    @form = form_params

    add_attachments(form_params)

    subject = prepare_subject("NGE: Type: Suggested Topic Topic: #{@form['suggested_topic']} Description: #{@form['description']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['firstname']} #{@form['lastname']}\" <#{DEFAULT_SENDER}>")
  end

  def nge_comments(form_params)
    @form = form_params

    add_attachments(form_params)

    subject = prepare_subject("NGE: Type: General Comment/Suggestion Suggestion: #{@form['suggestion']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['firstname']} #{@form['lastname']}\" <#{DEFAULT_SENDER}>")
  end

  def nge_errors(form_params)
    @form = form_params

    nge_page = @form['page']||@form['nge_page']

    add_attachments(form_params)

    subject = prepare_subject("NGE: Type: Tech Issue Article: #{nge_page} Error: #{@form['description']}")
    recipients = DEFAULT_RECIPIENTS

    if spam?(form_params)
      subject = "SPAM? #{subject}"
      recipients = SPAM_RECIPIENTS
    end

    mail(to: recipients,
         subject: subject,
         from: "\"#{@form['firstname']} #{@form['lastname']}\" <#{DEFAULT_SENDER}>")
  end

  def spam?(form_params)
    # e.g., GALILEO Comment: =================== ...
    if comment_equal_signs?(form_params)
      true
    # e.g., GALILEO Comment: >>> ...
    elsif comment_greaterthan_signs?(form_params)
      true
    # e.g., Contact: Ronaldvah RonaldvahVC
    elsif name_repeats?(form_params)
      true
    # e.g., Contact: VladimirVlasofs VladimirVlasofs
    elsif name_repeats_exactly?(form_params)
      true
    # e.g., Contact: KimKep <kimerax83@gmail.com>
    elsif no_lastname?(form_params)
      true
    # e.g., Contact: Eric Jones <eric.jones.z.mail@gmail.com>
    elsif eric_jones?(form_params)
      true
    # e.g., GALILEO Comment: Your website is violating the copyright protected images ...
    elsif violate_copyright?(form_params)
      true
    # e.g., Contact: Bogdanotj BogdanomxWN
    elsif name_repeats_first_few?(form_params)
      true
    # e.g., Contact: modMtty ModMtTY
    elsif name_ends_uppercase?(form_params)
      true
    # e.g., Contact: 샌즈카지노 CowL21Stypas
    elsif names_have_many_uppercase?(form_params)
      true
    # e.g., Contact: Lily Allen Lily Allen
    elsif name_repeats_multiple?(form_params)
      true
    # e.g., select terms
    elsif porn?(form_params)
      true
    else
      false
    end
  end

  # truncate and remove newlines
  def prepare_subject(subject)
    subject[0..160].gsub(/[\r\n]+/, ' ')
  end

  private

  def add_attachments(form_params)
    prepare_attachments(form_params).each do |attach|
      next if attach == ""
      file_path = File.absolute_path(attach.tempfile)
      attachments[attach.original_filename] = File.read(file_path)
    end
  end

  def prepare_attachments(form_params)
    attachments = []

    attachments << form_params[:attachment1] if form_params[:attachment1]
    attachments << form_params[:attachment2] if form_params[:attachment2]
    attachments << form_params[:attachment3] if form_params[:attachment3]

    attachments
  end

  def comment_equal_signs?(form_params)
    if form_params["description"] =~ /={5}/
      true
    else
      false
    end
  end

  def comment_greaterthan_signs?(form_params)
    if form_params["description"] =~ /^>>>/
      true
    else
      false
    end
  end

  def name_repeats?(form_params)
    # this should handle the case where 'firstname' contains both names
    if "#{form_params['firstname']} #{form_params['lastname']}" =~
        /^
        (\S+)     # first name (any non-space char, captured as \1)
        \s+       # space
        \1[A-Z]+  # last name same as first name plus some uppercase letters
        $/x
      true
    else
      false
    end
  end

  def name_repeats_exactly?(form_params)
    # this should also handle the case where 'firstname' contains both names
    if "#{form_params['firstname']} #{form_params['lastname']}" =~
        /^
        (\S+)     # first name (any non-space char, captured as \1)
        \s+       # space
        \b\1\b    # last name same as first name
        $/x
      true
    else
      false
    end
  end

  def no_lastname?(form_params)
    # galileo support form only asks for 'name', this checks if there are two names there
    if form_params['name'].present? && form_params['name'] !~ /\s+/
      true
    else
      false
    end
  end

  def eric_jones?(form_params)
    # galileo form asks for 'name', gkr form asks for 'firstname' 'lastname'
    if (
        form_params['name']                                      == "Eric Jones" ||
        "#{form_params['firstname']} #{form_params['lastname']}" == "Eric Jones"
       ) && (
        form_params['email'] == "eric.jones.z.mail@gmail.com" ||
        form_params['email'] == "ericjonesmyemail@gmail.com"
       )
      true
    else
      false
    end
  end

  def violate_copyright?(form_params)
     if form_params["description"] =~ /violating the copyright/   ||
        form_params["description"] =~ /infringing on a copyright/ ||
        form_params["description"] =~ /sued by the copyright/
      true
    else
      false
    end
  end

  def name_repeats_first_few?(form_params)
    # Marinayzc MarinahxzVZ
    # this should handle the case where 'firstname' contains both names
    if "#{form_params['firstname']} #{form_params['lastname']}" =~
        /^
        (\S{5})      # first 5 chars of first name (captured as \1)
        .*?          # remainder of name
        \s+          # space
        \1\S*?[A-Z]+ # beginning of last name is first 5 and then ends with uppercase letters (usually 2)
        $/x
      true
    else
      false
    end
  end

  def name_repeats_multiple?(form_params)
    name = if form_params['firstname'].present?
             "#{form_params['firstname']} #{form_params['lastname']}"
           elsif form_params['name'].present?
             form_params['name']
           else
             return false
           end
    # e.g., Lily Allen Lily Allen
    name_words = name.split(" ")
    count = name_words.size

    return false if count.odd?

    first_half = "#{name_words[0 .. count/2-1].join(" ")}"  # Lily Allen
    last_half  = "#{name_words[count/2 .. -1].join(" ")}"   # Lily Allen

    if first_half == last_half
      true
    else
      false
    end
  end

  def name_ends_uppercase?(form_params)
    # Contact: modMtty ModMtTY
    # this should handle the case where 'firstname' contains both names
    if "#{form_params['firstname']} #{form_params['lastname']}" =~
        /^
        \S+          # first name
        \s+          # space
        \S+[A-Z]{2,} # last name ends with uppercase letters (usually 2)
        $/x
      true
    else
      false
    end
  end

  def names_have_many_uppercase?(form_params)
    # ApkJoyNap Narkis
    # this should handle the case where 'firstname' contains both names
    if (form_params['firstname'] =~ /[A-Z]\S*[A-Z]\S*[A-Z]/) || (form_params['lastname'] =~ /[A-Z]\S*[A-Z]\S*[A-Z]/)
      true
    else
      false
    end
  end

  def porn?(form_params)
    name_words = "#{form_params['firstname']} #{form_params['lastname']}".split(" ")
    if %w[anal incest porn sex].intersection(name_words).size > 1
      true
    else
      false
    end
  end

end

