class BentoAppliedFilter < Struct.new(:key, :label, :values)
  def initialize(options = {})
    super(*options.values_at(*self.class.members))
    raise ArgumentError if key.nil?
    self.label = key if label.nil?
    self.values ||= []
  end
end
