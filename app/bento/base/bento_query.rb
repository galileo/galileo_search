class BentoQuery < Struct.new(:terms, :full_text, :page, :filters, :sort)
  def initialize(options = {})
    super(*options.values_at(*self.class.members))
    self.terms ||= ''
    self.full_text ||= false
    self.filters ||= {}
    self.page = page&.to_i
  end
end
