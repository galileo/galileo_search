class BentoExpandable < Struct.new(:label, :endpoint, :tooltip, :collapse_tooltip)

  def initialize(options = {})
    super(*options.values_at(*self.class.members))
  end

  def tooltip
    super || I18n.t('bento.bento_box.generic_expandable_expand_tooltip', label: label)
  end

  def collapse_tooltip
    super || I18n.t('bento.bento_box.generic_expandable_collapse_tooltip', label: label)
  end

end
