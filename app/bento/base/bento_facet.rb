class BentoFacet
  RESULTS_PER_SIDEBAR_FACET = 10
  RESULTS_PER_PAGE = 20

  def initialize(bento_response, raw_data)
    @response = bento_response
    @data = raw_data
  end

  attr_reader :response, :data

  def label
    'Filter'
  end

  def key
    nil
  end

  def raw_values
    []
  end

  def value_count
    raw_values&.size || 0
  end

  def wrap_value(raw_value)
    BentoFacetValue.new(raw_value)
  end

  def values(range = nil)
    items = range.present? ? (raw_values || [])[range] : (raw_values || [])
    items.map { |rv| wrap_value rv }
  end

  def sidebar_values
    values(0..(RESULTS_PER_SIDEBAR_FACET - 1))
  end

  def paginated_values(page = 1, per_page = RESULTS_PER_PAGE)
    first_item = (page - 1) * per_page
    last_item = first_item + per_page - 1
    values(first_item..last_item)
  end

  def for_json
    f = {
      key: key,
      label: label,
      type: 'facet',
      value_count: value_count,
      values: values
    }
    if response&.request&.respond_to? :query_facet
      f[:endpoint] = Rails.application.routes.url_helpers.bento_facet_api_path inst_code: response.bento.inst_code,
                                                                               bento_type: response.bento.slug,
                                                                               facet: key
    end
    f
  end

  def to_h
    {
      key: key,
      label: label,
      type: 'facet',
      values: values
    }
  end
end
