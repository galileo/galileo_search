# frozen_string_literal: true

# This is instantiated once per request to a bento API endpoint
class BentoRequest
  RESULTS_PER_BENTO = 5
  RESULTS_PER_PAGE = 20
  SEARCH_TERMS_PLACEHOLDER = '--.-BENTO-SEARCH-TERMS-.--'

  class BentoRequestError < StandardError
    attr_accessor :user_message, :debug_info
    def initialize(*args)
      super(*args)
      original_error = args[0]
      if original_error.present? && original_error.is_a?(Exception)
        @debug_info = {
          class: original_error.class,
          description: original_error.message
        }
      end
    end
  end
  class BadRequestError < BentoRequestError; end
  class PossibleIntermittentError < BentoRequestError; end

  def initialize(configured_bento, query)
    @configured_bento = configured_bento
    @query = query
  end

  attr_reader :configured_bento, :query
  delegate :page, :filters, to: :query

  def search_terms
    query.terms
  end

  def full_text?
    query.full_text
  end

  def do_query
    nil
  end

  def response_class
    BentoResponse
  end

  def supports_response_class?
    false
  end

  def respond
    if supports_response_class?
      response_class.new(self, past_max_page? ? nil : do_query)
    else
      legacy_respond
    end
  end

  def legacy_respond
    nil
  end

  def supports_refine_search?
    false
  end

  def self.supports_full_text_limiter?
    false
  end

  def refine_search_link
    nil
  end

  def is_more_results?
    !page.nil? && page > 0
  end

  def request_result_count
    is_more_results? ? RESULTS_PER_PAGE : RESULTS_PER_BENTO
  end

  def customization(key)
    configured_bento.customizations[key]
  end

  def institution
    configured_bento.institution
  end

  def bento_config
    configured_bento.bento_config
  end

  def no_results_message
    configured_bento.no_results_message
  end

  def self.sort_orders
    # First item will be considered default
    # However, if there's only one option, it's fine to leave this as an empty array.
    # In that case, request.sort_order will always be nil.
    []
  end

  def self.valid_sort_order?(sort_order)
    sort_order.nil? || sort_orders.include?(sort_order)
  end

  def sort_order
    raise BadRequestError unless self.class.valid_sort_order? query.sort
    query.sort || self.class.sort_orders.first
  end

  #region Mechanism for limiting how many pages of results one can see in-site before being forced to external site
  # This is currently just used by EDS and was created because requests gets slower as you get deeper into the results
  def max_allowed_page; end

  def take_external_label; end

  def past_max_page?
    max_allowed_page.present? && is_more_results? && page > max_allowed_page
  end
  #endregion
end
