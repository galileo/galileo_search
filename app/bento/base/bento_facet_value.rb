class BentoFacetValue < Struct.new(:value, :label, :count)
  def initialize(*args)
    super
    raise ArgumentError if value.nil?
    self.label = value if label.nil?
  end
end
