# frozen_string_literal: true

class GenericBento

  def initialize(request_class, inst, user_view, customizations={})
    if inst.is_a? SolrInstitution
      @institution = inst
      @inst_code = inst.code
    else
      @inst_code = inst
    end
    @user_view = user_view.to_s
    @request = request_class.new self, BentoQuery.new(terms: '',
                                                      full_text: false,
                                                      page: 1) # setting to page 1 so we emulate more results mode
    @response = @request.response_class.new @request, nil
    @customizations = customizations.merge({
                                             'Bibliographic Info Style' => 'Article' # most flexible default for EDS
                                           })
  end

  attr_reader :inst_code, :user_view, :customizations

  def institution
    @institution ||= InstitutionService.new.find inst_code
  end

  def method_missing(method, *args)
    if @response.respond_to? method
      @response.send method, *args
    end
  end

  def wrap_result(raw_item)
    result_record_class.new raw_item, self, 0
  end

end
