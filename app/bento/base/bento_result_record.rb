class BentoResultRecord

  def initialize(raw_record, bento_response, index)
    @record = raw_record
    @bento_response = bento_response
    @index = index
  end

  attr_reader :record, :bento_response, :index
  delegate :request, :bento, to: :bento_response

  #region Main things to overload
  def title; end
  def record_url; end
  def authorship_line; end
  def publication_info_line; end
  def description; end
  def additional_body; end
  def thumbnail_url; end
  def thumbnail_alt_text; end
  def thumbnail_link
    alt = thumbnail_alt_text
    img = helpers.possible_image(thumbnail_url, alt: alt, fallback_alt: alt_text_fallback)
    options = { target: '_blank' }
    options[:tabindex] = -1
    options[:'aria-hidden'] = 'true' if alt.blank?
    helpers.possible_link(img, record_url, **options)
  end

  def buttons
    []
  end
  def expandables
    []
  end
  #endregion

  #region Overload these for more advanced customizations

  def title_tooltip_guardrail
    400
  end

  def authorship_truncate_length
    expanded_view? ? 150 : 300
  end

  def publication_info_truncate_length
    300
  end

  def description_truncate_length
    expanded_view? ? 200 : 1800
  end

  def display_title
    safe_version (title.present? ? title : I18n.t('bento.error.no_title')), 160, 320
  end

  def tooltip_title
    safe_version title.to_str, 400
  end

  def display_authorship_line
    safe_version authorship_line, 150, 300
  end

  def display_publication_info_line
    # truncate length here is just a guardrail we're unlikely to get anywhere close to
    safe_version publication_info_line, 300
  end

  def display_description
    safe_version description, 200, 1800
  end

  def alt_text_fallback
    "Thumbnail for #{helpers.strip_tags(display_title&.to_str)}"
  end

  def title_heading
    helpers.possible_link(display_title, record_url, title: tooltip_title, target: '_blank')
  end

  #endregion

  #region General utilities

  def first_page?
    request.page == 1 || !request.is_more_results?
  end

  def first_result?
    first_page? && index.zero?
  end

  def result_number
    page_from_zero = [(request.page || 1) - 1, 0].max
    page_from_zero * BentoRequest::RESULTS_PER_PAGE + index + 1
  end

  def show_result_number?
    request.is_more_results? && index.present?
  end

  def expanded_view?
    request.is_more_results?
  end

  # Provide a concise way to use view helpers in subclasses
  def helpers
    ApplicationController.helpers
  end

  # @return [String]
  def safe_version(content, truncate_length = nil, expanded_length = nil)
    return nil if content.blank?
    html_safe = content.html_safe?
    truncate_length = expanded_length if expanded_view? && expanded_length
    content = content.truncate truncate_length if truncate_length
    content = if html_safe
                helpers.sanitize content
              else
                helpers.sanitize content, tags: []
              end
    content
  end

  #endregion

end
