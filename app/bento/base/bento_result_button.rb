class BentoResultButton < Struct.new(:label, :url, :fulltext_args, :image, :font_awesome_icon, :tooltip)

  def initialize(options = {})
    super(*options.values_at(*self.class.members))
  end

  def tooltip
    super || label || I18n.t('bento.bento_box.unknown_link')
  end

  def fulltext_link_for_inst(inst_code)
    if fulltext_args.present?
      Rails.application.routes.url_helpers.institution_bento_eds_fulltext_path(inst_code, fulltext_args)
    end
  end

  def link_for_inst(inst_code)
    url || fulltext_link_for_inst(inst_code)
  end

  def is_image?
    image.present?
  end
end
