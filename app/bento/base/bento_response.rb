# frozen_string_literal: true

class BentoResponse

  def result_record_class
    BentoResultRecord
  end

  def initialize(request, raw_response)
    @request = request
    @raw_response = raw_response
  end

  attr_reader :request, :raw_response
  delegate :page, :is_more_results?, :max_allowed_page, :take_external_label, :past_max_page?, to: :request

  def bento
    request.configured_bento
  end

  def applied_filters
    nil
  end

  def filters
    []
  end

  def has_filters?
    false
  end

  def result_count
    0
  end

  # Overwrite this if you need to show something other than a number for result count. E.g, 'unknown' or '10,000+'
  # You should still provide a numeric result_count to inform pagination
  def result_count_message; end

  # Overwrite this with the raw records that will be input to your BentoResultRecord subclass's constructor
  def raw_records
    []
  end

  def no_results_message
    request.no_results_message
  end

  def has_external_link?
    external_link.present?
  end

  def external_link
    nil
  end

  def button_style
    'inline' # the other possible value is 'column'
  end

  def wrap_result(raw_result, index = nil)
    throw Error if raw_result.nil?
    self.result_record_class.new raw_result, self, index
  end

  def records
    raw_records.map.with_index { |r, i| wrap_result r, i }
  end

  def has_more_results?
    result_count > BentoRequest::RESULTS_PER_BENTO
  end

  def show_more_results_link?
    has_more_results? && !is_more_results?
  end

  def search_state
    nil
  end
end