class BentoDateFilter
  attr_reader :response, :data, :key, :label, :min_date, :max_date, :preset_ranges

  def initialize(bento_response, key, label, min_date: nil, max_date: nil, preset_ranges: [])
    @response = bento_response
    @key = key
    @label = label
    @min_date = min_date
    @max_date = max_date
    @preset_ranges = preset_ranges
  end

  def to_h
    {
      key: key,
      label: label,
      type: 'date_range',
      min_date: min_date,
      max_date: max_date
    }
  end

  def for_json
    {
      key: key,
      label: label,
      type: 'date_range',
      min_date: min_date,
      max_date: max_date,
      preset_ranges: preset_ranges
    }
  end

  def self.format_limiter_value(value)
    return '' unless value
    if value =~ /^(\d+)y$/i
      if $1 == '1'
        return 'Past year'
      end
      return "Past #{$1} years"
    end
    if value.include? '...'
      return value.sub('...', ' – ')
    end
    ''
  end

end
