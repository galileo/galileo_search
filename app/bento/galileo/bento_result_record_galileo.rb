class BentoResultRecordGalileo < BentoResultRecord

  def title
    record.title
  end

  def title_heading
    copy_link_modal_button = helpers.link_to '<i class="fas fa-share-alt"></i>'.html_safe,
                                             "#copy-link-modal-#{ helpers.sanitize_resource_code(record.id) }",
                                             data: {
                                               toggle: 'modal'
                                             },
                                             class: 'ml-2',
                                             title: I18n.t('catalog.label.permalink_title')
    super + copy_link_modal_button
  end

  def record_url
    record.local_express_link
  end

  def thumbnail_url
    ImageService.new.url_for record.thumbnail_key
  end

  def thumbnail_link
    helpers.possible_link(helpers.possible_image(thumbnail_url,
                                                 alt: thumbnail_alt_text,
                                                 fallback_alt: alt_text_fallback,
                                                 class: 'database'),
                          record_url, target: '_blank')
  end

  def description
    record.short_descriptions&.join '\n'
  end

  def additional_body
    body = ''.html_safe
    if record.branding_image_key
      body += helpers.image_tag ImageService.new.url_for(record.branding_image_key),
                                class: 'mb-3 img-fluid branding',
                                alt: record.branding_note || 'Branding Image',
                                role: 'presentation'
    end
    if record.branding_note
      body += helpers.content_tag :p, record.branding_note, class: 'branding-text'
    end
    body + ApplicationController.render(partial: 'catalog/copy_link_modal', locals: { resource: record })
  end

  def buttons
    [
      BentoResultButton.new(label: I18n.t('bento.bento_box.db_more_info'),
                            url: Rails.application.routes.url_helpers.resource_path(record))
    ]
  end

end
