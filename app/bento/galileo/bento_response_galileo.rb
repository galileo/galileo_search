# frozen_string_literal: true

FACET_BLACKLIST = %w[for_institution_ss glri_b user_view_sms resource_name_first_char_ss]

class BentoResponseGalileo < BentoResponse

  def result_record_class
    BentoResultRecordGalileo
  end

  def result_count
    raw_response.dig 'response', 'numFound'
  end

  def raw_records
    raw_response.dig('response', 'docs').map { |d| Resource.new d }
  end

  def all_facets
    raw_response.dig('facet_counts', 'facet_fields')&.map { |f| BentoFacetGalileo.new self, f }
  end

  def filters
    exclude_facets = FACET_BLACKLIST + request.filters.keys
    all_facets.filter { |f| f.blacklight_facet.present? && !exclude_facets.include?(f.key) }
  end

  def applied_filters
    facet_lookup = all_facets.index_by &:key
    request.filters.to_h.map do |k, vals|
      BentoAppliedFilter.new key: k,
                             label: facet_lookup[k]&.label,
                             values: (vals.map { |v| BentoFacetValue.new v })
    end
  end

  def has_filters?
    filters.present?
  end

  def external_link
    request.refine_search_link
  end

end