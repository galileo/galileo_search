class BentoFacetGalileo < BentoFacet
  # `data` will be a pair with key and a list of values interleaved with counts
  # e.g., ['format_sms', ['Articles', 10, 'eBooks', 15, ...]]
  def key
    data[0]
  end

  def raw_values
    data[1].each_slice(2)
  end

  def self.blacklight_facets
    CatalogController.blacklight_config.facet_fields
  end

  def blacklight_facet
    self.class.blacklight_facets[key]
  end

  def label
    blacklight_facet.label
  end

  def wrap_value(raw)
    value, count = raw
    BentoFacetValue.new(value, value, count)
  end
end
