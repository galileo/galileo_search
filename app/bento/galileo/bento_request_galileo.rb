# frozen_string_literal: true

class BentoRequestGalileo < BentoRequest

  SORT_ORDERS = {
    'relevance' => 'score desc, resource_name_sort_s asc',
    'title' => 'resource_name_sort_s asc'
  }.freeze

  def supports_response_class?
    true
  end

  def response_class
    BentoResponseGalileo
  end

  def do_query
    blacklight_query = {
      q: search_terms,
      per_page: request_result_count,
      page: page || 1,
      f: filters.slice(*BentoFacetGalileo.blacklight_facets.keys).to_h,
      inst_code: configured_bento.inst_code,
      view: configured_bento.user_view,
      sort: SORT_ORDERS[sort_order]
    }
    solr_query = SearchBuilder.new(CatalogController).with(blacklight_query).processed_parameters
    solr_query.each do |key, val|
      if key.include? 'facet.limit'
        solr_query[key] = 1000  # It's possible to exceed this for the keywords facet.
                                # This is temporary until we work out getting the facet list asynchronously.
      end
    end
    result = Blacklight.default_index.connection.get 'select', params: solr_query
    result
  end

  def refine_search_link
    url_helpers = Rails.application.routes.url_helpers
    url_helpers.institution_databases_path(inst_code: configured_bento.inst_code,
                                           q: search_terms,
                                           view: configured_bento.user_view)
  end

  def supports_refine_search?
    true
  end

  def self.sort_orders
    SORT_ORDERS.keys
  end

end
