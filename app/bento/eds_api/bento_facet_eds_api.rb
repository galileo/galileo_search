class BentoFacetEdsApi < BentoFacet

  def self.configured_facets
    I18n.t!('services.eds_api.facets')
  rescue
    {}
  end

  def self.facet_keys
    configured_facets.keys.map &:to_s
  end

  def label
    # EDS-provided label at data['Label'], but we use our own labels
    self.class.configured_facets[key.to_sym]
  end

  def key
    data['Id']
  end

  def raw_values
    data['AvailableFacetValues']
  end

  def wrap_value(raw)
    BentoFacetValue.new(raw['Value'], raw['Value'], raw['Count'])
  end
end
