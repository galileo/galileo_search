class BentoResultRecordEncyclopedia < BentoResultRecordEdsApi

  BLURB_COMPATIBLE_DBS = ['Funk & Wagnalls New World Encyclopedia'].freeze

  # Hide author line and date
  def authorship_line
    nil
  end

  def display_date
    nil
  end

  # Where we'd normally put a journal title, put the db name (appropriate for F&W and Britannica)
  # ...or the collection name (appropriate for New Georgia Encyclopedia)
  def publication_title
    database_name = record.eds_database_name
    collections = request.collections_by_provider&.dig database_name
    return database_name if collections.nil? || collections.none?
    return collections.first if collections.size == 1

    collections.each do |collection_name|
      return collection_name if record&.eds_source_title&.include? collection_name
      return collection_name if record&.eds_extras_NoteTitleSource&.include? collection_name
    end

    database_name
  end

  def display_description
    show_blurb? ? nil : super
  end

  def additional_body
    return nil unless show_blurb?
    blurb
  end

  # Later: Extract bento_request.near_exact_match? into here
  def near_exact_match?
    request.near_exact_match?(record.title, request.search_terms)
  end

  def blurb_candidate?
    BLURB_COMPATIBLE_DBS.include?(record.eds_database_name) && near_exact_match?
  end

  def show_blurb?
    !expanded_view? && blurb_candidate? && first_result?
  end

  # Later: inline request.extract_blurb once it's no longer needed by pre-refactor views
  def blurb
    html_blurb = request.extract_blurb retrieve_sgml_fulltext
    return nil if html_blurb.blank?

    sanitized_blurb = helpers.sanitize(html_blurb, tags: %w[strong img])
    helpers.content_tag :div, helpers.raw(sanitized_blurb), class: 'encyclopedia-blurb'
  end

  # This is redundant now that all DLG catalog links are whitelisted.
  # Leaving in place as of 2022-11-03 until we verify we're keeping that policy.
  def show_eds_link?(link)
    super || link[:label] == 'Access URL'
  end

end
