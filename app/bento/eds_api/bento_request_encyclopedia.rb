# frozen_string_literal: true

# Take a regular EDS API request, change the partial, and inject the blurb
class BentoRequestEncyclopedia < BentoRequestEdsApi

  BLURB_COMPATIBLE_DBS = ['Funk & Wagnalls New World Encyclopedia'].freeze

  def legacy_respond
    response = super
    return response if response.is_a? BentoResponse
    # everything that follows is the pre-BentoResponse refactor version
    return response unless response[:result_count].present?

    response[:partial] = is_more_results? ? 'bento/more_results_encyclopedia' : 'bento/results_encyclopedia'
    response[:blurb] = nil
    unless is_more_results?
      records = response[:eds_results].records
      if records&.any? && blurb_candidate?(records[0])
        detail = eds_api.retrieve records[0].eds_database_id, records[0].eds_accession_number
        response[:blurb] = extract_blurb(detail.eds_html_fulltext)
      end
    end
    response
  end

  # @param [String] title
  # @param [String] query_terms
  # @return [Boolean]
  def near_exact_match?(title, query_terms)
    title = (title || '').downcase
    query_terms = (query_terms || '').downcase
    return true if title == query_terms || "#{title}s" == query_terms || "#{title}es" == query_terms
    return true if title =~ /^#{ Regexp.escape query_terms }(e?s)? \(.+\)$/
    return true if title.gsub(/\W+/, ' ').strip == query_terms.gsub(/\W+/, ' ').strip

    if title.include?(', ')
      title = title.split(', ').reverse.join(' ')
      return true if title == query_terms || "#{title}s" == query_terms || "#{title}es" == query_terms
    end
    false
  end

  def blurb_candidate?(record)
    BLURB_COMPATIBLE_DBS.include?(record.eds_database_name) && near_exact_match?(record.title, search_terms)
  end

  def extract_blurb(sgml_fulltext)
    return nil if sgml_fulltext.nil?

    begin
      doc = Nokogiri.HTML(Nokogiri::HTML.parse(sgml_fulltext).text)
      blurb = doc.css('p')[0]
      blurb.css('bold').each do |bold|
        strong = doc.create_element 'strong'
        strong.inner_html = bold.inner_html
        bold.replace strong
      end
      blurb.css('img').attr('alt') { |img| img.attr('title') || img.attr('alt') || '' }
      blurb.css('table, ephtml').remove
    rescue StandardError
      return nil
    end
    blurb.to_s
  end

end
