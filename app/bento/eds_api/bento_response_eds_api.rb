# frozen_string_literal: true

class BentoResponseEdsApi < BentoResponse

  PRESET_DATE_RANGES = [
    {
      label: 'Past year',
      value: '1y'
    },
    {
      label: 'Past 5 years',
      value: '5y'
    },
    {
      label: 'Past 10 years',
      value: '10y'
    }
  ]

  def result_record_class
    return BentoResultRecordEncyclopedia if bento.request_class == BentoRequestEncyclopedia
    BentoResultRecordEdsApi
  end

  def facets_in_bento_definition
    f = []
    f << 'ContentProvider' if request.customization('Content Provider Facet')&.any?
    f << 'SourceType' if request.customization('Source Type Facet')&.any?
    f << 'GZ' if request.collections_by_provider.any?
    f << 'FT' # we have a different interface for the full text limiter
    f
  end

  def mutually_excluded_limiters
    if (request.filters.keys & %w[FC RV]).any? || request.limit_to_custom_catalog?
      return %w[FC RV]
    end
    []
  end

  def facet_lookup
    raw_response&.results&.dig('SearchResult', 'AvailableFacets')&.index_by { |f| f['Id'] }
  end

  def limiter_lookup
    raw_response&.instance_variable_get(:@limiters)&.index_by { |f| f['Id'] }
  end

  def filter_lookup
    @filter_lookup ||= (facet_lookup || {}).merge(limiter_lookup || {})
  end

  def filters
    exclude_filters = request.filters.keys + facets_in_bento_definition + request.custom_hidden_eds_limiters +
                      mutually_excluded_limiters
    return [] if filter_lookup.nil?

    (BentoFacetEdsApi.facet_keys.map { |k| filter_lookup[k] }
                    .filter { |rf| rf && !exclude_filters.include?(rf['Id']) }
                    .map do |rf|
      if BentoRequestEdsApi::EDS_LIMITERS.include? rf['Id']
        # it's a limiter
        if BentoRequestEdsApi::DATE_RANGE_LIMITERS.include? rf['Id']
          # date limiter
          date_range = raw_response&.results&.dig('SearchResult', 'AvailableCriteria', 'DateRange')
          next unless date_range
          BentoDateFilter.new self, rf['Id'], get_label(rf['Id']),
                              min_date: parse_year(date_range['MinDate']), max_date: parse_year(date_range['MaxDate']),
                              preset_ranges: PRESET_DATE_RANGES
        elsif rf['Type'] == 'select'
          # Not sure if this will always hold, but the "select" limiters we know about are all booleans
          next if rf['Id'] == 'FC' && hide_library_catalog_limiter?
          BentoBooleanLimiterEdsApi.new self, rf
        else # Other types of limiters currently unsupported
          nil
        end
      else
        # it's a facet
        BentoFacetEdsApi.new self, rf
      end
    end).compact
  end

  def get_label(facet_id)
    BentoFacetEdsApi.configured_facets[facet_id.to_sym] || facet_id
  end

  def applied_filters
    applied_facets = (raw_response&.results&.dig('SearchRequest', 'SearchCriteria', 'FacetFilters')
                 &.map { |filter| filter['FacetValues'] || [] }
                 &.flatten
                 &.group_by { |f| f['Id']}
                 &.map { |id, fvs|
                   BentoAppliedFilter.new key: id,
                                          label: get_label(id),
                                          values: fvs.map {|fv| BentoFacetValue.new fv['Value'] }
                 }) || []
    applied_limiters = (raw_response&.results&.dig('SearchRequest', 'SearchCriteria', 'Limiters')&.map do |lim|
      value_label = ''
      if BentoRequestEdsApi::BOOLEAN_LIMITERS.include? lim['Id']
        value_label = 'Yes'
      elsif BentoRequestEdsApi::DATE_RANGE_LIMITERS.include? lim['Id']
        value = request.filters[lim['Id']]&.first
        value_label = BentoDateFilter.format_limiter_value value
      end
      BentoAppliedFilter.new key: lim['Id'],
                             label: get_label(lim['Id']),
                             values: [BentoFacetValue.new(value_label)]
    end) || []
    (applied_limiters + applied_facets).filter { |f| !facets_in_bento_definition.include? f.key }
  end

  def has_filters?
    filters.present?
  end

  def result_count
    raw_response.results.dig("SearchResult", "Statistics", "TotalHits") || 0
  end

  def raw_records
    raw_response.records
  end

  def has_external_link?
    request.supports_refine_search?
  end

  def external_link
    request.refine_search_link
  end

  def parse_year(date_string)
    return if date_string.blank?

    date_string.split('-').first
  end

  def search_state
    {
      'session_token' => request.session_token,
      'search_criteria' => raw_response.search_criteria
    }
  end

  def hide_library_catalog_limiter?
    request.custom_catalog_number.blank? ||
      /\bFC\b/i.match?(request.customization('Additional Limiters'))
  end

end