# frozen_string_literal: true

# Get content from the research starters section of the API response instead of the results section
class BentoRequestResearchStarters < BentoRequestEdsApi

  def response_class
    BentoResponseResearchStarters
  end

  def build_eds_query
    eds_query = super
    eds_query[:related_content] = (eds_query[:related_content] || []) + ['rs']
    eds_query.delete :actions # clear out any facet filters from the superclass method
    eds_query[:results_per_page] = 1 # we're not interested in the "results", but the API won't respond correctly to 0
    eds_query.delete 'page' # the API won't provide research starters for anything other than the first page
    eds_query
  end

  def result_range
    first_index = is_more_results? ? (RESULTS_PER_PAGE * (page - 1)) : 0
    last_index = first_index + request_result_count - 1
    first_index..last_index
  end

  def eds_results_to_hash_for_legacy_views(eds_results)
    results = eds_results.research_starters
    eds_results.records = results[result_range] || []
    for_view = super eds_results
    for_view[:result_count] = results.size
    for_view
  end

end
