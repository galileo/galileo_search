class BentoBooleanLimiterEdsApi < BentoFacetEdsApi

  def for_json
    {
      type: 'boolean',
      key: key,
      label: label,
      value: 'y',
      value_label: 'Yes'
    }
  end
end
