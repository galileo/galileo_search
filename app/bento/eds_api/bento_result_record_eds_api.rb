class BentoResultRecordEdsApi < BentoResultRecord
  delegate :eds_api, to: :request

  def title
    record.title
  end

  def record_url
    if FeatureFlags.enabled? :galileo_record_page
      galileo_record_link
    else
      record_express_link
    end
  end

  def galileo_record_link
    Rails.application.routes.url_helpers.eds_record_path(
      inst_code: bento.inst_code,
      an: record.eds_accession_number,
      db: record.eds_database_id
    )
  end

  def record_express_link
    Rails.application.routes.url_helpers.express_path(
      inst: bento.inst_code,
      an: record.eds_accession_number,
      db: record.eds_database_id,
      profile: native_eds_profile,
      view: bento.user_view
    )
  end

  def native_eds_profile
    profile = bento.bento_config&.credential 'Native EDS Profile'
    if profile.present?
      profile
    else
      DiscoverQueryService.eds_profile_for(bento.institution, bento.user_view)
    end
  end

  def authorship_line
    record.eds_authors.any? ? record.eds_authors.uniq.join('; ') : nil
  end

  def display_date
    helpers.best_available_publication_date(record)
  end

  def publication_title
    return nil unless request.parent_publication? && !helpers.publication_title_is_redundant?(record) &&
      record.eds_source_title.present?

    helpers.decode_html_symbols(record.eds_source_title)
  end

  def display_publication_title
    safe_version publication_title, 150
  end

  def publication_info_line
    [
      helpers.possible_content_tag('span', display_publication_title, class: 'publication-title'),
      helpers.possible_content_tag('span', display_date, class: 'publication-date')
    ].compact.join(' — ').html_safe
  end

  def description
    helpers.decode_html_symbols(record.eds_abstract)
  end

  def thumbnail_url
    record.eds_cover_thumb_url
  end

  def cover_image_url
    record.eds_cover_medium_url
  end

  def show_eds_link?(link)
    link[:type] != 'cataloglink' || helpers.whitelist_catalog_links?(record)
  end

  def button_from_eds_link(link)
    button = BentoResultButton.new
    if link[:url] == 'detail'
      button.fulltext_args = {
        dbid: record.eds_database_id,
        an: record.eds_accession_number,
        type: link[:type]
      }
    else
      button.url = helpers.decode_html_symbols(link[:url])
    end
    if link[:label].present?
      button.label = link[:label]
    else
      button.image = link[:icon]
    end
    if link[:icon] == 'PDF Full Text Icon'
      button.font_awesome_icon = 'file-pdf'
    end
    button
  end

  def buttons
    record.eds_all_links.filter { |link| show_eds_link? link }.map { |link| button_from_eds_link link }
  end

  # As of this comment, the below two methods are only used by the BentoResultRecordEncyclopedia subclass
  # But they are more generally applicable
  def retrieve_raw_full_version
    eds_api.retrieve record.eds_database_id, record.eds_accession_number
  end

  def retrieve_sgml_fulltext
    retrieve_raw_full_version.eds_html_fulltext
  end

  def database_name
    record.eds_database_name
  end

  def accession_number
    record.eds_accession_number
  end

  def document_type
    record.eds_document_type
  end

  def plink
    record.eds_plink
  end

  def toc
    return unless record.respond_to?(:eds_extras_TOC)
    value = record.send(:eds_extras_TOC)
    return if value.nil?

    if (matches = value.match(%r{^(?<label>.+):\s*(?<data>.*)}))
      return matches[:label], matches[:data]
    end
    return nil, value   
  end

  def fulltext_word_count
    count = parse_eds('eds_fulltext_word_count', {})
    return (count.to_i > 0) ? count : nil
  end

  # example field data (line breaks added for clarity):
  # <link>If you wish [...] Athens, GA 30602.</link>
  # <br />
  # <link linkTarget="URL" linkTerm="http://rightsstatements.org/vocab/InC/1.0/" linkWindow="_blank">
  # http://rightsstatements.org/vocab/InC/1.0/</link>

  def rights_statement
    return unless record.respond_to?(:eds_extras_Copyright)
    value = record.send(:eds_extras_Copyright)
    return if value.nil?

    text_regx = %r{<link>(?<text>.*?)</link>}
    link_regx = %r{<link .*?linkTerm="(?<url>.*?)"\s*linkWindow="(?<target>.*?)">(?<label>.*?)</link>}
    entries = []
    if (matches = value.match(text_regx))
      entries << helpers.decode_html_symbols(matches[:text])
    end
    if (matches = value.match(link_regx))
      entries << %Q{<a href="#{matches[:url]}" target="#{matches[:target]}">#{matches[:label]}</a>}
    end
    unless entries.present?
      entries << value
    end
    entries
  end

  # example field data: (/uga1/record/eds?an=159718015&db=s3h)
  # &lt;relatesTo&gt;1&lt;/relatesTo&gt; 5170University of Louisville, KY, USA; &lt;relatesTo&gt;2&lt;/relatesTo&gt; Laurentian University, Sudbury, ON, Canada; &lt;relatesTo&gt;3&lt;/relatesTo&gt; 8598University of Wisconsin–La Crosse, WI, USA
  def author_affiliations
    return unless record.respond_to?(:eds_author_affiliations)
    value = record.send(:eds_author_affiliations)
    return if value.nil?

    scan_regx = %r{
      &lt;relatesTo&gt;
      (\d+)               # [0], '1'
      &lt;/relatesTo&gt;
      (.*?)               # [1], 'Department of Politics and International Studies, University of Warwick, Coventry, UK'
      (:?;\ |$)
    }x
    matches = value.scan(scan_regx)
    entries = []
    matches&.each do |match|
      relation_number = match[0]
      relation_note   = helpers.decode_html_symbols(match[1])
      entries << "<sup>#{relation_number}</sup>#{relation_note}"
    end
    entries
  end

  # TODO (bmb 2022-11-01): maybe we just want to use eds_authors_composed all the time
  def authors
    entries = []
    if record.respond_to?(:eds_authors_composed)
      entries = authors_relatedto
      unless entries.present?
        entries = authors_editor
      end
    end
    unless entries.present?
      entries = parse_eds('eds_authors', {link: 'AR'})
    end
    entries
  end

  # example field data (line breaks added for clarity):
  # &lt;searchLink fieldCode=&quot;AR&quot; term=&quot;%22Boddula%2C+Rajender%22&quot;&gt;
  # Boddula, Rajender
  # &lt;/searchLink&gt;
  # , editor

  def authors_editor
    return unless record.respond_to?(:eds_authors_composed)
    value = record.send(:eds_authors_composed)
    return if value.nil?

    scan_regx = %r{
      &lt;searchLink\s*fieldCode=&quot;
      ([A-Z]+)            # [0], 'AR'
      &quot;.*?&gt;
      (.*?)               # [1], 'Boddula, Rajender'
      &lt;/searchLink&gt;
      (.*+)               # [2], ', editor'
      (:?&lt;br\ /&gt;|$)
    }x
    matches = value.scan(scan_regx)
    entries = []
    matches&.each do |match|
      link_index = match[0]
      author     = helpers.decode_html_symbols(match[1])
      note       = helpers.decode_html_symbols(match[2])
      return nil if note =~ /searchLink/  # short circuit if not just text [TODO (bmb 2022-11-01): kludge]
      link = helpers.link_to author,
               Rails.application.routes.url_helpers.institution_bento_search_path(
                 inst_code: bento.inst_code,
                 bento_search_query: %Q{#{link_index} "#{author}"})
      entries << "#{link}#{note}"
    end
    entries
  end

  # example field data (line breaks added for clarity):
  # &lt;searchLink fieldCode=&quot;AR&quot; term=&quot;%22L&#246;fflmann%2C+Georg%22&quot;&gt;L&#246;fflmann, Georg&lt;/searchLink&gt;
  # &lt;relatesTo&gt;1&lt;/relatesTo&gt;
  #  (AUTHOR)&lt;i&gt; g.lofflmann@warwick.ac.uk&lt;/i&gt;

  def authors_relatedto
    return unless record.respond_to?(:eds_authors_composed)
    value = record.send(:eds_authors_composed)
    return if value.nil?

    scan_regx = %r{
      &lt;searchLink\s*fieldCode=&quot;
      ([A-Z]+)            # [0], 'AR'
      &quot;.*?&gt;
      (.*?)               # [1], 'L&#246;fflmann, Georg'
      &lt;/searchLink&gt;
      &lt;relatesTo&gt;
      (\d+)               # [2], '1'
      &lt;/relatesTo&gt;
      (.*?)               # [3], '(AUTHOR)&lt;i&gt; g.lofflmann@warwick.ac.uk&lt;/i&gt;'
      (:?&lt;br\ /&gt;|$)
    }x
    matches = value.scan(scan_regx)
    entries = []
    matches&.each do |match|
      link_index      = match[0]  # can't use named matches with `.scan`
      author          = helpers.decode_html_symbols(match[1])
      relation_number = match[2]
      relation_note   = helpers.decode_html_symbols(match[3])
      link = helpers.link_to author,
               Rails.application.routes.url_helpers.institution_bento_search_path(
                 inst_code: bento.inst_code,
                 bento_search_query: %Q{#{link_index} "#{author}"})
      entries << "#{link}<sup>#{relation_number}</sup> #{relation_note}"
    end
    entries
  end

  # /uga1/record/eds?an=147010648&db=pwh
  # &lt;searchLink fieldCode=&quot;DE&quot; term=&quot;%22UNITED+States%22&quot;&gt;UNITED States&lt;/searchLink&gt;
  # &lt;searchLink fieldCode=&quot;AN&quot; term=&quot;%2216492896%22&quot;&gt;Report Available&lt;/searchLink&gt;
  def subjects_geographic
    return unless record.respond_to?(:eds_subjects_geographic)
    value = record.send(:eds_subjects_geographic)
    return if value.nil?

    scan_regx = %r{
      &lt;searchLink\s*fieldCode=&quot;
      ([A-Z]+)            # [0], 'DE' search index
      &quot;\ term=&quot;%22
      (.*?)               # [1], 'UNITED States' search terms
      %22&quot;\s*&gt;
      (.*?)               # [2], 'UNITED States' label
      &lt;/searchLink&gt;
    }x
    matches = value.scan(scan_regx)
    entries = []
    matches&.each do |match|
      link_index   = match[0]
      search_terms = helpers.decode_html_symbols(match[1]).gsub('+', ' ')
      label        = helpers.decode_html_symbols(match[2])
      link = helpers.link_to label,
                   Rails.application.routes.url_helpers.institution_bento_search_path(
                     inst_code: bento.inst_code,
                     bento_search_query: %Q{#{link_index} "#{search_terms}"})
      entries << link
    end
    entries
  end

  # example field data (line breaks added for clarity):
  # &lt;link linkTarget=&quot;help&quot; linkTerm=&quot;naics.htm&quot;&gt;NAICS/Industry Codes &lt;/link&gt;  (will ignore this line)
  # &lt;searchLink fieldCode=&quot;IC&quot; term=&quot;%22722512%22&quot;&gt;722512&lt;/searchLink&gt;  Limited-service eating places
  # &lt;br /&gt;
  # &lt;searchLink fieldCode=&quot;IC&quot; term=&quot;%22311812%22&quot;&gt;311812&lt;/searchLink&gt;  Commercial Bakeries
  # &lt;br /&gt;
  # &lt;searchLink fieldCode=&quot;IC&quot; term=&quot;%22311811%22&quot;&gt;311811&lt;/searchLink&gt;  Retail Bakeries
  # &lt;br /&gt;
  # &lt;searchLink fieldCode=&quot;IC&quot; term=&quot;%22722515%22&quot;&gt;722515&lt;/searchLink&gt;  Snack and Nonalcoholic Beverage Bars
  def naics
    return unless record.respond_to?(:eds_code_naics)
    value = record.send(:eds_code_naics)
    return if value.nil?

    # e.g., [['IC','722512','Limited-service eating places'], ['IC','311812','Commercial Bakeries'],...]
    scan_regx = %r{
      &lt;searchLink\s*fieldCode=&quot;
      ([A-Z]+)                             # [0], 'IC'
      &quot;\s*term=&quot;%22
      (\d+)                                # [1], '722512'
      %22&quot;.*?&lt;/searchLink&gt;\s*
      (.*?)                                # [2]  'Limited-service eating places'
      (:?&lt;br\ /&gt;|$)
    }x
    matches = value.scan(scan_regx)
    entries = []
    matches&.each do |match|
      link_index = match[0]
      naics_code = match[1]
      label = helpers.decode_html_symbols(match[2])
      link = helpers.link_to naics_code,
               Rails.application.routes.url_helpers.institution_bento_search_path(
                 inst_code: bento.inst_code,
                 bento_search_query: %Q{#{link_index} #{naics_code}})
      entries << "#{link} #{label}"
    end
    entries
  end

  def parse_eds(eds_field, field_specs)
    return unless record.respond_to?(eds_field)
    value = record.send(eds_field)
    return if value.nil?
    if field_specs.empty?
      if value.is_a?(Array)
        return value.map { |v| v = helpers.decode_html_symbols(v.to_s) }
      else
        return helpers.decode_html_symbols(value.to_s)
      end
    end

    split_regx = field_specs[:split]
    match_regx = field_specs[:match]  # should have one (capture) for '\1'
    link_index = field_specs[:link]

    entries = []
    if value.is_a?(Array)
      value.uniq!
      entries = value.map { |v| v = helpers.decode_html_symbols(v) }
    elsif split_regx.present?
      if match_regx.present?
        value.split(split_regx).each do |part|
          entries << helpers.decode_html_symbols(part.gsub(match_regx, '\1'))
        end
      else
        value.split(split_regx).each do |part|
          entries << helpers.decode_html_symbols(part)
        end
      end
    else
      entries << helpers.decode_html_symbols(value)
    end

    if entries.present? && link_index.present?
      entries.map! do |entry|
        entry = helpers.link_to entry,
                  Rails.application.routes.url_helpers.institution_bento_search_path(
                    inst_code: bento.inst_code,
                    bento_search_query: %Q{#{link_index} "#{entry}"})
      end
    end
    entries
  end

  def display_fields
    field_values = []
    display_field_specs.each do |spec|
      field_key  = spec[0]
      specs      = spec[1]
      our_meth   = specs[:meth]
      custom_key = specs[:custom_key]
      if our_meth.present?
        if custom_key
          key, field_value = send(our_meth)
          field_key = key if key.present?
        else
          field_value = send(our_meth)
        end
      else
        field_value = parse_eds(field_key, specs)
      end
      field_values << [field_key, field_value]
    end
    field_values
  end

  # entry[0]: eds field name/method, entry[1]: meth: 'alternate method', split:, match:, link:
  def display_field_specs
    [
      ['eds_authors',                     {meth: 'authors'}],
      # ['eds_authors_composed',            {meth: 'authors_composed'}],  see `authors` method above
      ['eds_source_title',                {link: 'TI'}],
      ['eds_publication_date',            {meth: 'display_date'}],
      ['eds_volume',                      {}],
      ['eds_issue',                       {}],
      ['eds_extras_TitleMeeting',         {}],
      ['eds_publisher',                   {}],
      ['eds_publication_info',            {}],
      ['eds_extras_EditionInfo',          {}],
      ['eds_publication_year',            {}],
      ['eds_subset',                      {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'GZ'}],
      ['eds_physical_description',        {}],
      ['eds_publication_type',            {}],
      ['eds_document_type',               {split: %r{<br ?/?>}}],
      ['eds_series',                      {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'SE'}],
      ['eds_subjects',                    {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'DE'}],
      ['eds_subjects_company',            {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'DE'}],
      ['eds_code_naics',                  {meth: 'naics'}],
      ['eds_subjects_genre',              {}],
      ['eds_subjects_geographic',         {meth: 'subjects_geographic'}],
      ['eds_subjects_mesh',               {}],
      ['eds_author_supplied_keywords',    {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'DE'}],
      ['eds_subjects_person',             {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'DE'}],
      ['eds_subjects_bisac',              {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'DE'}],
      ['eds_keywords',                    {split: %r{; }, match: %r{<searchLink .*?>(.*?)<\/searchLink>}, link:'ZW'}],
      ['eds_abstract',                    {}],
      ['eds_abstract_supplied_copyright', {}],
      ['eds_extras_AbstractNonEng',       {}],
      ['eds_extras_TOC',                  {}],
      ['eds_extras_Format',               {}],
      ['eds_languages',                   {}],
      ['eds_author_affiliations',         {meth: 'author_affiliations'}],
      ['eds_extras_RankLexile',           {}],
      ['eds_fulltext_word_count',         {meth: 'fulltext_word_count'}],
      ['eds_notes',                       {split: %r{&lt;br /&gt;}}],
      ['eds_other_titles',                {split: %r{&lt;br /&gt;}, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}}],
      ['eds_isbn_electronic',             {}],
      ['eds_isbn_print',                  {}],
      ['eds_isbns_related',               {}],
      ['eds_issn_print',                  {split: %r{&lt;br /&gt;}}],
      ['eds_extras_NumberControlLC',      {}],
      ['eds_extras_ISBN',                 {split: %r{<br />}}],
      ['eds_document_oclc',               {split: %r{&lt;br /&gt;}}],
      ['eds_document_doi',                {}],
      ['eds_extras_NoteTitleSource',      {}],
      ['eds_extras_Copyright',            {meth: 'rights_statement'}],
      ['eds_accession_number',            {}],
      ['eds_database_name',               {}],
      ['eds_access_level',                {}],
      ['eds_database_id',                 {}],
      ['eds_descriptors',                 {split: %r{, }, match: %r{&lt;searchLink .*?&gt;(.*?)&lt;/searchLink&gt;}, link: 'SU'}],
      ['eds_extras_PublisherInfo',        {split: %r{<br />}}],
      ['eds_publication_id',              {}],
      ['eds_publicate_scope_note',        {}],
      ['eds_publication_status',          {}],
    ]
  end
end
