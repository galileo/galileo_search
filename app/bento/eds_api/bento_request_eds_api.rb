# frozen_string_literal: true

# This is instantiated once per request to a bento API endpoint
class BentoRequestEdsApi < BentoRequest
  EDS_LIMITERS = %w(DT1 RV FC)
  DATE_RANGE_LIMITERS = %w(DT1)
  BOOLEAN_LIMITERS = %w(RV FC)

  attr_reader :session_token

  def supports_response_class?
    true
  end

  def response_class
    BentoResponseEdsApi
  end


  def set_search_state(state)
    @cached_search_state = state
  end

  def do_query
    eds_query = build_eds_query
    eds_response, @session_token = eds_api.search_and_get_token eds_query
    eds_response
  rescue EBSCO::EDS::Error, EBSCO::EDS::ApiError, Faraday::TimeoutError, EdsApi::EdsApiError => e
    raise self.class.wrap_eds_error(e)
  end


  def self.maybe_transient_network_error?(error)
    # At least one common network error ("Unable to establish a connection to any EDS host") falls into
    # the catchall EBSCO::EDS::ApiError, which doesn't inherit from EBSCO::EDS::Error, btw.
    # Not all ApiError's will be network errors, but the only one I know of that we regularly encounter is.
    error.is_a?(EBSCO::EDS::ApiError) ||
      error.is_a?(Faraday::TimeoutError) ||
      error.is_a?(EBSCO::EDS::TooManyRequests) ||
      error.is_a?(EBSCO::EDS::ServiceUnavailable) ||
      error.is_a?(EBSCO::EDS::ConnectionFailed)
  end

  def self.wrap_eds_error(eds_error)
    our_error_class = BentoRequestError
    fault = eds_error.respond_to?(:fault) ? eds_error.fault : nil
    error_number = fault&.dig :error_body, 'ErrorNumber'
    if maybe_transient_network_error? eds_error
      our_error_class = PossibleIntermittentError # HTTP 500 w/ try again & contact support links
    elsif eds_error.is_a?(EBSCO::EDS::BadRequest) && error_number == '106'
      our_error_class = PossibleIntermittentError # HTTP 500 w/ try again & contact support links
    elsif eds_error.is_a?(EBSCO::EDS::BadRequest) && error_number == '144'
      our_error_class = BentoRequestError # HTTP 500 w/ contact support link
    elsif eds_error.is_a? EBSCO::EDS::BadRequest
      our_error_class = BadRequestError  # HTTP 400
    end
    our_error = our_error_class.new eds_error
    our_error.debug_info = { class: eds_error.class.to_s,
                             description: fault&.dig(:error_body, 'ErrorDescription'),
                             detailed_description: fault&.dig(:error_body, 'DetailedErrorDescription'),
                             eds_error_number: error_number }
    if eds_error.is_a?(EBSCO::EDS::BadRequest) && (error_number == '143' || error_number == '126')
      # 143 is invalid syntax in search
      # 126 is invalid limiter value
      our_error.user_message = I18n.t('bento.error.invalid_syntax')
    else
      Sentry.capture_exception our_error
    end
    our_error
  end

  def legacy_respond
    results = do_query
    if results.nil?
      take_your_search_to_eds_view_hash
    else
      eds_results_to_hash_for_legacy_views results
    end
  end

  def supports_refine_search?
    customization('Equivalent Limiter Clause').present?
  end

  def self.supports_full_text_limiter?
    true
  end

  def refine_search_link
    limiters = customization('Equivalent Limiter Clause')
    profile = institution.profile_for configured_bento.user_view
    query_text = apply_extra_limiters(search_terms, limiters).strip

    options = {
      inst: institution.code,
      profile: profile,
      search_galileo_query: query_text
    }
    options[:full_text] = true if full_text?
    Rails.application.routes.url_helpers.express_path(options)
  end

  def parent_publication?
    bib_style = customization('Bibliographic Info Style')
    bib_style == 'Article' || (bib_style.empty? && customization('Has Parent Publication')) || false
  end

  def custom_catalog_number
    bento_config.credential('Custom Catalog Number')
  end

  def limit_to_custom_catalog?
    customization('Limit to Custom Catalog').present?
  end

  def custom_hidden_eds_limiters
    hidden = []
    hidden << 'FC' if customization('Hide Library Catalog Only') == 'true'
    hidden << 'RV' if customization('Hide Peer Reviewed Journals') == 'true'
    hidden
  end

  def custom_catalog_limiter
    return '' unless limit_to_custom_catalog?
    return '' unless custom_catalog_number.present?

    "AND LN #{custom_catalog_number}"
  end

  def apply_extra_limiters(query_text, extra_limiters)
    extra_limiters = "#{extra_limiters} #{custom_catalog_limiter}".strip
    query_text = (query_text || '').strip
    if query_text == ''
      # We're trying not to make it particularly easy to do a blank search, but if you hack your query string and do
      # one, it might as well return everything.
      query_text = 'FT Y OR FT N' # get everything
    end
    if extra_limiters.present?
      return extra_limiters unless query_text.present?
      extra_limiters = "(#{extra_limiters})" unless extra_limiters.match?(/^(AND|OR|NOT)\b/i)
      query_text = "(#{query_text}) #{extra_limiters}" if extra_limiters.present?
    end
    query_text
  end

  def eds_api
    EdsApi.new bento_config
  end

  # @return [Hash]
  def build_eds_query
    query_text = apply_extra_limiters search_terms, customization('Additional Limiters')

    actions = []
    actions << 'AddLimiter(FT:Y)' if full_text?

    customization('Source Type Facet')&.each do |source_type|
      actions << "addfacetfilter(sourcetype:#{escape_eds_action_arg source_type})"
    end

    content_providers = [].to_set
    collections = [].to_set
    @collections_by_provider = {}

    customization('Content Provider Facet')&.each do |provider_str|
      components = provider_str.split /\s*=>\s*/
      content_providers << components.first unless components.first.nil?
      if components.size > 1
        collections << components.join('-')
        @collections_by_provider[components.first] ||= []
        @collections_by_provider[components.first] << components.second
      end
    end

    content_providers.each do |content_provider|
      actions << "addfacetfilter(contentprovider:#{escape_eds_action_arg content_provider})"
    end

    collections.each do |collection|
      actions << "AddLimiter(GZ:#{escape_eds_action_arg collection})"
    end

    filters.each do |field, values|
      if EDS_LIMITERS.include? field
        value = if DATE_RANGE_LIMITERS.include? field
                  if(values.first =~ /^(\d+)y$/i)
                    (Date.today - $1.to_i.year).strftime("%Y-%m/")
                  elsif (values.first =~ /^(\d+)m$/i)
                      (Date.today - $1.to_i.months).strftime("%Y-%m/")
                  elsif (values.first =~ /^(\d+-\d{1,2})$/)
                    "#{$1}/"
                  else
                    parse_year_range values.first
                  end
                else
                  values.first
                end
        actions << "addlimiter(#{field}:#{escape_eds_action_arg value})"
      else
        values.each do |value|
          actions << "addfacetfilter(#{field}:#{escape_eds_action_arg value})"
        end
      end
    end

    eds_query = {
      query: query_text,
      mode: 'all',
      results_per_page: request_result_count,
      highlight: false,
      view: 'detailed',
      include_facets: true
    }
    eds_query[:sort] = sort_order if sort_order
    eds_query[:actions] = actions if actions.any?
    eds_query['page'] = page.to_i unless page.nil?
    if @cached_search_state
      eds_query[:cached_session_token] = @cached_search_state['session_token']
      eds_query[:cached_search_criteria] = @cached_search_state['search_criteria']
    end
    eds_query
  end

  def collections_by_provider
    @collections_by_provider || {}
  end

  # This will add a preceding backlash to the following characters: : ( ) \ ,
  def escape_eds_action_arg(arg)
    arg.gsub(/[:()\\,]/) { |m| "\\#{m}" }.strip
  end

  def eds_results_to_hash_for_legacy_views(eds_results)
    { title: configured_bento.display_name,
      page: page,
      is_more_results: is_more_results?,
      eds_results: eds_results,
      result_count: eds_results.results.dig("SearchResult", "Statistics", "TotalHits") || 0,
      collections_by_provider: @collections_by_provider,
      more_results_link: supports_refine_search? ? refine_search_link : nil,
      has_parent_publication: parent_publication?,
      display_options: {},
      no_results_message: no_results_message,
      partial: is_more_results? ? 'bento/more_results_eds' : 'bento/results_eds' }
  end

  def take_your_search_to_eds_view_hash
    {
      more_results_link: refine_search_link,
      partial: 'bento/take_search_to_eds'
    }
  end

  def take_external_label
    I18n.t('bento.bento_box.take_search_to_eds')
  end

  def parse_year_range(range_string)
    #todo: More validations
    years = range_string.split('...')
    start_year = years.first.present? ? "#{years.first}-01" : ""
    end_year = years.second.present? ? "#{years.second}-12" : ""
    "#{start_year}/#{end_year}"
  end

  def self.sort_orders
    %w[relevance newest oldest]
  end
end
