# frozen_string_literal: true

class BentoResponseResearchStarters < BentoResponseEdsApi

  def filters
    nil
  end

  def result_range
    first_index = is_more_results? ? (request.request_result_count * (page - 1)) : 0
    last_index = first_index + request.request_result_count - 1
    first_index..last_index
  end

  def all_raw_records
    raw_response.research_starters || []
  end

  def result_count
    all_raw_records.size
  end

  def raw_records
    all_raw_records[result_range]
  end

end
