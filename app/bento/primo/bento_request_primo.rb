# frozen_string_literal: true

# This is instantiated once per request to a bento API endpoint
class BentoRequestPrimo < BentoRequest

  SORT_ORDERS = {
    'relevance' => 'rank',
    'title' => 'title'
  }.freeze

  def response_class
    BentoResponsePrimo
  end

  def supports_response_class?
    true
  end

  def do_query
    primo_api = PrimoApiService.new bento_config
    offset = is_more_results? ? (RESULTS_PER_PAGE * (page - 1)) : 0
    sort = SORT_ORDERS[sort_order]
    primo_api.search search_terms, offset, request_result_count, filters.transform_values(&:first), sort
  end

  def legacy_respond
    results = do_query
    results_to_hash_for_legacy_views results
  end

  def supports_refine_search?
    false
  end

  def refine_search_link
    nil
  end

  def results_to_hash_for_legacy_views(results)
    { title: configured_bento.display_name,
      page: page,
      is_more_results: is_more_results?,
      results: results[:records],
      result_count: results[:count],
      more_results_link: refine_search_link,
      display_options: {},
      no_results_message: no_results_message,
      partial: 'bento/results_journals_primo'
    }
  end

  def self.sort_orders
    SORT_ORDERS.keys
  end

end
