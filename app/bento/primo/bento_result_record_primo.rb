class BentoResultRecordPrimo < BentoResultRecord

  def title
    record.title
  end

  def record_url
    record.record_link
  end

  def authorship_line
    record.publisher
  end

  def expandables
    if record.available_electronically?
      [
        BentoExpandable.new(label: I18n.t('bento.label.journals.expand_links_label'),
                            endpoint: record.expand_links_path,
                            tooltip: I18n.t('bento.label.journals.expand_links_tooltip'),
                            collapse_tooltip: I18n.t('bento.label.journals.collapse_links_tooltip'))
      ]
    else
      []
    end
  end

end
