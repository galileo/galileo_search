# frozen_string_literal: true

class BentoResponsePrimo < BentoResponse

  def result_record_class
    BentoResultRecordPrimo
  end

  def filters
    # Facets will appear based on their order in the translation file
    raw_facets = (raw_response[:facets] || []).index_by { |f| f['name'] }
    facet_keys = BentoFacetPrimo.facet_keys & raw_facets.keys - request.filters.keys
    facet_keys.map { |k| BentoFacetPrimo.new self, raw_facets[k] }
  end

  def applied_filters
    request.filters.to_h.map do |k, vals|
      BentoFacetPrimo.new self, {
        'name' => k,
        'values' => vals.map { |v| { 'value' => v } }
      }
    end
  end

  def has_filters?
    filters.present? && filters.any?
  end

  def result_count
    raw_response[:count]
  end

  def raw_records
    raw_response[:records]
  end


end
