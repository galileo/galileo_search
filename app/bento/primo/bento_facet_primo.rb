class BentoFacetPrimo < BentoFacet

  def self.configured_facets
    I18n.t!('services.primo.facets')
  rescue
    {}
  end

  def self.facet_keys
    configured_facets.keys.map &:to_s
  end

  def facet_config
    self.class.configured_facets[data['name'].to_sym]
  end

  def label
    facet_config.is_a?(String) ? facet_config : facet_config[:name]
  end

  def value_translations
    @value_translations ||= (facet_config.is_a?(Hash) && facet_config[:values]) || {}
  end

  def key
    data['name']
  end

  def raw_values
    data['values']&.sort_by { |raw| -raw['count'].to_i }
  end

  def wrap_value(raw)
    value = raw['value']
    label = value_translations[value.to_sym] || value
    parsed_count = raw['count'].to_i
    count = parsed_count == 0 ? raw['count'] : parsed_count # fallback on the string/nil version if not numeric
    BentoFacetValue.new(value, label, count)
  end

end
