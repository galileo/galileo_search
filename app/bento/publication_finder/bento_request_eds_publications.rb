# frozen_string_literal: true

# This is instantiated once per request to a bento API endpoint
class BentoRequestEdsPublications < BentoRequest
  attr_accessor :search_field

  NON_FACETS = ['ispeerreviewed']
  SORT_ORDERS = {
    'relevance' => 'relevance',
    'title' => 'titlename'
  }.freeze

  def max_allowed_page
    499
  end

  def supports_response_class?
    true
  end

  def response_class
    BentoResponsePublicationFinder
  end

  def do_query
    request = HTTParty.get search_url
    throw PossibleIntermittentError unless request.response.code == '200'
    request.parsed_response
  end

  def legacy_respond
    pub_finder = PublicationFinderService.new endpoint_url
    results = pub_finder.search search_terms, (page || 1), request_result_count
    pub_finder_results_to_hash_for_legacy_views results
  end

  def supports_refine_search?
    true
  end

  def refine_search_link
    url = URI.parse 'https://publications.ebsco.com'
    parameters = {
      search: search_terms,
      custId: institution.eds_customer_id,
      groupId: 'main',
      profileId: profile_name,
      resourcetype: 'Journal',
      searchField: search_field || 'titlename',
      searchtype: 'contains'
    }
    url.query = URI.encode_www_form parameters
    url.to_s
  end

  def profile_name
    # todo: the fallback on 'pfi' can be removed after the first successful prod deploy
    customization('Profile Name') || 'pfi'
  end

  def endpoint_url
    "https://api.ebsco.io/pf/pfaccount/#{institution.eds_customer_id}.main.#{profile_name}/publications"
  end

  def search_url()
    url = URI.parse endpoint_url
    parameters = {
      search: search_terms,
      offset: page || 1,
      count: request_result_count,
      orderby: SORT_ORDERS[sort_order],
      searchfield: search_field || 'titlename',
      includefacets: true,
      searchtype: 'contains',
      resourcetype: 'Journal'
    }
    filters.keys.each do |field|
      value = filters[field]
      next unless value.present?
      next if /\W/.match? field  # Eventually we may check the field against a whitelist,
                                 # but for now anything without special characters should be fine
      field = field.downcase
      param_name = NON_FACETS.include?(field) ? field : "#{field}Facet"
      parameters[param_name] = value
    end
    url.query = URI.encode_www_form parameters
    url
  end

  def url_for_journal(record)
    ids = record['identifiersList']
    issn = ids.find { |id| id['source'] == 'Print ISSN' }
    issn = ids.find { |id| id['source'] == 'Online ISSN' } if issn.nil?
    issn.nil? ? nil : "https://publications.ebsco.com/?custId=#{institution.eds_customer_id}&groupId=main&profileId=#{profile_name}&resourcetype=&search=#{issn['id']}&searchField=isxn"
  end

  # @param [Hash] publication_record: Publications IQ record to edit in-place
  def self.sort_customer_resources!(publication_record)
    sorted = publication_record['customerResourcesList'].sort_by do |publication_record|
      [
        -get_resource_end_date(publication_record).to_time.to_i, # Equivalent to sorting by date, DESCENDING
        get_resource_start_date(publication_record),
        publication_record['packageName']&.downcase
      ]
    end
    publication_record['customerResourcesList'] = sorted
  end

  # @param [String] date_string: date to parse
  # @param [Date] fallback: fallback if date_string is un-parseable or nil
  # @return [Date]
  def self.parse_date(date_string, fallback)
    return fallback if date_string.nil?

    begin
      Date.strptime(date_string)
    rescue Date::Error
      fallback
    end
  end

  # @param [Hash] resource: item from Publications IQ "customerResourceList"
  # @return [Date]
  def self.get_resource_start_date(resource)
    fallback = Date.today
    dates = resource['managedCoverageList'].map do |coverage|
      date_string = coverage['beginCoverage']
      parse_date(date_string, fallback)
    end
    dates.min || fallback
  end

  # @param [Hash] resource: item from Publications IQ "customerResourceList"
  # @return [Date]
  def self.get_resource_end_date(resource)
    fallback = Date.new 0
    dates = resource['managedCoverageList'].map do |coverage|
      date_string = coverage['endCoverage']
      parse_date(date_string, fallback)
    end
    dates.max || fallback
  end

  def pub_finder_results_to_hash_for_legacy_views(results)
    records = results[:records]
    records.each do |publication_record|
      self.class.sort_customer_resources! publication_record
      publication_record['issnLink'] = url_for_journal(publication_record)
    end
    { title: configured_bento.display_name,
      page: page,
      is_more_results: is_more_results?,
      results: records,
      result_count: results[:count],
      more_results_link: refine_search_link,
      display_options: {},
      no_results_message: no_results_message,
      partial: 'bento/results_journals'
    }
  end

  def self.sort_orders
    SORT_ORDERS.keys
  end

end
