class BentoResultRecordPublicationFinder < BentoResultRecord

  def title
    record['titleName']
  end

  def isxn
    ids = record['identifiersList']
    isxn = ids.find { |id| id['source'] == 'Print ISSN' || id['source'] == 'Online ISSN' }
    isxn&.dig('id')
  end

  def record_url
    if FeatureFlags.enabled? :galileo_record_page
      galileo_record_link
    else
      ebsco_publications_link
    end
  end

  def galileo_record_link
    isxn_id = isxn
    return nil if isxn_id.nil?
    Rails.application.routes.url_helpers.journals_ebsco_record_path(inst_code: bento.inst_code, isxn: isxn_id)
  end

  def isxn_search_link
    ids = record['identifiersList']
    issn = ids.find { |id| id['source'] == 'Print ISSN' }
    issn = ids.find { |id| id['source'] == 'Online ISSN' } if issn.nil?
    return nil if issn.nil?
    url = URI.parse 'https://publications.ebsco.com'
    url.query = URI.encode_www_form({
      custId: request.institution.eds_customer_id,
      groupId: 'main',
      profileId: request.profile_name,
      search: issn['id'],
      searchField: 'isxn'
    })
    url.to_s
  end

  def pubfinder_title_id_link
    title_id = record['titleId']
    return nil if title_id.blank?
    url = URI.parse 'https://publications.ebsco.com'
    url.path = "/details/#{title_id}"
    url.query = URI.encode_www_form({
      custId: request.institution.eds_customer_id,
      groupId: 'main',
      profileId: request.profile_name
    })
    url.to_s
  end

  def ebsco_publications_link
    if FeatureFlags.enabled? :pubfinder_title_id_links
      pubfinder_title_id_link
    else
      isxn_search_link
    end
  end

  def authorship_line
    publisher = record['publisherName']
    return nil if publisher == 'Unspecified'
    publisher
  end

  def self.button_for_resource(resource)
    BentoResultButton.new label: resource['packageName'], url: resource['linkOutTargetUrl']
  end

  def buttons
    record['customerResourcesList'].sort_by { |resource|
      [-self.class.get_resource_end_date(resource).to_time.to_i, # Equivalent to sorting by date, DESCENDING
      self.class.get_resource_start_date(resource),
      resource['packageName']&.downcase]
    }.map { |resource| self.class.button_for_resource resource }
  end

  def subjects
    record['subjectsList'].map{|subject|[subject['type'], subject['name']]}
  end
  def identifiers
    record['identifiersList'].map{|identifier| [identifier['source'], identifier['id']]}
  end

  # region Sorting buttons by date

  # @param [String] date_string: date to parse
  # @param [Date] fallback: fallback if date_string is un-parseable or nil
  # @return [Date]
  def self.parse_date(date_string, fallback)
    return fallback if date_string.nil?

    begin
      Date.strptime(date_string)
    rescue Date::Error
      fallback
    end
  end

  # @param [Hash] resource: item from Publications IQ "customerResourceList"
  # @return [Date]
  def self.get_resource_start_date(resource)
    fallback = Date.today
    dates = resource['managedCoverageList'].map do |coverage|
      date_string = coverage['beginCoverage']
      parse_date(date_string, fallback)
    end
    dates.min || fallback
  end

  # @param [Hash] resource: item from Publications IQ "customerResourceList"
  # @return [Date]
  def self.get_resource_end_date(resource)
    fallback = Date.new 0
    dates = resource['managedCoverageList'].map do |coverage|
      date_string = coverage['endCoverage']
      parse_date(date_string, fallback)
    end
    dates.max || fallback
  end

  # endregion

end
