class BentoFacetPublicationFinder < BentoFacet

  HARDCODED_LABELS = {
    'true' => 'Yes',
    'false' => 'No'
  }

  def label
    key&.underscore&.titleize
  end

  def key
    # It gives us plural names like 'Subjects' but to apply filters, we need to say 'SubjectFacet=x' (case insensitive)
    data[:key].sub /s$/, ''
  end

  def raw_values
    data[:values]&.sort_by { |raw| -raw['count'].to_i }
  end

  def label_key
    @label_key ||= raw_values.first&.keys&.without('count', 'id', 'parentId')&.first
  end

  def wrap_value(raw)
    label = raw[label_key]
    id = raw['id'] || label
    # deal with boolean values
    if id == true
      label = 'Yes'
    elsif id == false
      label = 'No'
    end
    BentoFacetValue.new(id, label, raw['count'])
  end
end
