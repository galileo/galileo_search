# frozen_string_literal: true

class BentoResponsePublicationFinder < BentoResponse

  EXCLUDE_FACETS = %w[AlphaMenus ResourceTypes]

  def result_record_class
    BentoResultRecordPublicationFinder
  end

  def all_facets
    @all_facets ||= raw_response['Facets']&.map do |k, v|
      BentoFacetPublicationFinder.new self, { key: k, values: v }
    end
  end

  def filters
    all_facets&.filter { |f| excluded_facets.exclude? f.data[:key].downcase }
  end

  def applied_filters
    facet_lookup = all_facets.index_by &:key
    request.filters.to_h.map do |k, vals|
      facet = facet_lookup[k]
      value_lookup = facet.values.index_by { |v| v.value.to_s }
      values = vals.map do |v|
        label = value_lookup[v]&.label || BentoFacetPublicationFinder::HARDCODED_LABELS[v] || ''
        BentoFacetValue.new v, label
      end
      BentoAppliedFilter.new key: k, label: facet.label, values: values
    end
  end

  def excluded_facets
    @excluded_facets ||= (EXCLUDE_FACETS + request.filters.keys + request.filters.keys.map {|f| "#{f}s"}).map &:downcase
  end

  def has_filters?
    filters.present? && filters.any?
  end

  def result_count
    raw_response.dig('SearchResult', 'Statistics', 'TotalHits') || 0
  end

  def result_count_message
    result_count == 10_000 ? '10,000+' : super
  end

  def raw_records
    raw_response.dig('SearchResult', 'Data', 'Records')
  end

  def button_style
    'column'
  end

end
