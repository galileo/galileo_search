class BentoFacetDlg < BentoFacet

  SORT_ORDERS = {
    'count' => 'count',
    'alphabetic' => 'index'
  }.freeze

  def self.configured_facets
    I18n.t!('services.dlg.facets')
  rescue
    {}
  end

  def self.facet_keys
    configured_facets.keys.map &:to_s
  end

  def self.get_locale_label(facet_key, value)
    value_translations = configured_facets.dig facet_key.to_sym
    value_translations = {} unless value_translations.is_a? Hash
    value_translations.dig(:values, value&.to_sym) || value
  end

  def facet_config
    self.class.configured_facets[key.to_sym]
  end

  def label
    facet_config.is_a?(String) ? facet_config : facet_config[:name]
  end

  def value_translations
    @value_translations ||= (facet_config.is_a?(Hash) && facet_config[:values]) || {}
  end

  def key
    data['name']
  end

  def raw_values
    data['items']
  end

  def wrap_value(raw)
    label = value_translations[raw['value']&.to_sym] || raw['label']
    BentoFacetValue.new(raw['value'], label, raw['hits'])
  end
end
