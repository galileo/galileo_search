# frozen_string_literal: true

class BentoFacetCrdl < BentoFacetDlg

  def self.configured_facets
    I18n.t!('services.crdl.facets')
  rescue
    {}
  end

end
