# frozen_string_literal: true

class BentoResponseDlg < BentoResponse

  def result_record_class
    BentoResultRecordDlg
  end

  def result_count
    raw_response.dig 'pages', 'total_count'
  end

  def raw_records
    raw_response['docs']
  end

  def all_facets
    @all_facets ||= raw_response['facets']&.compact&.map { |f| request.facet_class.new self, f }.index_by &:key
  end

  def filters
    @facets ||= request.facet_class.facet_keys
                  .map{ |k|
                    f = all_facets[k]
                    next unless f
                    if request.class::DATE_RANGE_LIMITERS.include? k
                      next unless raw_records&.any?
                      BentoDateFilter.new self, k, f.label, min_date: f.data&.dig('min'),
                                          max_date: f.data&.dig('max'), preset_ranges: request.class.preset_date_ranges
                    else
                      f.raw_values&.any? ? f : nil
                    end
                  }
                  .compact
                  .filter{|f| !request.filters[f.key].present?}
  end

  def applied_filters
    facet_lookup = all_facets
    request.filters.to_h.map do |k, vals|
      facet = facet_lookup[k]
      values = if request.class::DATE_RANGE_LIMITERS.include? k
                 value_label = BentoDateFilter.format_limiter_value vals.first
                 [BentoFacetValue.new(vals.first, value_label)]
               else
                 vals.map { |v| BentoFacetValue.new v, request.facet_class.get_locale_label(facet.key, v) }
               end

      BentoAppliedFilter.new key: facet.key,
                             label: facet.label,
                             values: values
    end
  end

  def has_filters?
    filters.present?
  end

end