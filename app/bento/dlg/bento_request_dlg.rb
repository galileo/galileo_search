# frozen_string_literal: true

class BentoRequestDlg < BentoRequest
  DATE_RANGE_LIMITERS = %w(year_facet)

  def self.base_url
    'https://dlg.usg.edu'
  end

  SORT_ORDERS = {
    'relevance' => 'score desc, yyyy_mm_dd_sort desc',
    'title' => 'title_sort asc',
    'newest' => 'yyyy_mm_dd_sort desc, title_sort asc',
    'oldest' => 'yyyy_mm_dd_sort asc, title_sort asc'
  }.freeze

  def self.preset_date_ranges
    []
  end

  def supports_response_class?
    true
  end

  def response_class
    BentoResponseDlg
  end

  def facet_class
    BentoFacetDlg
  end

  def query_facet(facet_name, page, sort_order)
    throw BadRequestError unless facet_class.facet_keys.include? facet_name
    bl_sort_order = facet_class::SORT_ORDERS[sort_order]
    throw BadRequestError unless bl_sort_order

    blacklight_query = build_query
    blacklight_query['facet.page'] = page
    blacklight_query['facet.sort'] = bl_sort_order
    response = call_blacklight_api "/records/facet/#{facet_name}.json", blacklight_query
    throw StandardError unless response
    facet_class.new nil, { 'name' => facet_name, 'items' => response.dig('facets', 'items') }
  end

  def do_query
    call_blacklight_api '/records.json', build_query
  end

  def call_blacklight_api(path, blacklight_query)
    url = URI.parse self.class.base_url
    url.path = path
    url.query = URI.encode_www_form blacklight_query
    request = HTTParty.get url
    throw BadRequestError if request.response.code == '400'
    throw PossibleIntermittentError unless request.response.code == '200'
    throw StandardError unless request.parsed_response.is_a? Hash
    request.parsed_response.dig 'response'
  end

  def build_query
    query = {
      q: search_terms,
      per_page: request_result_count,
      page: page || 1,
      search_field: 'all_fields',
      sort: SORT_ORDERS[sort_order],
      api_version: 2
    }
    filters.keys.each do |field|
      value = filters[field]&.first
      next unless value.present?
      next if /\W/.match? field  # Eventually we may check the field against a whitelist,
      # but for now anything without special characters should be fine

      if DATE_RANGE_LIMITERS.include? field
        if(value =~ /^(\d+)y$/i)
          query["range[#{field}][begin]"] = (Date.today - $1.to_i.year).strftime("%Y")
        else
          years = value.split('...')
          query["range[#{field}][begin]"] = years.first if years.first.present?
          query["range[#{field}][end]"] = years.second if years.second.present?
        end
      else
        query["f[#{ field }][]"] = value
      end

    end
    query["f[class_name][]"] = 'Item' # filter out collection records

    query
  end

  def supports_refine_search?
    true
  end

  def refine_search_link
    url = URI.parse self.class.base_url
    url.path = '/records'
    url.query = URI.encode_www_form({ q: search_terms })
    url.to_s
  end

  def self.sort_orders
    SORT_ORDERS.keys
  end

end
