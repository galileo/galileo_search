class BentoResultRecordDlg < BentoResultRecord

  DCTERMS_TYPE_ICONS = {
    # this relates dcterms_type to corresponding font awesome icon names
    Text: 'file-alt',
    Sound: 'volume-down',
    MovingImage: 'film',
    StillImage: 'image',
    Collection: 'layer-group',
    Image: 'image',
    Software: 'save',
    Dataset: 'database',
    InteractiveResource: 'window-maximize'
  }.freeze

  def self.dcterms_type_labels
    I18n.t!('services.dlg.facets.type_facet.values')
  rescue
    {}
  end

  def title
    record['title']
  end

  def display_title
    icons = dcterms_type_icons.join.html_safe
    helpers.content_tag(:span, icons, class: 'dcterms-type-icons') + super
  end

  def base_url
    request.class.base_url
  end

  def record_url
    "#{base_url}/record/#{record['id']}"
  end

  def thumbnail_url
    return nil if record['collection_id'].blank?
    return nil if record['dcterms_type']&.include? 'Sound'
    collection_dir = record['collection_id'].sub '_', '/'
    "#{base_url}/thumbnails/#{collection_dir}/#{record['id']}.jpg"
  end

  def authorship_line
    record['dcterms_creator']&.join '; '
  end

  def publication_info_line
    dates = helpers.possible_content_tag('span', display_dates, style: 'color: black')
    collection = helpers.possible_content_tag('span', record['collection_title'], class: 'publication-title')
    contributing_institutions = helpers.content_tag('span', contributing_institutions_line, style: 'color: black')
    [dates, collection, contributing_institutions].compact.join('<br>').html_safe
  end

  def display_dates
    dates = record['dc_date'] || []
    dates.map{ |d| d.split '/' }.flatten.map { |d | format_date d }.compact.join '/'
  end

  def contributing_institutions_line
    contributing_institutions = record['dcterms_provenance']
    contributing_institutions&.any? ? contributing_institutions.join('; ') : nil
  end

  def format_date(raw_date)
    if /^\d{4}-\d\d?-\d\d?$/ =~ raw_date
      Date.parse(raw_date).strftime('%B %e, %Y').gsub(/\s+/, ' ')
    elsif /^\d{4}-\d\d?$/ =~ raw_date
      Date.strptime(raw_date, '%Y-%m').strftime('%B %e, %Y').gsub(/\s+/, ' ')
    elsif /^\d{4}$/ =~ raw_date
      raw_date
    else
      nil
    end
  rescue Date::Error
      nil
  end

  def description
    record['dcterms_description']&.first
  end

  def buttons
    [digital_object_button].compact
  end

  def dcterms_type_icons
    (record['dcterms_type'] || []).map do |type|
      type = type.to_sym
      fa_id = DCTERMS_TYPE_ICONS[type]
      return nil unless fa_id
      label = self.class.dcterms_type_labels[type] || type
      "<span title=\"Type: #{ label }\"><i class=\"fas fa-#{fa_id}\"></i></span>"
    end.compact
  end

  #region button implementations

  # Unused as of 2022-11-04
  def metadata_button
    url = record.dig 'edm_is_shown_at', 0
    return nil if url.nil?
    return nil if %w[dlg.usg.edu dlg.galileo.usg.edu].include? URI.parse(url).host # The heading link probably already goes to the same place
    BentoResultButton.new label: 'Metadata at Partner Site', url: url
  rescue URI::InvalidURIError
    return nil
  end

  # Unused as of 2022-11-04
  def full_text_button
    BentoResultButton.new(label: 'Plain Text Transcript',
                          url: "#{record_url}/fulltext.text",
                          font_awesome_icon: 'file-alt') if record['fulltext'].present?
  end

  def digital_object_button
    url = record.dig 'edm_is_shown_by', 0
    return nil if url.nil?
    BentoResultButton.new label: "View Item", url: url
  end

  #region methods used by digital_object_button
  def mime_type
    formats = record['dc_format']
    return nil unless formats&.size == 1
    formats.first
  end

  def file_type_label
    case mime_type
    when 'application/pdf'
      'PDF'
    when /^image\//
      'Image'
    else
      'Digital Object'
    end
  end

  def file_type_icon
    case mime_type
    when 'application/pdf'
      'file-pdf'
    when /^image\//
      'image'
    else
      'file'
    end
  end
  #endregion
  #endregion

end
