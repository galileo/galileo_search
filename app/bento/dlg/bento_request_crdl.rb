# frozen_string_literal: true

class BentoRequestCrdl < BentoRequestDlg

  def self.base_url
    'https://crdl.usg.edu'
  end

  def self.preset_date_ranges
    [
      {
        label: '1950s',
        value: '1950...1959'
      },
      {
        label: '1960s',
        value: '1960...1969'
      }
    ]
  end

  def facet_class
    BentoFacetCrdl
  end

end
