# frozen_string_literal: true

# Represent a Bento from Solr
class BentoConfig
  include Blacklight::Solr::Document

  self.unique_key = 'blacklight_id_ss'

  # @return [String]
  def id
    self[:blacklight_id_ss]
  end

  # @return [String] Bento Type
  def bento_type
    self[:bento_type_ss]
  end

  # @return [String] Inst Code
  def inst_code
    self[:inst_code_ss]
  end

  # @return [String] User ID
  def user_id
    self[:user_id_ss]
  end

  # @return [String] Password
  def password
    self[:password_ss]
  end

  # @return [String] View Type
  def view_type
    self[:view_type_ss]
  end

  # @return [String] API Profile
  def api_profile
    self[:api_profile_ss]
  end

  def credentials
    return @credentials unless @credentials.nil?

    begin
      @credentials = self[:credentials_ss].nil? ? {} : JSON.parse(self[:credentials_ss])
    rescue JSON::ParserError
      @credentials = {}
    end
    @credentials
  end

  def credential(key)
    credentials[key]
  end

end
