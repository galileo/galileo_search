# frozen_string_literal: true

# Represents a result set from the EDS API
class EdsResults
  def initialize(results_hash)
    @results_hash = results_hash
  end

  def records
    @records ||= parse_results
  end

  def parse_results
    results = @results_hash.dig('SearchResult', 'Data', 'Records')

    return [] if results.nil?

    results.map do |record_hash|
      Record.new record_hash
    end
  end

  # Represents a single result from the EDS API
  class Record
    def initialize(record_hash)
      @result_hash = record_hash
    end

    def plink
      @result_hash['PLink']
    end

    def title
      titles = @result_hash.dig('RecordInfo', 'BibRecord', 'BibEntity', 'Titles')
                 &.find { |item| item['Type'] == 'main' }
      titles['TitleFull'] if titles
    end

    def publication_year
      part = @result_hash.dig('RecordInfo', 'BibRecord', 'BibRelationships', 'IsPartOfRelationships')&.first

      return if part.nil?

      published = part.dig('BibEntity', 'Dates')&.find { |item| item['Type'] == 'published' }

      return if published.nil?

      published['Y']
    end

    def authors
      bib_relationships = @result_hash.dig('RecordInfo', 'BibRecord', 'BibRelationships')

      return unless bib_relationships

      bib_relationships.extend Hashie::Extensions::DeepFind
      bib_relationships.deep_find_all('NameFull')&.uniq&.join('; ')
    end

    def cover_thumb_url
      thumb = @result_hash['ImageInfo']&.find { |item| item['Size'] == 'thumb' }

      return if thumb.nil?

      thumb['Target']
    end

    def abstract
      abstract = @result_hash['Items']&.find { |item| item['Name'] == 'Abstract' }

      return if abstract.nil?

      Nokogiri::HTML.parse(abstract['Data']).text
    end
  end
end
