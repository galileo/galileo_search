# frozen_string_literal: true

# Represent a Resource from Solr
class Resource
  include Blacklight::Solr::Document
  include ActionView::Helpers::UrlHelper

  self.unique_key = 'blacklight_id_ss'

  # DublinCore uses the semantic field mappings below to assemble an OAI-compliant Dublin Core document
  # Semantic mappings of solr stored fields. Fields may be multi or
  # single valued. See Blacklight::Document::SemanticFields#field_semantics
  # and Blacklight::Document::SemanticFields#to_semantic_values
  # Recommendation: Use field names from Dublin Core
  # use_extension(Blacklight::Document::DublinCore)

  # Standard email text for Resource - URL is added in view.
  # See: views/record_mailer/email_record.text.erb
  def to_email_text
    body = []
    body << 'Link to GALILEO database'
    body << "Resource: #{title}"
    body.join("\n") unless body.empty?
  end

  # Standard SMS text for Resource - URL is added in view.
  # See: views/record_mailer/sms_record.text.erb
  def to_sms_text
    body = []
    body << 'Link to GALILEO database'
    body << "Resource: #{title}"
    body.join("\n") unless body.empty?
  end

  # @return [String]
  def id
    self[:blacklight_id_ss]
  end

  # @return [String] Resource Code
  def code
    self[:for_resource_ss]
  end

  # @return [String] Resource Title/Name
  def title
    self[:resource_name_ss]
  end

  # @return [String] AWS S3 Key for thumbnail
  def thumbnail_key
    self[:thumbnail_us]
  end

  # @return [String] Institution code for Resource
  def institution_code
    self[:for_institution_ss]
  end

  # @return [String] URL for IP Access to the resource
  def ip_link
    self[:ip_link_ss]
  end

  # @return [String] URL for Remote Access to the resource
  def remote_link
    self[:remote_link_ss]
  end

  # @return [String] URL for OpenAthens Access to the resource
  def open_athens_link
    self[:open_athens_link_ss]
  end

  # @return [Boolean] for disallowing OpenAthens for the resource
  def bypass_open_athens?
    self[:bypass_open_athens_bs]
  end

  # @return [String] branding note
  def branding_note
    self[:branding_text_us]
  end

  # @return [String] AWS key for branding image
  def branding_image_key
    self[:branding_image_us]
  end

  # @return [String] Note about Remote Access
  def remote_note
    self[:remote_note_us]
  end

  # @return [TrueClass, FalseClass]
  def bypass_authentication?
    self[:bypass_authentication_bs]
  end

  # "remote_always"
  # @return [TrueClass, FalseClass] whether or not to display the remote_note
  def always_show_credentials?
    self[:credential_display_us] == 'always'
  end

  # show remote note if the user is remote
  # @return [TrueClass, FalseClass] whether or not to display the remote_note
  def remote_show_credentials?
    self[:credential_display_us] == 'remote'
  end

  def oa_proxy_enabled?
    self[:oa_proxy_bs]
  end

  # @return [Array] two dimensional Array of UN/PW for resource
  def credentials
    self[:remote_credentials_usm]
  end

  # @return [String]
  def express_link
    "https://www.galileo.usg.edu/express?link=#{code}&inst=#{institution_code}"
  end

  # @return [String]
  def local_express_link
    "/express?link=#{code}&inst=#{institution_code}"
  end

  # @return [String]
  def short_descriptions
    self[:resource_short_description_texts]
  end
end
