# frozen_string_literal: true

# Represents a result from the Primo API
class PrimoRecord
  include ActionView::Helpers::UrlHelper
  def initialize(record_hash, bento_config)
    @raw = record_hash
    @bento_config = bento_config
  end

  def pnx
    @raw['pnx']
  end

  def delivery
    @raw['delivery']
  end

  def record_id
    pnx&.dig 'control', 'recordid', 0
  end

  def title
    pnx&.dig 'display', 'title', 0
  end

  def publisher
    strip_brackets(pnx&.dig('addata', 'pub', 0))
  end

  def strip_brackets(input)
    input&.gsub(/[\[\]]/, ' ')&.gsub(/\s+/, ' ')&.strip
  end

  def host
    "#{primo_subdomain}.primo.exlibrisgroup.com"
  end

  def vid
    @bento_config.credential 'VID'
  end

  def scope
    @bento_config.credential 'Scope'
  end

  def primo_subdomain
    @bento_config.credential('Primo Subdomain') || 'galileo'
  end

  def record_link
    "https://#{host}/discovery/jfulldisplay?docid=#{record_id}&context=L&vid=#{vid}"
  end

  def available_electronically?
    delivery&.dig('deliveryCategory')&.include? 'Alma-E'
  end

  def uresolver_link
    delivery&.dig 'GetIt1', 0, 'links', 0, 'link'
  end

  def uresolver_xml_url
    html_url = uresolver_link
    return nil unless html_url.present?

    parsed_url = URI.parse html_url
    parsed_query = CGI::parse parsed_url.query
    parsed_query['svc_dat'] = 'cto'
    parsed_url.query = URI.encode_www_form parsed_query
    parsed_url.to_s
  end

  def expand_links_path
    uri = URI.parse Rails.application.routes.url_helpers.alma_links_api_path
    uri.query = URI.encode_www_form({
                                      'uresolve_info' => PrimoApiService.distill_delivery_info(record_id,
                                                                                               primo_subdomain,
                                                                                               vid)
                                    })
    uri.to_s
  end
end
