# frozen_string_literal: true

# Represent a ConfiguredBento from Solr
class ConfiguredBento
  include Blacklight::Solr::Document

  DEFAULT_ORDER = 9999.0

  def initialize(source_doc = nil, inst = nil)
    super(source_doc)
    @institution = inst
  end

  def institution
    @institution = InstitutionService.new.find inst_code if @institution.nil?
    @institution
  end

  def inst_code
    self[:inst_code_ss]
  end

  def user_view
    self[:user_view_ss]
  end

  def slug
    self[:slug_ss]
  end

  def service
    self[:service_ss]
  end

  def code
    self[:code_ss]
  end

  def display_name
    self[:display_name_ss]
  end

  def description
    self[:description_ss]
  end

  def no_results_message
    custom_message = self[:no_results_message_ss]
    return custom_message unless custom_message.blank?

    "#{I18n.translate('bento.bento_box.no_results_in')} #{display_name}"
  end

  def order
    self[:order_es] || DEFAULT_ORDER
  end

  def active?
    self[:active_bs]
  end

  def shown_by_default?
    self[:shown_by_default_bs]
  end

  def customizations
    return @customizations unless @customizations.nil?

    begin
      @customizations = self[:customizations_ss].nil? ? {} : JSON.parse(self[:customizations_ss])
    rescue JSON::ParserError
      @customizations = {}
    end
    @customizations
  end

  def requires_config?
    %w[eds_api primo].include? service
  end

  # @return [String]
  def bento_config_id
    self[:bento_config_id_ss]
  end

  def bento_config
    return nil unless requires_config?
    return @bento_config unless @bento_config.nil?

    @bento_config = BentoService.new(inst_code).find_by_id(bento_config_id)
  end

  def query(*args)
    request_class.new(self, *args).respond
  end

  def request_class
    return BentoRequestResearchStarters if service == 'eds_api' && code == 'research_starters'
    return BentoRequestEncyclopedia if service == 'eds_api' && (
      code =~ /^encyclopedia\b/i || customizations&.dig('Bibliographic Info Style') == 'Encyclopedia'
    )
    return BentoRequestEdsApi if service == 'eds_api'
    return BentoRequestEdsPublications if service == 'eds_publications'
    return BentoRequestPrimo if service == 'primo'
    return BentoRequestDlg if service == 'dlg'
    return BentoRequestCrdl if service == 'crdl'
    return BentoRequestGalileo if service == 'galileo'

    BentoRequest
  end

  def results_page
    Rails.application.routes.url_helpers.bento_full_results_path(inst_code, slug)
  end

  def refinable_endpoint
    Rails.application.routes.url_helpers.bento_full_results_api_path(inst_code, slug)
  end

  # @return [String] path ready to have the search terms appended to it e.g., /path/to/endpoint?search=
  def endpoint
    url_helpers = Rails.application.routes.url_helpers
    if service == 'galileo'
      return url_helpers.institution_databases_bento_path(inst_code,
                                                          q: '',
                                                          display_name: display_name,
                                                          no_results_message: no_results_message)
    end

    url_helpers.institution_custom_bento_api_path(inst_code, slug, bento_search_query: '')
  end

  def supports_full_text_limiter?
    request_class.supports_full_text_limiter?
  end

  def external_search_url_format(full_text = false)
    dummy_request = request_class.new self, BentoQuery.new(terms: BentoRequest::SEARCH_TERMS_PLACEHOLDER,
                                                                         full_text: full_text)
    return nil unless dummy_request.supports_refine_search?
    dummy_request.refine_search_link
  end

  def external_search_label
    I18n.t('services').dig(service.to_sym, :external_search) || I18n.t('services.default.external_search')
  end

  def available_sort_orders
    request_class.sort_orders.map do |order_key|
      localizations = I18n.t('services')
      label = localizations.dig(service.to_sym, :sort_orders, order_key.to_sym) ||
              localizations.dig(:default, :sort_orders, order_key.to_sym) ||
              order_key.titleize
      { key: order_key, label: label }
    end
  end

end
