# frozen_string_literal: true

# Represent a Bento from Solr
class BannerSolrDocument < SolrDocument
  # @return [String] Message
  def audience
    self[:audience_ss]
  end
  # @return [String] Message
  def message
    self[:message_ss]
  end

  # @return [String] Start Time
  def start_time
    self[:start_time_ds]
  end

  # @return [String] End Time
  def end_time
    self[:end_time_ds]
  end

  # @return [Boolean] galileo?
  def everyone?
    self[:audience_ss] == 'everyone'
  end

  # @return [Boolean] institution?
  def institution?
    self[:audience_ss] == 'institution'
  end

  # @return [Boolean] institution_group?
  def institution_group?
    self[:audience_ss] == 'institution_group'
  end

  # @return [String] Institution Code
  def institution_code
    self[:institution_code_ss]
  end

  # @return [String] Institution Code
  def institution_group_code
    self[:institution_code_ss]
  end

  # @return [String] Style
  def style
    self[:style_ss]
  end

  # @return [Array<String>] Position
  def user_view_codes
    self[:user_view_codes_sms] || []
  end
end