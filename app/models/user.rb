# frozen_string_literal: true

# :nodoc:
class User
  attr_accessor :inst, :site, :remote, :auth_type, :last_activity, :login_time, :first_name, :last_name
  ## Create a new user
  # options must contain a :solr_institution and an :auth_type
  # optionally it can contain a :site_code and a :remote value
  def initialize(options)
    @inst = Institution.new(options[:solr_institution])
    @remote = options[:remote]
    @auth_type = options[:auth_type]
    @first_name = options[:first_name]
    @last_name = options[:last_name]
    @login_time = Time.zone.now.to_s
    @last_activity = @login_time
    return unless options[:site_code]

    solr_site = InstitutionService.new.find_site options[:site_code]
    return unless solr_site&.child_of?(@inst)

    @site = Site.new(solr_site)
  end

  def remote?
    @remote == true
  end

  # institution object for session
  class Institution
    attr_accessor :code, :pub_code, :inst_group_code, :name, :proxy, :type, :site, :pass
    def initialize(solr_institution)
      @code = solr_institution.code
      @pub_code = solr_institution.public_code
      @inst_group_code = solr_institution.institution_group_code
      @name = solr_institution.name
      @proxy = solr_institution.proxy
      @type = solr_institution.type
      @pass = solr_institution.passphrase
    end
  end

  # site object for session
  class Site
    attr_accessor :name, :code, :type
    def initialize(solr_site)
      @name = solr_site.name
      @code = solr_site.code
      @type = solr_site.type
    end
  end
end
