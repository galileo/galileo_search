# frozen_string_literal: true

# :nodoc:
class SearchBuilder < Blacklight::SearchBuilder
  include Blacklight::Solr::SearchBuilderBehavior

  ELEM_FACET_TAG = 'Elementary School'
  MIDD_FACET_TAG = 'Middle School'
  HIGH_FACET_TAG = 'High School'

  self.default_processor_chain += %i[
    add_facet_values_to_query
    allocations_only
    filter_by_institution
    for_display
    apply_youth_view_limit
    apply_glri_limit
  ]

  def add_facet_values_to_query(solr_parameters)
    return unless solr_parameters[:sort]&.start_with? /score\b/

    query_terms = solr_parameters[:q]
    terms_for_ranking = []
    terms_for_ranking << query_terms unless query_terms.blank?
    facets = blacklight_params['f'] || {}
    %w[keywords_sms subject_sms].each do |facet_type|
      terms_for_ranking += facets[facet_type] || []
    end
    unless terms_for_ranking.empty?
      # rq is a Solr parameter for re-ranking queries
      # See https://solr.apache.org/guide/6_6/query-re-ranking.html
      # rqq is an arbitrary name we're using to break the query out into its own parameter
      rerank_query = RSolr.solr_escape(terms_for_ranking.join(' ').strip)
      return if rerank_query.blank?

      solr_parameters[:rqq] = rerank_query
      solr_parameters[:rq] = '{!rerank reRankQuery=$rqq reRankDocs=9999 reRankWeight=100}'
      # The re-rank should take over the sort order
      solr_parameters.delete :sort
    end
  end

  def allocations_only(solr_parameters)
    solr_parameters[:fq] ||= []
    solr_parameters[:fq] << 'class_ss:Allocation'
  end

  def filter_by_institution(solr_parameters)
    return unless blacklight_params[:inst_code]

    solr_parameters[:fq] << "for_institution_ss:#{blacklight_params[:inst_code]}"
  end

  def for_display(solr_parameters)
    solr_parameters[:fq] << 'display_bs:true'
  end

  def apply_youth_view_limit(solr_parameters)
    filter = facet_limit_for_view

    solr_parameters[:fq] << "user_view_sms:\"#{filter}\"" if filter
  end

  def apply_glri_limit(solr_parameters)
    solr_parameters[:fq] << 'glri_b:true' if blacklight_params[:glri] == 'true'
  end

  private

  def facet_limit_for_view
    case blacklight_params[:view]
    when 'elementary' then ELEM_FACET_TAG
    when 'middle' then MIDD_FACET_TAG
    when 'highschool' then HIGH_FACET_TAG
    end
  end
end
