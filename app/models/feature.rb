# frozen_string_literal: true

# Represent a Bento from Solr
class Feature < SolrDocument
  # region includes
  include Rails.application.routes.url_helpers
  include ActionView::Helpers
  # endregion

  # region constants
  FEATURE_DESC_LENGTH_RANGE = (10..350).freeze
  FEATURE_COUNT = 6
  # endregion

  # region solr_fields
  # @return [String]
  def featuring_type
    self[:featuring_type_ss]
  end

  # @return [String]
  def featuring_id
    self[:featuring_id_ss]
  end

  # @return [String]
  def featuring_code
    self[:featuring_code_ss]
  end

  # @return [String]
  def view_type
    self[:view_type_ss]
  end

  # @return [Integer]
  def position
    self[:position_ss].to_i
  end

  # @return [String]
  def link_url
    self[:link_url_ss]
  end

  # @return [String]
  def link_label
    self[:link_label_ss]
  end

  # @return [String]
  def link_description
    self[:link_description_ss]
  end

  # @return [String]
  def image_key
    self[:thumbnail_key_ss]
  end
  # endregion

  # region public_methods
  # @return [Boolean]
  def institution?
    featuring_type == 'Institution'
  end

  # @return [Boolean]
  def institution_group?
    featuring_type == 'InstitutionGroup'
  end

  # @param [SolrInstitution] institution
  # @return [String] relative url
  def spotlight_link_url(institution = nil)
    feature_link = link_url.strip

    return feature_link unless feature_link =~ /express.*link=(?<code>#{ValidationService.resource_code_regx})/

    #handle express link
    if institution
      ResourceService.new(institution.code).find(Regexp.last_match(:code))&.local_express_link
    else
      link_path Regexp.last_match(:code)
    end
  end
  # endregion
end