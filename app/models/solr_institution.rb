# frozen_string_literal: true

# :nodoc:
class SolrInstitution
  include Blacklight::Solr::Document

  self.unique_key = 'code_ss'

  # region public_methods
  def code
    self[:code_ss]
  end

  def name
    self[:name_ss]
  end

  def wayfinder_keywords
    self[:wayfinder_keywords_ss]
  end

  def open_athens
    self[:open_athens_bs]
  end

  def glri?
    self[:glri_bs]
  end

  def public_code
    self[:public_code_ss]
  end

  def institution_group_code
    self[:institution_group_code_ss]
  end

  def passphrase
    self[:current_passphrase_ss]
  end

  def lti_secret
    self[:lti_secret_ss]
  end

  def oa_entity_id
    self[:open_athens_entity_id_ss]
  end

  def oa_scope
    self[:open_athens_scope_ss]
  end

  def oa_api_name
    self[:open_athens_api_name_ss]
  end

  def oa_proxy
    self[:oa_proxy_bs]
  end

  def oa_proxy_org
    self[:oa_proxy_org_ss]
  end

  def journals_link
    self[:journals_link_us]
  end

  def proxy
    self[:gal_proxy_us]
  end

  def parent
    self[:parent_inst_code_ss]
  end


  def type
    self[:type_ss]
  end
  alias_method :inst_type, :type

  def eds_customer_id
    self[:eds_customer_id_ss]
  end

  def eds_profile
    self[:eds_profile_default_us]
  end

  def elem_eds_profile
    self[:eds_profile_kids_us]
  end

  def midd_eds_profile
    self[:eds_profile_teen_us]
  end

  def high_eds_profile
    self[:eds_profile_high_school_us]
  end

  def profile_for(view)
    view = view&.to_sym
    case view
    when :elementary then elem_eds_profile
    when :middle then midd_eds_profile
    when :highschool then high_eds_profile
    else eds_profile
    end
  end

  def links
    [
      self[:link1_us],
      self[:link2_us],
      self[:link3_us],
      self[:link4_us],
      self[:link5_us]
    ].reject(&:nil?)
  end

  def zotero_bib?
    self[:zotero_bib_bs]
  end

  # @param [SolrInstitution] institution
  # @return [TrueClass, FalseClass]
  def child_of?(institution)
    return false unless parent

    self[:parent_inst_code_ss] == institution.code
  end

  def features(view)
    view ||= :default
    features = []
    (1..6).each do |n|
      feature = feature(n, view.to_sym)
      features << feature if feature[:link]
    end
    features
  end

  def feature(num, view)
    field_prefix = case view
                   when :elementary then 'elem_'
                   when :middle then 'midd_'
                   when :highschool then 'high_'
                   when :educator then 'educ_'
                   when :full then 'full_'
                   else ''
                   end
    image = begin
              self[:"#{field_prefix}feature#{num}_usm"].try(:fetch, 3)
            rescue StandardError => _e
              nil
            end
    {
      label: self[:"#{field_prefix}feature#{num}_usm"].try(:fetch, 0),
      link: self[:"#{field_prefix}feature#{num}_usm"].try(:fetch, 1),
      desc: self[:"#{field_prefix}feature#{num}_usm"].try(:fetch, 2),
      image: image
    }
  end

  def banners
    return @banners unless @banners.nil?

    begin
      @banners = self[:banners_ss].nil? ? {} : JSON.parse(self[:banners_ss])
    rescue JSON::ParserError
      @banners = {}
    end
    @banners
  end

  def widgets
    return {} unless self[:widgets_ss]
    JSON.parse(self[:widgets_ss])

  rescue JSON::ParserError
    {}
  end

  def view_switching
    self[:view_switching_ss]
  end

  # @return [Boolean]
  def display_views?
    return false unless inst_type

    inst_type.to_sym == :k12 || inst_type.to_sym == :publiclib
  end

  # @return [Array<Symbol>]
  def all_views
    case inst_type
      when 'k12' then %i[elementary middle highschool educator]
      when 'publiclib' then %i[elementary middle highschool full]
      when 'highered' then %i[default]
      else %i[default]
    end
  end

  # @param [Symbol] max_view
  # @return [Array<Symbol>]
  def available_views(max_view)
   case view_switching
   when 'strict'
     all_views[..all_views.index(max_view.to_sym)]
   when 'medium'
     all_views[..(all_views.index(max_view.to_sym) + 1)]
   when 'off'
     [max_view.to_sym]
   else
     all_views
   end
  end

  # @param [Symbol] requested_view
  # @param [Symbol] max_view
  # @return [Boolean]
  def accessible_view?(requested_view, max_view)
    return false unless all_views.include? requested_view

    case view_switching
    when 'strict'
      all_views.index(requested_view) <= all_views.index(max_view)
    when 'medium'
      all_views.index(requested_view) <= (all_views.index(max_view) + 1)
    when 'off'
      requested_view == max_view
    else
      true # Do Nothing
    end
  end
  # endregion
end
