export function errorFromFailedRequest(request) {
    let errorType = 'unhandled-error'; // Default. Show a contact support link but not a retry link
    if (request.status === 0) errorType = 'non-http-error' // No HTTP response from server. E.g., no internet
    if (request.status === 400) errorType = 'error'; // User error. Don't offer contact support or retry link
    if (request.status === 502 || request.status === 503) errorType = 'intermittent-error';
    const contentType = (request.getResponseHeader('content-type') || '').split(';')[0];
    if (contentType.indexOf('application/json') > -1) {
        try {
            const parsed = JSON.parse(request.responseText);
            if (parsed.possibly_temporary) errorType = 'intermittent-error';
            if (parsed.error) {
                return {errorType, errorMessage: parsed.error};
            }
        } catch (e) {}
    }
    return {errorType};
}

export function errorMessageFromTemplate(bentoTitle, reloadHandler, errorType = 'unhandled-error', specificMessage) {
    // errorType = error, unhandled-error, non-http-error, or intermittent-error
    let templateName = `#polite-${errorType}-template`;
    let template = $($(templateName).html());
    template.find('[data-bento-error-bento-title]').text(bentoTitle);
    template.find('[data-retry-link]').click(function(){
        reloadHandler();
        return false;
    });
    if (specificMessage) template.filter('[data-error-message]').text(specificMessage);
    return template;
}