function showMoreLinks() {
    $(this).closest('.result-links').addClass('show-more')
    return false;
}

function showFewerLinks() {
    $(this).closest('.result-links').removeClass('show-more')
    return false;
}

function wireUpShowHideExtraLinksButtons(container) {
    $(container).find('[data-show-more]').click(showMoreLinks);
    $(container).find('[data-show-fewer]').click(showFewerLinks);
}

export default function wireUpBentoBodyEvents(container) {
    wireUpShowHideExtraLinksButtons(container);
    $(container).find('[data-expand]').click(toggleExpansion);
}

function toggleExpansion() {
    let $link = $(this);
    let $container = $link.closest('[data-expanded]');
    let $target = $container.find('[data-expand-area]');
    let state = $container.attr('data-expanded');
    $container.removeAttr('data-failed');
    if (state !== 'false') {
        $container.attr('data-expanded', 'false');
        $link.attr('title', $link.attr('data-expand-title'));
    } else {
        $link.attr('title', $link.attr('data-collapse-title'));
        if ($target.data('ready')) {
            $container.attr('data-expanded', 'true');
        } else {
            $container.attr('data-expanded', 'loading');
            $container.attr('aria-busy', true);
            let uri = $(this).data('expand');
            $.get(uri).then(function (response) {
                $target.html(response);
                wireUpShowHideExtraLinksButtons($target[0]);
                $container.attr('aria-busy', false);
                $container.attr('data-expanded', 'true');
                $target.data('ready', true);
            }).fail(function () {
                $container.attr('aria-busy', false);
                $container.attr('data-expanded', 'false');
                $container.attr('data-failed', true);
            });
        }
    }
    return false;
}
