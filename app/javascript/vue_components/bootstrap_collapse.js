require('bootstrap/js/dist/collapse');

export default {
    mounted: function (el, binding) {
        $(el).addClass('collapse');
        if (!binding.value) $(el).addClass('show');
        $(el).collapse({ toggle: false });
    },
    updated: function(el, binding) {
        if ($(el).hasClass('collapsing')) {
            // This may happen if you double-click quickly so your second click happens mid-transition.
            // Boostrap collapse ignores your second click, but Vue won't so we want to force Bootstrap to stay in sync.
            $(el).off('bsTransitionEnd').removeClass('collapsing').addClass('collapse').css('height', '')
            let collapseData = $(el).data('bs.collapse');
            if (collapseData) collapseData._isTransitioning = false;
            if (!binding.value) $(el).addClass('show');
        } else {
            // normal scenario
            $(el).collapse(binding.value ? 'hide' : 'show');
        }
    }
};