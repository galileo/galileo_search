const RESULTS_PER_PAGE = 20
export default {
    props: {page: Number, resultCount: Number, resultCountMessage: String},
    emits: ['update:page'],
    computed: {
        hide() {
            return (this.resultCount === null) || (this.resultCount === undefined) || isNaN(this.resultCount);
        },
        firstResultOnPage() {
            return Math.min(RESULTS_PER_PAGE * (this.page - 1) + 1, this.resultCount);
        },
        lastResultOnPage() {
            return Math.min(this.resultCount, RESULTS_PER_PAGE * this.page);
        },
        maxPage() {
            return Math.ceil(this.resultCount / 20);
        },

        // Why not put these directly into the template? Because currently Vue templates don't support the ?. operator.
        displayFirstResult() {
            return this.firstResultOnPage?.toLocaleString();
        },
        displayLastResult() {
            return this.lastResultOnPage?.toLocaleString();
        },
        displayResultCount() {
            return this.resultCountMessage || this.resultCount?.toLocaleString();
        }
    },
    methods: {
        previous() {
            if (this.page <= 1) return;
            this.$emit('update:page', this.page - 1);
        },
        next() {
            if (this.page >= this.maxPage) return;
            this.$emit('update:page', this.page + 1);
        }
    }
}