require('./application');
import wireUpBentoBodyEvents from '../shared/bento_body_events';
import {errorFromFailedRequest, errorMessageFromTemplate} from '../shared/bento_error_handling';

function setupBentoAjax() {
    window.$ = $;

    const BENTO_MARGIN_BOTTOM = 12;

    function getQueryParameter(param) {
        return (new URLSearchParams(window.location.search)).get(param);
    }

    function simpleArrangeIntoColumns(bentos, columns) {
        bentos.each(function (i, el) {
            $(el).appendTo(columns[i % columns.length]);
        });
    }

    function alwaysPutBentoInShortestColumn(bentos, columns) {
        let colHeights = columns.map(x => 0).toArray();
        let stagedColumns = columns.map(x => [[]]); // note: jQuery's map flattens once, so think of this as x => []
        bentos.each(function (i, el) {
            let lowestHeight = Math.min.apply(null, colHeights);
            let col = colHeights.indexOf(lowestHeight);
            stagedColumns[col].push(el);
            colHeights[col] += el.offsetHeight + BENTO_MARGIN_BOTTOM;
        });
        stagedColumns.each(function (i, stagedChildren) {
            let column = columns[i];
            column.append.apply(column, stagedChildren);
        });
    }

    function assignBentosIdsAndAriaFlowTo() {
        let bentos = $('.bento-col>.card');
        let prevBento = null;
        bentos.each(function (i, bento) {
            if (!bento.id) bento.id = 'bento-' + i;
            if (prevBento) $(prevBento).attr('aria-flowto', bento.id);
            prevBento = bento;
        });
    }

    let rearrangeBentos = window.rearrangeBentos = function () {
      let bentos = $('.bento-col>.card');
      bentos.each(function (i, bento) {
        if ($(bento).data('bento-index') === undefined) $(bento).data('bento-index', i);
      });
      bentos.sort((a, b) => $(a).data('bento-index') - $(b).data('bento-index'));
      bentos.each(function (i, bento) {
        // renumber to ensure integer indices
        $(bento).data('bento-index', i);
      });
      let columns = $('.bento-col:visible');
      alwaysPutBentoInShortestColumn(bentos, columns);
      return bentos;
    };

    function getInst() {
        return $('form[data-bento-search] input[name=inst]').val();
    }

    function getView() {
        return $('form[data-bento-search] input[name=view]').val();
    }

    const localStorageKey = function () {
        return `bento-customization:${getInst()}:${getView()}`;
    }

    let addedBentos = [];
    let removedBentos = [];

    const consolidateAddedRemovedBentos = function () {
        const netAdditions = {}
        addedBentos.forEach(code => netAdditions[code] = (netAdditions[code] || 0) + 1);
        removedBentos.forEach(code => netAdditions[code] = (netAdditions[code] || 0) - 1);
        const reducedAddedBentos = [];
        const reducedRemovedBentos = [];
        for (let code in netAdditions) {
            if (netAdditions[code] > 0) {
                reducedAddedBentos.push(code);
            } else if (netAdditions[code] < 0) {
                reducedRemovedBentos.push(code);
            }
        }
        addedBentos = reducedAddedBentos;
        removedBentos = reducedRemovedBentos;
    }

    const saveBentoArrangement = function () {
        let bentos = rearrangeBentos();
        consolidateAddedRemovedBentos();
        let bentoOrder = bentos.map((i, e) => $(e).find('[data-bento-title]').attr('name')).toArray();
        localStorage[localStorageKey()] = JSON.stringify({
            addedBentos,
            removedBentos,
            bentoOrder: bentoOrder
        });
        setArrangementPermalink();
    };

    function setArrangementPermalink() {
        let bentoOrder = $('.bento-col>.card').sort((a, b) => $(a).data('bento-index') - $(b).data('bento-index')).map((i, e) => $(e).find('[data-bento-title]').attr('name')).toArray();
        let params = new URLSearchParams({
            arrangement: bentoOrder.join(' '),
            view: getView()
        });
        let orderQueryString = params.toString();
        params.set('bento_search_query', bentoSearchQuery)
        if (full_text) params.set('full_text', 'on');
        let everythingQueryString = params.toString();
        let orderPermalink = `${window.location.origin}${window.location.pathname}?${orderQueryString}`;
        let everythingPermalink = `${window.location.origin}${window.location.pathname}?${everythingQueryString}`;
        $('[data-bento-customization-only-permalink]').val(orderPermalink);
        $('[data-bento-customization-and-search-permalink]').val(everythingPermalink);
        $('[data-search-term-placeholder]').text(bentoSearchQuery)
    }

    let defaultTitle = document.title;
    let bento_cell_elements = $('[data-bento-endpoint]');
    let bento_search_form = $('[data-bento-search]')[0];
    let bento_search_box = $('[data-bento-query]')[0];
    // stop here if the expected elements aren't present
    if (!(bento_cell_elements.length && bento_search_form && bento_search_box)) return;

    // setup anchors at top of page
    let anchorContainer = $('[data-bento-anchor-placeholder]')[0];
    let anchorTemplate = null;
    if (anchorContainer) {
        anchorTemplate = anchorContainer.firstElementChild.cloneNode(true);
        anchorContainer.innerHTML = '';
    }
    function BentoAnchor(name, anchorLink, bento) {
        if (anchorTemplate) {
            let element = anchorTemplate.cloneNode(true);
            $(element).find('[data-bento-name]').text(name);
            $(element).find('a').attr('href', '#' + anchorLink);
            $(element).appendTo(anchorContainer);
            const defaultClickHandler = bento.getMoreResultsClickHandler();
            $(element).on('click', function (e){
                if ($('#bento-anchors').hasClass('customization-mode')) return false;
                if (e.shiftKey) {
                    bento.update();
                    e.preventDefault();
                    return false;
                } else if (e.altKey || $(element).hasClass('failed')) {
                    let bento = $('a[name=' + anchorLink + ']').closest('[data-bento-endpoint]')[0];
                    bento.scrollIntoView();
                    e.preventDefault();
                    return false;
                } else {
                    defaultClickHandler(e);
                }
            });
            this.element = element;
        }
    }
    BentoAnchor.prototype.setResultCountArea = function (text) {
        $(this.element).find('[data-bento-result-count]').text(text);
    }
    BentoAnchor.prototype.setLoading = function () {
        $(this.element).attr('aria-busy', true);
        $(this.element).removeClass('failed');
        this.setResultCountArea('');
    }
    BentoAnchor.prototype.setReady = function (resultCount) {
        $(this.element).attr('aria-busy', false);
        $(this.element).removeClass('failed');
        if (resultCount != undefined) this.setResultCountArea('(' + resultCount + ')');
    }
    BentoAnchor.prototype.setFailed = function () {
        $(this.element).attr('aria-busy', false);
        $(this.element).addClass('failed');
        this.setResultCountArea('');
    }

    function addBentoFromMenu(menuItem) {
        var bentoSlug = $(menuItem).data('bento-addition');
        var node =  $(`<div
    class="card browseCard shadow-sm bento-card rounded-lg"
    aria-live="polite"
    data-bento-endpoint="${$(menuItem).data('bento-addition-endpoint')}<<query>>"
    data-refinable-results-page="/${getInst()}/bentos/${bentoSlug}">
    <div class="card-header">
      <h1><a name="${bentoSlug}" data-bento-title>${$(menuItem).text()}</a></h1>
      <h1 class="loading-indicator" aria-label="Loading">
        <span>.</span>
        <span>.</span>
        <span>.</span>
      </h1>

    </div>
    <div class="card-body" data-bento-content>
      <div class="card-content">
       <h2 class="card-title loading"></h2>
        <p class="description loading"></p>
      </div>
      <div class="card-content">
        <h2 class="card-title loading"></h2>
        <p class="description loading"></p>
      </div>
      <div class="card-content">
        <h2 class="card-title loading"></h2>
        <p class="description loading"></p>
      </div>
      <div class="card-content">
        <h2 class="card-title loading"></h2>
        <p class="description loading"></p>
      </div>
      <div class="card-content">
        <h2 class="card-title loading"></h2>
        <p class="description loading"></p>
      </div>
      <div class="card-content">
        <h2 class="card-title loading"></h2>
        <p class="description loading"></p>
      </div>
    </div>
  </div>`).appendTo('.bento-col-4');
        var cell=new BentoCell(node, `${$(menuItem).data('bento-addition-endpoint')}<<query>>`);
        cell.update()
        bento_cells.push(cell)
        $(menuItem).remove();
    }

    function addBentoFromCode(code) {
        let menuItem = $(`[data-bento-addition='${code}']`)[0];
        if (menuItem) addBentoFromMenu(menuItem);
    }

    const onBentoAddition = function(e) {
        addBentoFromMenu(this);
        addedBentos.push($(this).data('bento-addition'));
        saveBentoArrangement();
        setupDraggableAnchors();
        e.stopPropagation();
    }
    function removeBentoFromAnchor($anchorContainer) {
        let removed = $anchorContainer.remove();
        let code = removed.find('a').attr('href').replace('#','');
        let bento = $('[name=' + code + ']').closest('.bento-card').remove();
        // insert into "Add" dropdown
        let title = removed.find('[data-bento-name]').text();
        let node = $(`<a class="dropdown-item" data-bento-addition="${code}" data-bento-addition-endpoint="${bento.data('bento-endpoint').replace(/<<query>>$/, '')}">${title}</a>`);
        $('[data-bento-addition]').each(function(i, el) {
            if (title && $(el).text().toLowerCase() > title.toLowerCase()) {
                node.click(onBentoAddition).insertBefore(el);
                title = null;
            }
        });
        if (title) {
            node.click(onBentoAddition).appendTo($('[data-bento-additions]'))
        }
        return code
    }

    function removeBentoFromCode(code) {
        let anchorContainer = $(`a.bento-anchor[href="#${code}"]`).closest('.bento-anchor-container');
        if (anchorContainer.length) removeBentoFromAnchor(anchorContainer);
    }

    let shownByDefaultBentos;

    function restoreBentos(savedState) {
        let currentlyVisibleBentos = $('a.bento-anchor').map((i, e) => $(e).attr('href')?.replace('#','')).toArray();
        if (!shownByDefaultBentos) shownByDefaultBentos = currentlyVisibleBentos;
        if (!savedState) savedState = localStorage[localStorageKey()];
        if (!savedState) return;
        let bentoOrder;
        ({addedBentos, removedBentos, bentoOrder} = JSON.parse(savedState));
        addedBentos = addedBentos.filter(code => !shownByDefaultBentos.includes(code));
        removedBentos = removedBentos.filter(code => shownByDefaultBentos.includes(code));
        addedBentos.forEach(addBentoFromCode);
        removedBentos.forEach(removeBentoFromCode);
        let i = -1;
        let firstAnchorContainer = $('.bento-anchor-container').filter((i, e) => {
            let code = $(e).find('a.bento-anchor').attr('href').replace('#', '');
            return !bentoOrder.includes(code);
        })[0];
        bentoOrder.reverse().forEach(code => {
            let anchorContainer = $('#bento-anchors').find(`a[href="#${code}"]`).closest('.bento-anchor-container');
            if (!anchorContainer.length) return;
            $('[name=' + code + ']').closest('.bento-card').data('bento-index', i--);
            if (firstAnchorContainer) {
                anchorContainer.insertBefore(firstAnchorContainer)
            }
            firstAnchorContainer = anchorContainer;
        });
        rearrangeBentos();
    }

    function setBentosFromList(desiredOrder) {
        let currentlyVisibleBentos = $('a.bento-anchor').map((i, e) => $(e).attr('href')?.replace('#','')).toArray();
        let toAdd = desiredOrder.filter(code => !currentlyVisibleBentos.includes(code));
        let toRemove = currentlyVisibleBentos.filter(code => !desiredOrder.includes(code));
        toAdd.forEach((x) => {
            addBentoFromCode(x);
            addedBentos.push(x);
        });
        toRemove.forEach((x) => {
            removeBentoFromCode(x);
            removedBentos.push(x);
        });
        let i = -1;
        let firstAnchorContainer = $('.bento-anchor-container').filter((i, e) => {
            let code = $(e).find('a.bento-anchor').attr('href').replace('#', '');
            return !desiredOrder.includes(code);
        })[0];
        desiredOrder.slice().reverse().forEach(code => {
            let anchorContainer = $('#bento-anchors').find(`a[href="#${code}"]`).closest('.bento-anchor-container');
            if (!anchorContainer.length) return;
            $('[name=' + code + ']').closest('.bento-card').data('bento-index', i--);
            if (firstAnchorContainer) {
                anchorContainer.insertBefore(firstAnchorContainer)
            }
            firstAnchorContainer = anchorContainer;
        });
        rearrangeBentos();
        setupDraggableAnchors();
    }

    function restoreDefaultBentos() {
        if (shownByDefaultBentos) setBentosFromList(shownByDefaultBentos);
        delete localStorage[localStorageKey()];
        addedBentos = [];
        removedBentos = [];
    }

    function adoptLayoutFromList(list) {
        setBentosFromList(list)
        saveBentoArrangement()
    }

    window.setBentosFromList = setBentosFromList
    window.restoreDefaultBentos = restoreDefaultBentos



    const setupDraggableAnchors = function () {
        $('.bento-anchor').attr(
            'draggable', true
        ).off('dragstart').on('dragstart', function (e) {
            e.originalEvent.dataTransfer.setData("text/plain", $(this).attr('href'));
            $(this).closest('.bento-anchor-container').addClass('dragging');
        }).off('dragend').on('dragend', function (e) {
            e.originalEvent.dataTransfer.setData("text/plain", $(this).attr('href'));
            $(this).closest('.bento-anchor-container').removeClass('dragging');
            $('#bento-anchors').find('.bento-anchor-container').removeClass('may-drop-before');
            $('.bento-anchor-container, [data-bento-last-position-target]').data('drag-counter', 0);
            e.preventDefault();
        });
        $('.bento-anchor-container, [data-bento-last-position-target]').off('dragenter').on('dragenter', function (e) {
            let counter = ($(this).data('drag-counter') || 0) + 1;
            $(this).data('drag-counter', counter);
            if ($(this).is('[data-bento-last-position-target]')) {
                $('[data-bento-last-position-primary-target]').addClass('may-drop-before');
            } else {
                $(this).addClass('may-drop-before');
            }
            e.preventDefault();
        }).off('dragleave').on('dragleave', function () {
            let counter = ($(this).data('drag-counter') || 0) - 1;
            $(this).data('drag-counter', counter);
            if (counter <= 0) $($(this).is('[data-bento-last-position-target]') ? '[data-bento-last-position-primary-target]' : this).removeClass('may-drop-before');
        }).off('dragover').on('dragover', function (e) {
            e.preventDefault();
        }).off('drop').on('drop', function (e) {
            $('#bento-anchors').find('.bento-anchor-container, [data-bento-last-position-target]').removeClass('may-drop-before');
            let droppedId = e.originalEvent.dataTransfer.getData("text/plain");
            let droppedAnchor = $('#bento-anchors').find(`a[href="${droppedId}"]`);
            let droppedBento = $('[name=' + droppedId.replace('#','') + ']').closest('.bento-card');
            let referenceAnchorContainer = this;
            let direction = 'before'
            if ($(this).is('[data-bento-last-position-target]')) {
                direction = 'after'
                referenceAnchorContainer = $('.bento-anchor-container').last()[0];
            }
            let referenceBento = $('[name=' + $(referenceAnchorContainer).find('a').attr('href').replace('#','') + ']').closest('.bento-card');
            let droppedContainer = droppedAnchor.closest('.bento-anchor-container');
            if (direction === 'after') {
                droppedContainer.insertAfter(referenceAnchorContainer);
            } else {
                droppedContainer.insertBefore(referenceAnchorContainer);
            }
            droppedBento.data('bento-index', referenceBento.data('bento-index') + (direction === 'after' ? 0.5 : -0.5 ));
            saveBentoArrangement();
            $('.bento-anchor-container').data('drag-counter', 0);
            e.preventDefault();
        });


        $('[data-bento-remove-button]').off('click').on('click', function(e) {
            let code = removeBentoFromAnchor($(this).closest('.bento-anchor-container'))
            removedBentos.push(code);
            saveBentoArrangement();
            e.stopPropagation();
            return false;
        });
    }

    function showAnchors() {


        $('[data-bento-start-customizing]').on('click', function(){
            $('#bento-anchors').toggleClass('customization-mode')
            setupDraggableAnchors();
        });

        $('[data-bento-stop-customizing]').on('click', function(){
            $('.bento-anchor').attr('draggable', false)
            $('.bento-ancho').off('dragstart')
            $('#bento-anchors').toggleClass('customization-mode')
        });


        $('[data-bento-addition]').off('click').click(onBentoAddition);
        $('[data-bento-restore-defaults]').off('click').click(restoreDefaultBentos);
        $('[data-bento-addition-menu-toggle]').off('click').click(function (e){
            $(this).toggleClass('shown');
            $('[data-bento-additions]').collapse('toggle');
            e.stopPropagation()
        });


        if (anchorContainer) {
            anchorContainer.setAttribute('data-bento-anchors', true)
            anchorContainer.removeAttribute('data-bento-anchor-placeholder')
        }
    }


    let bentoSearchQuery = bento_search_box.value;
    let full_text = bento_search_form['full_text'].checked;

    function refinableResultsQueryString() {
        let query = {
            q: bentoSearchQuery
        };
        if (full_text) query.full_text = 'on';
        return (new URLSearchParams(query)).toString();
    }

    function ResponseDOM(responseHTML) {
        let dom = document.createElement('html');
        dom.innerHTML = responseHTML;
        this.jq = $(dom);
    }
    ResponseDOM.prototype.getBody = function () {
        return this.jq.find('body')[0].innerHTML;
    }
    ResponseDOM.prototype.getMeta = function (key) {
        return this.jq.find('meta[name=' + key + ']').attr('content');
    }

    function BentoCell(container, endpoint) {
        this.container = container;
        this.body = $(container).find('[data-bento-content]')[0];
        this.placeholderContent = this.body.innerHTML;
        this.endpoint = endpoint;
        this.bentoType = $(container).find('a[name]').attr('name');
        this.titleElement = $(container).find('[data-bento-title]')[0]
        let title = $(this.titleElement).text();
        this.title = title
        this.anchor = new BentoAnchor(title, this.bentoType, this);
        this.currentRequest = null;
        this.requestUrl = null;
        const resultsPageUrl = $(container).data('refinable-results-page');
        if (!resultsPageUrl) {
            $(this.titleElement).click(this.getMoreResultsClickHandler());
            $(this.titleElement).attr('data-target', '#more-results-modal');
            $(this.titleElement).attr('data-toggle', 'modal');
        }
    }

    BentoCell.prototype.getMoreResultsClickHandler = function () {
        let self = this;
        const resultsPageUrl = $(this.container).data('refinable-results-page');
        if (resultsPageUrl) return function (e) {
            window.location.href = `${ resultsPageUrl }?${ refinableResultsQueryString() }`;
            e.stopPropagation();
            e.preventDefault();
        }
        return () => self.showMoreResults(1);
    }

    BentoCell.prototype.clearRequest = function () {
        this.currentRequest = null;
        this.requestUrl = null;
    }

    BentoCell.prototype.setReady = function (resultCount) {
        if(resultCount != undefined) {
            let displayResultCount = parseInt(resultCount).toLocaleString('en');
            if (displayResultCount !== 'NaN') {
                $(this.container).find('[data-bento-result-count]').text(displayResultCount);
                this.anchor.setReady(displayResultCount);
            }
        } else {
            // Valid output from server but no result count
            // This should never happen and we're not handling it particularly elegantly
            // It's just a stub in case we someday have a bento for which result counts are
            // unavailable or irrelevant.
            $(this.container).find('[data-bento-result-count]').text('');
            this.anchor.setReady();
        }
        $(this.container).attr('aria-busy', false);
        $(this.container).removeClass('failed');
        rearrangeBentos();
    }
    BentoCell.prototype.setLoading = function () {
        $(this.container).attr('aria-busy', true);
        $(this.container).removeClass('failed');
        this.anchor.setLoading();
    }
    BentoCell.prototype.setFailed = function (errorType, errorMessage) {
        $(this.container).attr('aria-busy', false);
        $(this.container).addClass('failed');
        this.displayErrorFromTemplate(errorType, errorMessage);
        rearrangeBentos();
    }

    BentoCell.prototype.displayErrorFromTemplate = function (errorType, specificMessage) {
        this.body.innerHTML = '';
        let self = this;
        errorMessageFromTemplate(this.title,
                     () => self.update(),
                                 errorType,
                                 specificMessage).appendTo(this.body);
        this.anchor.setFailed();
    }
    BentoCell.prototype.buildBentoAPIUrl = function () {
        let url = this.endpoint.replace('<<query>>', encodeURIComponent(bentoSearchQuery));
        if (full_text) {
            url += `&full_text`; //Add Full-text checkbox param
        }
        return url;
    }

    let moreResultsRequest = null;
    let moreResultsBento = null;

    function moreResultsFailed() {
        $('[data-more-results-wrapper]').addClass('failed');
        $('[data-more-results-wrapper]').attr('aria-busy', false);
    }

    function hideMoreResultsToolbar() {
        $('[data-modal-pagination]').hide();
        $('[data-more-results-external]').hide();
    }

    BentoCell.prototype.showMoreResultsToolbar = function (page, resultCount, refineLink, maxAllowedPage) {
        let numPerPage = 20;
        resultCount = parseInt(resultCount);
        maxAllowedPage = parseInt(maxAllowedPage) || null;
        let firstItem = Math.min((page - 1) * numPerPage + 1, resultCount);
        let lastItem = Math.min(page * numPerPage, resultCount);
        $('[data-page-number]').text('' + firstItem.toLocaleString('en') + '–' + lastItem.toLocaleString('en'));
        let maxPage = Math.ceil(resultCount / numPerPage);
        if (maxAllowedPage) maxPage = Math.min(maxPage, maxAllowedPage + 1);
        $('[data-page-count]').text(resultCount.toLocaleString('en'));
        if (refineLink) {
            $('[data-more-results-external]').show();
            $('[data-more-results-external]').attr('href', refineLink);
            $('[data-results-continue-externally-button]').attr('href', refineLink);
        } else {
            $('[data-more-results-external]').hide();
        }
        let prevButtons = $('[data-results-prev-button]');
        let nextButtons = $('[data-results-next-button]');
        let continueExternallyButtons = $('[data-results-continue-externally-button]');
        let modalPagination = $('[data-modal-pagination]');
        prevButtons.off('click');
        nextButtons.off('click');
        if (page > 1) {
            prevButtons.attr('disabled', null);
            prevButtons.click(x => this.showMoreResults(page - 1));
        } else {
            prevButtons.attr('disabled', true);
        }
        if (page < maxPage) {
            nextButtons.attr('disabled', null);
            nextButtons.click(x => this.showMoreResults(page + 1));
        } else {
            nextButtons.attr('disabled', true);
        }

        // Special show/hide conditions depending on maxAllowedPage
        if (maxAllowedPage && (page >= maxAllowedPage)) {
            nextButtons.addClass('next-continues-search-externally');
        } else {
            nextButtons.removeClass('next-continues-search-externally');
        }
        if (maxAllowedPage && (page > maxAllowedPage)) {
            modalPagination.addClass('no-page-number');
        } else {
            modalPagination.removeClass('no-page-number');
        }
        if (maxAllowedPage && (page === maxAllowedPage) && refineLink) {
            continueExternallyButtons.show();
        } else {
            continueExternallyButtons.hide();
        }

        modalPagination.show();
    }

    BentoCell.prototype.tryShowMoreResultsToolbar = function (page) {
        if (this.resultsMetadataDeferred.state() !== 'resolved') {
            hideMoreResultsToolbar();
        }
        let self = this;
        $.when(this.resultsMetadataDeferred).then(function ({resultCount, refineLink, maxAllowedPage, takeExternalLabel}){
            if (moreResultsBento !== self.bentoType) return;
            self.showMoreResultsToolbar(page, resultCount, refineLink, maxAllowedPage);
            if (takeExternalLabel) {
                $('[data-results-continue-externally-label]').text(takeExternalLabel);
            }
        });
    }

    BentoCell.prototype.showMoreResults = function (page) {
        moreResultsBento = this.bentoType;
        $('[data-modal-title]').text(this.title);
        this.tryShowMoreResultsToolbar(page);
        let modal_body = $('[data-more-results-body]')[0];
        $('[data-more-results-wrapper]').removeClass('failed');
        $('[data-more-results-wrapper]').attr('aria-busy', true);
        let url = this.buildBentoAPIUrl() + '&page=' + page;
        if (moreResultsRequest) {
            moreResultsRequest.abort();
        }
        let self = this;
        moreResultsRequest = $.get(url, null, null, 'html');
        moreResultsRequest.then(function (response) {
            moreResultsRequest = null;
            let responseDOM = new ResponseDOM(response);
            if (responseDOM.getMeta('bento-success')) {
                self.resultsMetadataDeferred.resolve({
                    resultCount: responseDOM.getMeta('result-count'),
                    refineLink: responseDOM.getMeta('refine-link'),
                    maxAllowedPage: responseDOM.getMeta('max-allowed-page'),
                    takeExternalLabel: responseDOM.getMeta('take-external-label')
                });
                $('[data-more-results-wrapper]').attr('aria-busy', false);
                $('[data-more-results-wrapper]').removeClass('failed');
                modal_body.innerHTML = responseDOM.getBody();
                wireUpBentoBodyEvents(modal_body);
            } else {
                this.setMoreResultsFailed(page);
            }
        }).fail(function (request){
            if (request.statusText === 'abort') {
                return;
            }
            let {errorType, errorMessage} = errorFromFailedRequest(request);
            self.setMoreResultsFailed(page, errorType, errorMessage);
        });
    }

    BentoCell.prototype.setMoreResultsFailed = function (page, errorType, errorMessage) {
        let messageArea = $('[data-bento-more-results-error]')[0];
        let self = this;
        let reloadHandler = () => self.showMoreResults(page);
        messageArea.innerHTML = '';
        errorMessageFromTemplate(this.title, reloadHandler, errorType, errorMessage).appendTo(messageArea);
        moreResultsFailed();
    }

    BentoCell.prototype.showResults = function (responseDOM) {
        let body = this.body;
        body.innerHTML = responseDOM.getBody();
        this.resultCount = responseDOM.getMeta('result-count');
        this.resultsMetadataDeferred.resolve({
            resultCount: responseDOM.getMeta('result-count'),
            refineLink: responseDOM.getMeta('refine-link'),
            maxAllowedPage: responseDOM.getMeta('max-allowed-page'),
            takeExternalLabel: responseDOM.getMeta('take-external-label')
        });
        this.setReady(this.resultCount);
        $(body).find('[data-more-results-link]').click(this.getMoreResultsClickHandler());
        wireUpBentoBodyEvents(body);
    }

    BentoCell.prototype.setMoreResultsLink = function () {
        // This is only for bentos that support the refinable results page. Other bentos will get a more results modal
        // and are unaffected by this function.
        const resultsPageUrl = $(this.container).data('refinable-results-page')
        if (resultsPageUrl) {
            $(this.titleElement).attr('href', `${ resultsPageUrl }?${ refinableResultsQueryString() }`);
        }
    }

    BentoCell.prototype.update = function (state) {
        // note that state is only populated if the method was triggered by a popstate event (back/forward buttons)
        this.setMoreResultsLink();
        let url = this.buildBentoAPIUrl();
        let cachedResults = state && state.results && state.results[url];
        if (this.currentRequest) {
            if (this.requestUrl !== url) {
                // user initiated a new search before the old one returned; abort old one
                this.currentRequest.abort();
                this.clearRequest();
            } else if (cachedResults) {
                // same url, but now we have access to cached results that  we didn't have before popstate event
                // abort old request
                this.currentRequest.abort();
                this.clearRequest();
            } else {
                // nothing has changed; abort new search
                return;
            }
        }
        this.resultsMetadataDeferred = $.Deferred();
        let body = this.body;
        if (cachedResults != undefined) {
            let responseDOM = new ResponseDOM(cachedResults);
            this.showResults(responseDOM);
            return;
        }
        body.innerHTML = this.placeholderContent;
        this.setLoading();
        this.resultCount = null;
        let self = this;
        this.currentRequest = $.get(url, null, null, 'html');
        this.requestUrl = url;
        this.currentRequest.then(function (response) {
            self.clearRequest();
            let responseDOM = new ResponseDOM(response);
            if (responseDOM.getMeta('bento-success')) {
                self.showResults(responseDOM);
                // save results in history.state, so they're available if we navigate away and come back via back button
                let state = history.state;
                if (!state) state = {
                    query: bentoSearchQuery,
                    full_text: full_text,
                    results: {}
                };
                state.results[url] = response;
                history.replaceState(state, bentoSearchQuery);
            } else {
                body.innerHTML = 'Invalid response';
                self.setFailed();
            }
        }).fail(function (request) {
            if (request.statusText === 'abort') {
                return;
            }
            self.clearRequest();
            let {errorType, errorMessage} = errorFromFailedRequest(request);
            self.setFailed(errorType, errorMessage);
        });
    };

    let bento_cells = [];
    bento_cell_elements.each(function (i, element) {
        let endpoint = $(element).attr('data-bento-endpoint');
        bento_cells.push(new BentoCell(element, endpoint));
    });

    // Placards
    let placardContainer = $('[data-placards]')[0];
    let placardEndpoint = $(placardContainer).data('placards');
    let placardRequest = null;
    let placardRequestUrl = null;

    function showPlacards(responseDOM) {
        placardContainer.innerHTML = responseDOM.getBody();
        $(placardContainer).find('[data-suggested-search]').click(function (e){
            bento_search_box.value = $(this).data('suggested-search');
            recordNewSearch();
            updateAll();
            e.preventDefault();
            return false;
        });
    }

    function updatePlacards(optionalState) {
        if (!placardContainer) return;
        placardContainer.innerHTML = '';
        let url = placardEndpoint.replace('<<query>>', encodeURIComponent(bentoSearchQuery));
        let cachedResults = optionalState && optionalState.results && optionalState.results[url];
        if (placardRequest) {
            if (placardRequestUrl !== url || cachedResults) {
                placardRequest.abort();
                placardRequest = null;
                placardRequestUrl = null;
            } else {
                return;
            }
        }
        if (cachedResults != undefined) {
            showPlacards(new ResponseDOM(cachedResults));
            return;
        }
        placardRequestUrl = url;
        placardRequest = $.get(url, null, null, 'html');
        placardRequest.then(function (response){
            placardRequest = null;
            placardRequestUrl = null;
            let responseDOM = new ResponseDOM(response);
            if (responseDOM.getMeta('bento-success')) {
                showPlacards(responseDOM);
                let state = history.state;
                if (!state) state = {
                    query: bentoSearchQuery,
                    full_text: full_text,
                    results: []
                };
                state.results[url] = response;
                history.replaceState(state, bentoSearchQuery);
            }
        });
    }

    function updateTitle() {
        document.title = bentoSearchQuery + ' — ' + defaultTitle;
    }

    function updateAll(optionalState) {
        if (bentoSearchQuery) {
            $(document.body).removeClass('no-bento-search')
            updateTitle();
            setArrangementPermalink();
            updatePlacards(optionalState);
            $.each(bento_cells, function (i, cell) {
                cell.update(optionalState);
            });
            rearrangeBentos();
        } else {
            $(document.body).addClass('no-bento-search')
        }
    }

    function buildQueryString(searchTerms, fullText) {
        // Building a URL to put in the address bar
        // FYI, we don't really need to escape our query; if you feed an unescaped URL into pushState, it escapes it for you.
        // I'm just doing it here so I can represent spaces as '+' instead of '%20', to match how our URLs look when
        // coming from the home page.
        // This is purely aesthetic; either representation gets the same results.
        let encodedQuery = encodeURIComponent(searchTerms).replace(/%20/g, '+');
        let qs = '?bento_search_query=' + encodedQuery;
        if (fullText) {
            qs += '&full_text=on'
        }
        return qs;
    }

    function updateViewSwitcher() {
        let qs = buildQueryString(bentoSearchQuery, full_text);
        $('#choose-view .card-title a[href]').each(function (i, a) {
            let route = a.href.split('?')[0];
            a.href = route + qs;
        });
    }

    function recordNewSearch() {
        bentoSearchQuery = bento_search_box.value;
        full_text = bento_search_form['full_text'].checked;
        let url = buildQueryString(bentoSearchQuery, full_text);

        // Below line changes the page URL the user sees in the browser and creates a new item in their browser history
        // First argument is any data that can help us reconstruct the new state if the user were to navigate away
        //    from it and then hit "Back"
        // Second argument is pretty much arbitrary
        // Final argument is the URL (passing just query string will leave everything before the querystring the same)
        history.pushState({
            query: bentoSearchQuery,
            full_text: full_text,
            results: {}
        }, bentoSearchQuery, url);
        updateViewSwitcher();
    }

    // Handle user submitting a new search
    $(bento_search_form).on('submit', function () {
        if ((bentoSearchQuery !== bento_search_box.value) || (full_text !== bento_search_form['full_text'].checked)) {
            recordNewSearch();
        }
        updateAll();
        return false;
    });

    // Set up the initial state similar to the pushState call above
    history.replaceState({
        query: bentoSearchQuery,
        full_text: full_text,
        results: {}
    }, bentoSearchQuery);

    // Back button
    // ...or forward button; "popstate" is kind of a misnomer
    $(window).on('popstate', function (e) {
        let state = e.originalEvent.state;
        if (!state) return;
        let query = state.query;
        bentoSearchQuery = query;
        bento_search_box.value = query;
        full_text = !!state.full_text
        bento_search_form['full_text'].checked = full_text
        updateViewSwitcher();
        updateAll(state);
    });

    restoreBentos();
    let bentoOrderFromURL = getQueryParameter('arrangement');
    if (bentoOrderFromURL) {
        let order = bentoOrderFromURL.split(' ')
        adoptLayoutFromList(order);
    }
    assignBentosIdsAndAriaFlowTo();
    updateAll();
    showAnchors();

    // rearrange bentos if resize changes the number of visible columns
    let lastColumnCount = $('.bento-col:visible').length;
    function handleResize() {
        let columnCount = $('.bento-col:visible').length;
        if (columnCount !== lastColumnCount) {
            rearrangeBentos();
            lastColumnCount = columnCount;
        }
    }
    $(window).on('resize', handleResize);
}

// run on page load
$(setupBentoAjax);

