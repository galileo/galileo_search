
$(function (){
    // make sure there are no libchat modals by giving it a high "last-dismissed" timestamp
    localStorage.setItem("libchat_auto", JSON.stringify({
        date: Math.floor(new Date(2038, 1, 1) / 1e3)
    }));
    var $libchat = $('[data-libchat-container]');
    var $insertedButton = $libchat.find('button')
    if ($insertedButton.length) {
        // libchat did its thing
        $libchat.find('button').attr('data-chat-label', $libchat.attr('data-chat-label'))
            .attr('data-chat-offline-label', $libchat.attr('data-chat-offline-label'));
    } else {
        // libchat did not insert a button. show our manual one
        $('[data-libchat-alt]').show();
        // libchat may have done something else. remove anything it may have created
        $('[id^=s-lch-]').remove();
        $('[data-libchat-container]').remove();
        $('.lcs_slide_out').remove();
    }
});