/* eslint no-console: 0 */

import { createApp } from 'vue/dist/vue.esm-bundler'
import RefinableResults from '../vue_components/refinable_results.vue'
import Facet from '../vue_components/facet.vue'
require('./application')

document.addEventListener('DOMContentLoaded', () => {
  const app = createApp({
    data(){
      return {};
    },
    components: { RefinableResults, Facet }
  });
  app.config.unwrapInjectedRef = true
  app.mount('[data-refinable-results-app]');

});
