# frozen_string_literal: true

# :nodoc:
module WelcomeHelper
  def render_institution_type_help(institution_type)
    case institution_type
    when 'highered'
      render 'welcome/help/highered'
    when 'k12'
      render 'welcome/help/k12'
    when 'publiclib'
      render 'welcome/help/publiclib'
    else
      render 'welcome/help/default'
    end
  end

  def instruction_page_title
    @selected.name
  end

  def wayfinder_placeholder(type)
    case type
    when 'k12' then 'Your Georgia School'
    when 'publiclib' then 'Your Georgia Public Library'
    when 'highered' then 'Your Georgia College or University'
    else
      'Enter school or library name.'
    end
  end
end
