# frozen_string_literal: true

# common methods for Controllers that need to login via API to the OpenAthens proxy
module OpenAthensProxyHelper
  class OpenAthensProxySessionsError < StandardError; end

  def open_athens_proxy_logout
    HTTParty.get open_athens_logout_url
  rescue SocketError, Errno::ECONNREFUSED, Net::OpenTimeout => _e
    # Log errors only as needed
  end

  # @param [Hash] options The options hash.
  # @option options [String] :return_url to come back to galileo
  # @option options [String] :return_data to go on to a resource
  def open_athens_proxy_login(options={})
    resp = HTTParty.post open_athens_proxy_connection_uri,
      {
        :headers => open_athens_api_headers,
        :body => open_athens_body(options)
      }
    raise OpenAthensProxySessionsError, resp.message unless resp.code == 200
    JSON.parse(resp.body)['sessionInitiatorUrl']
    rescue SocketError, Errno::ECONNREFUSED => _e
      # Warning: logging errors here may blow up log file. Use only as needed
      return
    rescue OpenAthensProxySessionsError => e
      if FeatureFlags.enabled?(:open_athens_proxy_error_notification)
        ExceptionNotifier.notify_exception( e, data: { response: resp } )
      end
      return
  end

  def open_athens_proxy
    user_solr_institution.oa_proxy
  end

  private

  def user_solr_institution
    InstitutionService.new.find(user.inst.code)
  end

  def open_athens_proxy_org
    solr_inst = user_solr_institution
    return unless solr_inst.oa_proxy
    solr_inst.oa_proxy_org || user.inst.code
  end

  def open_athens_api_headers
    {
        "Content-Type" => "application/vnd.eduserv.iam.auth.localAccountSessionRequest+json",
        :Accept => "application/json",
        :Authorization => "OAApiKey #{open_athens_proxy_api_key}"
    }
  end

  def open_athens_body(options={})
    inst_code = open_athens_proxy_org
    body = {
      connectionID: open_athens_proxy_connection_id,
      uniqueUserIdentifier: inst_code,
      displayName: inst_code,
      attributes: {
        InstitutionID: inst_code
      }
    }
    if options[:return_url]
      body[:returnUrl] = options[:return_url]
    elsif options[:return_data]
      body[:returnData] = options[:return_data]
    else
      body[:returnUrl] = root_url
    end
    body.to_json
  end

  def open_athens_proxy_api_key
    Rails.application.credentials.open_athens[:oaproxy_api_key]
  end

  def open_athens_proxy_connection_id
    Rails.application.config.open_athens_proxy_connection_id
  end

  def open_athens_proxy_connection_uri
    "https://login.openathens.net/api/v1/wa.galileo.usg.edu/organisation/72175461/local-auth/session"
  end

  def open_athens_logout_url
    "https://login.openathens.net/signout"
  end

  def path_to_url(path)
    root_url.sub(/\/$/, '') + path
  end

end
