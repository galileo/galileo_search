# frozen_string_literal: true

# :noodoc:
module BlacklightHelper
  include Blacklight::BlacklightHelperBehavior

  # # generates a link without Blacklight tracking params added
  # def link_to_document(doc, _ = nil, _ = {})
  #   resource_link = link_to doc.title, url_for_document(doc)
  #   info_link = link_to 'Info', search_state.url_for_document(doc, {}), class: 'badge badge-primary'
  #   resource_link + ' ' + info_link
  # end

  # @param [Resource] document
  # @return [String, NilClass]
  def show_branding_area?(document)
    document.branding_image_key || document.branding_note
  end

  def branding_image(document)
    image_tag @image_service.url_for(document.branding_image_key),
              class: 'img-fluid', alt: document.branding_note
  end

  def strip_html(obj)
    stripped_value = strip_tags obj[:value].first
    decode_html_symbols(stripped_value)
  end

  def sanitize_html(obj)
    sanitize(obj[:value].first,
             tags: %w[strong em a p br],
             attributes: %w[href])
  end

  def linkify(obj)
    link_to nil, obj[:value].first
  end

  # returns a Resource URL sensitive to user's auth context
  def link_to_resource(doc, value = nil)
    label = value || doc.title
    link_to label, doc.local_express_link
  end

  def resource_thumbnail(resource, _)
    image_url = if resource.thumbnail_key.present?
                  @image_service.url_for(resource.thumbnail_key)
                else
                  image_path 'thumbnail-glri-default.png'
                end
    image_tag image_url, alt: "Thumbnail for #{resource.title} search result",
              class: 'img-fluid'
  end

  def thumbnail_link(obj)
    if obj.present?
      @image_service.url_for(obj[:value].first)
    else
      image_path 'thumbnail-glri-default.png'
    end
  end

  # Format array into a set of Bootstrap badges
  # @param [Array<String>] array
  # @return [String]
  def format_array_field_as_badges(obj)
    return '' unless obj.present?

    badges = obj[:value].sort.map do |value|
      badge value, obj[:field]
    end
    badges.join(' ').html_safe
  end

  # Tag for a primary badge
  # @param [String] value
  def badge(value, field)
    badge_link = if %w[format_sms subject_sms keywords_sms].include? field
                   institution_databases_path(inst_code: params[:inst_code], view: @view, f: Hash[field.to_sym, [value]])
                 else
                   institution_databases_path(inst_code: params[:inst_code], view: @view, q: value)
                 end
    "<a href='#{badge_link}'><span class='badge badge-primary'>#{value}</span></a>".html_safe
  end
end
