# frozen_string_literal: true

# Helpers for A to Z header
module AToZHelper

  # @param [ActionController::Parameters] params
  # @return [Array<String>] letter labels to display as active
  def active_letters(params)
    if params[:f] && params[:f]["resource_name_first_char_ss"]
      params[:f]["resource_name_first_char_ss"]
    else
      ['All']
    end
  end
end
