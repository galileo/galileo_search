# frozen_string_literal: true

# Helper methods for Contact Form stuff
module ContactHelper
  # @return [Array<Array>]
  def support_form_selections
    [
      [t('contact_forms.labels.support_form_selections.access_issue'), 'Access Issue'],
      [t('contact_forms.labels.support_form_selections.bento_search'), 'Bento Search'],
      [t('contact_forms.labels.support_form_selections.discover_galileo_eds'), 'Discover GALILEO (EDS)'],
      [t('contact_forms.labels.support_form_selections.open_athens'), 'OpenAthens'],
      [t('contact_forms.labels.support_form_selections.glri'), 'GLRI'],
      [t('contact_forms.labels.support_form_selections.alma'), 'Alma'],
      [t('contact_forms.labels.support_form_selections.ip_change'), 'IP Change'],
      [t('contact_forms.labels.support_form_selections.usage_statistics'), 'Usage Statistics'],
      [t('contact_forms.labels.support_form_selections.in_the_spotlight'), 'In the Spotlight'],
      [t('contact_forms.labels.support_form_selections.other'), 'Other']
    ]
  end

  # @return [Array<Array>]
  def other_form_selections
    [
      [t('contact_forms.labels.other_form_selections.marketing'), 'Marketing'],
      [t('contact_forms.labels.other_form_selections.feedback'), 'Feedback'],
      [t('contact_forms.labels.other_form_selections.licensing'), 'Licensing'],
      [t('contact_forms.labels.other_form_selections.training'), 'Training'],
      [t('contact_forms.labels.other_form_selections.content_recommendation'), 'Content Recommendation'],
      [t('contact_forms.labels.other_form_selections.policy'), 'Policy'],
      [t('contact_forms.labels.other_form_selections.other'), 'Other']
    ]
  end

  # @return [Array<Array>]
  def institution_selections
    InstitutionService.new.all_names_with_codes
  end

  # @return [Array<Array>]
  def resource_selections(institution_code)
    ResourceService.new(institution_code).all_names_with_codes
  end

  def service_name(formtype)
    case formtype
    when 'support', 'other'
      'GALILEO'
    when 'nge'
      'New Georgia Encyclopedia'
    when 'gkr'
      'Georgia Knowledge Repository'
    when 'dlg'
      'Digital Library of Georgia'
    when 'crdl'
      'Civil Rights Digital Library'
    when 'ghnp'
      'Georgia Historic Newspapers'
    else
      'GALILEO'
    end
  end

  def legacy_login_status( auth_type, remote )
    case auth_type.to_s
    when 'ip'
      ' (login.via.ip)'
    when 'passphrase'
      ' (login.via.password)'
    when 'geo'
      ' (login.via.ip2loc)'
    when 'pines'
      ' (login.via.pines)'
    when 'oa'
      " (login.via.openathensid_#{remote ? 'remote' : 'ip'})"
    when 'lti'  # TODO: GIL login
      " (login.via.d2lid_#{remote ? 'remote' : 'ip'})"
    else
      ''
    end
  end

  def valid_mime_types
    %w[
      image/png image/gif image/jpeg
      application/msword application/pdf text/plain
      application/vnd.openxmlformats-officedocument.wordprocessingml.document
      application/vnd.ms-excel text/csv
      application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
      application/zip
    ].join(',')
  end

end
