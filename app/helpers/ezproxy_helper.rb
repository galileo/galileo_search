# frozen_string_literal: true

# common methods for Controllers that need to modify EZp cookies
module EzproxyHelper
  class EZproxySessionsError < StandardError; end

  def remove_ezproxy_cookie
    # Logout user from EZProxy
    HTTParty.get ezp_logout_url, follow_redirects: false
  rescue SocketError, Errno::ECONNREFUSED, Net::OpenTimeout => _e
    # Warning: logging errors here may blow up log file. Use only as needed
    # TODO: Retry logic for Net::OpenTimeout
  ensure
    cookies.delete :ezproxy, domain: :all
  end

  # Check for existing cookie
  def login_to_proxy?
    !cookies[:ezproxy] && user.inst.proxy
  end

  def ezp_login_url(passphrase)
    # TODO: might the proxy come with http prefix?
    "http://#{user.inst.proxy}/login?user=#{passphrase}"
  end

  def ezp_logout_url
    "http://#{user.inst.proxy}/logout"
  end

  def ezp_login_and_set_cookie
    resp = HTTParty.get ezp_login_url(passphrase), follow_redirects: false
    ezp_cookies = parse_cookie(resp)
    resp = HTTParty.get ezproxy_redirect(resp), cookies: ezp_cookies
    set_ezproxy_cookie(ezp_cookies)

    rescue SocketError, Errno::ECONNREFUSED => _e
      # Warning: logging errors here may blow up log file. Use only as needed
      return
    rescue EZproxySessionsError => e
      if FeatureFlags.enabled?(:ezproxy_error_notification)
        ExceptionNotifier.notify_exception( e, data: { response: resp } )
      end
      return
  end

  def ezproxy_redirect(resp)
    ezp_redirect = resp.response['location']
    unless ezp_redirect
      raise EZproxySessionsError,
          "#{request.remote_ip} - #{user.inst.code} - Can't finish EZproxy login?"
    end
    ezp_redirect
  end

  def set_ezproxy_cookie(ezp_cookies)
    ezp_session = ezp_cookies[:ezproxy]
    unless ezp_session
      raise EZproxySessionsError,
          "#{request.remote_ip} - #{user.inst.code} - No session value from EZproxy, are we hitting max sessions?"
    end
    cookies[:ezproxy] = { value: ezp_session,
                          domain: :all,
                          secure: false,
                          same_site: :lax }
  end

  private

  def passphrase
    ezp_logger = Logger.new("#{Rails.root}/log/ez_proxy_logger.log")
    inst = InstitutionService.new.find user.inst.code
    passphrase = inst.passphrase
    if passphrase != user.inst.pass
      message = "#{request.remote_ip} - session_password:#{user.inst.pass}, current_passphrase: #{passphrase}, EZProxy login url: #{ezp_login_url(passphrase)}"
      ezp_logger.info message
    end
    passphrase
  end

  # set-cookie looks like: ezproxy=1gNcvflTC1RKVgI; Path=/; Domain=.galileo.usg.edu
  def parse_cookie(resp)
    unless resp.get_fields('Set-Cookie')
      raise EZproxySessionsError,
          "#{request.remote_ip} - #{user.inst.code} - No 'Set-Cookie' field in response from EZproxy, are we hitting max sessions?"
    end
    cookie_hash = HTTParty::CookieHash.new
    resp.get_fields('Set-Cookie').each { |c| cookie_hash.add_cookies(c) }
    cookie_hash
  end
end
