# frozen_string_literal: true

# :nodoc:
module LinkHelper

  def open_athens_conditions_met?(user,resource)
    user.auth_type.to_sym == :oa && user.remote? && resource.open_athens_link.present? && !(resource.bypass_open_athens?)
  end

  def auth_context_url_for(user,resource)
    if open_athens_conditions_met?(user,resource)
      resource.open_athens_link
    elsif user.remote?
      resource.remote_link || resource.ip_link
    else
      resource.ip_link
    end
  end

end
