# frozen_string_literal: true

module EdsResultFormatHelper
  # @param [EBSCO::EDS::Record] eds_result
  # @return [String, nil] formatted date (e.g., 'May 15, 1994'; 'May 1994', '1994', or nil) depending on available precision
  #
  # EDS results come with a structured date (available as a YYYY-MM-DD string from eds_publication_date or in hash form.
  # The date hash might also contain a property called 'Text' which contains an un-standardized date string,
  # e.g., anything from "May 5, 1994" to "19940507" to "Spring1994" to "mayo-junio94" if present at all.
  #
  # We'd just use the structured date but for one issue: for items with blank day or month (e.g, a periodical just dated
  # to May 1994 has a blank day) EDS uses 1 to represent the blank. If we err on the side of caution and treat all 1s
  # as blank, we can never display a precise date for any article that actually was published on the first of the month.
  #
  # So, we try to parse the unstructured date string with a number of common formats. If any results in a date that
  # agrees with the structured date, we return that date, formatted with full precision. Otherwise we fall back on the
  # structured date, treating 1s cautiously as blanks (e.g., 1994-05-01 is just rendered 'May 1994').
  #
  # In other words, the logic that parses the unstructured date strings should only affect records where the structured
  # date is the first of the month or year.
  def best_available_publication_date(eds_result)
    dates = eds_result.instance_variable_get('@bib_part')&.dig 'BibEntity', 'Dates'
    date_hash = dates&.find { |d| d['Type'] == 'published' }
    return eds_result.eds_publication_year if date_hash.nil?

    year = eds_result.eds_publication_year.to_i
    y = date_hash['Y']&.to_i
    return eds_result.eds_publication_year if y != year # Weird situation. Just return year.

    month = date_hash['M']&.to_i
    day = date_hash['D']&.to_i
    text = date_hash['Text']
    if text.present?
      check_date = Date.new year, month, day
      formatted = try_parse_date_text(text, check_date)
      return formatted if formatted.present?
    end
    date_from_ambiguous_parts(year, month, day)
  end

  # @param [EBSCO::EDS::Record] record
  # @return [Boolean]
  def publication_title_is_redundant?(record)
    if record.eds_title.blank? || record.eds_source_title.blank?
      false
    else
      normalize_title(record.eds_source_title).start_with? normalize_title(record.eds_title)
    end
  end

  # @param [EBSCO::EDS::Record] record
  # @return [Boolean]
  def from_custom_catalog?(record)
    (record&.eds_database_id&.match? /^cat\d+\w?$/) || false
  end

  # @param [EBSCO::EDS::Record] record
  # @return [Boolean]
  def from_gkr?(record)
    record&.eds_database_id == 'ir00501a'
  end

  # @param [EBSCO::EDS::Record] record
  # @return [Boolean]
  def from_dlg?(record)
    record&.eds_database_id == 'ir01612a'
  end

  # @param [EBSCO::EDS::Record] record
  # @return [Boolean]
  def whitelist_catalog_links?(record)
    from_custom_catalog?(record) || from_gkr?(record) || from_dlg?(record)
  end

  private

  def normalize_title(title)
    (title.downcase.gsub /\W+/, ' ').strip
  end

  def date_from_ambiguous_parts(year, maybe_month, maybe_day)
    day = month = nil
    if maybe_day.present? && maybe_day > 1
      day = maybe_day
      month = maybe_month
    elsif maybe_month.present? && maybe_month > 1
      month = maybe_month
    end
    date_from_parts(year, month, day)
  end

  def date_from_parts(year, month, day)
    if year.nil?
      nil
    elsif month.nil?
      year.to_s
    elsif day.nil?
      format_month Date.new(year, month)
    else
      format_date Date.new(year, month, day)
    end
  rescue Date::Error
    nil
  end

  def try_parse_date_text(input_str, check_date)
    try_ambiguous_format('%Y%m%d', input_str, check_date) ||
      try_date_format('%Y-%m-%d', input_str, check_date) ||
      try_date_format('%b %d, %Y', input_str, check_date) ||
      try_date_format('%B %e, %Y', input_str, check_date) ||
      try_date_format('%m/%d/%Y', input_str, check_date) ||
      try_date_format('%m-%d-%Y', input_str, check_date) ||
      try_date_format('%d %b %Y', input_str, check_date) ||
      try_date_format('%d %B %Y', input_str, check_date) ||
      try_month_format('%B %Y', input_str, check_date) ||
      try_month_format('%b %Y', input_str, check_date) ||
      try_month_format('%b%Y', input_str, check_date) ||
      try_month_format('%b%y', input_str, check_date)
  end

  def try_ambiguous_format(format, input_str, match_date)
    date = Date.strptime input_str, format
    date_from_ambiguous_parts(date.year, date.month, date.day) if date == match_date
  rescue Date::Error
    nil
  end

  private

  def format_date(date)
    date.strftime('%B %e, %Y')&.gsub(/\s+/, ' ')
  end

  def format_month(date)
    date.strftime('%B %Y')
  end

  def try_date_format(format, input_str, match_date)
    date = Date.strptime input_str, format
    format_date(date) if date == match_date
  rescue Date::Error
    nil
  end

  def try_month_format(format, input_str, match_date)
    date = Date.strptime input_str, format
    format_month(date) if date == match_date
  rescue Date::Error
    nil
  end

  def try_year_format(input_str, format, match_date); end

  # @param [EBSCO::EDS::Record] eds_record
  # @param [Hash] possible_collections -- array of possible collection names for each database name
  # @return [String] collection name if possible, otherwise database name
  def collection_or_database_title(eds_record, possible_collections)
    database_name = eds_record.eds_database_name
    return database_name if possible_collections.nil?

    collections = possible_collections[database_name]
    return database_name if collections.nil? || collections.none?
    return collections[0] if collections.size == 1

    collections.each do |collection_name|
      return collection_name if eds_record.eds_source_title.include? collection_name
      return collection_name if eds_record.eds_extras_NoteTitleSource.include? collection_name
    rescue NoMethodError
      # Ignored
    end
    database_name
  end

end
