# frozen_string_literal: true

# :nodoc:
module BentoHelper

  def tabbed_bento_view?
    FeatureFlags.enabled?(:bento_tabs) && [:middle, :highschool].include?(@view&.to_sym)
  end

  # this just passes through the plain content if the link url is blank
  def possible_link(content, destination, *args)
    return nil if content.blank?
    return content if destination.blank?
    link_to content, destination, *args
  end

  # This will set aria-hidden=true if alt is not present
  # If fallback alt text is available, it will use it but will still set aria-hidden=true
  def possible_image(image_url, options = {})
    return nil unless image_url
    alt = options[:alt]
    fallback_alt = options.delete :fallback_alt
    if alt.blank?
      options[:alt] = fallback_alt if fallback_alt.present?
      options[:'aria-hidden'] = 'true'
    end
    options[:onerror] = "this.style.display='none'" # DLG especially needs this, but it's nice when dealing with images
                                                    # from unreliable services in general.
    image_tag image_url, options
  end

  def possible_content_tag(tag_name, content, *args)
    return nil if content.blank?
    content_tag tag_name, content, *args
  end

  def font_awesome_icon(identifier, solid=true)
    return nil unless identifier.present?
    "<i class=\"#{solid ? 'fas' : 'fao'} fa-#{identifier}\"></i>".html_safe
  end

  def bento_button(bento_result_button)
    link_to(
      bento_result_button.link_for_inst(user.inst.code),
      {
        class: bento_result_button.is_image? ? "" : "btn btn-sm btn-outline-primary",
        type: bento_result_button.is_image? ? "" : "button",
        target: "_blank", # open in new tab
      }
    ) do
      if bento_result_button.is_image?
        image_tag bento_result_button.image, alt: bento_result_button.tooltip
      else
        icon = font_awesome_icon(bento_result_button.font_awesome_icon)
        # Note that concatenating HTML-safe + -unsafe strings will automatically escape the unsafe part
        (icon || ''.html_safe) + bento_result_button.label
      end
    end
  end

  def collapse_excess(items, over: nil, &block)
    return if items.nil?
    uncollapsed = items[..(over && over - 1)]
    collapsed = over && items[over..]
    uncollapsed.each &block
    return unless collapsed&.any?
    concat(content_tag(:div, class: 'more-links') do
      collapsed&.each &block
    end)
    concat content_tag(:p, (
             link_to("Show #{ collapsed.size } more...", '#', data: { 'show-more' => 1}) +
             link_to('Show fewer', '#', data: {'show-fewer' => 1}))
           )
  end

end
