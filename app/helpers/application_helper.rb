# frozen_string_literal: true

# :nodoc:
module ApplicationHelper

  # @return [String]
  def oa_sub_id
    client_id = Rails.application.credentials.open_athens[:client_id]
    client_id.sub 'galileo.usg.edu.oidc-app-v1.', ''
  end

  # @return [String]
  def oa_wayfless_link(institution, destination = nil)
    return nil unless institution.open_athens

    destination ||= institution_homepage_url(inst_code: institution.code)
    "https://connect.openathens.net/galileo.usg.edu/#{oa_sub_id}/login?entity=#{institution.oa_entity_id}&target=#{ERB::Util.url_encode(destination)}"
  end

  # @param [String] destination
  # @return [String]
  def oa_proxy_link(destination = nil)
    "https://proxy.openathens.net/login?entityID=https://idp.wa.galileo.usg.edu/entity&qurl=#{ERB::Util.url_encode(destination)}"
  end

  # The Open Athens redirector link uses the API Name.  In most cases
  # this is the same as the Scope.  But there can be exceptions.
  # @param [SolrInstitution] institution
  # @param [String] destination
  # @return [String (frozen)]
  def oa_redirector_url(institution, destination)
    api_name = if institution.oa_api_name.present?
                 institution.oa_api_name
               else
                 institution.oa_scope
               end

    "https://go.openathens.net/redirector/#{api_name}?url=#{ERB::Util.url_encode(destination)}"
  end

  # @return [String]
  def passphrase_input_placeholder
    if @institution
      "Password for #{@institution.name}"
    else
      'Your GALILEO password'
    end
  end

  # @param [User::Institution nil] inst
  # @return [Bool]
  def institution_display_views?(inst)
    return false unless inst
    inst.type.to_sym == :k12 || inst.type.to_sym == :publiclib
  end

  # @return [Bool]
  def display_view_switcher?
    institution_display_views?(@institution) && @available_views && @available_views.size > 1
  end

  def inst_view_label
    return unless user&.inst
    view_label = view_type_label_new_header
    if institution_display_views?(@institution) && view_label.present?
      "#{user.inst.name} #{view_label}"
    else
      user.inst.name
    end
  end


  # Indicate if view selector modal should be displayed
  # @param [User] user
  # @return [Bool]
  def popup_view_modal?(user)
    return false if view_already_selected?(user)

    institution_display_views?(user.inst)
  end

  # @param [User] user
  # @return [Bool]
  def view_already_selected?(user)
    if cookies[:galileo_search_preferred_view] ||
       session[:flash] || view_in_user_site?(user)
      true
    else
      false
    end
  end

  # @param [User] user
  # @return [Bool]
  def view_in_user_site?(user)
    return false unless user.site

    user.site.type.in? %w[elem midd highschool]
  end

  # @param [String] id CSS ID to skip to
  def skip_link_to(id, label = nil)
    label ||= t('blacklight.skip_links.main_content')
    link_to label, id,
            class:
              'element-invisible element-focusable rounded-bottom py-2 px-3',
            data: { turbolinks: 'false' }
  end

  # @return [Bool]
  def show_whats_new_button?
    if controller_name == 'welcome' && action_name == 'index'
      return true
    end
    if controller_name == 'homepage' && action_name == 'show'
      return true
    end

    false
  end

  # @return [Bool]
  def show_tour_button?
    # this tour button starts an intro.js tour that includes all elements with data-intro, including ones that are
    # in the header/footer and thus common to all pages. To exclude these, use show_limited_tour_button?
    if controller_name == 'homepage' && (action_name == 'show' || action_name == 'bento_home')
      return true
    end

    false
  end

  def show_limited_tour_button?
    # the limited tour won't include any elements from the header or footer
    if controller_name == 'custom_bento' && (action_name == 'bento_search' || action_name == 'full_results')
      return true
    end

    false
  end

  def tour_button_tooltip
    I18n.t('tour_button_tooltips', default: {}).dig(controller_name.to_sym, action_name.to_sym) || I18n.t('tour_button_tooltips.default')
  end

  def faq_link
    I18n.t('faq.links', default: {}).dig(controller_name.to_sym, action_name.to_sym) || I18n.t('faq.links.default')
  end

  # @return [String]
  def sanitize_resource_code(resource_code)
    resource_code.gsub(':', '-')
  end

  # @return [String]
  def decode_html_symbols(any_string)
    return '' if any_string.blank?

    Nokogiri::HTML.parse(any_string).text
  end

  # @return [String]
  def parse_libchat_code(libchat_url)
    uri = URI.parse(libchat_url)
    uri.path.split('/').last
  end

end
