module BannerHelper

  # @param [Array<BannerSolrDocument>] banners
  # @param [Symbol] banners
  def show_banners(banners, current_view = @view)
    banners.map do |banner|
      if banner.user_view_codes.empty? || banner.user_view_codes.include?(current_view.to_s)
        banner_selector(banner)
      end
    end.join.html_safe
  end

  def banner_selector(banner)
    if banner['dismissible']
      dismissible_banner(banner)
    else
      normal_banner(banner)
    end
  end

  # @param [BannerSolrDocument] banner
  def normal_banner(banner)
    "<div class=\"alert alert-#{banner.style} mb-0 show text-center\" role=\"alert\">
    #{banner.message}
    </div>".html_safe
  end

  # @param [BannerSolrDocument] banner
  def dismissible_banner(banner)
    "<div class=\"alert alert-#{banner.style} mb-0 alert-dismissible fade show text-center\" role=\"alert\">
      <strong>#{banner.message}</strong>
      <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-label=\"Close\">
        <span aria-hidden=\"true\">&times;</span>
      </button>
   </div>".html_safe
  end


  def outage_banner(startDate, endDate, message)
    today = Date.current
    startDate = Date.parse(startDate)
    endDate = Date.parse(endDate)
    if today.between?(startDate,endDate)
      message = "<div class=\"banner alert-danger\">
                <div class=\"container\">
                  <div class=\"row\">
                    <div class=\"col-sm-12 text-center\">
                      #{message}
                    </div>
                  </div>
                </div>
              </div>"
      message.html_safe
    end
  end
  def survey_banner(startDate, endDate, message)
    today = Date.current
    startDate = Date.parse(startDate)
    endDate = Date.parse(endDate)
    if today.between?(startDate,endDate)
      message = "<div class=\"alert survey alert-success alert-dismissible fade
                  show text-center\" role=\"alert\">
                  #{icon('fas', 'clipboard-check', class: 'fa-lg')}
                  <strong>#{message}</strong>
                 <button type=\"button\" class=\"close\" data-dismiss=\"alert\"
                  aria-label=\"Close\">
                 <span aria-hidden=\"true\">&times;</span>
                 </button>
                 </div>"
      message.html_safe
    end
  end
end