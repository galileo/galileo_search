# frozen_string_literal: true

# Helper methods for Embeddable search box
module EmbedHelper
  def eds_for(profile = nil)
    eds = 'zbds'
    return eds unless profile

    "zbds:#{profile}"
  end

  def galileo_homepage_link(inst_code = nil, target = '_parent')
    destination = inst_code ? express_url(inst: inst_code) : root_path
    link_to 'www.galileo.usg.edu', destination, target: target
  end

  # TODO: decide if we'll support custom headings (like we used to)
  def heading_from(heading = nil)
    tagline = 'Imagine. Discover. Explore.'
    heading = if !heading
                "#{tagline}"
              elsif heading =~ /<h/
                # assume it's styled
                heading
              else
                  "<div class='col-12 text-center galileo-tagline'>#{heading}</div>"
              end
    heading.html_safe
  end

  def advanced_search_url(profile, inst_code = nil)
    express_url params: { instcode: inst_code,
                          advanced: 1,
                          profile: profile }
  end

  def bento_type_input(value, view = nil, inst_code = nil, type = nil)
    label = ConfiguredBentoService.get(inst_code, view, value)&.display_name
    name = 'bento_type'
    id = if type
           "#{type}-#{name}"
         else
           name
         end
    label_tag name, {class: 'form-check-label', for: id} do
      check_box_tag(name, value, true, id: id, class: 'form-check-input') + label + " Bento"
    end
  end

  def full_text_input(value, type = nil)
    label = 'Full Text'
    name = 'full_text'
    id = if type
           "#{type}-#{name}"
         else
           name
         end
    conditional_field value, name, label, id
  end

  def peer_reviewed_input(value, type = nil)
    label = 'Scholarly (Peer Reviewed) Journals'
    name = 'peer_reviewed'
    id = if type
           "#{type}-#{name}"
         else
           name
         end
    conditional_field value, name, label, id
  end

  def catalog_only_input(value, type = nil)
    label = 'Catalog Only'
    name = 'catalog_only'
    id = if type
           "#{type}-#{name}"
         else
           name
         end
    conditional_field value, name, label, id
  end

  def library_collection_input(value, type = nil)
    label = 'Available in Library Collection'
    name = 'library_collection'
    id = if type
           "#{type}-#{name}"
         else
           name
         end
    conditional_field value, name, label, id
  end

  def conditional_field(value, name, label, id)
    case value
    when 'hidden'
      hidden_field_tag name, 'on', id: id
    when 'unselected'
      label_tag name, {:class => 'form-check-label', for: id} do
        check_box_tag(name, nil, false, id: id, class: 'form-check-input') + label
      end
    when 'selected'
      label_tag name, {:class => 'form-check-label', for: id} do
        check_box_tag(name, nil, true, id: id, class: 'form-check-input') + label
      end
    end
  end

  def disciplines_line_break(config)
    if config.fetch_values(:bento_type, :full_text, :peer_reviewed, :catalog_only, :library_collection).compact.any?
      "<br>\n".html_safe
    else
      ""
    end
  end

  def disciplines_label(label)
    (label || 'Select subject(s)').html_safe
  end

  def disciplines_options_from(config, type = nil)
    array_of_options = []
    selected = []
    config[:disciplines].each do |d|
      code = d[:code][0..3]
      label = disciplines[code.to_sym]
      next unless label

      selected << code if d[:selected]
      id = if type
             "#{type}-#{code}"
           else
             code
           end
      array_of_options << [label, code, { id: id }]
    end
    options_for_select(array_of_options, selected: selected)
  end

  # @return [Hash{Symbol->String (frozen)}]
  def disciplines
    {
      agri: 'Agriculture and Agribusiness',
      anat: 'Anatomy and Physiology',
      anth: 'Anthropology',
      appl: 'Applied Sciences',
      arch: 'Architecture',
      arts: 'Arts and Entertainment',
      astr: 'Astronomy and Astrophysics',
      biog: 'Biography',
      biol: 'Biology',
      biot: 'Biotechnology',
      bota: 'Botany',
      busi: 'Business and Management',
      calm: 'Complementary and Alternative Medicine',
      chem: 'Chemistry',
      cohe: 'Consumer Health',
      comm: 'Communication and Mass Media',
      comp: 'Computer Science',
      cons: 'Construction and Building',
      danc: 'Dance',
      dent: 'Dentistry',
      dipl: 'Diplomacy and International Relations',
      dram: 'Drama and Theater Arts',
      eart: 'Earth and Atmospheric Sciences',
      econ: 'Economics',
      educ: 'Education',
      engi: 'Engineering',
      envi: 'Environmental Sciences',
      ethn: 'Ethnic and Cultural Studies',
      film: 'Film',
      fore: 'Forestry',
      geog: 'Geography and Cartography',
      geol: 'Geology',
      heal: 'Health and Medicine',
      hist: 'History',
      info: 'Information Technology',
      lang: 'Language and Linguistics',
      lawx: 'Law',
      libr: 'Library and Information Science',
      life: 'Life Sciences',
      lite: 'Literature and Writing',
      mark: 'Marketing',
      math: 'Mathematics',
      mili: 'Military History and Science',
      mini: 'Mining and Mineral Resources',
      musi: 'Music',
      nurs: 'Nursing and Allied Health',
      nutr: 'Nutrition and Dietetics',
      ocea: 'Oceanography',
      phar: 'Pharmacy and Pharmacology',
      phys: 'Physics',
      ptot: 'Physical Therapy and Occupational Therapy',
      posc: 'Political Science',
      pogv: 'Politics and Government',
      powe: 'Power and Energy',
      psyc: 'Psychology',
      publ: 'Public Health',
      reli: 'Religion and Philosophy',
      scie: 'Science',
      socy: 'Sociology',
      sowo: 'Social Work',
      spls: 'Sports and Leisure',
      spme: 'Sports Medicine',
      sshu: 'Social Sciences and Humanities',
      tech: 'Technology',
      vetm: 'Veterinary Medicine',
      visu: 'Visual Arts',
      wofe: "Women's Studies and Feminism",
      zool: 'Zoology'
    }
  end
end
