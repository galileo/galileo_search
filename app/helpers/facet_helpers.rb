# frozen_string_literal: true

# Helpers for facet links and behaviors
module FacetHelpers
  # @param [String] facet_id from catalog_controller config
  # @param [String] label to display
  def facet_modal_link(facet_id, label)
    safe_label = label.html_safe
    link_to(safe_label, search_facet_path(controller: :catalog, id: facet_id, 'facet.sort': :index),
            data: { 'blacklight-modal': 'trigger' }, class: 'card-link')
  end
end