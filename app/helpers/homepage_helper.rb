# frozen_string_literal: true

# :nodoc:
module HomepageHelper

  # construct url for eds advanced search link
  def discovery_advanced_search_url
    express_path(params: {
                   instcode: user.inst.code, advanced: 1,
                   profile: eds_profile_for(@institution)
                 })
  end

  # get appropriate URL for "Journal List" link
  # typically expect a Primo URL for highered
  # otherwise a EBSCO Fulltext Finder express link (use link for now?)
  # @param [User::Institution] inst
  # @return [String] absolute or relative url
  def url_for_journals(inst)
    val = inst.journals_link
    return val if val =~ /http/

    link_path('zbft')
  end

  # @param [Hash] feature
  # @return [String] absolute or relative url
  def spotlight_image_url(feature)
    feature_image = feature.dig :image

    feature_image_url = @image_service.url_for feature_image

    feature_image_url.blank? ? image_path('spotlight-default.png') : feature_image_url
  end

  def feature_image_url(feature, image_service = @image_service)
    feature_image_url = image_service.url_for feature.image_key

    feature_image_url.blank? ? image_path('spotlight-default.png') : feature_image_url
  end

  # @param [Hash] feature
  # @return [String] relative url
  def spotlight_link_url(feature)
    feature_link = feature.dig(:link).strip

    if feature_link =~ /express.*link=(?<code>#{ValidationService.resource_code_regx})/
      if @institution
        ResourceService.new(@institution.code).find(Regexp.last_match(:code))&.local_express_link
      else
        link_path Regexp.last_match(:code)
      end
    else
      feature_link
    end
  end

  # @return [Bool]
  def elementary_view?
    @view&.to_sym == :elementary
  end

  # @param [User::Institution] inst
  # @return [Array] features
  def institution_view_features(inst)
    inst.features(@view)
  end

  # @param [Array<Feature>] features
  # @return [Array<Feature>]
  def displayed_features(features, feature_count: Feature::FEATURE_COUNT)
    return [] if features.empty?

    inst_features = features.find_all{|feature| feature.institution?}
    inst_group_features = features.find_all{|feature| feature.institution_group?}

    features = (1..feature_count).map do |i|
      inst_feature = inst_features.find{|feature| feature.position == i}
      inst_group_feature = inst_group_features.find{|feature| feature.position == i}
      inst_feature || inst_group_feature
    end
    features.compact
  end


  # @param [String,Symbol] view
  # @return [String] view_type_label
  def view_type_label_new_header(view = @view)
    case view&.to_sym
    when :elementary then '| Elementary School'
    when :middle then '| Middle School'
    when :highschool then '| High School'
    when :educator then '| Educator'
    when :full then '| Full'
    else
      ''
    end
  end

  # @param [String,Symbol] view
  # @return [String] view_type_label
  def view_type_label(view = @view)
    case view&.to_sym
    when :elementary then '- Elementary School'
    when :middle then '- Middle School'
    when :highschool then '- High School'
    when :educator then '- Educator'
    when :full then '- Full'
    else
      ''
    end
  end

  # @param [User::Institution] inst
  # @return [String] eds_profile
  def eds_profile_for(inst)
    DiscoverQueryService.eds_profile_for inst, @view
  end

  # @return [String] css class
  def hero_image_class
    case @view&.to_sym
    when :elementary
      'kids-hero'
    when :middle
      'middle-hero'
    when :highschool
      'hs-hero'
    else
      'default-hero'
    end
  end

  def show_bento_toggle?
    (request.parameters[:controller] == 'homepage' && %w[bento_home show].include?(request.parameters[:action])) ||
      request.parameters[:controller] == 'bento' && request.parameters[:action] == 'bento_search'
  end

  def branding_destination_url
    return root_path unless user

    institution_homepage_path(user.inst.code, view: @view)
  end

  def color_for_kids_spotlight(index)
    index = index % 6
    %w[blue green orange orange blue green][index]
  end

end
