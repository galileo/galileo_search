# frozen_string_literal: true

# :nodoc:
module ToolsSectionHelper

  K12_VIEWS = %i[elementary middle highschool educator]


  def available_k12_views
    K12_VIEWS & (@available_views || [])
  end

  # @return [Array<Hash>]
  def sharable_links_k12_homepage_data
    available_k12_views.map do |view|
      {
        id: "homepage-#{view}",
        label: t("tools.shareable_links.k12.homepage.#{view}_view"),
        url: institution_homepage_url(view: view)
      }
    end
  end

  # @return [Array<Hash>]
  def sharable_links_k12_databases_data
    available_k12_views.map do |view|
      {
        id: "databases-az-#{view}",
        label: t("tools.shareable_links.k12.databases.#{view}_view"),
        url: institution_databases_url(view: view)
      }
    end
  end

  # @return [Array<Hash>]
  def sharable_links_k12_bento_search_data
    inst_code = @institution.code
    available_k12_views.map do |view|
      {
        id: "bento-search-#{view}",
        label: t("tools.shareable_links.k12.bento_search.#{view}_view"),
        data: configured_bento_data(inst_code, view)
      }
    end
  end

  # @return [Array<Hash>]
  def sharable_links_publiclib_homepage_data
    [
      {
        id: 'homepage-elementary',
        label: t('tools.shareable_links.publiclib.homepage.elementary_view'),
        url: institution_homepage_url(view: :elementary)
      },
      {
        id: 'homepage-middle',
        label: t('tools.shareable_links.publiclib.homepage.middle_view'),
        url: institution_homepage_url(view: :middle)
      },
      {
        id: 'homepage-highschool',
        label: t('tools.shareable_links.publiclib.homepage.highschool_view'),
        url: institution_homepage_url(view: :highschool)
      },
      {
        id: 'homepage-view',
        label: t('tools.shareable_links.publiclib.homepage.full_view'),
        url: institution_homepage_url(view: :full)
      }
    ]
  end

  # @return [Array<Hash>]
  def sharable_links_publiclib_databases_data
    [
      {
        id: 'databases-az-elementary',
        label: t('tools.shareable_links.publiclib.databases.elementary_view'),
        url: institution_databases_url(view: :elementary)
      },
      {
        id: 'databases-az-middle',
        label: t('tools.shareable_links.publiclib.databases.middle_view'),
        url: institution_databases_url(view: :middle)
      },
      {
        id: 'databases-az-highschool',
        label: t('tools.shareable_links.publiclib.databases.highschool_view'),
        url: institution_databases_url(view: :highschool)
      },
      {
        id: 'databases-az-full',
        label: t('tools.shareable_links.publiclib.databases.full_view'),
        url: institution_databases_url(view: :full)
      }
    ]
  end

  # @return [Array<Hash>]
  def sharable_links_publiclib_bento_search_data
    inst_code = @institution.code
    [
      {
        id: 'bento-search-elementary',
        label: t('tools.shareable_links.publiclib.bento_search.elementary_view'),
        data: configured_bento_data(inst_code, 'elementary')
      },
      {
        id: 'bento-search-middle',
        label: t('tools.shareable_links.publiclib.bento_search.middle_view'),
        data: configured_bento_data(inst_code, 'middle')
      },
      {
        id: 'bento-search-highschool',
        label: t('tools.shareable_links.publiclib.bento_search.highschool_view'),
        data: configured_bento_data(inst_code, 'highschool')
      },
      {
        id: 'bento-search-full',
        label: t('tools.shareable_links.publiclib.bento_search.full_view'),
        data: configured_bento_data(inst_code, 'full')
      }
    ]
  end

  # @return [Array<Hash>]
  def sharable_links_general_homepage_data
    [
      {
        id: 'homepage',
        label: t('tools.shareable_links.modal.homepage_label'),
        url: institution_homepage_url
      }
    ]
  end

  # @return [Array<Hash>]
  def sharable_links_general_databases_data
    [
      {
        id: 'databases-az',
        label: t('tools.shareable_links.modal.databases_label'),
        url: institution_databases_url
      }
    ]
  end

  # @return [Array<Hash>]
  def sharable_links_general_bento_search_data
    configured_bento_data
  end

  # @return [Array<Hash>]
  def configured_bento_data(inst_code = @institution.code, view = 'default')
    ConfiguredBentoService.new(inst_code, view).default_list.each.map do |cb|
      url = if view == 'default'
              bento_full_results_url(inst_code: inst_code, bento_type: cb.slug)
            else
              bento_full_results_url(inst_code: inst_code, bento_type: cb.slug, view: view)
            end
      {
        id: cb.id.gsub(' ', '-').downcase,
        label: cb.display_name,
        url: url,
        search_box: search_box_embed_code(inst_code, eds_profile_for(@institution), cb.slug, view)
      }
    end
  end

# @return String
def search_box_embed_code(inst_code, profile, bento_type, view)
  view_code = if view == 'default'
                ''
              else
                %Q(\n        "view": "#{view}",)
              end
  div_id = SecureRandom.uuid
  <<-_end_
    <div id="#{div_id}"></div>
    <script src="#{search_galileo_js_url}"></script>
    <script>
      search_galileo( {
        "id": "#{div_id}",
        "pane_height": "300px",
        "instcode": "#{inst_code}",
        "profile": "#{profile}",
        "bento_search": true,
        "bento_type": "#{bento_type}",#{view_code}
        "advanced": true,
        "home": true
      } );
    </script>
  _end_
end

end
