stages:
  - dependency_scan
  - testing
  - deploy
  - push

# Dependency Scan
bundler_audit:
  stage: dependency_scan
#  only:
#    - dev
#    - staging
#    - prod
#    - master
  script:
    - bundle audit check --update
  tags:
    - shell

# Run test suite in containerized environment
run_tests:
  stage: testing
  except:
    - dev
    - staging
    - prod
  script:
    - git clone https://gitlab.galileo.usg.edu/infra/solr-configs/galileo.git /tmp/gal_solr_${CI_PIPELINE_ID}
    - docker login -u ${galhub_user} -p ${galhub_pass} galhub.galib.uga.edu
    - docker network create gs_testing_${CI_PIPELINE_ID}
    - docker run -d --name solr_${CI_PIPELINE_ID} --expose 8983 -e SOLR_HEAP="2G" --mount type=tmpfs,destination=/var/solr,tmpfs-mode=1777 -v `pwd`:/data -w /data -v /tmp/gal_solr_${CI_PIPELINE_ID}/galileo:/opt/solr/server/solr/configsets/galileo/conf --network gs_testing_${CI_PIPELINE_ID} --network-alias=solr galhub.galib.uga.edu/solr:8.11.1
    - docker run --name db_${CI_PIPELINE_ID} --expose 5432 --network gs_testing_${CI_PIPELINE_ID} -e POSTGRES_HOST_AUTH_METHOD="trust" -e POSTGRES_PASSWORD="testing" --network-alias=db -d postgres:14
    - docker run --name redis_${CI_PIPELINE_ID} --expose 6379 --network gs_testing_${CI_PIPELINE_ID} --network-alias=redis -d redis:5
    - echo "Wait for support containers to start" && sleep 2
    - docker ps
    - docker login -u ${galhub_user} -p ${galhub_pass} galhub.galib.uga.edu
    - docker run -d --shm-size=2g --cap-add=SYS_ADMIN --name web_${CI_PIPELINE_ID} -v `pwd`:/code -w /code --network gs_testing_${CI_PIPELINE_ID} --network-alias=web --entrypoint "/bin/bash" galhub.galib.uga.edu/galileo-search:3.2.2 /code/provision/web-entrypoint.sh && docker cp $test_credential_key web_${CI_PIPELINE_ID}:/code/config/credentials/test.key && docker attach web_${CI_PIPELINE_ID}
  after_script:
    - rm -rf /tmp/gal_solr_${CI_PIPELINE_ID}
    - docker rm -f solr_${CI_PIPELINE_ID} db_${CI_PIPELINE_ID} redis_${CI_PIPELINE_ID} web_${CI_PIPELINE_ID}
    - docker network rm gs_testing_${CI_PIPELINE_ID}
  tags:
    - shell

# Dev Push
# Automatic job to push the last commit to master to dev
push_to_dev:
  stage: push
  only:
    - master
  script:
    - git clone -n https://gitlab.galileo.usg.edu/galileo/galileo_search.git /tmp/galileo_search_${CI_PIPELINE_ID}
    - cd /tmp/galileo_search_${CI_PIPELINE_ID}
    - git checkout ${CI_COMMIT_SHA} 
    - sudo -H -u pushbot git push origin HEAD:dev 
    - cd /tmp
    - rm -rf /tmp/galileo_search_${CI_PIPELINE_ID}
  tags:
    - shell

# Deploy Dev 
# Deploys the code to dev
deploy_to_dev:
  stage: deploy
  only:
    - dev 
  script:
    - cd /app/galileo_search
    - git pull
    - cp $dev_credential_key config/credentials/dev.key
    - chmod 600 config/credentials/dev.key
    - bundle config set deployment 'true'
    - bundle install
    - RAILS_ENV=dev bundle exec rake db:migrate --trace
    - RAILS_ENV=dev bundle exec rake webpacker:clean --trace
    - yarn install
    - RAILS_ENV=dev bundle exec rake assets:precompile --trace
    - cp templates/robots-no-crawl public/robots.txt
    - curl -sk https://gs-dev.galileo.usg.edu > /dev/null
    - passenger-config restart-app /app/galileo_search
  environment:
    name: dev
    url: https://gs-dev.galileo.usg.edu
  tags:
    - gs-dev-shell


# Staging push
# Manual job to push the dev server code to staging
push_to_staging:
  stage: push
  only:
    - dev
  when: manual
  script:
    - git clone -n https://gitlab.galileo.usg.edu/galileo/galileo_search.git /tmp/galileo_search_${CI_PIPELINE_ID}
    - cd /tmp/galileo_search_${CI_PIPELINE_ID}
    - git checkout ${CI_COMMIT_SHA}
    - sudo -H -u pushbot git push origin HEAD:staging
    - cd /tmp
    - rm -rf /tmp/galileo_search_${CI_PIPELINE_ID}
  tags:
    - shell 

# Deploy Staging
# Deploys the code to staging
deploy_to_staging:
  stage: deploy
  only:
    - staging
  script:
    - export ANSIBLE_API_USER=${ANSIBLE_API_USER}
    - export ANSIBLE_API_PASS=${ANSIBLE_API_PASS}
    - scripts/staging_deploy.sh
  environment:
    name: staging
    url: https://gs-staging.galileo.usg.edu
  tags:
    - shell

# Production push
# Manual job to push the staging server code to production
push_to_prod:
  stage: push
  only:
    - staging 
  when: manual
  script:
    - git clone -n https://gitlab.galileo.usg.edu/galileo/galileo_search.git /tmp/galileo_search_${CI_PIPELINE_ID}
    - cd /tmp/galileo_search_${CI_PIPELINE_ID}
    - git checkout ${CI_COMMIT_SHA}
    - sudo -H -u pushbot git push origin HEAD:prod
    - cd /tmp
    - rm -rf /tmp/galileo_search_${CI_PIPELINE_ID}
  tags:
    - shell

# Deploy Production
# Deploys the code to production this job
# can be triggered to deploy to production immediately.
deploy_code_to_production:
  stage: deploy
  only:
    - prod
  script:
    - export ANSIBLE_API_USER=${ANSIBLE_API_USER}
    - export ANSIBLE_API_PASS=${ANSIBLE_API_PASS}
    - scripts/prod_deploy.sh
  environment:
    name: production 
    url: https://www.galileo.usg.edu
  tags:
    - shell
