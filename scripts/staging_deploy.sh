#!/bin/bash
JOB=$(curl -s -f -k -H 'Content-Type:application/json' -XPOST --user $ANSIBLE_API_USER:$ANSIBLE_API_PASS https://resonator.galib.uga.edu/api/v2/job_templates/9/launch/ | jq -r '.job')
echo "Code deploy running, please wait."
sleep 10
STATUS=$(curl -s -f -k -H 'Content-Type:application/json' --user $ANSIBLE_API_USER:$ANSIBLE_API_PASS https://resonator.galib.uga.edu/api/v2/jobs/$JOB/ | jq -r '.status')

while [[ $STATUS == "running" || $STATUS == "pending" ]];
do
  echo Job is $STATUS, please wait.  More details available at https://resonator.galib.uga.edu/api/v2/jobs/$JOB/
  sleep 15 
  STATUS=$(curl -s -f -k -H 'Content-Type:application/json' --user $ANSIBLE_API_USER:$ANSIBLE_API_PASS https://resonator.galib.uga.edu/api/v2/jobs/$JOB/ | jq -r '.status')
done;

FINAL=$(curl -s -f -k -H 'Content-Type:application/json' --user $ANSIBLE_API_USER:$ANSIBLE_API_PASS https://resonator.galib.uga.edu/api/v2/jobs/$JOB/ | jq -r '.status')
curl -s -L -f -k -H 'Content-Type:application/json' --user $ANSIBLE_API_USER:$ANSIBLE_API_PASS https://resonator.galib.uga.edu/api/v2/jobs/$JOB/stdout/?format=txt | tail
echo Final status: $FINAL
echo Job output available at: https://resonator.galib.uga.edu/api/v2/jobs/$JOB/stdout/
