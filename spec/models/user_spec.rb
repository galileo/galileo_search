# frozen_string_literal: true

require 'rails_helper'

describe User, type: :model do

  subject do
    User.new solr_institution: solr_institution, auth_type: :test, remote: true
  end

  # simulate data sources for User creation
  let :solr_institution do
    instance_double SolrInstitution
  end

  # stub responses
  before do
    allow(solr_institution).to receive(:code).and_return('code')
    allow(solr_institution).to receive(:public_code).and_return('pubcode')
    allow(solr_institution).to receive(:institution_group_code).and_return('univsystem')
    allow(solr_institution).to receive(:name).and_return('Name')
    allow(solr_institution).to receive(:passphrase).and_return('phrase')
    allow(solr_institution).to receive(:proxy).and_return('proxygsu.edu')
    allow(solr_institution).to receive(:type).and_return(:k12)
  end

  context 'attributes' do
    it 'has a remote? boolean' do
      expect(subject.remote?).to be_truthy
    end
    it 'has an auth_type value returning the authentication type used' do
      expect(subject.auth_type).to eq :test
    end
  end

  context 'institution' do
    let(:inst) { subject.inst }
    it 'exists' do
      expect(inst).not_to be_nil
    end
    it 'has a the usual attributes' do
      expect(inst.code).to eq 'code'
      expect(inst.pub_code).to eq 'pubcode'
      expect(inst.name).to eq 'Name'
      expect(inst.pass).to eq 'phrase'
    end

    context 'site' do
      # TODO
    end

  end
end