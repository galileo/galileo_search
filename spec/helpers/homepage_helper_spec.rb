# frozen_string_literal: true

require 'rails_helper'

RSpec.describe HomepageHelper do
  include HomepageHelper

  describe 'spotlight_link_url' do

    it 'returns link path (central resource)' do
      expect(spotlight_link_url({link: "express?link=abcd"})).to eq '/link/abcd'
    end

    it 'returns link path (with slash)' do
      expect(spotlight_link_url({link: "express/?link=abcd"})).to eq '/link/abcd'
    end

    it 'returns link path (with spaces)' do
      expect(spotlight_link_url({link: "   express/?link=abcd   "})).to eq '/link/abcd'
    end

    it 'returns link path (glri resource)' do
      expect(spotlight_link_url({link: "express?link=abcd-uga1"})).to eq '/link/abcd-uga1'
    end

    it 'returns link path (deeplink resource)' do
      expect(spotlight_link_url({link: "express?link=abcd:xyz"})).to eq '/link/abcd:xyz'
    end

    # TODO: don't let bogus resource leave galileo_admin
    it 'returns link path (bogus resource)' do
      expect(spotlight_link_url({link: "express?link=abcde"})).to eq '/link/abcd'
    end

    it 'returns link path (prod express link)' do
      expect(spotlight_link_url({link: "https://www.galileo.usg.edu/express?link=abcd"})).to eq '/link/abcd'
    end

    it 'returns link path (with inst param)' do
      expect(spotlight_link_url({link: "https://www.galileo.usg.edu/express?link=abcd&inst=uga1"})).to eq '/link/abcd'
    end

    it 'returns url' do
      expect(spotlight_link_url({link: "https://example.com"})).to eq 'https://example.com'
    end

    it 'returns url (with spaces' do
      expect(spotlight_link_url({link: "   https://example.com   "})).to eq 'https://example.com'
    end

  end

end
