# frozen_string_literal: true

require 'rails_helper'

class MockEdsResult
  # @param [Hash] options
  def initialize(options)
    @doc_title = options[:doc_title]
    @pub_title = options[:pub_title]
    @pub_year = options[:pub_year]&.to_s
    if options.key? :date_hash
      date_hash = options[:date_hash]
      @pub_year = date_hash['Y'] if @pub_year.nil? && date_hash&.key?('Y')
      @bib_part = {
        'BibEntity' => {
          'Dates' => [date_hash]
        }
      }
    else
      @bib_part = nil
    end
  end

  # returns a new MockEdsResult with an EDS-style date hash already created
  def self.new_with_date(year, month, day, text = nil)
    date_hash = {
      'Type' => 'published',
      'D' => day.to_s.rjust(2, '0'),
      'M' => month.to_s.rjust(2, '0'),
      'Y' => year.to_s
    }
    date_hash['Text'] = text unless text.nil?
    MockEdsResult.new date_hash: date_hash
  end

  def eds_title
    @doc_title
  end

  def eds_source_title
    @pub_title
  end

  def eds_publication_year
    @pub_year
  end
end

RSpec.describe EdsResultFormatHelper do
  include EdsResultFormatHelper

  describe 'publication_title_is_redundant?' do
    it 'is redundant if document title is same as publication title' do
      item = MockEdsResult.new doc_title: 'Some Great Story', pub_title: 'Some Great Story'
      expect(publication_title_is_redundant?(item)).to be true
    end
    it 'is redundant if document title is same as publication title with differences in letter casing' do
      item = MockEdsResult.new doc_title: 'Some Great Story', pub_title: 'some great story'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: 'SOME GREAT STORY', pub_title: 'Some Great Story'
      expect(publication_title_is_redundant?(item)).to be true
    end
    it 'is redundant if document title is same as publication title with differences in punctuation' do
      item = MockEdsResult.new doc_title: '"Some Great Story"', pub_title: 'Some Great Story'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: 'Some Great Story', pub_title: '"Some Great Story"'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: 'Some -- Great -- Story', pub_title: 'Some *Great* Story'
      expect(publication_title_is_redundant?(item)).to be true
    end
    it 'is redundant if publication title starts with document title' do
      item = MockEdsResult.new doc_title: 'Some Great Story', pub_title: 'Some Great Story by Some Great Author'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: 'Some Lecture', pub_title: 'Some Lecture / 5th Annual Boring Conference'
      expect(publication_title_is_redundant?(item)).to be true
    end
    it 'is redundant if publication title starts with document title with differences in punctuation and casing' do
      item = MockEdsResult.new doc_title: 'Some Great Story', pub_title: 'some great story by some great author'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: 'some great story', pub_title: 'Some Great Story -- Some Great Author -- 2021'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: '"Some Lecture"', pub_title: 'Some Lecture / 5th Annual Boring Conference'
      expect(publication_title_is_redundant?(item)).to be true
    end
    it 'is redundant if publication title and document title differ in type of word separator (space or punctuation)' do
      item = MockEdsResult.new doc_title: 'Counter-culture', pub_title: 'Counter culture'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: 'Counter culture', pub_title: 'Counter-culture'
      expect(publication_title_is_redundant?(item)).to be true
      item = MockEdsResult.new doc_title: 'Counter*culture', pub_title: 'Counter-culture'
      expect(publication_title_is_redundant?(item)).to be true
    end
    it 'is NOT redundant if document title has words the publication title does not' do
      item = MockEdsResult.new doc_title: 'Some Great Story', pub_title: 'Some Great Anthology'
      expect(publication_title_is_redundant?(item)).to be false
    end
    it 'is NOT redundant if document title starts with publication title, but then has more words' do
      item = MockEdsResult.new doc_title: 'Journal of Stuff -- Letters to the Editor', pub_title: 'Journal of Stuff'
      expect(publication_title_is_redundant?(item)).to be false
    end
    it 'is NOT redundant if document title and publication title only differ in whether two words are separated' do
      item = MockEdsResult.new doc_title: 'Counterculture', pub_title: 'Counter-culture'
      expect(publication_title_is_redundant?(item)).to be false
      item = MockEdsResult.new doc_title: 'Counter-culture', pub_title: 'Counterculture'
      expect(publication_title_is_redundant?(item)).to be false
    end
  end

  describe 'best_available_publication_date' do
    it 'uses the day, month, and year from the date hash if each is greater than one, regardless of date text' do
      item = MockEdsResult.new_with_date 2011, 11, 11
      expect(best_available_publication_date(item)).to eq 'November 11, 2011'
      item = MockEdsResult.new_with_date 2011, 11, 11, '11/11/2011'
      expect(best_available_publication_date(item)).to eq 'November 11, 2011'
      item = MockEdsResult.new_with_date 2011, 11, 11, 'arbitrary text'
      expect(best_available_publication_date(item)).to eq 'November 11, 2011'
      item = MockEdsResult.new_with_date 2011, 11, 11, 'May 5, 2005'
      expect(best_available_publication_date(item)).to eq 'November 11, 2011'
    end
    it 'treats a day or month equal to "1" as a lack of precision in the absence of date text' do
      item = MockEdsResult.new_with_date 2011, 11, 1
      expect(best_available_publication_date(item)).to eq 'November 2011'
      item = MockEdsResult.new_with_date 2011, 1, 1
      expect(best_available_publication_date(item)).to eq '2011'
    end
    it 'treats a date with day > 1 as precise to the day, even if month is "1"' do
      item = MockEdsResult.new_with_date 2011, 1, 11
      expect(best_available_publication_date(item)).to eq 'January 11, 2011'
    end
    context 'For dates with day/month equal to "1", consult the date text to see if it matches' do
      it 'shows the date with full precision if the date text matches the month/day values' do
        item = MockEdsResult.new_with_date 2011, 11, 1, 'November 1, 2011'
        expect(best_available_publication_date(item)).to eq 'November 1, 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'January 1, 2011'
        expect(best_available_publication_date(item)).to eq 'January 1, 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'January 2011'
        expect(best_available_publication_date(item)).to eq 'January 2011'
      end
      it 'limits precision if the date text does not confirm the date' do
        item = MockEdsResult.new_with_date 2011, 11, 1, 'arbitrary text'
        expect(best_available_publication_date(item)).to eq 'November 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'arbitrary text'
        expect(best_available_publication_date(item)).to eq '2011'
        item = MockEdsResult.new_with_date 2011, 11, 1, 'November 5, 2011'
        expect(best_available_publication_date(item)).to eq 'November 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'May 5, 2011'
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'understands the "mm/dd/yyyy" format' do
        item = MockEdsResult.new_with_date 2011, 11, 1, '11/1/2011'
        expect(best_available_publication_date(item)).to eq 'November 1, 2011'
        item = MockEdsResult.new_with_date 2011, 11, 1, '11/5/2011'
        expect(best_available_publication_date(item)).to eq 'November 2011'
      end
      it 'understands the "mm-dd-yyyy" format' do
        item = MockEdsResult.new_with_date 2011, 11, 1, '11-1-2011'
        expect(best_available_publication_date(item)).to eq 'November 1, 2011'
        item = MockEdsResult.new_with_date 2011, 11, 1, '11-5-2011'
        expect(best_available_publication_date(item)).to eq 'November 2011'
      end
      it 'understands the "yyyy-mm-dd" format' do
        item = MockEdsResult.new_with_date 2011, 11, 1, '2011-11-01'
        expect(best_available_publication_date(item)).to eq 'November 1, 2011'
        item = MockEdsResult.new_with_date 2011, 11, 1, '2011-11-05'
        expect(best_available_publication_date(item)).to eq 'November 2011'
      end
      it 'understands the "mmm dd, yyyy" format' do
        item = MockEdsResult.new_with_date 2011, 11, 1, 'Nov 1, 2011'
        expect(best_available_publication_date(item)).to eq 'November 1, 2011'
        item = MockEdsResult.new_with_date 2011, 11, 1, 'Nov 5, 2011'
        expect(best_available_publication_date(item)).to eq 'November 2011'
      end
      it 'understands the "Month dd, yyyy" format' do
        item = MockEdsResult.new_with_date 2011, 11, 1, 'November 1, 2011'
        expect(best_available_publication_date(item)).to eq 'November 1, 2011'
        item = MockEdsResult.new_with_date 2011, 11, 1, 'November 5, 2011'
        expect(best_available_publication_date(item)).to eq 'November 2011'
      end
      it 'understands the "Month yyyy" format' do
        item = MockEdsResult.new_with_date 2011, 1, 1, 'January 2011'
        expect(best_available_publication_date(item)).to eq 'January 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'May 2011'
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'understands the "mmm yyyy" format' do
        item = MockEdsResult.new_with_date 2011, 1, 1, 'Jan 2011'
        expect(best_available_publication_date(item)).to eq 'January 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'Mar 2011'
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'understands the "mmmyyyy" format (common in EDS)' do
        item = MockEdsResult.new_with_date 2011, 1, 1, 'jan2011'
        expect(best_available_publication_date(item)).to eq 'January 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'mar2011'
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'understands the "mmmyy" format (common in EDS)' do
        item = MockEdsResult.new_with_date 2011, 1, 1, 'jan11'
        expect(best_available_publication_date(item)).to eq 'January 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, 'mar11'
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'does NOT trust date text in the "yyyymmdd" format to any more precise than the date hash' do
        item = MockEdsResult.new_with_date 2011, 11, 1, '20111101'
        expect(best_available_publication_date(item)).to eq 'November 2011'
        item = MockEdsResult.new_with_date 2011, 1, 1, '20110101'
        expect(best_available_publication_date(item)).to eq '2011'
      end
    end
    context 'Unexpected situations' do
      it 'returns the eds_publication_year if there is no date at all' do
        item = MockEdsResult.new pub_year: 2011
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'returns the eds_publication_year if it does not match the date' do
        item = MockEdsResult.new pub_year: 2005, date_hash: { 'Y' => '2011', 'M' => '11', 'D' => '11', 'Type' => 'published' }
        expect(best_available_publication_date(item)).to eq '2005'
      end
      it 'ignores any date type other than "published"' do
        item = MockEdsResult.new pub_year: 2011, date_hash: { 'Y' => '2011', 'M' => '11', 'D' => '11', 'Type' => 'foo' }
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'treats a day or month equal to 0 as a lack of precision' do
        item = MockEdsResult.new_with_date 2011, 11, 0
        expect(best_available_publication_date(item)).to eq 'November 2011'
        item = MockEdsResult.new_with_date 2011, 0, 0
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'treats a day or month equal to nil as a lack of precision' do
        item = MockEdsResult.new_with_date 2011, 11, nil
        expect(best_available_publication_date(item)).to eq 'November 2011'
        item = MockEdsResult.new_with_date 2011, nil, nil
        expect(best_available_publication_date(item)).to eq '2011'
      end
      it 'treats other invalid text as a lack of precision' do
        item = MockEdsResult.new_with_date 2011, 11, 'foo'
        expect(best_available_publication_date(item)).to eq 'November 2011'
        item = MockEdsResult.new_with_date 2011, 'foo', 'bar'
        expect(best_available_publication_date(item)).to eq '2011'
      end
    end
  end
end
