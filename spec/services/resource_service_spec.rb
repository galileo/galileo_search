# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ResourceService, type: :model do

  subject(:service) { ResourceService.new('asc1') }

  describe '#all_names_with_codes' do
    it 'returns an array for names and codes' do
      all_names = service.all_names_with_codes
      expect(all_names).to be_an Array
    end
  end
end