# frozen_string_literal: true

require 'rails_helper'

RSpec.describe AuthApiService, type: :model do
  include AuthApiHelpers

  subject(:service) { AuthApiService }

  before(:each) { stub_auth_api }

  describe '#ip' do
    it 'returns institution response object based on a known IP' do
      inst = service.ip '1.1.1.1'
      expect(inst).to be_a AuthApiService::Response::Institution
      expect(inst.code).to eq 'dem4'
      expect(inst.success?).to be_truthy
      expect(inst.auth_context).to be_an(
        AuthApiService::Response::Institution::AuthContext
      )
      expect(inst.auth_context.remote?).to be_falsey
      expect(inst.auth_context.type).to eq :ip
    end
    it 'returns error response object for unknown IP' do
      error = service.ip '0.0.0.0'
      expect(error).to be_a AuthApiService::Response::Error
      expect(error.message).to eq 'Institution not found'
      expect(error.success?).to be_falsey
    end
  end

  describe '#passphrase' do
    it 'returns institution response object based on a passphrase' do
      inst = service.passphrase 'success'
      expect(inst).to be_a AuthApiService::Response::Institution
      expect(inst.code).to eq 'dem4'
      expect(inst.auth_context.remote?).to be_truthy
      expect(inst.auth_context.type).to eq :passphrase
    end
    it 'returns error response object for bad passphrase' do
      error = service.passphrase 'failed'
      expect(error).to be_a AuthApiService::Response::Error
      expect(error.message).to eq 'Institution not found'
    end
  end
end