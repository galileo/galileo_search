# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ValidationService, type: :model do

  subject(:service) { ValidationService.new }

  describe '#institution_code' do

    it 'returns false for blank' do
      response = service.is_valid_institution_code?('')
      expect(response).to be_falsy
    end

    it 'returns false for short' do
      response = service.is_valid_institution_code?('123')
      expect(response).to be_falsy
    end

    it 'returns false for long' do
      response = service.is_valid_institution_code?('12345')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation' do
      response = service.is_valid_institution_code?('!@#$')
      expect(response).to be_falsy
    end

    it 'returns false for uppercase' do
      response = service.is_valid_institution_code?('UGA1')
      expect(response).to be_falsy
    end

    it 'returns true' do
      response = service.is_valid_institution_code?('uga1')
      expect(response).to be_truthy
    end

  end

  describe '#resource_code' do

    it 'returns false for blank' do
      response = service.is_valid_resource_code?('')
      expect(response).to be_falsy
    end

    it 'returns false for short' do
      response = service.is_valid_resource_code?('123')
      expect(response).to be_falsy
    end

    it 'returns false for short inst' do
      response = service.is_valid_resource_code?('1234-123')
      expect(response).to be_falsy
    end

    it 'returns false for short deeplink' do
      response = service.is_valid_resource_code?('1234:')
      expect(response).to be_falsy
    end

    it 'returns false for long' do
      response = service.is_valid_resource_code?('12345')
      expect(response).to be_falsy
    end

    it 'returns false for long inst' do
      response = service.is_valid_resource_code?('1234-12345')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation' do
      response = service.is_valid_resource_code?('!@#$')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation inst' do
      response = service.is_valid_resource_code?('1234-!@#$')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation deeplink' do
      response = service.is_valid_resource_code?('1234:!@#$')
      expect(response).to be_falsy
    end

    it 'returns false for uppercase' do
      response = service.is_valid_resource_code?('DLG1')
      expect(response).to be_falsy
    end

    it 'returns false for uppercase inst' do
      response = service.is_valid_resource_code?('dlg1-UGA1')
      expect(response).to be_falsy
    end

    it 'returns false for uppercase deeplink' do
      response = service.is_valid_resource_code?('dlg1:CivilWar')
      expect(response).to be_falsy
    end

    it 'returns true' do
      response = service.is_valid_resource_code?('dlg1')
      expect(response).to be_truthy
    end

    it 'returns true inst' do
      response = service.is_valid_resource_code?('dlg1-uga1')
      expect(response).to be_truthy
    end

    it 'returns true deeplink' do
      response = service.is_valid_resource_code?('dlg1:civil-war_battle-625')
      expect(response).to be_truthy
    end
  end

  describe '#site_code' do

    it 'returns false for blank' do
      response = service.is_valid_site_code?('')
      expect(response).to be_falsy
    end

    it 'returns false for short' do
      response = service.is_valid_site_code?('123')
      expect(response).to be_falsy
    end

    it 'returns false for short child' do
      response = service.is_valid_site_code?('1234:')
      expect(response).to be_falsy
    end

    it 'returns false for long' do
      response = service.is_valid_site_code?('12345')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation' do
      response = service.is_valid_site_code?('!@#$')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation child' do
      response = service.is_valid_site_code?('1234:!@#$')
      expect(response).to be_falsy
    end

    it 'returns false for uppercase' do
      response = service.is_valid_site_code?('UGA1')
      expect(response).to be_falsy
    end

    it 'returns false for uppercase child' do
      response = service.is_valid_site_code?('uga1:ABC')
      expect(response).to be_falsy
    end

    it 'returns true' do
      response = service.is_valid_site_code?('uga1')
      expect(response).to be_truthy
    end

    it 'returns true child' do
      response = service.is_valid_site_code?('uga1:abc')
      expect(response).to be_truthy
    end

    it 'returns true for numbers/hyphen child' do
      response = service.is_valid_site_code?('uga1:123-456')
      expect(response).to be_truthy
    end

  end

  describe '#passphrase' do

    it 'returns false for blank' do
      response = service.is_valid_passphrase?('')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation' do
      response = service.is_valid_passphrase?('!#%$')
      expect(response).to be_falsy
    end

    it 'returns true' do
      response = service.is_valid_passphrase?('DeadBeef@987')
      expect(response).to be_truthy
    end

  end

  describe '#institution_name' do

    it 'returns false for blank' do
      response = service.is_valid_institution_name?('')
      expect(response).to be_falsy
    end

    it 'returns false for punctuation' do
      response = service.is_valid_institution_name?('!#%$')
      expect(response).to be_falsy
    end

    it 'returns true' do
      response = service.is_valid_institution_name?('Zebra/Man1, Bonny-Girl2 & Adam:Boy3\'s School (Peak@Mount).')
      expect(response).to be_truthy
    end

  end

  describe '#active_institution' do

    it 'returns false for invalid code' do
      response = service.is_active_institution?('12345')
      expect(response).to be_falsy
    end

    it 'returns false for inactive institution' do
      response = service.is_active_institution?('9999')
      expect(response).to be_falsy
    end

    it 'returns true' do
      response = service.is_active_institution?('tuni')
      expect(response).to be_truthy
    end

  end

  describe '#active_resource' do

    it 'returns false for invalid code' do
      response = service.is_active_resource?('12345')
      expect(response).to be_falsy
    end

    it 'returns false for inactive resource' do
      response = service.is_active_resource?('9999')
      expect(response).to be_falsy
    end

    it 'returns true' do
      response = service.is_active_resource?('zbac')
      expect(response).to be_truthy
    end

  end

end
