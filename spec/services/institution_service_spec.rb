# frozen_string_literal: true

require 'rails_helper'

RSpec.describe InstitutionService, type: :model do

  subject(:service) { InstitutionService.new }

  describe '#open_athens_ready' do

    it 'returns a hash' do
      open_athens_ready = service.open_athens_ready
      expect(open_athens_ready).to be_a Hash
    end

  end

  describe '#all' do

    it 'returns a hash' do
      all = service.all
      expect(all).to be_a Hash
    end

  end

  describe '#all_names_with_codes' do

    it 'returns an array of names' do
      all_names = service.all_names_with_codes
      expect(all_names).to be_an Array
    end

  end

  describe '#all_with_sites_codes_and_keywords' do

    it 'returns an array' do
      all_with_sites_codes_and_keywords = service.all_with_sites_codes_and_keywords
      expect(all_with_sites_codes_and_keywords).to be_a Hash
    end

  end

  describe '#find' do

    it 'returns nil with invalid inst code' do
      find = service.find('123')
      expect(find).to be_nil
    end

    it 'returns nil when inst not found' do
      find = service.find('9999')
      expect(find).to be_nil
    end

    it 'returns a SolrInstitution' do
      find = service.find('tuni')
      expect(find).to be_a SolrInstitution
    end

  end

  describe '#find_site' do

    it 'returns nil with invalid inst code' do
      find_site = service.find_site('123')
      expect(find_site).to be_nil
    end

    it 'returns nil when inst not found' do
      find_site = service.find_site('9999')
      expect(find_site).to be_nil
    end

    it 'returns a SolrInstitution' do
      find_site = service.find_site('tsch:middle')
      expect(find_site).to be_a SolrInstitution
    end

  end

  describe '#find_by_name' do

    it 'returns nil with invalid inst name' do
      find_by_name = service.find_by_name('!#%$')
      expect(find_by_name).to be_nil
    end

    it 'returns nil when inst not found' do
      find_by_name = service.find_by_name('Great Institution')
      expect(find_by_name).to be_nil
    end

    it 'returns a SolrInstitution with inst name' do
      find_by_name = service.find_by_name('Test University')
      expect(find_by_name).to be_a SolrInstitution
    end

    it 'returns a SolrInstitution with site name' do
      find_by_name = service.find_by_name('Test Elementary School (Test County)')
      expect(find_by_name).to be_a SolrInstitution
    end

  end

  describe '#find_by_pass' do

    it 'returns nil with invalid passphrase' do
      find_by_pass = service.find_by_pass('!#%$')
      expect(find_by_pass).to be_nil
    end

    it 'returns nil when passphrase not found' do
      find_by_pass = service.find_by_pass('DeadBeef@987')
      expect(find_by_pass).to be_nil
    end

    it 'returns a SolrInstitution' do
      find_by_pass = service.find_by_pass('testuni')
      expect(find_by_pass).to be_a SolrInstitution
    end

  end

  describe '#find_for_lti' do

    it 'returns nil when consumer key not found' do
      find_for_lti = service.find_for_lti('BogusConsumerKey')
      expect(find_for_lti).to be_nil
    end

    it 'returns a SolrInstitution' do
      find_for_lti = service.find_for_lti('GALILEOAutoLogin', '1')
      expect(find_for_lti).to be_a SolrInstitution
    end

  end

  describe '#find_for_oa_scope' do

    it 'returns nil when scope not found' do
      find_for_oa_scope = service.find_for_oa_scope('bogusscope.edu')
      expect(find_for_oa_scope).to be_nil
    end

    it 'returns a SolrInstitution' do
      find_for_oa_scope = service.find_for_oa_scope('tuni.edu')
      expect(find_for_oa_scope).to be_a SolrInstitution
    end

  end

  describe '#find_for_oa_subscope' do

    it 'returns nil when subscope not found' do
      find_for_oa_subscope = service.find_for_oa_subscope('bogusscope.edu')
      expect(find_for_oa_subscope).to be_nil
    end

    it 'returns a SolrInstitution' do
      find_for_oa_subscope = service.find_for_oa_subscope('22345678.tuni.edu')
      expect(find_for_oa_subscope).to be_a SolrInstitution
    end

  end

  describe '#find_for_pines' do

    it 'returns nil when subscope not found' do
      find_for_pines = service.find_for_pines('BogusPinesCode')
      expect(find_for_pines).to be_nil
    end

    it 'returns a SolrInstitution' do
      find_for_pines = service.find_for_pines('WORTH')
      expect(find_for_pines).to be_a SolrInstitution
    end

  end

end
