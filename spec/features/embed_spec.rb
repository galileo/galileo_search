# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Embeddable search box endpoint', type: :feature do

  def encoded_json(params)
    { json: CGI.escape(params.to_json) }
  end
  before do
    visit embed_path(params: encoded_json(params))
  end
  context 'athens regional library case' do
    let(:params) do
      { input_width: '500px',
        profile: 'gpls_pines',
        advanced: 1,
        home: 1 }
    end
    scenario 'works' do
      expect(page).to have_css 'input[name=profile]', visible: false
      expect(page).to have_link 'Advanced Search'
      expect(page).to have_link 'www.galileo.usg.edu'
    end
  end
  context 'disciplines case' do
    let(:params) do
      {
        profile: 'usg',
        disciplines: 'libr*,lawx*,reli',
        disciplines_type: 'select multiple 4',
        peer_reviewed: 'hidden',
        library_collection: 'selected',
        full_text: 'unselected'
      }
    end
    scenario 'shows the desired fields and labels' do
      expect(page).to have_css 'input[name=peer_reviewed]', visible: false
      expect(page).to have_checked_field 'library_collection'
      expect(page).to have_unchecked_field 'full_text'
      expect(page).to have_select :"search_galileo-discipline",
                                  with_options: [
                                    'Library and Information Science', 'Law',
                                    'Religion and Philosophy'
                                  ],
                                  selected: [
                                    'Library and Information Science', 'Law'
                                  ]
    end
  end
  context 'submission' do
    let(:params) do
      { profile: 'usg' }
    end
    scenario 'the user is redirected to the login page' do
      fill_in 'search_galileo_query1', with: 'auto repair'
      click_on id: 'search_galileo-discovery-search-button'
      expect(page.current_path).to eq welcome_path
    end
  end
end
