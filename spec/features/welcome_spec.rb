# frozen_string_literal: true

require 'rails_helper'

RSpec.feature 'Welcome page', type: :feature do
  include EzProxyRequestHelpers
  include RssRequestHelpers
  scenario 'shows a wayfinder that allows simple selection of an institution',
           js: true do
    visit welcome_path
    fill_in 'galileo-wayfinder-all', with: 'Test'
    expect(page).to have_text 'Test University 5'
    page.find('div', class: 'tt-selectable', text: /University 5/).click
    expect(page.current_path).to eq instructions_path
    expect(page).to have_text 'Test University 5'
  end
  scenario 'allows logging in using the password input' do
    stub_ezp_requests
    stub_rss_requests
    allow_any_instance_of(SessionsController).to receive(:open_athens_proxy_login).and_return(institution_homepage_path(inst_code: 'tuni'))
    visit welcome_path
    fill_in 'passphrase', with: 'testuni'
    click_button 'Go', class: 'passphrase-login-button'
    expect(page.current_path).to eq institution_homepage_path(inst_code: 'tuni')
    expect(page).to have_text 'Test University'
  end
end
