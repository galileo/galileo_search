require "rails_helper"

RSpec.describe ContactMailer, type: :mailer do

  context "Check for spam" do

    it "is spam when comment has multiple equal signs" do
      expect( subject.spam?( "description" => "...=====..." ) ).to be true
    end

    it "is spam when comment starts with multiple greater than signs" do
      expect( subject.spam?( "description" => ">>>_Every month ..." ) ).to be true
    end

    it "is spam when firstname/lastname repeat in expected way" do
      expect( subject.spam?( "firstname" => "FirstName", "lastname" => "FirstNameXX" ) ).to be true
    end

    it "is spam when firstname same as lastname" do
      expect( subject.spam?( "firstname" => "FirstName", "lastname" => "FirstName" ) ).to be true
    end

    it "is spam when there's no last name" do
      expect( subject.spam?( "name" => "FirstName" ) ).to be true
    end

    it "is spam when multiple firstname/lastname repeat, actual example" do
      expect( subject.spam?( "firstname" => "Lily Allen", "lastname" => "Lily Allen" ) ).to be true
    end

    it "is spam when multiple firstname/lastname repeat, extreme example 1" do
      expect( subject.spam?( "firstname" => "One Fish Two Fish Red Fish Blue Fish", "lastname" => "One Fish Two Fish Red Fish Blue Fish" ) ).to be true
    end

    it "is spam when multiple firstname/lastname repeat, extreme example 2" do
      expect( subject.spam?( "name" => "One Fish Two Fish Red Fish Blue Fish One Fish Two Fish Red Fish Blue Fish" ) ).to be true
    end

    it "is not spam when name is blank" do
      expect( subject.spam?( "name" => "" ) ).to be false
      expect( subject.spam?( "firstname" => "" ) ).to be false
    end

    it "is spam when name is Eric Jones, ...z.mail" do
      expect( subject.spam?( "name" => "Eric Jones", "email" => "eric.jones.z.mail@gmail.com" ) ).to be true
    end

    it "is spam when name is Eric Jones, ...myemail" do
      expect( subject.spam?( "name" => "Eric Jones", "email" => "ericjonesmyemail@gmail.com" ) ).to be true
    end

    it "is spam when firstname/lastname is Eric Jones" do
      expect( subject.spam?( "firstname" => "Eric", "lastname" => "Jones", "email" => "eric.jones.z.mail@gmail.com" ) ).to be true
    end

    it "is spam when claiming copyright violation" do
      expect( subject.spam?( "description" => "violating the copyright" ) ).to be true
    end

    it "is spam when claiming copyright infringement" do
      expect( subject.spam?( "description" => "infringing on a copyright" ) ).to be true
    end

    it "is spam when claiming copyright suit" do
      expect( subject.spam?( "description" => "sued by the copyright" ) ).to be true
    end

    it "is spam when firstname/lastname repeat in first few characters" do
      expect( subject.spam?( "firstname" => "FirstABCDE", "lastname" => "FirstlmnopXX" ) ).to be true
    end

    it "is spam when ends with uppercase" do
      expect( subject.spam?( "firstname" => "Firstname", "lastname" => "lastnameXX" ) ).to be true
    end

    it "is spam when name have many uppercase characters" do
      expect( subject.spam?( "firstname" => "FirstNameBoy", "lastname" => "Lastname" ) ).to be true
      expect( subject.spam?( "firstname" => "Firstname",    "lastname" => "LastNameGirl" ) ).to be true
    end

    it "is spam when contains porn related terms 1" do
      expect( subject.spam?( "firstname" => "group incest sex" ) ).to be true
    end

    it "is spam when contains porn related terms 1" do
      expect( subject.spam?( "firstname" => "anal incest porn" ) ).to be true
    end

  end
end
