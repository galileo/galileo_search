# frozen_string_literal: true

require 'rails_helper'

describe 'Express link endpoint', type: :request do
  context 'Basic Express Links' do
    before do
      allow_any_instance_of(ValidationService)
      .to receive(:is_active_institution?).and_return true
      allow_any_instance_of(ValidationService)
      .to receive(:is_active_resource?).and_return true
    end
    context 'With no parameters' do
      before do
        @institution = double SolrInstitution
        allow(@institution).to receive(:code).and_return 'test'
        allow(@institution).to receive(:open_athens).and_return false
        allow(@institution).to receive(:user).and_return true
        allow_any_instance_of(InstitutionService)
        .to receive(:find).and_return @institution
        allow_any_instance_of(IpLookupService)
        .to receive(:ip_is_for?).and_return @institution.code
      end
      context 'for on-campus requests (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'logins user and redirects to institution home page' do
          pending('Needs to login user and redirect to user home institution')

          get '/express'
          expect(response).to redirect_to institution_homepage_path 'test'
        end
      end
    end

    context 'With a pinesid param' do
      it 'redirects to login with pinesid param set' do
        get '/express?pinesid=test'
        expect(response).to redirect_to login_path(pinesid: 'test')
      end
      it 'ignores other params if pinesid is set' do
        get '/express?pinesid=test&link=test'
        expect(response).to redirect_to login_path(pinesid: 'test')
      end
    end

    context 'with only a password param' do
      context 'for on-campus requests (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'redirects to login with passphrase params set' do
          get '/express?password=test'
          expect(response).to redirect_to login_path(passphrase: 'test')
        end
      end
      context 'for off-campus requests (not IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return false
        end
        it 'redirects to login with passphrase params set' do
          get '/express?password=test'
          expect(response).to redirect_to login_path(passphrase: 'test')
        end
      end
    end

    context 'With only an inst parameter' do
      it 'redirects to the login path with the inst param set' do
        get '/express?inst=test'
        expect(response).to redirect_to login_path(inst: 'test')
      end
    end

    context 'with a inst param and an password param' do
      context 'for on-campus requests (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'redirects to login with passphrase params set' do
          get '/express?inst=test&password=test'
          expect(response).to redirect_to login_path(passphrase: 'test')
        end
      end
      context 'for off-campus requests (not IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return false
        end
        it 'redirects to login with passphrase params set' do
          get '/express?inst=test&password=test'
          expect(response).to redirect_to login_path(passphrase: 'test')
        end
      end
    end

    context 'with only a link param' do
      context 'for on-campus requests (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'redirects to link_path' do
          pending('Needs to redirect user to the link_path')

          get '/express?link=test'
          expect(response).to redirect_to link_path('test')
        end
        it 'redirects to link_path when a public resource' do
          get '/express?link=ptst'
          expect(response).to redirect_to 'https://www.public-resource.com/ip'
        end
      end
      context 'for off-campus requests (not authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return false
        end
        it 'redirects to login with link params set' do
          pending('Needs to redirect user to the login_path with destination')

          get '/express?link=test'
          expect(response).to redirect_to login_path(link: 'test')
        end
        it 'redirects to link_path when a public resource' do
          get '/express?link=ptst'
          expect(response).to redirect_to 'https://www.public-resource.com/ip'
        end
      end
    end

    context 'with a link param and a password param' do
      context 'for on-campus requests (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'redirects to login_path with passphrase and dest params set' do
          get '/express?link=test&password=test'
          expect(response).to redirect_to login_path(passphrase: 'test', dest: link_path('test'))
        end
      end
      context 'for off-campus requests (not authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return false
        end
        it 'redirects to login_path with passphrase and dest params set' do
          get '/express?link=test&password=test'
          expect(response).to redirect_to login_path(passphrase: 'test', dest: link_path('test'))
        end
      end
    end

    context 'with a link param and a inst param' do
      context 'for on-campus requests where request IP is in inst range (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'redirects to link_path' do
          pending('Needs to setup test context')
          get '/express?link=test&inst=test'
          expect(response).to redirect_to link_path('test')
        end
      end
      context 'for off-campus requests (not authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return false
        end
        it 'redirects to login_path for that institution' do
          pending('Needs to setup test context')
          get '/express?link=test&inst=test'
          expect(response).to redirect_to login_path(inst: 'test', dest: link_path('test'))
        end
      end
    end

    context 'with a link param, password param, and inst param' do
      context 'for on-campus requests (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'redirects to login_path with passphrase and dest params set' do
          get '/express?link=test&password=test&inst=test'
          expect(response).to redirect_to login_path(passphrase: 'test', dest: link_path('test'))
        end
      end
      context 'for off-campus requests (not authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return false
        end
        it 'redirects to login_path with passphrase and dest params set' do
          get '/express?link=test&password=test&inst=test'
          expect(response).to redirect_to login_path(passphrase: 'test', dest: link_path('test'))
        end
      end
    end

    context 'for going directly to a Resource' do
      it 'redirects to the link route' do
        get '/express?link=ptst'
        expect(response).to redirect_to 'https://www.public-resource.com/ip'
      end
      context 'with a password specified' do
        it 'redirects to login with passphrase and dest params set' do
          get '/express?password=test&link=test'
          expect(response).to redirect_to login_path(passphrase: 'test',
                                                     dest: link_path('test'))
        end
      end
      context 'and bypassing authentication' do
        it 'redirects to the ip_link' do
          free_resource_code = 'tbat-tsch'
          get "/express?link=#{free_resource_code}"
          expect(response).to redirect_to 'https://www.test-resource.com/ip'
        end
      end
    end

    context 'for going directly to an Allocation (institution-specific)' do
      before do
        @institution = double SolrInstitution
        allow(@institution).to receive(:code).and_return 'test'
        allow(@institution).to receive(:open_athens).and_return false
        allow(@institution).to receive(:user).and_return true
        allow_any_instance_of(InstitutionService)
        .to receive(:find).and_return @institution
        allow_any_instance_of(IpLookupService)
        .to receive(:ip_is_for?).and_return @institution.code
      end
      context 'for on-campus requests (IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return true
        end
        it 'redirects to institution home page with no other parameters' do
          pending('Needs redirect to login user')
          get '/express'
          expect(response).to redirect_to institution_homepage_path 'test'
        end
        it 'redirects to the destination resource' do
          get '/express?inst=test&link=test'
          expect(response).to redirect_to link_path 'test'
        end
        it 'redirects to GLRI resources using only the link param' do
          get '/express?link=inst-glri'
          expect(response).to redirect_to link_path 'inst-glri'
        end
      end
      context 'for remote requests (not IP authenticated)' do
        before do
          allow_any_instance_of(ExpressController)
          .to receive(:ip_lookup_matches_inst).and_return false
        end
        context 'for an OpenAthens-enabled Institution' do
          before do
            allow(@institution).to receive(:open_athens).and_return true
            allow(@institution).to receive(:oa_entity_id).and_return 'test.edu'
            allow_any_instance_of(ExpressController)
            .to receive(:oa_wayfless_link).and_return 'wayfless-link'
          end
          it 'redirects to the wayfless link' do
            get '/express?inst=test&link=test'
            expect(response).to redirect_to 'wayfless-link'
          end
        end
        context 'for an Institution not yet enabled with OpenAthens' do
          before do
            allow(@institution).to receive(:open_athens).and_return false
          end
          it 'redirects to instructions page with inst and dest params set' do
            get '/express?inst=test&link=test'
            expect(response).to redirect_to instructions_path(
                                            inst: 'test',
                                            dest: link_path('test')
                                            )
          end
        end
      end
    end
  end

  context 'Proxy Express links' do
    context 'proxy param only' do
      it 'redirects to login with url to proxy' do
        get '/express?proxy=https%3A%2F%2Fexample.com'
        expect(response).to redirect_to login_path(proxy: 'https://example.com')
      end
    end
    context 'proxy plus other params' do
      before do
        allow_any_instance_of(ValidationService)
        .to receive(:is_active_institution?).and_return true
        allow_any_instance_of(ValidationService)
        .to receive(:is_active_resource?).and_return true
      end
      it 'is not understood with inst param' do
        get '/express?proxy=https%3A%2F%2Fexample.com&inst=test'
        expect(flash[:alert]).to eq 'Express link could not be understood!'
      end
      it 'is not understood with password param' do
        get '/express?proxy=https%3A%2F%2Fexample.com&password=kittens'
        expect(flash[:alert]).to eq 'Express link could not be understood!'
      end
      it 'is not understood with link param' do
        get '/express?proxy=https%3A%2F%2Fexample.com&link=test'
        expect(flash[:alert]).to eq 'Express link could not be understood!'
      end
    end
  end

  context 'Discover Express links' do
    context 'advanced param' do
      context 'With only an advanced param' do
        it 'redirects to discover with advanced param set' do
          get '/express?advanced=1'
          expect(response).to redirect_to discover_path(advanced: '1')
        end
      end

      context 'With an advanced param and profile param' do
        it 'redirects to discover with advanced param set' do
          get '/express?advanced=1&profile=gkr1'
          expect(response).to redirect_to discover_path(advanced: '1', profile: 'gkr1')
        end
      end

      context 'With an advanced param and instcode param' do
        it 'redirects to discover with advanced param set' do
          get '/express?advanced=1&instcode=tes1'
          expect(response).to redirect_to discover_path(advanced: '1', instcode: 'tes1')
        end
      end

      context 'With an advanced, instcode, and profile params' do
        it 'redirects to discover with advanced param set' do
          get '/express?advanced=1&instcode=tes1&profile=gkr1'
          expect(response).to redirect_to discover_path(advanced: '1', instcode: 'tes1', profile: 'gkr1')
        end
      end
    end
    context 'search_galileo_query param' do
      context 'With only an search_galileo_query param' do
        it 'redirects to discover with search_galileo_query param set' do
          get '/express?search_galileo_query=test'
          expect(response).to redirect_to discover_path(search_galileo_query: 'test')
        end
      end

      context 'With an search_galileo_query param and profile param' do
        it 'redirects to discover with search_galileo_query param set' do
          get '/express?search_galileo_query=test&profile=gkr1'
          expect(response).to redirect_to discover_path(search_galileo_query: 'test', profile: 'gkr1')
        end
      end

      context 'With an search_galileo_query param and instcode param' do
        it 'redirects to discover with search_galileo_query param set' do
          get '/express?search_galileo_query=test&instcode=tes1'
          expect(response).to redirect_to discover_path(search_galileo_query: 'test', instcode: 'tes1')
        end
      end

      context 'With an search_galileo_query, instcode, and profile params' do
        it 'redirects to discover with advanced param set' do
          get '/express?search_galileo_query=test&instcode=tes1&profile=gkr1'
          expect(response).to redirect_to discover_path(search_galileo_query: 'test', instcode: 'tes1', profile: 'gkr1')
        end
      end
    end
  end
end
