# frozen_string_literal: true

require 'rails_helper'

describe 'Redirect (open athens) endpoint', type: :request do

  context 'with a valid inst and url' do
    it 'redirects to an OpenAthens redirector URL' do
      get '/redirect?inst=asc1&url=https://www.test.com?param=hi'
      expect(response).to redirect_to(
        'https://go.openathens.net/redirector/agnesscott.edu?url=https%3A%2F%2Fwww.test.com%3Fparam%3Dhi'
      )
    end
  end
  context 'with a non-existent institution' do
    it 'redirects to the welcome path' do
      get '/redirect?inst=nope&url=https://www.test.com'
      expect(response).to redirect_to welcome_path
      expect(flash[:error]).to eq 'Sorry, you cant use /redirect with this institution code'
    end
  end
  context 'with an invalid url' do
    it 'redirects to the welcome path' do
      get '/redirect?inst=asc1&url=invalid_url'
      expect(response).to redirect_to welcome_path
      expect(flash[:error]).to eq 'Sorry, that is not a valid URL'
    end
  end
end