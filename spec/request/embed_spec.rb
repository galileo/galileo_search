# frozen_string_literal: true

require 'rails_helper'

describe 'Embeddable search box endpoint', type: :request do

  def encoded_json(params)
    { json: CGI.escape(params.to_json) }
  end

  context 'athens regional library case' do
    params = { input_width: '500px',
               profile: 'gpls_pines',
               advanced: 1,
               home: 1,
               host: 'www.athenslibrary.org',
               protocol: 'http' }
    it 'works' do
      get '/cgi/search_galileo', params: encoded_json(params)
      expect(response.code).to eq '200'
      expect(response.body).to include 'gpls_pines'
    end
  end
  context 'disciplines case' do
    params = {
      profile: 'usg',
      disciplines: 'libr*,lawx*,reli',
      disciplines_type: 'select multiple 2',
      peer_reviewed: 'hidden',
      library_collections: 'selected',
      full_text: 'unselected'
    }
    it 'works' do
      get '/cgi/search_galileo', params: encoded_json(params)
      expect(response.code).to eq '200'
      expect(response.body).to include 'usg'
      # expect(response.body).to include 'lawx'
    end
  end
end