# frozen_string_literal: true

# stub responses for EZproxy cookie snatching
module EzProxyRequestHelpers
  def stub_ezp_requests
    stub_request(:get, /http:\/\/proxygsu-/).to_return(
      status: 200, body: '', headers: { 'set-cookie' => 'testezpcookie' }
    )
  end
end
