# frozen_string_literal: true

# stub responses for Authentication Support API V1
module AuthApiHelpers
  include JSONFixtures

  def stub_auth_api
    stub_unsuccessful_ip_request
    stub_successful_ip_request
    stub_successful_passphrase_request
    stub_unsuccessful_passphrase_request
  end

  def stub_successful_ip_request
    stub_request(:get, api_url('ip=1.1.1.1'))
      .with(request_headers)
      .to_return(
        successful_json_response(
          json_string('api_ip_success.json')
        )
      )
  end

  def stub_unsuccessful_ip_request
    stub_request(:get, api_url('ip=0.0.0.0'))
      .with(request_headers)
      .to_return(
        successful_json_response(
          json_string('api_failed.json')
        )
      )
  end

  def stub_successful_passphrase_request
    stub_request(:get, api_url('passphrase=success'))
      .with(request_headers)
      .to_return(
        successful_json_response(
          json_string('api_passphrase_success.json')
        )
      )
  end

  def stub_unsuccessful_passphrase_request
    stub_request(:get, api_url('passphrase=failed'))
      .with(request_headers)
      .to_return(
        successful_json_response(
          json_string('api_failed.json')
        )
      )
  end

  def api_url(params)
    Rails.application.credentials.auth_support_api[:url] + 'inst?' + params
  end

  def request_headers
    { headers: { 'X-User-Token' => 'test-token' } }
  end

  private

  def successful_json_response(body)
    { status: 200,
      headers: { 'Content-Type' => 'application/json' },
      body: body }
  end

end
