# frozen_string_literal: true

# stub responses for EZproxy cookie snatching
module RssRequestHelpers
  def stub_rss_requests
    stub_request(:get, 'https://about.galileo.usg.edu/news/feed/').
      with(
        headers: {
        'Accept'=>'*/*',
        'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
        'User-Agent'=>'Ruby'
        }).to_return(status: 200, body: '', headers: {})

    stub_request(:get, "https://about.galileo.usg.edu/system_status/feed/").
      with(
        headers: {
          'Accept'=>'*/*',
          'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
          'User-Agent'=>'Ruby'
        }).to_return(status: 200, body: "", headers: {})
  end
end
