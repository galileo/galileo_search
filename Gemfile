# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.2.2'

gem 'addressable', '~>2.8.0'
gem 'aws-sdk-rails'
gem 'aws-sdk-s3'
gem 'bindata', '>= 2.4.10'
gem 'blacklight', '~> 7.7'
gem 'bootsnap', '>= 1.4.2', require: false
gem 'bootstrap', '~> 4.0'
gem 'bootstrap_form', '>= 4.2.0'
gem 'ebsco-eds'
gem 'exception_notification'
gem 'font-awesome-sass', '~> 5.15.1'
gem 'hashie'
gem 'hiredis'
gem 'hiredis-client'
gem 'httparty'
gem 'ims-lti'
gem 'jbuilder', '~> 2.7'
gem 'mock_redis'
gem 'net-ftp'
gem 'nokogiri', '>= 1.13.5'
gem 'oj'
gem 'openid_connect'
gem 'pg', '>= 0.18', '< 2.0'
gem 'psych', '< 4'
gem 'puma'
gem 'rails', '~> 7.0'
gem 'rails_warden'
gem 'recaptcha'
gem 'redis'
gem 'rsolr', '>= 1.0', '< 3'
gem 'rss'
gem 'sass-rails', '>= 6.0'
gem "sentry-ruby"
gem "sentry-rails"
gem 'sidekiq', '>= 6.4.0'
gem 'slack-notifier'
gem 'turbolinks', '~> 5'
gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]
gem 'webpacker', '~> 4.0'

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'rspec-rails', '~>5.1'
end

group :development do
  gem 'letter_opener'
  gem 'listen'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara'
  gem 'capybara-screenshot'
  gem 'fabrication'
  gem 'faker'
  gem 'selenium-webdriver', '~> 4.24'
  gem 'simplecov'
  gem 'webmock'
end
