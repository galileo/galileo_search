#!/bin/bash
groupadd -g 999 gitlab-runner && useradd -u 999 -g 999 gitlab-runner && usermod -d /code gitlab-runner
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -
curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
echo 'deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main' | tee /etc/apt/sources.list.d/google-chrome.list
apt-get update -qq && apt-get -y install sudo google-chrome-stable
curl 'http://solr:8983/solr/admin/cores?action=CREATE&name=galileo-test&instanceDir=/var/solr&configSet=galileo'
rm -rf vendor && mv /vendor .
HOME=/code gem install bundler
HOME=/code bundle config set deployment 'true'
HOME=/code bundle install
cp /code/config/database.yml.ci /code/config/database.yml
cp /code/config/blacklight.yml.ci /code/config/blacklight.yml
HOME=/code bundle exec rake db:create RAILS_ENV=test
HOME=/code bundle exec rake db:schema:load RAILS_ENV=test
chown -R gitlab-runner:gitlab-runner /code /code/.bundle
HOME=/code sudo -E -u gitlab-runner yarn install
HOME=/code sudo -E -u gitlab-runner bundle exec rake assets:precompile RAILS_ENV=test
sudo -E -u gitlab-runner google-chrome-stable --disable-setuid-sandbox --headless --disable-gpu --no-sandbox --remote-debugging-port=9222 http://localhost &
provision/load_solr_data_ci.sh
HOME=/code sudo -E -u gitlab-runner bundle exec rspec --color --format documentation
