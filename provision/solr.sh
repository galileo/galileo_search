#!/bin/sh -e

# install solr
wget https://ai.galib.uga.edu/files/solr-8.5.1.tgz
tar -xvf solr-8.5.1.tgz

# use galileo config
mkdir solr-8.5.1/server/solr/configsets/galileo
mkdir solr-8.5.1/server/solr/configsets/galileo/conf
mkdir solr-8.5.1/server/solr/configsets/galileo/conf/lang

# copy config from solr-configs project
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/schema.xml --output solr-8.5.1/server/solr/configsets/galileo/conf/schema.xml
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/solrconfig.xml --output solr-8.5.1/server/solr/configsets/galileo/conf/solrconfig.xml
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/elevate.xml --output solr-8.5.1/server/solr/configsets/galileo/conf/elevate.xml
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/stopwords.txt --output solr-8.5.1/server/solr/configsets/galileo/conf/stopwords.txt
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/stopwords_en.txt --output solr-8.5.1/server/solr/configsets/galileo/conf/stopwords_en.txt
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/synonyms.txt --output solr-8.5.1/server/solr/configsets/galileo/conf/synonyms.txt
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/protwords.txt --output solr-8.5.1/server/solr/configsets/galileo/conf/protwords.txt

# start solr, creating collection
solr-8.5.1/bin/solr start -c -force
solr-8.5.1/bin/solr create -c galileo-development -d galileo -force
solr-8.5.1/bin/solr create -c galileo-test -d galileo -force
solr-8.5.1/bin/solr stop
