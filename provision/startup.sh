# Redis
./redis-*/src/redis-server --daemonize yes --protected-mode no

# Solr
./solr-*/bin/solr start -c -force
