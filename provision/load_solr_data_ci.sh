#!/usr/bin/env bash

curl http://solr:8983/solr/galileo-test/update?commit=true -d  '<delete><query>*:*</query></delete>'

curl -X POST 'http://solr:8983/solr/galileo-test/update/json' --data-binary @provision/data/allocations.json -H 'Content-type:application/json'
curl -X POST 'http://solr:8983/solr/galileo-test/update/json' --data-binary @provision/data/institutions.json -H 'Content-type:application/json'
curl -X POST 'http://solr:8983/solr/galileo-test/update/json?commit=true' --data-binary @provision/data/sites.json -H 'Content-type:application/json'
