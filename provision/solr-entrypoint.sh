#!/usr/bin/env bash

# use galileo config
mkdir -p /var/solr/configsets/galileo/conf
mkdir -p /var/solr/configsets/galileo/conf/lang

# copy config from solr-configs project
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/schema.xml --output /var/solr/configsets/galileo/conf/schema.xml
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/solrconfig.xml --output /var/solr/configsets/galileo/conf/solrconfig.xml
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/elevate.xml --output /var/solr/configsets/galileo/conf/elevate.xml
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/stopwords.txt --output /var/solr/configsets/galileo/conf/stopwords.txt
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/stopwords_en.txt --output /var/solr/configsets/galileo/conf/stopwords_en.txt
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/synonyms.txt --output /var/solr/configsets/galileo/conf/synonyms.txt
curl https://gitlab.galileo.usg.edu/infra/solr-configs/galileo/raw/master/galileo/protwords.txt --output /var/solr/configsets/galileo/conf/protwords.txt

/opt/solr/bin/solr create_core -c galileo-test -d /var/solr/configsets/galileo -p 8983
