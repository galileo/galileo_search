#!/usr/bin/env bash

# Run this _ONLY_ from /provision as:
# `./load_solr_data.sh` (linux)
# ..or...
# `sh load_solr_data.sh`

curl http://localhost:9983/solr/galileo-test/update?commit=true -d  '<delete><query>*:*</query></delete>'

curl -X POST 'http://localhost:9983/solr/galileo-test/update/json' --data-binary @data/allocations.json -H 'Content-type:application/json'
curl -X POST 'http://localhost:9983/solr/galileo-test/update/json' --data-binary @data/institutions.json -H 'Content-type:application/json'
curl -X POST 'http://localhost:9983/solr/galileo-test/update/json?commit=true' --data-binary @data/sites.json -H 'Content-type:application/json'
