# frozen_string_literal: true

require 'net/ftp'
require 'csv'
require 'fileutils'

# TODO: once behavior is established, factor out this procedural code into
# StatsService and/or an FtpService
namespace :galileo do
  desc 'Pushes recent stats files from local /stats dir to galftp'
  # Connect to `galftp` and upload the link and login stats files from yesterday
  task push_yesterdays_stats: :environment do
    def host_prefix(hostname)
      return hostname unless hostname.include? '.'

      hostname[0..(hostname.index('.') - 1)]
    end

    job_log = Logger.new './log/jobs.log'

    ftp = Net::FTP.new('galftp.galib.uga.edu',
                       debug_mode: !Rails.env.production?,
                       port: 990,
                       ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE,
                              min_version: OpenSSL::SSL::TLS1_2_VERSION,
                              max_version: OpenSSL::SSL::TLS1_2_VERSION },
                       username:
                         Rails.application.credentials.dig(:galftp, :username),
                       password:
                         Rails.application.credentials.dig(:galftp, :password))

    # make env dir unless it exists
    begin
      ftp.chdir Rails.env.to_s.downcase
    rescue Net::FTPPermError
      ftp.mkdir Rails.env.to_s.downcase
      ftp.chdir Rails.env.to_s.downcase
    end

    # Compose path values for files to be transmitted
    stats_dir = File.join Rails.root, 'stats'
    yesterday_date_string = DateTime.yesterday.strftime(
      StatisticsService::STATS_FILE_DATE_SUFFIX_FORMAT
    )
    link_stats_file_name = "links.stats.#{yesterday_date_string}"
    link_stats_file_path = File.join(stats_dir, link_stats_file_name)
    login_stats_file_name = "login.stats.#{yesterday_date_string}"
    login_stats_file_path = File.join(stats_dir, login_stats_file_name)
    search_stats_file_name = "search.stats.#{yesterday_date_string}"
    search_stats_file_path = File.join(stats_dir, search_stats_file_name)

    # attempt to open stats files
    if File.exist? link_stats_file_path
      begin
        replacement_link_header_row = 'Session Id,Begin Time,Action,Database Name,Server,Institution Code'
        system "sed --in-place '1 s/^.*$/#{replacement_link_header_row}/' #{link_stats_file_path}"
      rescue StandardError => e
        job_log.error "Link stat file (#{link_stats_file_name}) could not be processed: #{e.message}"
      end
    else
      job_log.error "#{link_stats_file_path} not found - headers not updated"
    end
    if File.exist? login_stats_file_path
      begin
        replacement_login_header_row = 'Session Id,Begin Time,Action,IP,Server,Institution Code,Auth Method,Site'
        system "sed --in-place '1 s/^.*$/#{replacement_login_header_row}/' #{login_stats_file_path}"
      rescue StandardError => e
        job_log.error "Login stat file (#{login_stats_file_name}) could not be processed: #{e.message}"
      end
    else
      job_log.error "#{login_stats_file_path} not found - headers not updated"
    end
    if File.exist? search_stats_file_path
      begin
        replacement_search_header_row = 'Time,Institution Code,Site Code,Query Type,Query'
        system "sed --in-place '1 s/^.*$/#{replacement_search_header_row}/' #{search_stats_file_path}"
      rescue StandardError => e
        job_log.error "Search stat file (#{search_stats_file_name}) could not be processed: #{e.message}"
      end
    else
      job_log.error "#{search_stats_file_path} not found - headers not updated"
    end

    @hostname = host_prefix Socket.gethostname

    # try to upload yesterday's files
    if File.exist? link_stats_file_path
      ftp.puttextfile(link_stats_file_path,
                      "#{File.basename(link_stats_file_path)}.#{@hostname}")
    else
      job_log.error "#{link_stats_file_path} not found - not transmitted"
    end
    if File.exist? login_stats_file_path
      ftp.puttextfile(login_stats_file_path,
                      "#{File.basename(login_stats_file_path)}.#{@hostname}")
    else
      job_log.error "#{login_stats_file_path} not found - not transmitted"
    end
    if File.exist? search_stats_file_path
      ftp.puttextfile(search_stats_file_path,
                      "#{File.basename(search_stats_file_path)}.#{@hostname}")
    else
      job_log.error "#{search_stats_file_path} not found - not transmitted"
    end

    # TODO: validate transmission?
    # TODO: slack a notification?
    # TODO: ensure files from the-day-before-yesterday wern't missed?

    # close connection
    ftp.quit
  end

  desc 'Pushes stats files for a given date from local /stats dir to galftp'
  task(:push_stats, [:yyyymmdd] => [:environment]) do |_, args|
    def host_prefix(hostname)
      return hostname unless hostname.include? '.'

      hostname[0..(hostname.index('.') - 1)]
    end

    job_log = Logger.new './log/jobs.log'

    ftp = Net::FTP.new('galftp.galib.uga.edu',
                       debug_mode: !Rails.env.production?,
                       port: 990,
                       ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE,
                              min_version: OpenSSL::SSL::TLS1_2_VERSION,
                              max_version: OpenSSL::SSL::TLS1_2_VERSION },
                       username:
                         Rails.application.credentials.dig(:galftp, :username),
                       password:
                         Rails.application.credentials.dig(:galftp, :password))

    # make env dir unless it exists
    begin
      ftp.chdir Rails.env.to_s.downcase
    rescue Net::FTPPermError
      ftp.mkdir Rails.env.to_s.downcase
      ftp.chdir Rails.env.to_s.downcase
    end

    # Compose path values for files to be transmitted
    stats_dir = File.join Rails.root, 'stats'
    date_string = args[:yyyymmdd].to_date.strftime(
      StatisticsService::STATS_FILE_DATE_SUFFIX_FORMAT
    )
    link_stats_file_name = "links.stats.#{date_string}"
    link_stats_file_path = File.join(stats_dir, link_stats_file_name)
    login_stats_file_name = "login.stats.#{date_string}"
    login_stats_file_path = File.join(stats_dir, login_stats_file_name)
    search_stats_file_name = "search.stats.#{date_string}"
    search_stats_file_path = File.join(stats_dir, search_stats_file_name)

    # attempt to open stats files
    if File.exist? link_stats_file_path
      begin
        replacement_link_header_row = 'Session Id,Begin Time,Action,Database Name,Server,Institution Code'
        system "sed --in-place '1 s/^.*$/#{replacement_link_header_row}/' #{link_stats_file_path}"
      rescue StandardError => e
        job_log.error "Link stat file (#{link_stats_file_name}) could not be processed: #{e.message}"
      end
    else
      job_log.error "#{link_stats_file_path} not found - headers not updated"
    end
    if File.exist? login_stats_file_path
      begin
        replacement_login_header_row = 'Session Id,Begin Time,Action,IP,Server,Institution Code,Auth Method,Site'
        system "sed --in-place '1 s/^.*$/#{replacement_login_header_row}/' #{login_stats_file_path}"
      rescue StandardError => e
        job_log.error "Login stat file (#{login_stats_file_name}) could not be processed: #{e.message}"
      end
    else
      job_log.error "#{login_stats_file_path} not found - headers not updated"
    end
    if File.exist? search_stats_file_path
      begin
        replacement_search_header_row = 'Time,Institution Code,Site Code,Query Type,Query'
        system "sed --in-place '1 s/^.*$/#{replacement_search_header_row}/' #{search_stats_file_path}"
      rescue StandardError => e
        job_log.error "Search stat file (#{search_stats_file_name}) could not be processed: #{e.message}"
      end
    else
      job_log.error "#{search_stats_file_path} not found - headers not updated"
    end

    @hostname = host_prefix Socket.gethostname

    # try to upload files
    if File.exist? link_stats_file_path
      ftp.puttextfile(link_stats_file_path,
                      "#{File.basename(link_stats_file_path)}.#{@hostname}")
    else
      job_log.error "#{link_stats_file_path} not found - not transmitted"
    end
    if File.exist? login_stats_file_path
      ftp.puttextfile(login_stats_file_path,
                      "#{File.basename(login_stats_file_path)}.#{@hostname}")
    else
      job_log.error "#{login_stats_file_path} not found - not transmitted"
    end
    if File.exist? search_stats_file_path
      ftp.puttextfile(search_stats_file_path,
                      "#{File.basename(search_stats_file_path)}.#{@hostname}")
    else
      job_log.error "#{search_stats_file_path} not found - not transmitted"
    end

    # close connection
    ftp.quit
  end


  desc 'Pushes current login stats from local /stats dir to galftp'
  # Connect to `galftp` and upload the current login stats file
  # This task should be run via cron every 15 minutes
  task push_current_login_stats: :environment do
    def host_prefix(hostname)
      return hostname unless hostname.include? '.'

      hostname[0..(hostname.index('.') - 1)]
    end

    job_log = Logger.new './log/jobs.log'

    ftp = Net::FTP.new('galftp.galib.uga.edu',
                       debug_mode: !Rails.env.production?,
                       port: 990,
                       ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE,
                              min_version: OpenSSL::SSL::TLS1_2_VERSION,
                              max_version: OpenSSL::SSL::TLS1_2_VERSION },
                       username:
                         Rails.application.credentials.dig(:galftp, :username),
                       password:
                         Rails.application.credentials.dig(:galftp, :password))

    # make current dir unless it exists
    begin
      ftp.chdir Rails.env.to_s.downcase
    rescue Net::FTPPermError
      ftp.mkdir Rails.env.to_s.downcase
      ftp.chdir Rails.env.to_s.downcase
    end

    # create current files dir
    begin
      ftp.chdir 'current'
    rescue Net::FTPPermError
      ftp.mkdir 'current'
      ftp.chdir 'current'
    end

    today_date_string = DateTime.now.strftime(
      StatisticsService::STATS_FILE_DATE_SUFFIX_FORMAT
    )

    # Compose path values for file to be transmitted
    stats_dir = File.join Rails.root, 'stats'
    login_stats_file_name = "login.stats.#{today_date_string}"

    # attempt to copy stats file
    begin
      login_stats_file_path = File.join stats_dir, login_stats_file_name
      copy_file_path = "#{login_stats_file_path}.tmp"
      FileUtils.copy_file login_stats_file_path, copy_file_path
    rescue StandardError => e
      job_log.error "Link stat file (#{login_stats_file_name}) could not be copied: #{e.message}"
      ftp.quit
      next
    end

    @hostname = host_prefix Socket.gethostname

    # try to upload current file
    begin
      ftp.puttextfile(copy_file_path,
                      "#{File.basename(login_stats_file_path)}.#{@hostname}")
    rescue StandardError => e
      job_log.error "Problem with FTP transfer: #{e}"
    ensure
      FileUtils.remove copy_file_path
    end

    # close connection
    ftp.quit
  end

  desc 'Pushes current login stats from local /stats dir to galftp on a shutdown'
  # Connect to `galftp` and upload the current login stats file
  # This task should be run only when the system is being shut down and destroyed
  task shutdown_send_login_stats: :environment do
    def host_prefix(hostname)
      return hostname unless hostname.include? '.'

      hostname[0..(hostname.index('.') - 1)]
    end

    job_log = Logger.new './log/jobs.log'

    ftp = Net::FTP.new('galftp.galib.uga.edu',
                       debug_mode: !Rails.env.production?,
                       port: 990,
                       ssl: { verify_mode: OpenSSL::SSL::VERIFY_NONE,
                              min_version: OpenSSL::SSL::TLS1_2_VERSION,
                              max_version: OpenSSL::SSL::TLS1_2_VERSION },
                       username:
                         Rails.application.credentials.dig(:galftp, :username),
                       password:
                         Rails.application.credentials.dig(:galftp, :password))

    # make current dir unless it exists
    begin
      ftp.chdir Rails.env.to_s.downcase
    rescue Net::FTPPermError
      ftp.mkdir Rails.env.to_s.downcase
      ftp.chdir Rails.env.to_s.downcase
    end

    # create current files dir
    begin
      ftp.chdir 'current'
    rescue Net::FTPPermError
      ftp.mkdir 'current'
      ftp.chdir 'current'
    end

    today_date_string = DateTime.now.strftime(
      StatisticsService::STATS_FILE_DATE_SUFFIX_FORMAT
    )

    # Compose path values for file to be transmitted
    stats_dir = File.join Rails.root, 'stats'
    login_stats_file_name = "login.stats.#{today_date_string}"

    # attempt to copy stats file
    begin
      login_stats_file_path = File.join stats_dir, login_stats_file_name
      copy_file_path = "#{login_stats_file_path}.tmp"
      FileUtils.copy_file login_stats_file_path, copy_file_path
    rescue StandardError => e
      job_log.error "Link stat file (#{login_stats_file_name}) could not be copied: #{e.message}"
      ftp.quit
      next
    end

    @hostname = host_prefix Socket.gethostname
    suffix = DateTime.now.strftime('%H.%M.%S')

    # try to upload current file
    begin
      ftp.chdir 'shutdown'
      ftp.puttextfile(copy_file_path,
                      "#{File.basename(login_stats_file_path)}.#{@hostname}.#{suffix}")
    rescue StandardError => e
      job_log.error "Problem with FTP transfer: #{e}"
    ensure
      FileUtils.remove copy_file_path
    end

    # close connection
    ftp.quit
  end
end
