# frozen_string_literal: true

module Exceptions
  class EZproxySessionsError < StandardError; end
end
