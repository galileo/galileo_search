# frozen_string_literal: true

require 'strategies/galileo_strategy'

# thrown after identifying the institution via the PINES code if the inst is OA-enabled
class UseOpenAthensForInstitution < StandardError
  def initialize(institution)
    super
    @inst_code = institution.code
  end
  attr_reader :inst_code
end

# handle confirmation of PINES code
class PinesStrategy < GalileoStrategy

  # pinesid looks like '79bc5c8fb5de41d53865eb762fdf9e4c'

  def authenticate!
    pinesid = params[:pinesid]
    pines_url = "#{Rails.configuration.gapines_host}/osrf-gateway-v1?format=xml&service=open-ils.actor&method=open-ils.actor.safe_token.home_lib.shortname&input_format=json&param=%22#{pinesid}%22&param=%22galileo%22"

    response = HTTParty.get pines_url
    code_match = response.body.match(%r{<payload><string>([^<]+)</string>})
    if code_match.nil?
      fail!
      return
    end
    code = code_match[1]
    if code.blank?
      fail!
      return
    end
    institution_service = InstitutionService.new
    institution = institution_service.find_for_pines code

    if institution
      pines_user_login(institution)
    else
      regional_code_match = code.match(/([^-]+)-/)
      if regional_code_match.nil?
        fail!
        return
      end
      regional_code = regional_code_match[1]
      unless regional_code
        fail!
        return
      end

      institution = institution_service.find_for_pines regional_code
      if institution
        pines_user_login(institution)
      else
        fail!
      end
    end
  end

  def pines_user_login(institution)
    stop_if_open_athens_inst institution
    on_campus = IpLookupService.new.ip_is_for?(request.remote_ip, institution.code)
    remote = !on_campus
    user = generate_and_login_user institution, :pines, remote, params[:site]
    success! user
  end

  def stop_if_open_athens_inst(institution)
    raise UseOpenAthensForInstitution, institution if institution.open_athens
  end
end
