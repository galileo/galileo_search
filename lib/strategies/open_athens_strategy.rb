# frozen_string_literal: true

require 'strategies/galileo_strategy'
require 'open_athens_worker'

# Handle authentication of a GALILEO user _after_ performing OpenAthens
# authentication, setting a open_athens_id param after redirect
class OpenAthensStrategy < GalileoStrategy
  def valid?
    ip = request.remote_ip
    params[:code] && ip.present?
  end

  def authenticate!
    code = params[:code]
    unless code
      pass
      return
    end

    begin
      oa_worker = OpenAthensWorker.new session[:redirect_url]
    rescue StandardError => _e
      pass
      return
    end

    nonce = session[:oa_nonce]
    begin
      user_info = oa_worker.redirect code, nonce
    rescue OpenIDConnect::ResponseObject::IdToken::InvalidNonce => _e
      fail!
      return
    end

    claims = user_info.raw_attributes
    scoped_affiliation = claims['eduPersonScopedAffiliation']
    unless scoped_affiliation
      fail!
      return
    end

    institution = institution_for_scope(scoped_affiliation)
    unless institution
      fail!
      return
    end

    first_name = claims['forenames']
    last_name = claims['surname']

    local = IpLookupService.new.ip_is_for?(request.remote_ip, institution.code)
    auth_type = :oa
    user = generate_and_login_user institution, auth_type, !local, params[:site], first_name, last_name
    success! user
  end

  # return first matching institution
  def institution_for_scope(aff)
    institution_service = InstitutionService.new
    scopes(aff).each do |scope|
      inst = institution_service.find_for_oa_subscope(scope) || institution_service.find_for_oa_scope(scope)
      return inst if inst
    end
    nil
  end

  # make scopes an array regardless
  def scopes(aff)
    if aff.is_a?(String)
      [domain(aff)]
    elsif aff.is_a?(Array)
      aff.map {|a| domain(a) }.uniq
    end
  end

  # from member@example.edu, return example.edu
  def domain(aff)
    aff[(aff.index('@') + 1).. -1]
  end
end
