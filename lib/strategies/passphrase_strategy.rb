# frozen_string_literal: true

require 'strategies/galileo_strategy'

# :nodoc:
class PassphraseStrategy < GalileoStrategy
  def valid?
    params[:passphrase]
  end

  def authenticate!
    institution = InstitutionService.new.find_by_pass params[:passphrase]
    unless institution
      fail!
      return
    end
    # note: password authenticated users are always considered remote
    user = generate_and_login_user institution, :password, true, params[:site]
    success! user
  end
end
