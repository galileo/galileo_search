# frozen_string_literal: true

# abstract auth strategy actions
class GalileoStrategy < Warden::Strategies::Base

  # @return [Session] object for serialization
  # @param [SolrInstitution] institution
  # @param [Symbol] auth_type
  # @param [TrueClass, FalseClass] remote
  # @param [String] site_code
  def generate_and_login_user(institution, auth_type, remote, site_code = nil,
                              first_name = nil, last_name = nil)
    StatisticsService.login(request, session, institution.code, auth_type.to_s,
                            remote, site_code)
    User.new(
      solr_institution: institution, auth_type: auth_type, site_code: site_code,
      remote: remote, first_name: first_name, last_name: last_name
    )
  end
end
