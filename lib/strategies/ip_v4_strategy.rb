# frozen_string_literal: true

require 'strategies/galileo_strategy'
require 'resolv'

# :nodoc:
class IpV4Strategy < GalileoStrategy
  def valid?
    ip = request.remote_ip
    ip.present? && ip =~ Resolv::IPv4::Regex
  end

  def authenticate!
    inst_code = IpLookupService.new.inst_code_for request.remote_ip
    institution = InstitutionService.new.find inst_code
    if institution
      user = generate_and_login_user institution, :ip, false
      success!(user)
    else
      # pass here is appropriate as we immediately return. also, IP auth
      # may be used in a situation where we want to cascade to another strategy
      # rather than halt!
      pass
    end
  rescue StandardError => e
    pass
    ExceptionNotifier.notify_exception(e)
  end
end
