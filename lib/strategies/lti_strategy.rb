# frozen_string_literal: true

require 'strategies/galileo_strategy'

# Handle authentication of a GALILEO user _after_ performing OpenAthens
# authentication, setting a open_athens_id param after redirect
class LtiStrategy < GalileoStrategy
  def valid?
    # check that all required params are given
    # oauth_nonce, oauth_timestamp, consumer_key, etc.
    true
  end

  def authenticate!
    institution = InstitutionService.new.find_for_lti(
      params['oauth_consumer_key'], params['custom_inst_id']
    )

    unless institution
      fail!
      return
    end

    # see: https://github.com/instructure/ims-lti
    authenticator = IMS::LTI::Services::MessageAuthenticator.new(
      request.url, request.request_parameters, institution.lti_secret
    )

    # validate LTI response
    unless authenticator&.valid_signature?
      fail!
      return
    end

    # init redis only when needed
    @redis = RedisService.new
    unless @redis.unique_nonce?(params['oauth_nonce'])
      fail!
      return
    end
    # Add valid nonce
    @redis.nonce params['oauth_nonce']

    # check age of request
    if DateTime.strptime(params['oauth_timestamp'], '%s') < 5.minutes.ago
      fail!
      return
    end

    local = IpLookupService.new.ip_is_for?(request_ip, institution.code)
    user = generate_and_login_user(
      institution,
      :lti,
      !local
    )
    success!(user)
  rescue StandardError => e
    pass
    ExceptionNotifier.notify_exception(e)
  end

  def request_ip
    request.remote_ip
  end
end