# frozen_string_literal: true
# For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

Rails.application.routes.draw do

  root to: 'welcome#index'
  get :welcome, to: 'welcome#index'

  get 'express', to: 'express#show'
  post 'express', to: 'express#show'

  get 'discover', to: 'discover#index'

  get 'redirect', to: 'redirect#show'

  get 'cgi/search_galileo', to: 'embed#index', as: :embed
  get 'javascript/search_galileo.js', to: 'embed#search_galileo', as: :search_galileo_js
  get 'embed_test', to: 'embed#show'

  # 'home' is an old route but we want to continue to support it with these redirects
  # (see the preferred 'institution_homepage' route below)
  get ':inst_code/home/:view', to: redirect(path: '/%{inst_code}/%{view}')
  get ':inst_code/home',       to: redirect(path: '/%{inst_code}')

  get '(:inst_code)/bento/search', to: 'custom_bento#bento_search', as: 'institution_bento_search'

  get ':inst_code/databases(/:view)', to: 'catalog#index', as: 'institution_databases'
  get ':inst_code/search(/:view)', to: 'catalog#index', as: 'institution_search'
  get ':inst_code/database/:id(/:view)', to: 'catalog#show', as: 'institution_resource'
  get ':inst_code/resource/:id(/:view)', to: 'catalog#show'
  get ':inst_code/embed(/:view)', to: 'homepage#embed', as: 'institution_embed'
  get ':inst_code/sharable_links(/:view)', to: 'homepage#sharable_links', as: 'institution_sharable_links'
  get 'permalink_url_converter', to: 'open_athens_proxy_url_converter#open_athens_proxy_url_converter'
  post 'permalink_url_converter', to: 'open_athens_proxy_url_converter#open_athens_proxy_url_converter'
  get 'open_athens_proxy_url_converter', to: redirect(path: 'permalink_url_converter')

  get ':inst_code/bento/home/:view', to: redirect(path: '/%{inst_code}/%{view}')
  get ':inst_code/bento/home',       to: redirect(path: '/%{inst_code}')
  get ':inst_code/bento/placard(/:view)', to: 'bento#placard_api', as: 'institution_bento_placard'
  get ':inst_code/bento/fulltext', to: 'bento#eds_fulltext', as: 'institution_bento_eds_fulltext'
  get ':inst_code/bento/databases(/:view)', to: 'catalog#bento', as: 'institution_databases_bento'
  get 'bento/links', to: 'bento#alma_links_api', as: 'alma_links_api'
  get 'bento', to: 'bento#index', as: 'bento_search_query'

  get '(:inst_code)/bentos/:bento_type/', to: 'custom_bento#full_results', as: 'bento_full_results'
  match ':inst_code/bentos/:bento_type/api', to: 'custom_bento#full_results_api', via: %i[get post], as: 'bento_full_results_api'
  get ':inst_code/bentos/:bento_type/api/facets/:facet', to: 'custom_bento#facet_api', as: 'bento_facet_api'

  get ':inst_code/bento/api/:bento_type', to: 'custom_bento#bento_api', as: 'institution_bento_api'

  #TODO: will be removed around December release
  get ':inst_code/bento-custom/api/:bento_type', to: 'custom_bento#bento_api', as: 'institution_custom_bento_api'
  get ':inst_code/bento-custom/search', to: 'custom_bento#bento_search', as: 'institution_custom_bento_search'

  # Single record pages
  get ':inst_code/record/eds', to: 'records#eds', as: 'eds_record'
  get ':inst_code/record/journals_ebsco', to: 'records#journals_ebsco', as: 'journals_ebsco_record'

  # wayfinder-related
  post :select, to: 'welcome#select'
  get :clear_remembered, to: 'welcome#clear_remembered'
  post :clear_remembered_selection, to: 'welcome#clear_remembered_selection'
  get :instructions, to: 'welcome#instructions'
  get :wayfinder_data, to: 'wayfinder_data#wayfinder_data'

  get :credentials, to: 'credentials#show'

  scope :auth do
    post :login, to: 'sessions#new'
    get :login, to: 'sessions#new'
    get :logout, to: 'sessions#destroy'
    get :info, to: 'sessions#show'
  end

  post :lti, to: 'lti#create'

  scope :oa do
    get :login, to: 'open_athens#login', as: :oa_login
    get :redirect, to: 'open_athens#redirect', as: :oa_redirect
  end

  get :contact, to: 'contact#index'
  namespace :contact do
    get :dlg
    get :support
    get :other
    get :gkr
    get :nge_revisions
    get :nge_new_article
    get :nge_comments
    get :nge_errors
    post :submit
    post :relay
  end
  post 'cgi/contact', to: 'contact#submit'

  mount Blacklight::Engine => '/'

  concern :searchable, Blacklight::Routes::Searchable.new
  concern :exportable, Blacklight::Routes::Exportable.new

  get '/link/:code', to: 'link#show', as: 'link'

  resources :view, only: [:index]

  resource :catalog, only: [:index], as: 'catalog', path: '/catalog', controller: 'catalog' do
    concerns :searchable
  end

  get '/database/:id', to: 'catalog#show', as: 'resource'

  resources :resources, only: [:show], path: '/resource', controller: 'catalog' do
    concerns :exportable
  end

  resources :bookmarks do
    concerns :exportable

    collection do
      delete 'clear'
    end
  end



  match '/404', to: 'errors#file_not_found', via: :all
  match '/422', to: 'errors#unprocessable', via: :all
  match '/500', to: 'errors#internal_server_error', via: :all

  # these routes need to be at the end
  get ':view', to: 'sessions#new', as: 'view_login', view: /elementary|middle|highschool|educator|full/
  get ':inst_code(/:view)', to: 'homepage#show', as: 'institution_homepage', constraints: { inst_code: /[a-z0-9][a-z0-9][a-z0-9][a-z0-9]/, view: /elementary|middle|highschool|educator|full/ }


  unless Rails.env.production?
    get 'demo/button_options', to: 'demo#button_differentiation_options'
  end
end
