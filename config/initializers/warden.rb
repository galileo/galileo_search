# frozen_string_literal: true

require 'oj'
require 'strategies/ip_v4_strategy'
require 'strategies/pines_strategy'
require 'strategies/passphrase_strategy'
require 'strategies/open_athens_strategy'
require 'strategies/lti_strategy'

Rails.configuration.middleware.use RailsWarden::Manager do |manager|
  manager.failure_app = UnauthorizedController
  manager.default_strategies :ip
  # manager.intercept_401 = false # Warden will intercept 401 responses, which can cause conflicts
end

# :nodoc:
class Warden::SessionSerializer
  def serialize(user)
    Oj.dump user, {}
  end

  def deserialize(user_json)
    Oj.load user_json, {}
  end
end

module RailsWarden
  module Mixins
    # :nodoc:
    module HelperMethods
      # Presence of #current_user from RailsWarden helpers was triggering BL
      # to render bookmark stuff
      # TODO: rewire this to use warden session user (bookmarks, searches)
      remove_method :current_user
    end
  end
end

Warden::Strategies.add :lti, LtiStrategy
Warden::Strategies.add :open_athens, OpenAthensStrategy
Warden::Strategies.add :passphrase, PassphraseStrategy
Warden::Strategies.add :pines, PinesStrategy
Warden::Strategies.add :ip, IpV4Strategy
