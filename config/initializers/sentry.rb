Sentry.init do |config|
  config.enabled_environments = %w[dev staging production]
  config.dsn = Rails.application.credentials.dig(:sentry, :dsn)
  config.breadcrumbs_logger = [:active_support_logger, :http_logger]

  # Set traces_sample_rate to 1.0 to capture 100%
  # of transactions for performance monitoring.
  # We recommend adjusting this value in production.
  config.traces_sample_rate = 0.125

  # Control Errors rate sent to sentry
  # config.sample_rate = 1.0
end