# frozen_string_literal: true

# Configures the Rails session store to be secure by default
# See: https://api.rubyonrails.org/classes/ActionDispatch/Session/CookieStore.html

Rails.application.config.session_store :cookie_store,
                                       secure: !(Rails.env.development? || Rails.env.test?)