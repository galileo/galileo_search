# frozen_string_literal: true

redis_url = if Rails.env.development?
              'redis://localhost:3379'
            else
              'redis://localhost:6379'
            end

Sidekiq.configure_server do |config|
  config.redis = {
    driver: :hiredis,
    url: redis_url
  }
end

Sidekiq.configure_client do |config|
  config.redis = {
    driver: :hiredis,
    url: redis_url
  }
end