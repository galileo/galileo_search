## Testing

----------------------------------------------------------------------

### Welcome Page

- [ ] Tour
- [ ] Wayfinder
- [ ] Password entry
- [ ] Instructions page
- Log in as
   - [ ] usg
   - [ ] techinst
   - [ ] publiclibs
   - [ ] ampals
   - [ ] gpals
   - [ ] k12

----------------------------------------------------------------------

### Homepage

- [ ] GALILEO icon link
- [ ] Tour
- [ ] Support
- [ ] Log out
- [ ] Change View (K12)
- [ ] EDS search
- [ ] Advanced Search
- [ ] All Databases
- [ ] All Journals
- [ ] In the Spotlight
- [ ] About the school
- Tools
   - [ ] Embedding a GALILEO Search Box
   - [ ] Shareable Links
   - [ ] Power Notes
   - [ ] Zotero
- [ ] GALILEO News
- [ ] GALILEO System Alerts
- [ ] Footer links

----------------------------------------------------------------------

### Contact Us (see footer)

- [ ] Support Questions, Contact Support Services
- [ ] General Questions/Concerns, Contact Customer Relations

----------------------------------------------------------------------

### Database Search Results page

- [ ] Database search
- [ ] Previous/Next
- [ ] Sort options
- [ ] Per page options
- Facets
   - [ ] Types
   - [ ] Subjects
   - [ ] A-Z
   - [ ] Keywords
   - [ ] Start over
- [ ] Database title link
- [ ] Database description/branding
- [ ] More Info
- [ ] Express Link
- [ ] Icon image

----------------------------------------------------------------------

### Database More Info page

- [ ] Database search
- [ ] Start Over
- [ ] Back to Search
- [ ] Database title/description, etc.
- [ ] Express Link
- [ ] Subjects, Formats, Keywords, Vendor
-  Tools
   - [ ] Email
   - [ ] Send to Phone

----------------------------------------------------------------------

### Bento

- On hompage
  - [ ] Test Search
  - [ ] Test Search with Full text
  - [ ] Advanced Search link
- On bento results page
  - [ ] Test Search
  - [ ] Test Search with Full text
  - [ ] Advanced Search link
- On bento boxes
  - [ ] Bento box title link (more results)
  - [ ] Description [i] icon
  - [ ] Show More Results
- On more results modal
  - [ ] Next/Prev arrows
  - [ ] Refine search
  - [ ] [X] to close/[Esc] to close

----------------------------------------------------------------------

### Academic using OpenAthens

- [ ] Bento Search (e.g.: search “Education”) (confirm bento boxes load)
- [ ] Advanced Search link
- [ ] Journal Placard
- [ ] Bento box heading anchors
- [ ] Database link
- [ ] Database More Info button
- [ ] Database bento modal
- [ ] Database bento refine search link
- [ ] New Bento Search with Full Text Limiter (e.g.: search “George Washington”)
- [ ] Journals database link
- [ ] Link to item in native EDS
- [ ] PDF eBook Full Text
- [ ] ePub eBook Full Text
- [ ] PDF Full Text Button
- [ ] EDS Custom Link (e.g.Films on Demand)
- [ ] Bento Header link (loads show more results modal)
- [ ] Show More Results link 
- [ ] Show More Results “Next” button
- [ ] Show More Results Refine Search link

----------------------------------------------------------------------

### Public Library (Athens Regional Library System - Full View)

- [ ] Bento Search (e.g.: search “Education”) (confirm bento boxes load)
- [ ] Advanced Search link
- [ ] Journal Placard
- [ ] Bento box heading anchors
- [ ] Database link
- [ ] Database More Info button
- [ ] Database bento modal
- [ ] Database bento refine search link
- [ ] New Bento Search with Full Text Limiter (e.g.: search “Windows”)
- [ ] Journals database link
- [ ] Link to item in native EDS
- [ ] PDF eBook Full Text
- [ ] ePub eBook Full Text
- [ ] PDF Full Text Button
- [ ] EDS Custom Link (e.g.Learning Express Library)
- [ ] Bento Header link (loads show more results modal)
- [ ] Show More Results link 
- [ ] Show More Results “Next” button
- [ ] Show More Results Refine Search link
- [ ] **_Switch views_**

----------------------------------------------------------------------

#### The Monday following production deployment

- [ ] Confirm database copy of production database (this should happen automatically over the weekend)
- [ ] Import production data into Dev
  - `gitlab-runner@ga-dev:/app/galileo_admin$ bundle exec rake "import_data" RAILS_ENV="dev"`
- [ ] Import production data into Staging
  - `gitlab-runner@ga-staging:/app/galileo_admin$ bundle exec rake "import_data" RAILS_ENV="staging"`
- [ ] Import production data into local development environments

