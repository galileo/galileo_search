## Testing

----------------------------------------------------------------------

### Academic using GALILEO password (Atlanta Metropolitan State College)

- [ ] Bento Search (e.g.: search “Education”) (confirm bento boxes load)
- [ ] Advanced Search link
- [ ] Journal Placard
- [ ] Bento box heading anchors
- [ ] Database  link
- [ ] Database More Info button
- [ ] Database bento modal
- [ ] Database bento refine search link
- [ ] New Bento Search with Full Text Limiter (e.g.: search “George Washington”)
- [ ] Journals database link
- [ ] Link to item in native EDS
- [ ] PDF eBook Full Text
- [ ] ePub eBook Full Text
- [ ] PDF Full Text Button
- [ ] See in GIL-Find button
- [ ] EDS Custom Link (e.g.Films on Demand)
- [ ] Bento Header link (loads show more results modal)
- [ ] Show More Results link 
- [ ] Show More Results “Next” button
- [ ] Show More Results Refine Search link

----------------------------------------------------------------------

### Academic using OpenAthens (Athens Technical College)

- [ ] Bento Search (e.g.: search “Education”) (confirm bento boxes load)
- [ ] Advanced Search link
- [ ] Journal Placard
- [ ] Bento box heading anchors
- [ ] Database link
- [ ] Database More Info button
- [ ] Database bento modal
- [ ] Database bento refine search link
- [ ] New Bento Search with Full Text Limiter (e.g.: search “George Washington”)
- [ ] Journals database link
- [ ] Link to item in native EDS
- [ ] PDF eBook Full Text
- [ ] ePub eBook Full Text
- [ ] PDF Full Text Button
- [ ] EDS Custom Link (e.g.Films on Demand)
- [ ] Bento Header link (loads show more results modal)
- [ ] Show More Results link 
- [ ] Show More Results “Next” button
- [ ] Show More Results Refine Search link

----------------------------------------------------------------------

### Public Library (Athens Regional Library System - Full View)

- [ ] Bento Search (e.g.: search “Education”) (confirm bento boxes load)
- [ ] Advanced Search link
- [ ] Journal Placard
- [ ] Bento box heading anchors
- [ ] Database link
- [ ] Database More Info button
- [ ] Database bento modal
- [ ] Database bento refine search link
- [ ] New Bento Search with Full Text Limiter (e.g.: search “Windows”)
- [ ] Journals database link
- [ ] Link to item in native EDS
- [ ] PDF eBook Full Text
- [ ] ePub eBook Full Text
- [ ] ProQuest Central Full eBook button
- [ ] PDF Full Text Button
- [ ] EDS Custom Link (e.g.Learning Express Library)
- [ ] Bento Header link (loads show more results modal)
- [ ] Show More Results link 
- [ ] Show More Results “Next” button
- [ ] Show More Results Refine Search link

----------------------------------------------------------------------

### K-12 ( Clarke County Schools - High School View)

- [ ] Bento Search (e.g.: search “Education”) (confirm bento boxes load)
- [ ] Advanced Search link
- [ ] Journal Placard
- [ ] Bento box heading anchors
- [ ] Database link
- [ ] Database More Info button
- [ ] Database bento modal
- [ ] Database bento refine search link
- [ ] New Bento Search with Full Text Limiter (e.g.: search “George Washington”)
- [ ] Journals database link
- [ ] Link to item in native EDS
- [ ] PDF eBook Full Text
- [ ] ePub eBook Full Text
- [ ] ProQuest Central Full eBook button
- [ ] PDF Full Text Button
- [ ] EDS Custom Link (e.g.Britannica)
- [ ] Bento Header link (loads show more results modal)
- [ ] Show More Results link 
- [ ] Show More Results “Next” button
- [ ] Show More Results Refine Search link
